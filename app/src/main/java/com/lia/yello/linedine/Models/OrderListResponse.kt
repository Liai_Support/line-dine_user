package com.lia.yello.linedine.Models

import com.google.gson.annotations.SerializedName
import java.io.Serializable

class OrderListResponse: Serializable {


    @SerializedName("error")
    var error: Boolean? = null

    @SerializedName("message")
    var message: String? = null

    @SerializedName("data")
    var data: OrderListData? = null


}

class OrderListData : Serializable {



    @SerializedName("cart_data")
    var orderdata: ArrayList<orderlist>? = null



}
class orderlist : Serializable {

    @SerializedName("id")
    var id: Int? = null


    @SerializedName("menu_id")
    var menu_id: Int? = null


    @SerializedName("res_id")
    var res_id: Int? = null



    @SerializedName("vat")
    var vat: Int? = null

    @SerializedName("menu_name")
    var menuname: String? = null

    @SerializedName("menu_description")
    var menu_description: String? = null

    @SerializedName("menu_image")
    var menu_images: String? = null

    @SerializedName("quantity")
    var quantity: Int? = null

    @SerializedName("cost")
    var cost: Double? = null

    @SerializedName("menuavilable")
    var menuavilable: Int? = null


    @SerializedName("special_instruction")
    var special_instruction: String? = null
}
