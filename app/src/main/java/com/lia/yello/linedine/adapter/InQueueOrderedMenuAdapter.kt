package com.lia.yello.linedine.adapter

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.lia.yello.linedine.Models.FineDineMenudetail
import com.lia.yello.linedine.R
import com.lia.yello.linedine.databinding.OrderprocessinngmenuadapterBinding
import com.lia.yello.linedine.fragment.FineDineOrderListStatusFragment
import com.lia.yello.linedine.fragment.InQueueStatusFragment

class InQueueOrderedMenuAdapter(
    var inQueueStatusFragment: InQueueStatusFragment,
    var context: Context,
    var list: ArrayList<FineDineMenudetail>?
) :
    RecyclerView.Adapter<InQueueOrderedMenuAdapter.HomeHeaderViewHolder>() {



    inner class HomeHeaderViewHolder(view: View) : RecyclerView.ViewHolder(view) {

        var binding: OrderprocessinngmenuadapterBinding = OrderprocessinngmenuadapterBinding.bind(view)

        /*   lateinit var menuname: TextView
           lateinit var menucost: TextView


           init {
               menuname = view.findViewById(R.id.menuname)
               menucost = view.findViewById(R.id.menutotalcost)


           }*/
    }


    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): HomeHeaderViewHolder {
        return HomeHeaderViewHolder(
            LayoutInflater.from(parent.context).inflate(
                R.layout.orderprocessinngmenuadapter,
                parent,
                false
            )
        )
    }

    override fun getItemCount(): Int {
        return list!!.size
        // return itemCount
    }


    override fun onBindViewHolder(holder: HomeHeaderViewHolder, position: Int) {


        holder.binding.menuname.text = list!![position].menu_name
        holder.binding.menutotalcost.text =    list!![position].amount.toString() + " SAR"




    }

}












