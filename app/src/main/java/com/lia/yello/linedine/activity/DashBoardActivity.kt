package com.lia.yello.linedine.activity

import android.content.ActivityNotFoundException
import android.content.Intent
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.net.Uri
import android.os.Bundle
import android.util.Log
import android.view.View
import android.widget.FrameLayout
import android.widget.ImageView
import androidx.appcompat.app.AppCompatActivity
import androidx.core.content.ContextCompat
import androidx.core.view.GravityCompat
import androidx.fragment.app.FragmentTransaction
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import com.google.android.material.bottomsheet.BottomSheetBehavior
import com.google.firebase.iid.FirebaseInstanceId
import com.google.firebase.messaging.FirebaseMessaging
import com.lia.yello.linedine.Models.ProfileDetails
import com.lia.yello.linedine.R
import com.lia.yello.linedine.Session.SharedHelper
import com.lia.yello.linedine.Utils.Constants
import com.lia.yello.linedine.Utils.DialogUtils
import com.lia.yello.linedine.Utils.NetworkUtils
import com.lia.yello.linedine.Utils.UiUtils
import com.lia.yello.linedine.ViewModels.ProfileViewModel
import com.lia.yello.linedine.fragment.*
import com.lia.yello.linedine.interfaces.DialogCallBack
import com.lia.yello.linedine.interfaces.SingleTapListener
import kotlinx.android.synthetic.main.activity_dash_board.*
import kotlinx.android.synthetic.main.activity_login.*
import kotlinx.android.synthetic.main.carticon.*
import kotlinx.android.synthetic.main.carticon.cartframe
import kotlinx.android.synthetic.main.fragment_home.*
import kotlinx.android.synthetic.main.fragment_profile.*
import kotlinx.android.synthetic.main.layout_home_drawer1.*
import kotlinx.android.synthetic.main.popup_filter.*
import android.view.ViewGroup
import android.widget.Button
import android.widget.Toast
import com.lia.yello.linedine.BuildConfig
import com.zoho.salesiqembed.ZohoSalesIQ
import java.lang.Exception
import java.lang.RuntimeException


class DashBoardActivity : BaseActivity() {
    private lateinit var fragmentTransaction: FragmentTransaction
    private var homeFragment: HomeFragment? = null
    private var profileFragment: ProfileFragment? = null
    private var walletFragment: WalletFragment? = null
    private var myOrderFragment: MyOrderFragment? = null
    private var favoritesFragment: FavoritesFragment? = null
    private var helpSupportFragment: HelpSupportFragment? = null
    private var paymentOptionFragment: PaymentOptionFragment? = null
    private var notificationFragment: NotificationFragment? = null
    private var cartFragment: CartFragment? = null
    private var settingFragment: SettingFragment? = null
    private var imageList = arrayListOf<ImageView>()

    private var sharedHelper: SharedHelper? = null
    private var profileViewModel: ProfileViewModel? = null

    private var myLat: Double? = null
    private var myLng: Double? = null
    var mBottomSheetBehavior: BottomSheetBehavior<*>? = null
    var mBottomSheetBehavior2: BottomSheetBehavior<*>? = null
    var imgid: Int? = 0
    var amount: Int? = 0

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_dash_board)
        sharedHelper = SharedHelper(this)
        profileViewModel = ViewModelProvider(this).get(ProfileViewModel::class.java)
        val bottomSheet = findViewById<View>(R.id.bottom_sheet)
        mBottomSheetBehavior = BottomSheetBehavior.from(bottomSheet)
        (mBottomSheetBehavior as BottomSheetBehavior<View>).setState(BottomSheetBehavior.STATE_HIDDEN)
        navigationDrawer.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
/*
        ZohoSalesIQ.init(application, "cNqGFFENdBA4zVpSY9WYX0%2FNnFzT15aitgi0Wza5uJEoEuRaOgP2bug6ShCO49Mfl9L39dkTxDPeh3%2F0hvSx8CyTm%2BwHDCPz_in", "fDW0ZyDpRSfGhaZDNXU7ia41D9JQBb8MibyhdXJzzJMrguz4tracbdwmd2XThMfeUS8oXnX0N9schFPagWn2SDllztLEJsmajLlRWPaseQNaHFWd%2BnpFskGizu%2BTRlmc0g5jYw6u8Yqx%2Ba4g2DEnOq5O4kA7mtjt" );
*/

        Log.d("notificationtitle", "aswedrftgyhujikol;")

        val bottomSheet2 = findViewById<View>(R.id.bottom_sheet2)
        mBottomSheetBehavior2 = BottomSheetBehavior.from(bottomSheet2)
        (mBottomSheetBehavior2 as BottomSheetBehavior<View>).setState(BottomSheetBehavior.STATE_HIDDEN)

      /*  if (!NetworkUtils.isNetworkConnected(this)){

              UiUtils.showSnack(
                findViewById(android.R.id.content), "no_internet_connection")
        }*/
        updateDeviceToken()
        getProfileData()

        bottom_navigation_main.itemIconTintList = null
        bottom_navigation_main.setOnNavigationItemSelectedListener {
            when (it.itemId) {
                R.id.fav -> {
                    setAdapter(4)
                    true
                }
                R.id.wallet -> {
                    setAdapter(1)
                    true
                }
                R.id.offer -> {
                    setAdapter(3)
                    //showComingSoon()
                    true
                }
                R.id.profile -> {
                    setAdapter(2)
                    true
                }
                else -> false
            }
        }
        if (intent.extras == null) {
            setAdapter(0)
        } else {
          /*  var value = intent.extras!!.getInt(Constants.IntentKeys.HOMETYPE, 0)
            setAdapter(value)*/
            var value = intent.extras!!.getString("langfrom")
            var value1 = intent.extras!!.getString("frominqueue")
            var value3 = intent.extras!!.getString("fromnotification")
            Log.d("fromniujyjfrdresre", "" + value3)
            Log.d("skjkcghcv", "" + value)
            Log.d("fromnotificationnnn", "" + value3)
            if(value.equals("settings")){

                setAdapter(5)

            }
            else if (value1.equals("backfrominqueue")){

                val i = Intent(this, OnBoardActivity::class.java)
                startActivity(i)

            }
            else if (value3.equals("tableassigned")){

                setAdapter(3)
            }
            else if (value3.equals("pickup")){

                setAdapter(3)
            }
        }



    }

    private fun updateDeviceToken() {

        if (sharedHelper?.loggedIn!!)
            FirebaseMessaging.getInstance().subscribeToTopic("allDevices")
            FirebaseMessaging.getInstance().token
            val token = FirebaseInstanceId.getInstance().token
            sharedHelper!!.token= token!!
            Log.d("tokennn", "" + sharedHelper!!.token)
            profileViewModel?.updateDeviceToken(this)

    }


    private fun getProfileData() {
        DialogUtils.showLoader(this)
        profileViewModel?.getProfileDetails(this)
            ?.observe(this, Observer {
                DialogUtils.dismissLoader()
                it?.let {
                    it.error?.let { error ->
                        if (error) {
                            it.message?.let { msg ->
                                // UiUtils.showSnack(root, msg)
                            }
                        } else {
                            it.data?.let { data ->
                                handleResponse(data)

                                /* data.profileDetails?.let { list ->
                                     if (list.size != 0) {
                                         handleResponse(list[0])
                                     }

                                 }*/

                            }
                        }
                    }

                }
            })
    }

    private fun handleResponse(data: ProfileDetails) {

        data.name?.let {
            name_head.text = it
        }
        sharedHelper?.selectedaddressid = data.addressid!!
    }


    fun showComingSoon() {

        DialogUtils.showAlertWithHeader(this, object : SingleTapListener {
            override fun singleTap() {

            }
        }, "Coming soon !!", "Oops")
    }

    public fun setAdapter(position: Int) {
        setUiTab(position)
        when (position) {
            0 -> {
                homeFragment = HomeFragment()
                fragmentTransaction = supportFragmentManager.beginTransaction()
                fragmentTransaction.setCustomAnimations(R.anim.screen_in, R.anim.screen_out)
                fragmentTransaction.replace(R.id.container, homeFragment!!)
                fragmentTransaction.addToBackStack(null)
                fragmentTransaction.commit()


            }
            1 -> {
                walletFragment = WalletFragment()
                fragmentTransaction = supportFragmentManager.beginTransaction()
                fragmentTransaction.setCustomAnimations(R.anim.screen_in, R.anim.screen_out)
                fragmentTransaction.replace(R.id.container, walletFragment!!)
                fragmentTransaction.addToBackStack(null)
                fragmentTransaction.commit()


            }
            2 -> {
                profileFragment = ProfileFragment()
                fragmentTransaction = supportFragmentManager.beginTransaction()
                fragmentTransaction.setCustomAnimations(R.anim.screen_in, R.anim.screen_out)
                fragmentTransaction.replace(R.id.container, profileFragment!!)
                fragmentTransaction.addToBackStack(null)
                fragmentTransaction.commit()
            }

            3 -> {
                myOrderFragment = MyOrderFragment()
                fragmentTransaction = supportFragmentManager.beginTransaction()
                fragmentTransaction.setCustomAnimations(R.anim.screen_in, R.anim.screen_out)
                fragmentTransaction.replace(R.id.container, myOrderFragment!!)
                fragmentTransaction.addToBackStack(null)
                fragmentTransaction.commit()
            }

            4 -> {
                favoritesFragment = FavoritesFragment()
                fragmentTransaction = supportFragmentManager.beginTransaction()
                fragmentTransaction.setCustomAnimations(R.anim.screen_in, R.anim.screen_out)
                fragmentTransaction.replace(R.id.container, favoritesFragment!!)
                fragmentTransaction.addToBackStack(null)
                fragmentTransaction.commit()
            }

            5 -> {
                settingFragment = SettingFragment()
                fragmentTransaction = supportFragmentManager.beginTransaction()
                fragmentTransaction.setCustomAnimations(R.anim.screen_in, R.anim.screen_out)
                fragmentTransaction.replace(R.id.container, settingFragment!!)
                fragmentTransaction.addToBackStack(null)
                fragmentTransaction.commit()
            }
            6 -> {
                helpSupportFragment = HelpSupportFragment()
                fragmentTransaction = supportFragmentManager.beginTransaction()
                fragmentTransaction.setCustomAnimations(R.anim.screen_in, R.anim.screen_out)
                fragmentTransaction.replace(R.id.container, helpSupportFragment!!)
                fragmentTransaction.addToBackStack(null)
                fragmentTransaction.commit()
            }
            7 -> {
                paymentOptionFragment = PaymentOptionFragment()
                fragmentTransaction = supportFragmentManager.beginTransaction()
                fragmentTransaction.setCustomAnimations(R.anim.screen_in, R.anim.screen_out)
                fragmentTransaction.replace(R.id.container, paymentOptionFragment!!)
                fragmentTransaction.addToBackStack(null)
                fragmentTransaction.commit()
            }
            8 -> {
                notificationFragment = NotificationFragment()
                fragmentTransaction = supportFragmentManager.beginTransaction()
                fragmentTransaction.setCustomAnimations(R.anim.screen_in, R.anim.screen_out)
                fragmentTransaction.replace(R.id.container, notificationFragment!!)
                fragmentTransaction.addToBackStack(null)
                fragmentTransaction.commit()
            }


            9 -> {
                val bundle = Bundle()

                bundle.putString("promocode", "")

                cartFragment = CartFragment()
                cartFragment!!.arguments = bundle
                fragmentTransaction = supportFragmentManager.beginTransaction()
                fragmentTransaction.setCustomAnimations(R.anim.screen_in, R.anim.screen_out)
                fragmentTransaction.replace(R.id.container, cartFragment!!)
                fragmentTransaction.addToBackStack(null)
                fragmentTransaction.commit()
            }


        }
    }

    private fun setUiTab(position: Int) {
        for (i in 0 until imageList.size) {
            if (position == i) {
                // imageList[i].background = ContextCompat.getDrawable(this, R.drawable.tab_selected_background)
                //  imageList[i].setColorFilter(UiUtils.fetchAccentColor(this))
            } else {
                imageList[i].setBackgroundColor(ContextCompat.getColor(this, R.color.black))
                imageList[i].setColorFilter(ContextCompat.getColor(this, R.color.black))
            }
        }
    }

    fun onAccountClicked(view: View) {
        if (sharedHelper?.loggedIn!!)
            setAdapter(2)
        else
            pleaseLogin()
        closeDrawer()
    }

    fun onPaymentOptionClicked(view: View) {
        if (sharedHelper?.loggedIn!!){
            setAdapter(7)
            closeDrawer()
            //showComingSoon()
        }

        else
            pleaseLogin()
        closeDrawer()
    }

    fun onHelpClicked(view: View) {
        if (sharedHelper?.loggedIn!!)
        {
          setAdapter(6)
            //closeDrawer()
            //showComingSoon()
        }
        else
            pleaseLogin()
        closeDrawer()
    }

    fun onSettingClicked(view: View) {
        if (sharedHelper?.loggedIn!!)
            setAdapter(5)
        else
            pleaseLogin()
        closeDrawer()
    }

    fun onFavClicked(view: View) {
        if (sharedHelper?.loggedIn!!)
            setAdapter(4)
        else
            pleaseLogin()
        closeDrawer()
    }
    fun onCardViewClicked(view: View) {

       mBottomSheetBehavior2?.setState(BottomSheetBehavior.STATE_HIDDEN)
     mBottomSheetBehavior?.setState(BottomSheetBehavior.STATE_HIDDEN)

    }

    fun onMyordersClicked(view: View) {
        if (sharedHelper?.loggedIn!!)
            setAdapter(3)
        else
            pleaseLogin()
        closeDrawer()
    }


    fun onHomeeeClicked(view: View) {
        if (sharedHelper?.loggedIn!!){
            val i = Intent(this, OnBoardActivity::class.java)
            startActivity(i)
        }else{
            pleaseLogin()
            closeDrawer()
        }


    }


    fun on(view: View) {
        if (sharedHelper?.loggedIn!!)
            setAdapter(3)
        else
            pleaseLogin()
        closeDrawer()
    }


    fun onDrawerMenuClicked(view: View) {
        mBottomSheetBehavior2?.setState(BottomSheetBehavior.STATE_HIDDEN)
        mBottomSheetBehavior?.setState(BottomSheetBehavior.STATE_HIDDEN)

        drawer.openDrawer(GravityCompat.START)
        navigationDrawer.bringToFront()
    }

    fun onNotificationClicked(view: View) {
        setAdapter(8)
    }

    fun onCartClicked(view: View) {

        if (sharedHelper!!.choose.equals("finedine")||sharedHelper!!.choose.equals("inqueue")){

        }else{
            setAdapter(9)

        }
    }

    fun onBackClicked(view: View) {
        setAdapter(0)
    }

    fun onCloseClick(view: View) {
        closeDrawer()
    }


    /* override fun onBackPressed() {
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START)
        } else {
            finish()
        }
    }
*/
    fun onLogoutClicked(view: View) {
        closeDrawer()
           Log.d("hhbfhbfvhbfvhb","hjjdhjbdhjbbdhj")
        sharedHelper?.loggedIn = false
        sharedHelper?.token = ""
        sharedHelper?.userImage = ""
        sharedHelper?.id = 0
        sharedHelper?.name = ""
        sharedHelper?.email = ""
        sharedHelper?.mobileNumber = ""
        sharedHelper?.countryCode = ""

        val intent =
            Intent(this, LoginActivity::class.java)
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP)
        intent.flags =
            Intent.FLAG_ACTIVITY_NEW_TASK or Intent.FLAG_ACTIVITY_CLEAR_TASK
        startActivity(intent)
        finish()

    }


    private fun closeDrawer() {
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START)
        }
    }

    /* fun onBackPressed(view: View) {
        onBackPressed()
    }*/

    private fun pleaseLogin() {

        DialogUtils.showAlertDialog(this,
            getString(R.string.login_feature),
            getString(R.string.please_login),
            getString(R.string.ok),
            getString(R.string.cancel),
            object : DialogCallBack {
                override fun onPositiveClick() {

                    startActivity(
                        Intent(this@DashBoardActivity, LoginActivity::class.java)
                            .putExtra(Constants.IntentKeys.FINISHACT, true)
                    )
                }

                override fun onNegativeClick() {

                }
            })

    }

    fun onHomeClicked(view: View) {
    //    setAdapter(0)

        if (sharedHelper?.loggedIn!!){
            val i = Intent(this, OnBoardActivity::class.java)
            startActivity(i)
        }else{
            pleaseLogin()
            closeDrawer()
        }
    }




    fun onShareApp(view: View) {
        try {
            val shareIntent = Intent(Intent.ACTION_SEND)
            shareIntent.type = "text/plain"
            shareIntent.putExtra(Intent.EXTRA_SUBJECT, "Line an Dine")
            var shareMessage = getString(R.string.checkoutthisapp)
            shareMessage =
                """
                ${shareMessage}https://play.google.com/store/apps/details?id=${BuildConfig.APPLICATION_ID}
                
                
                """
            shareIntent.putExtra(Intent.EXTRA_TEXT, shareMessage)
            startActivity(Intent.createChooser(shareIntent, "choose one"))
        } catch (e: Exception) {
            //e.toString();
        }
    }




    fun onTermsClicked(view: View) {
        closeDrawer()
        //   showComingSoon()
        //https://hakeem.com.sa/terms-and-conditions/
        val intent = Intent(
            "android.intent.action.VIEW",
            Uri.parse("https://lineandine.com/termsconditions.php")
        )
        startActivity(intent)


    }


    fun onRatingClicked(view: View) {
        closeDrawer()
        if (sharedHelper?.loggedIn!!) {

                    launchMarket()

        } else
            pleaseLogin()
    }

    private fun launchMarket() {
        val uri: Uri = Uri.parse("market://details?id=$packageName")
        val myAppLinkToMarket = Intent(Intent.ACTION_VIEW, uri)
        try {
            startActivity(myAppLinkToMarket)
        } catch (e: ActivityNotFoundException) {
            Toast.makeText(this, " unable to find market app", Toast.LENGTH_LONG).show()
        }
    }


}



