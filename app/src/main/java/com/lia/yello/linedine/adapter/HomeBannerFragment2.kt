package com.lia.yello.linedine.adapter

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import androidx.viewpager.widget.PagerAdapter
import com.lia.yello.linedine.R
import com.lia.yello.linedine.Utils.UiUtils
import com.lia.yello.linedine.activity.Banner
import com.lia.yello.linedine.fragment.HomeBannerFragment
import com.lia.yello.linedine.fragment.HomeFragment

class HomeBannerFragment2(

    var homeFragment: HomeFragment,
    var context: Context,
    var list: ArrayList<Banner>?

): PagerAdapter() {

    override fun instantiateItem(container: ViewGroup, position: Int): Any {

        val inflater = LayoutInflater.from(context)
        val layout = inflater.inflate(R.layout.homebanner, container, false) as ViewGroup

        val imageView: ImageView = layout.findViewById(R.id.imageViewbanner)

        list!![position].image?.let { imageUrl ->
            UiUtils.loadImage(imageView, imageUrl)
        }

        imageView.setOnClickListener{
            list!![position].resid?.let { it1 -> homeFragment.checkfavroite(it1) }
            homeFragment.promocode= list!![position].promocode.toString()

        }

        container.addView(layout)


        return layout
    }


    override fun getCount(): Int {

        return list!!.size


    }

    override fun isViewFromObject(view: View, `object`: Any): Boolean {
        return view == `object`

    }

    override fun destroyItem(container: ViewGroup, position: Int, `object`: Any) {


        container.removeView(`object` as View)
    }





}