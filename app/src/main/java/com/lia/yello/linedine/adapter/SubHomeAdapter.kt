package com.lia.yello.linedine.adapter

import android.content.Context
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.content.ContextCompat

import androidx.fragment.app.FragmentTransaction
import androidx.recyclerview.widget.RecyclerView
import com.facebook.FacebookSdk
import com.lia.yello.linedine.Models.listresturant
import com.lia.yello.linedine.R
import com.lia.yello.linedine.Utils.UiUtils
import com.lia.yello.linedine.databinding.CardSubhomeBinding
import com.lia.yello.linedine.fragment.FineDineFragment1
import com.lia.yello.linedine.fragment.SubHomeFragment
import com.lia.yello.linedine.fragment.SubsubHomeFragment
import kotlinx.android.synthetic.main.fragment_subhome.*


class SubHomeAdapter(
    var subHomeFragment: SubHomeFragment,
    var context: Context,
    var list: ArrayList<listresturant>?
) :
    RecyclerView.Adapter<SubHomeAdapter.HomeHeaderViewHolder>() {

    var id:Int=0
    var countryFilterList = ArrayList<listresturant>()

    private lateinit var fragmentTransaction: FragmentTransaction
    private var subsubHomeFragment: SubsubHomeFragment? = null
    private var fineDineFragment1: FineDineFragment1? = null

    inner class HomeHeaderViewHolder(view: View) : RecyclerView.ViewHolder(view) {
        var binding: CardSubhomeBinding = CardSubhomeBinding.bind(view)
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): HomeHeaderViewHolder {
        return HomeHeaderViewHolder(
            LayoutInflater.from(context).inflate(
                R.layout.card_subhome,
                parent,
                false
            )
        )
    }

    override fun getItemCount(): Int {
        return list?.size!!
    }

    override fun onBindViewHolder(holder: HomeHeaderViewHolder, position: Int) {
       Log.d("qwertyuiosdfghjk",subHomeFragment.sharedHelper!!.choose)
       if (subHomeFragment.sharedHelper!!.choose=="inqueue"){
           holder.binding.onlinestatus.visibility=View.VISIBLE
           if (list!![position].queuestatus.equals("Full")){
               holder.binding.onlinestatus.backgroundTintList = ContextCompat.getColorStateList(FacebookSdk.getApplicationContext(),R.color.colorPrimary2)
               holder.binding.onlinestatus.text= "  "+list!![position].queuestatus+"  "


           }else{
               holder.binding.onlinestatus.text= "  "+list!![position].queuestatus+"  "
               holder.binding.onlinestatus.backgroundTintList = ContextCompat.getColorStateList(FacebookSdk.getApplicationContext(),R.color.green)

           }
       }




        list!![position].resimage?.let { imageUrl ->
            UiUtils.loadImage(holder.binding.listresturantimg, imageUrl)}

            Log.d("sdfghyujikol", "" + subHomeFragment.inputText)
            holder.binding.title.text = list!![position].name
            holder.binding.main.setOnClickListener(View.OnClickListener {
                if(subHomeFragment.sharedHelper!!.choose.equals("finedine")){
                    Log.d("emffkmrfrf",""+list!![position].id!!)
                    val bundle = Bundle()
                    bundle.putInt("id", list!![position].id!!)
                    subHomeFragment.inputText?.let { it1 -> bundle.putInt("imgid", it1) }
                    bundle.putString("queuestatus", "")

                    bundle.putString("type", subHomeFragment.type)
                    fineDineFragment1 = FineDineFragment1()
                    fineDineFragment1!!.arguments=bundle
                    fragmentTransaction = subHomeFragment.requireActivity().supportFragmentManager.beginTransaction()
                    fragmentTransaction.setCustomAnimations(R.anim.screen_in,R.anim.screen_out)
                    fragmentTransaction.replace(R.id.container, fineDineFragment1!!)
                    fragmentTransaction.addToBackStack(null)
                    fragmentTransaction.commit()
                }
                else if (subHomeFragment.sharedHelper!!.choose.equals("pickup")){
                    Log.d("asdxcfvghnjmk",""+list!![position].id!!)
                    val bundle = Bundle()
                    bundle.putInt("id", list!![position].id!!)
                    subHomeFragment.inputText?.let { it1 -> bundle.putInt("imgid", it1) }
                    bundle.putString("type", subHomeFragment.type)
                    subsubHomeFragment = SubsubHomeFragment()
                    subsubHomeFragment!!.arguments=bundle
                    fragmentTransaction = subHomeFragment.requireActivity().supportFragmentManager.beginTransaction()
                    fragmentTransaction.setCustomAnimations(R.anim.screen_in,R.anim.screen_out)
                    fragmentTransaction.replace(R.id.container, subsubHomeFragment!!)
                    fragmentTransaction.addToBackStack(null)
                    fragmentTransaction.commit()
                }  else if (subHomeFragment.sharedHelper!!.choose.equals("inqueue") && list!![position].queuestatus.equals("Full")){
                    Log.d("emffkmrfrf",""+list!![position].id!!)
                    val bundle = Bundle()
                    bundle.putInt("id", list!![position].id!!)
                    subHomeFragment.inputText?.let { it1 -> bundle.putInt("imgid", it1) }
                    bundle.putString("queuestatus", list!![position].queuestatus)

                    bundle.putString("type", subHomeFragment.type)
                    fineDineFragment1 = FineDineFragment1()
                    fineDineFragment1!!.arguments=bundle
                    fragmentTransaction = subHomeFragment.requireActivity().supportFragmentManager.beginTransaction()
                    fragmentTransaction.setCustomAnimations(R.anim.screen_in,R.anim.screen_out)
                    fragmentTransaction.replace(R.id.container, fineDineFragment1!!)
                    fragmentTransaction.addToBackStack(null)

                    fragmentTransaction.commit()
                } else if (subHomeFragment.sharedHelper!!.choose.equals("inqueue") && list!![position].queuestatus.equals("Open")){
                    Log.d("emffkmrfrf",""+list!![position].id!!)
                    val bundle = Bundle()
                    bundle.putInt("id", list!![position].id!!)
                    subHomeFragment.inputText?.let { it1 -> bundle.putInt("imgid", it1) }
                    bundle.putString("type", subHomeFragment.type)
                    bundle.putString("queuestatus", list!![position].queuestatus)
                    fineDineFragment1 = FineDineFragment1()
                    fineDineFragment1!!.arguments=bundle
                    fragmentTransaction = subHomeFragment.requireActivity().supportFragmentManager.beginTransaction()
                    fragmentTransaction.setCustomAnimations(R.anim.screen_in,R.anim.screen_out)
                    fragmentTransaction.replace(R.id.container, fineDineFragment1!!)
                    fragmentTransaction.addToBackStack(null)
                    fragmentTransaction.commit()
                }

            })
        }
}



