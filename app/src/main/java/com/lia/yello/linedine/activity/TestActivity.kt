package com.lia.yello.linedine.activity

import android.content.Intent
import android.net.Uri
import android.os.Bundle
import android.view.View
import android.webkit.WebView
import androidx.appcompat.app.AppCompatActivity
import com.lia.yello.linedine.R
import kotlinx.android.synthetic.main.activity_test.*


class TestActivity : BaseActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_test)
        text.setOnClickListener(View.OnClickListener {
            /*val browserIntent = Intent(Intent.ACTION_VIEW, Uri.parse("https://docs.google.com/document/d/1fWHXE1WJAqz9e0vae1OLkItnw0ZSXOvN1YA-silP6Kw/edit"))
            startActivity(browserIntent)*/

            val webView = findViewById<View>(R.id.webView1) as WebView
            webView.settings.javaScriptEnabled = true
            webView.loadUrl("https://docs.google.com/document/d/1fWHXE1WJAqz9e0vae1OLkItnw0ZSXOvN1YA-silP6Kw/edit")

        })


    }

}