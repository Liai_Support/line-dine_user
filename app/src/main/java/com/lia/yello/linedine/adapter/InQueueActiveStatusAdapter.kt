package com.lia.yello.linedine.adapter

import android.content.Context
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.FragmentTransaction
import androidx.recyclerview.widget.RecyclerView
import com.lia.yello.linedine.Models.BookingList
import com.lia.yello.linedine.Models.Menudetail
import com.lia.yello.linedine.R
import com.lia.yello.linedine.Utils.UiUtils
import com.lia.yello.linedine.databinding.CardOrdersBinding
import com.lia.yello.linedine.fragment.*


class InQueueActiveStatusAdapter(
    var myOrderFragment: MyOrderFragment,
    var context: Context,
    var list: ArrayList<BookingList>?

) :
    RecyclerView.Adapter<InQueueActiveStatusAdapter.HomeHeaderViewHolder>() {

    var id:Int=0
    var price:Double=0.0
    var img:String=""
    var menu_name:String=""
    var splins:String=""
    var quantity:Int=0
    var list2: ArrayList<Menudetail>? = null

    private lateinit var fragmentTransaction: FragmentTransaction
    private var myCompleteOrderFragment: MyCompleteOrderFragment? = null
    private var fineDineOrderListStatus: FineDineOrderListStatusFragment? = null
    private var inQueueStatusFragment: InQueueStatusFragment? = null
    private var fineDineCompleteBookingFragment: FineDineCompleteBookingFragment? = null
    inner class HomeHeaderViewHolder(view: View) : RecyclerView.ViewHolder(view) {
        var binding: CardOrdersBinding = CardOrdersBinding.bind(view)
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): HomeHeaderViewHolder {
        return HomeHeaderViewHolder(
            LayoutInflater.from(context).inflate(
                R.layout.card_orders,
                parent,
                false
            )
        )
    }

    override fun getItemCount(): Int {
        return list!!.size
    }

    override fun onBindViewHolder(holder: HomeHeaderViewHolder, position: Int) {

        holder.binding.time.text = list!![position].timeslot
        holder.binding.date.text = list!![position].date
        holder.binding.resName.text = list!![position].resname
        holder.binding.bookingamount.text =("" + String.format("%.2f",list!![position].initial_amount )+" SAR")
        Log.d("asdfghjkl.;/",""+list!![position].amount.toString())

        list!![position].resimg?.let { imageUrl ->
            UiUtils.loadImage(holder.binding.img, imageUrl)
        }


        holder.binding.card.setOnClickListener(View.OnClickListener {
                val bundle = Bundle()
                bundle.putInt("orderid",list!![position].id!! )
                bundle.putString("status",list!![position].status!!)
                bundle.putString("date",list!![position].date)
                bundle.putInt("resid", list!![position].resid!!)
                Log.d("resssid",""+list!![position].resid!!)
                bundle.putString("resname",list!![position].resname)
                bundle.putString("resimg",list!![position].resimg)
                inQueueStatusFragment = InQueueStatusFragment()
                inQueueStatusFragment!!.arguments = bundle
                fragmentTransaction = myOrderFragment.requireActivity().supportFragmentManager.beginTransaction()
                fragmentTransaction.setCustomAnimations(R.anim.screen_in, R.anim.screen_out)
                fragmentTransaction.replace(R.id.container, inQueueStatusFragment!!)
                fragmentTransaction.addToBackStack(null)
                fragmentTransaction.commit()

        })



    }
}