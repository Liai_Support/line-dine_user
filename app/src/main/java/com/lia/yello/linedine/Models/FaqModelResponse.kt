package com.lia.yello.linedine.Models

import com.google.gson.annotations.SerializedName
import java.io.Serializable

class FaqModelResponse : Serializable {


    @SerializedName("error")
    var error: Boolean? = null

    @SerializedName("message")
    var message: String? = null

    @SerializedName("data")
    var data: FaqData? = null

}
class FaqData : Serializable {

    @SerializedName("FAQ_data")
    var faq: ArrayList<FaqDataa>? = null
}

class FaqDataa : Serializable {

    @SerializedName("id")
    var id: Int? = null

    @SerializedName("question")
    var question: String? = null


    @SerializedName("answer")
    var answer: String? = null


}






