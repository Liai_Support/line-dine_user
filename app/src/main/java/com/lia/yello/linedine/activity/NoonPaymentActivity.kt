package com.lia.yello.linedine.activity

import android.annotation.SuppressLint
import android.net.Uri
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.webkit.WebView
import android.webkit.WebViewClient
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.ViewModelProvider
import com.lia.yello.linedine.R
import com.lia.yello.linedine.Session.SharedHelper
import com.lia.yello.linedine.Utils.UiUtils
import com.lia.yello.linedine.ViewModels.MapViewModel
import com.lia.yello.linedine.databinding.ActivityNoonPaymentBinding

class NoonPaymentActivity : BaseActivity() {

    var binding: ActivityNoonPaymentBinding? = null
    var viewModel: MapViewModel? = null
    var sharedHelper: SharedHelper? = null


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        // setContentView(R.layout.activity_payment)
        binding = DataBindingUtil.setContentView(this, R.layout.activity_noon_payment)
        viewModel = ViewModelProvider(this).get(MapViewModel::class.java)
        sharedHelper = SharedHelper(this)

        getIntentValues()
        // val profileName = intent.getStringExtra("amount")

    }

    private fun getIntentValues() {
        val bundle = intent.extras
        if (bundle != null) {
            val amount = bundle.getDouble("amount", 0.0)
            Log.d("xcxhv", "" + amount)
            if (amount != 0.0) {
                viewModel?.payment(this, amount)?.observe(this, androidx.lifecycle.Observer {
                    it?.let {
                        it.rcode?.let { rcode ->
                            if (rcode != 0) {
                                it.message?.let { msg ->
                                    UiUtils.showSnack(binding!!.webView, msg)
                                }
                            } else {
                                it.data?.let { data ->
                                    Log.d("xcxvxv", "" + data.cdata!!.posturl)

                                    if (data.cdata != null) {
                                        loadUrl(data.cdata!!.posturl!!,this)
                                    }
                                }
                            }
                        }

                    }
                })

            }else{
                UiUtils.showSnack(findViewById(android.R.id.content), getString(R.string.invalidamount))

            }
        }
    }


    @SuppressLint("SetJavaScriptEnabled")
    private fun loadUrl(url: String, noonPaymentActivity: NoonPaymentActivity) {
        binding!!.webView.settings.javaScriptEnabled = true
        binding!!.webView.settings.loadWithOverviewMode = true
        binding!!.webView.settings.useWideViewPort = true
        binding!!.webView.settings.builtInZoomControls = true
        binding!!.webView.webViewClient = object : WebViewClient() {
            override fun shouldOverrideUrlLoading(view: WebView, url: String): Boolean {
                view.loadUrl(url)
                return true
            }

            //Show loader on url load
            override fun onLoadResource(view: WebView, url: String) {

            }
            override fun onPageFinished(view: WebView, url: String) {
                try {
                    Log.d("URLLOADING ", url)
                    val paymentorderid = Uri.parse(url).getQueryParameter("orderId")
                    Log.d("chvhzcb ",""+paymentorderid)
                    if (url.contains("https://google.com/")) {
                        if(paymentorderid != null){
                            procees(url,paymentorderid.toString())
                        }
                    }

                } catch (exception: Exception) {
                    exception.printStackTrace()
                }
            }
        }

        //Load url in webView
        binding!!.webView.loadUrl(url)
    }

    fun procees(url: String, paymentorderid: String) {
        viewModel?.payment1(this, paymentorderid!!)?.observe(this, androidx.lifecycle.Observer {
            it?.let {
                it.rcode?.let { rcode ->
                    if (rcode != 0) {
                        it.message?.let { msg ->
                            UiUtils.showSnack(binding!!.root, msg)
                        }
                    } else {
                        viewModel?.payment2(this@NoonPaymentActivity, paymentorderid!!)?.observe(this@NoonPaymentActivity, androidx.lifecycle.Observer {
                            it?.let {
                                it.rcode?.let { rcode ->
                                    if (rcode != 0) {
                                        it.message?.let { msg ->
                                            UiUtils.showSnack(binding!!.root, msg)
                                        }
                                    } else {
                                        val paymenttransactionid = it.result?.transaction?.id
                                        if (url.contains("https://google.com/")) {
                                            setResult(RESULT_OK,intent.putExtra("paymenttransactionid",paymenttransactionid.toString()).putExtra("paymentorderid",paymentorderid.toString()))
                                            finish()
                                        }
                                    }
                                }

                            }
                        })
                    }
                }

            }
        })
    }

    override fun onBackPressed() {
        super.onBackPressed()
        setResult(RESULT_CANCELED)
        finish()
    }
}