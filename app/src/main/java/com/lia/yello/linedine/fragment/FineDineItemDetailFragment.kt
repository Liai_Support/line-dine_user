package com.lia.yello.linedine.fragment
import android.app.AlertDialog
import android.content.Intent
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.os.Bundle
import android.text.Editable
import android.text.TextWatcher
import android.util.DisplayMetrics
import android.util.Log
import android.view.Gravity
import android.view.LayoutInflater
import android.view.View
import android.view.WindowManager
import android.widget.TextView
import android.widget.Toast
import androidx.activity.OnBackPressedCallback
import androidx.core.content.ContextCompat
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentTransaction
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import com.fasterxml.jackson.module.kotlin.jacksonObjectMapper
import com.google.android.material.snackbar.BaseTransientBottomBar
import com.google.android.material.snackbar.Snackbar
import com.lia.yello.linedine.Models.MenuDetailJson
import com.lia.yello.linedine.Models.itemdetail
import com.lia.yello.linedine.R
import com.lia.yello.linedine.Session.SharedHelper
import com.lia.yello.linedine.Utils.DialogUtils
import com.lia.yello.linedine.Utils.NetworkUtils
import com.lia.yello.linedine.Utils.UiUtils
import com.lia.yello.linedine.ViewModels.MapViewModel
import com.lia.yello.linedine.activity.NoonPaymentActivity
import com.lia.yello.linedine.activity.OnSwipeTouchListener1
import com.lia.yello.linedine.databinding.FragmentFineDineItemDetailBinding
import kotlinx.android.synthetic.main.carticon.*
import kotlinx.android.synthetic.main.fragment_fine_dine_item_detail.*
import kotlinx.android.synthetic.main.fragment_subsubsubhome.*
import kotlinx.android.synthetic.main.fragment_subsubsubhome.backtomenu
import kotlinx.android.synthetic.main.fragment_subsubsubhome.itemdescription
import kotlinx.android.synthetic.main.fragment_subsubsubhome.itemimg
import kotlinx.android.synthetic.main.fragment_subsubsubhome.minus
import kotlinx.android.synthetic.main.fragment_subsubsubhome.name
import kotlinx.android.synthetic.main.fragment_subsubsubhome.plus
import kotlinx.android.synthetic.main.fragment_subsubsubhome.preparetime
import kotlinx.android.synthetic.main.fragment_subsubsubhome.ratingitemdetail
import kotlinx.android.synthetic.main.fragment_subsubsubhome.total
import kotlinx.android.synthetic.main.fragment_subsubsubhome.txtcount
import kotlinx.android.synthetic.main.header.*
import kotlinx.android.synthetic.main.placeorder.view.*
import kotlinx.android.synthetic.main.popupbackfd.view.*
import java.io.Serializable
import java.util.*
import kotlin.collections.ArrayList

class FineDineItemDetailFragment : Fragment(R.layout.fragment_fine_dine_item_detail) {
    lateinit var list: ArrayList<itemdetail>
    private var mapViewModel: MapViewModel? = null
    public var menuid1:Int? = 0
    public var resid:Int? = 0
    public var fav:Boolean? =false
    private var fineDineMenuListFragment: FineDineMenuListFragment? = null
    private lateinit var fragmentTransaction: FragmentTransaction
    public var menuid:Int? = 0
    public var orderid:Int? = 0
    public var resid1:Int? = 0
    public var cost:Double? = 0.0
    public var sharedHelper: SharedHelper? = null
    val mapper = jacksonObjectMapper()
    var count:Int=0
    var subtotal: Double = 0.0
    var vat:Double=0.0
    var lat:Double=0.0
    var lng:Double=0.0
    var menurating:Double=0.0
    var inarray:Boolean = false
    var menulist = ArrayList<MenuDetailJson>()
    var binding:FragmentFineDineItemDetailBinding?=null
    var status:String=""
    var hotelname:String=""
    var resimage:String=""
    private var myLat: Double? = null
    private var myLng: Double? = null
    private var paymenttype: String? = ""
    private var statuspaid: String? = ""
    private var backres: String? = ""
    var descrip:String=""

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        sharedHelper = SharedHelper(requireContext())
        mapViewModel = ViewModelProvider(this).get(MapViewModel::class.java)
        val head = view.findViewById<TextView>(R.id.head)
        binding = DataBindingUtil.findBinding(view)
        head.setText(getString(R.string.menu))
        if (!NetworkUtils.isNetworkConnected(requireContext())) {
            UiUtils.showSnack(
                view, "no_internet_connection"
            )
        }

        menuid1 = arguments?.getInt("id")
        resid = arguments?.getInt("resid")
        orderid = arguments?.getInt("orderid")
        menurating = arguments?.getDouble("menurating")!!
        inarray = arguments?.getBoolean("inarray")!!
        menulist = arguments?.getSerializable("menulist") as ArrayList<MenuDetailJson>
        status = arguments?.getString("status")!!
        statuspaid = arguments?.getString("paidstatus")!!
        resimage = arguments?.getString("resimg")!!
        hotelname = arguments?.getString("resname")!!
        myLat = arguments?.getDouble("lat")!!
        myLng = arguments?.getDouble("lng")!!
        paymenttype = arguments?.getString("paymenttype")!!
        backres = arguments?.getString("isfd")

        if (sharedHelper!!.language == "ar"){
            back.rotation= 180F
        }
        if (sharedHelper!!.choose.equals("finedine")||sharedHelper!!.choose.equals("inqueue")|| sharedHelper!!.cartcount == 0){

            //var cartframe: FrameLayout =findViewById(R.id.cartframe)
            cartframe.visibility=View.GONE
        }
        specialins2.addTextChangedListener(object : TextWatcher {
            override fun beforeTextChanged(s: CharSequence, start: Int, count: Int, after: Int) {

            }

            override fun onTextChanged(s: CharSequence, start: Int, before: Int, count: Int) {
            }

            override fun afterTextChanged(s: Editable) {
                descrip=specialins.text.toString()
                Log.d("sdfghjkl",""+descrip)
            }
        })
        back.setOnClickListener(View.OnClickListener {
            //requireActivity().supportFragmentManager.popBackStack()
            val bundle = Bundle()
      /*      resid?.let { it1 -> bundle.putInt("resid", it1) }
            orderid?.let { it1 -> bundle.putInt("orderid", it1) }
            bundle.putSerializable("menulist",menulist as Serializable)*/

            bundle.putInt("orderid", orderid!!)
            bundle.putString("status", status)
            bundle.putString("resname", hotelname)
            bundle.putString("resimg", resimage)
            bundle.putInt("resid", resid!!)
            bundle.putDouble("lat", myLat!!)
            bundle.putDouble("lng", myLng!!)
            bundle.putString("paidstatus", statuspaid)
            bundle.putString("paymenttype", paymenttype)
            bundle.putString("inqueue", "false")

            bundle.putSerializable("menulist",menulist as Serializable)
            bundle.putString("isfd", backres)

            fineDineMenuListFragment = FineDineMenuListFragment()
            fineDineMenuListFragment!!.arguments = bundle
            fragmentTransaction = requireActivity().supportFragmentManager.beginTransaction()
            fragmentTransaction.setCustomAnimations(R.anim.screen_in, R.anim.screen_out)
            fragmentTransaction.replace(R.id.container, fineDineMenuListFragment!!)
            fragmentTransaction.commit()
        })

        activity?.onBackPressedDispatcher?.addCallback(this, object : OnBackPressedCallback(true) {
            override fun handleOnBackPressed() {
                // this for single back press
                // isEnabled = false
                Log.d("xcx", "backsucess")
                val bundle = Bundle()
               /* resid?.let { it1 -> bundle.putInt("resid", it1) }
                orderid?.let { it1 -> bundle.putInt("orderid", it1) }
                bundle.putSerializable("menulist",menulist as Serializable)*/

                bundle.putInt("orderid", orderid!!)
                bundle.putString("status", status)
                bundle.putString("resname", hotelname)
                bundle.putString("resimg", resimage)
                bundle.putInt("resid", resid!!)
                bundle.putDouble("lat", myLat!!)
                bundle.putDouble("lng", myLng!!)
                bundle.putDouble("lng", myLng!!)
                bundle.putString("isfd", backres)
                bundle.putString("paidstatus", statuspaid)
                bundle.putString("paymenttype", paymenttype)
                bundle.putString("inqueue", "false")
                bundle.putSerializable("menulist",menulist as Serializable)

                fineDineMenuListFragment = FineDineMenuListFragment()
                fineDineMenuListFragment!!.arguments = bundle
                fragmentTransaction = requireActivity().supportFragmentManager.beginTransaction()
                fragmentTransaction.setCustomAnimations(R.anim.screen_in, R.anim.screen_out)
                fragmentTransaction.replace(R.id.container, fineDineMenuListFragment!!)
                fragmentTransaction.commit()
            }
        })
        if (sharedHelper?.language!!.equals("ar")) {
            preparetime.setBackgroundResource(R.drawable.border_greysubsubsub1)
            total.setBackgroundResource(R.drawable.border_greysubsubsub2)
            Log.d("arabic", "arabicbg")
        }

        Log.d("fdfdbvf",""+menulist)
        val jsonArray = mapper.writeValueAsString(menulist)
        Log.d("ghffreb ",""+jsonArray)
        if(inarray == true){
            for (i in 0 until menulist!!.size){
                if(menulist!![i].menuid == menuid1){
                    count = menulist[i].quantity
                }
            }
        }
        if(menurating.equals("")){
            ratingitemdetail.text = "0" + "/" + "5"

        }else{
            ratingitemdetail.text = menurating.toString() + "/" + "5"

        }
        cart_badge.text = "" + sharedHelper!!.cartcount
        txtcount.text = count.toString()
        getvalues()
        backtomenu.setOnClickListener {
            val bundle = Bundle()
          /*  resid?.let { it1 -> bundle.putInt("resid", it1) }
            orderid?.let { it1 -> bundle.putInt("orderid", it1) }
            bundle.putSerializable("menulist",menulist as Serializable)*/

            bundle.putInt("orderid", orderid!!)
            bundle.putString("status", status)
            bundle.putString("resname", hotelname)
            bundle.putString("resimg", resimage)
            bundle.putInt("resid", resid!!)
            bundle.putDouble("lat", myLat!!)
            bundle.putDouble("lng", myLng!!)
            bundle.putString("paidstatus", statuspaid)
            bundle.putString("paymenttype", paymenttype)
            bundle.putSerializable("menulist",menulist as Serializable)
            bundle.putString("isfd", backres)
            bundle.putString("inqueue", "false")

            Log.d("asdfghbnjm,",""+menulist)
            fineDineMenuListFragment = FineDineMenuListFragment()
            fineDineMenuListFragment!!.arguments = bundle
            fragmentTransaction = requireActivity().supportFragmentManager.beginTransaction()
            fragmentTransaction.setCustomAnimations(R.anim.screen_in, R.anim.screen_out)
            fragmentTransaction.replace(R.id.container, fineDineMenuListFragment!!)
            fragmentTransaction.commit()
        }
        plus.setOnClickListener(View.OnClickListener {
            count++
            txtcount.setText("" + count)
            Log.d("count", "" + count)
        })
        minus.setOnClickListener(View.OnClickListener {
            if (count == 0|| count ==1 ) {
                txtcount.setText("" + count)
                UiUtils.showSnack(root_finedineitemdetail,getString(R.string.itemcannotbelessthan1))
            } else {
                count--
                txtcount.setText("" + count)
            }
        })
        finedineadditem.setOnClickListener(View.OnClickListener {
            if (inarray == true){
                for (i in 0 until menulist!!.size){
                    if(menulist!![i].menuid == menuid1){
                        if (menulist!![i].quantity==0)
                        {

                        }else{
                           menulist.set(i,MenuDetailJson(menuid1!!,count))
                            val mDialogView = LayoutInflater.from(requireContext()).inflate(R.layout.popupbackfd, null)
                            val mBuilder = AlertDialog.Builder(requireContext()).setView(mDialogView).setCancelable(false)
                            val mAlertDialog = mBuilder.show()
                            val layoutParams = WindowManager.LayoutParams()
                            val displayMetrics = DisplayMetrics()
                            requireActivity().windowManager.defaultDisplay.getMetrics(displayMetrics)
                            val displayWidth = displayMetrics.widthPixels
                            val displayHeight = displayMetrics.heightPixels
                            layoutParams.copyFrom(mAlertDialog.getWindow()?.getAttributes())
                            layoutParams.width = ((displayWidth * 0.9f).toInt())
                            mAlertDialog.getWindow()?.setAttributes(layoutParams)
                            mAlertDialog.getWindow()?.setGravity(Gravity.CENTER)
                            mAlertDialog.getWindow()?.setBackgroundDrawable(
                                ColorDrawable(
                                    Color.TRANSPARENT)
                            )
                            mDialogView.popup_ok.setOnClickListener {
                                mAlertDialog.dismiss()

                                val bundle = Bundle()
                                bundle.putInt("orderid", orderid!!)
                                bundle.putString("status", status)
                                bundle.putString("resname", hotelname)
                                bundle.putString("resimg", resimage)
                                bundle.putInt("resid", resid!!)
                                bundle.putDouble("lat", myLat!!)
                                bundle.putDouble("lng", myLng!!)
                                bundle.putString("paidstatus", statuspaid)
                                bundle.putString("paymenttype", paymenttype)
                                bundle.putSerializable("menulist",menulist as Serializable)
                                bundle.putString("isfd", backres)
                                bundle.putString("inqueue", "false")

                                Log.d("asdfghbnjm,",""+menulist)
                                fineDineMenuListFragment = FineDineMenuListFragment()
                                fineDineMenuListFragment!!.arguments = bundle
                                fragmentTransaction = requireActivity().supportFragmentManager.beginTransaction()
                                fragmentTransaction.setCustomAnimations(R.anim.screen_in, R.anim.screen_out)
                                fragmentTransaction.replace(R.id.container, fineDineMenuListFragment!!)
                                fragmentTransaction.commit()
                            }


                        }
                    }
                }
               // UiUtils.showSnack(root_finedineitemdetail,getString(R.string.menuisupdatedsuccessfully))
            }
            else{

                if (count==0){
                    UiUtils.showSnack(root_finedineitemdetail,getString(R.string.productquantityis0))
                }
                else {
                    inarray = true
                    menulist!!.add(MenuDetailJson(menuid1!!, count))
                    val jsonArray = mapper.writeValueAsString(menulist)
                    Log.d("ghb ", "" + jsonArray)
                  //  UiUtils.showSnack(root_finedineitemdetail,getString(R.string.menuisupdatedsuccessfully))

                    val mDialogView = LayoutInflater.from(requireContext()).inflate(R.layout.popupbackfd, null)
                    val mBuilder = AlertDialog.Builder(requireContext()).setView(mDialogView).setCancelable(false)
                    val mAlertDialog = mBuilder.show()
                    val layoutParams = WindowManager.LayoutParams()
                    val displayMetrics = DisplayMetrics()
                    requireActivity().windowManager.defaultDisplay.getMetrics(displayMetrics)
                    val displayWidth = displayMetrics.widthPixels
                    val displayHeight = displayMetrics.heightPixels
                    layoutParams.copyFrom(mAlertDialog.getWindow()?.getAttributes())
                    layoutParams.width = ((displayWidth * 0.9f).toInt())
                    mAlertDialog.getWindow()?.setAttributes(layoutParams)
                    mAlertDialog.getWindow()?.setGravity(Gravity.CENTER)
                    mAlertDialog.getWindow()?.setBackgroundDrawable(
                        ColorDrawable(
                            Color.TRANSPARENT)
                    )
                    mDialogView.popup_ok.setOnClickListener {
                        mAlertDialog.dismiss()
                        val bundle = Bundle()
                        bundle.putInt("orderid", orderid!!)
                        bundle.putString("status", status)
                        bundle.putString("resname", hotelname)
                        bundle.putString("resimg", resimage)
                        bundle.putInt("resid", resid!!)
                        bundle.putDouble("lat", myLat!!)
                        bundle.putDouble("lng", myLng!!)
                        bundle.putString("paidstatus", statuspaid)
                        bundle.putString("paymenttype", paymenttype)
                        bundle.putSerializable("menulist",menulist as Serializable)
                        bundle.putString("isfd", backres)
                        bundle.putString("inqueue", "false")

                        Log.d("asdfghbnjm,",""+menulist)
                        fineDineMenuListFragment = FineDineMenuListFragment()
                        fineDineMenuListFragment!!.arguments = bundle
                        fragmentTransaction = requireActivity().supportFragmentManager.beginTransaction()
                        fragmentTransaction.setCustomAnimations(R.anim.screen_in, R.anim.screen_out)
                        fragmentTransaction.replace(R.id.container, fineDineMenuListFragment!!)
                        fragmentTransaction.commit()



                    }



                }
            }
        })



        swipe2.setOnTouchListener(object : OnSwipeTouchListener1(context){
            override fun onSwipeLeft() {
                //    Toast.makeText(context, "left", Toast.LENGTH_SHORT).show();

                val bundle = Bundle()
                resid?.let { it1 -> bundle.putInt("resid", it1) }
                orderid?.let { it1 -> bundle.putInt("orderid", it1) }
                bundle.putSerializable("menulist",menulist as Serializable)
                Log.d("asdfghbnjm,",""+menulist)
                fineDineMenuListFragment = FineDineMenuListFragment()
                fineDineMenuListFragment!!.arguments = bundle
                fragmentTransaction = requireActivity().supportFragmentManager.beginTransaction()
                fragmentTransaction.setCustomAnimations(R.anim.screen_in, R.anim.screen_out)
                fragmentTransaction.replace(R.id.container, fineDineMenuListFragment!!)
                fragmentTransaction.commit()

            }
            override fun onSwipeRight() {
                Toast.makeText(context, "left", Toast.LENGTH_SHORT).show();

            }
        })
    }
    // items
    fun getvalues() {
        DialogUtils.showLoader(requireContext())
        resid?.let {
            menuid1?.let { it1 ->
                mapViewModel?.getItemDetail(requireContext(), it1, it)
                    ?.observe(viewLifecycleOwner,
                        Observer {
                            DialogUtils.dismissLoader()
                            it?.let {
                                it.error?.let { error ->
                                    if (error) {
                                        it.message?.let { msg ->
                                            UiUtils.showSnack(root_finedineitemdetail, msg)
                                            snackbarToast(msg)
                                        }
                                    } else {
                                        it.data?.let { data ->
                                            handleResponse(data)
                                        }
                                    }
                                }
                            }
                        })
            }
        }
    }
    fun handleResponse(data: itemdetail) {
        data.name?.let { name.text = it }
        data.description?.let { itemdescription.text = it }
        //data.price?.let { total.text = it.toString() + " SAR" }

        total.text=String.format(Locale("en", "US"),"%.2f",data.price)+ " SAR"

        data.prepTime?.let { preparetime.text = it }
        data.description?.let { itemdescription.text = it }
        resid1 = data.resid
        menuid = data.menuid
        cost = data.price
        lat = data.lat!!
        lng = data.lng!!
        data.images?.let {
            UiUtils.loadImage(
                itemimg,
                it,
                ContextCompat.getDrawable(requireContext(), R.drawable.maskgroup46)!!
            )
        }
    }
    fun snackbarToast(msg: String) {
        var snackbar: Snackbar
        Snackbar.make(view!!, msg, Snackbar.LENGTH_SHORT)
            .setTextColor(ContextCompat.getColor(context!!, R.color.design_default_color_error))
            .setBackgroundTint(ContextCompat.getColor(context!!, R.color.white))
            .setDuration(BaseTransientBottomBar.LENGTH_LONG)
            .setAnimationMode(BaseTransientBottomBar.ANIMATION_MODE_SLIDE)
            .show()
    }
}