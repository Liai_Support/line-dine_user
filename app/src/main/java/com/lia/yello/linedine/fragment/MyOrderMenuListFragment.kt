package com.lia.yello.linedine.fragment

import android.os.Bundle
import android.util.Log
import androidx.fragment.app.Fragment
import android.view.View
import android.widget.Button
import android.widget.LinearLayout
import android.widget.TextView
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.FragmentTransaction
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.lia.yello.linedine.R
import com.lia.yello.linedine.Session.SharedHelper
import com.lia.yello.linedine.Utils.DialogUtils
import com.lia.yello.linedine.Utils.NetworkUtils
import com.lia.yello.linedine.Utils.UiUtils
import com.lia.yello.linedine.ViewModels.MapViewModel
import com.lia.yello.linedine.adapter.MyOrderMenuListAdapterFineDine
import com.lia.yello.linedine.databinding.FragmentMyOrderMenuListBinding
import kotlinx.android.synthetic.main.carticon.*
import kotlinx.android.synthetic.main.fragment_fine_dine_order_list_status.*
import kotlinx.android.synthetic.main.fragment_fine_dine_order_list_status.finedineorderid
import kotlinx.android.synthetic.main.fragment_fine_dine_order_list_status.menushowrecycler
import kotlinx.android.synthetic.main.fragment_my_order_menu_list.*
import kotlinx.android.synthetic.main.fragment_profile.*
import kotlinx.android.synthetic.main.header.*


class MyOrderMenuListFragment : Fragment(R.layout.fragment_my_order_menu_list) {

    var binding: FragmentMyOrderMenuListBinding? = null
    var mapViewModel: MapViewModel? = null
    lateinit var adapter: RecyclerView.Adapter<RecyclerView.ViewHolder>
    lateinit var layoutManager: RecyclerView.LayoutManager
    var orderid:Int=0
    lateinit var ordersubtotal: TextView
    lateinit var vat_txt: TextView
    lateinit var totalmount: TextView
    var status: String=""
    var menuid:Int=0
    lateinit var bot: LinearLayout
    lateinit var linaer_activeorder2: LinearLayout
    lateinit var reorder: Button
    lateinit var review: Button

    public var sharedHelper: SharedHelper? = null
    private var myActiveOrderFragment: MyActiveOrderFragment? = null
    private lateinit var fragmentTransaction: FragmentTransaction
    private var reviewFragment: ReviewFragment? = null

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        binding = DataBindingUtil.findBinding(view)
        sharedHelper = SharedHelper(requireContext())

        mapViewModel = ViewModelProvider(this).get(MapViewModel::class.java)
        val head = view.findViewById<TextView>(R.id.head)
        if (sharedHelper!!.language == "ar"){
            back.rotation= 180F
        }
        back.setOnClickListener(View.OnClickListener {
            requireActivity().supportFragmentManager.popBackStack()
        })
        if (!NetworkUtils.isNetworkConnected(requireContext())) {
            UiUtils.showSnack(
                view, "no_internet_connection"
            )
        }
/*        ordersubtotal = view.findViewById<TextView>(R.id.ordersubtotal)
        vat_txt = view.findViewById<TextView>(R.id.vat_txt)
        totalmount = view.findViewById<TextView>(R.id.totalmount)
        bot = view.findViewById<LinearLayout>(R.id.bot)
        reorder = view.findViewById<Button>(R.id.reorder)
        review = view.findViewById<Button>(R.id.review)*/

  /*      reorder.visibility = View.GONE
        review.visibility = View.GONE*/

        head.setText(getString(R.string.myordersdetails))
        cart_badge.text = ""+ sharedHelper!!.cartcount

        orderid = arguments?.getInt("orderid")!!
        status = arguments?.getString("status")!!
        Log.d("wedrfghnjm,",""+orderid)

        getbookinglist(status)


        if (sharedHelper!!.choose.equals("finedine")||sharedHelper!!.choose.equals("inqueue")|| sharedHelper!!.cartcount == 0){

            //var cartframe: FrameLayout =findViewById(R.id.cartframe)
            cartframe.visibility=View.GONE
        }

    }

    public fun getbookinglist(status:String){
        DialogUtils.showLoader(requireContext())
        mapViewModel?.getbookinglist(requireContext(),status)
            ?.observe(viewLifecycleOwner,
                Observer {
                    DialogUtils.dismissLoader()

                    it?.let {
                        it.error?.let { error ->
                            if (error) {
                                it.message?.let { msg ->
                                    UiUtils.showSnack(root, msg)
                                }
                            } else {
                                it.data?.let { data ->

                                    for (i in 0 until data!!.size) {
                                        if (data[i].id!! == orderid) {
                                            if (data[i].menudetails!!.size != 0) {

                                                menurecycler.layoutManager =
                                                    LinearLayoutManager(context)
                                                menurecycler.adapter =
                                                    MyOrderMenuListAdapterFineDine(
                                                        this,
                                                        requireContext(),
                                                        data[i].menudetails
                                                    )
                                            }
                                        }

                                    }


                                }
                            }
                        }

                    }
                })
    }


}
