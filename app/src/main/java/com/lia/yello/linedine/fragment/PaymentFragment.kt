package com.lia.yello.linedine.fragment


import android.app.Activity
import android.app.DatePickerDialog
import android.content.Intent
import android.os.Bundle
import android.view.View
import android.widget.TextView
import android.widget.Toast
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentTransaction
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.LinearLayoutManager
import com.lia.yello.linedine.Models.ProfileDetails
import com.lia.yello.linedine.R
import com.lia.yello.linedine.Session.SharedHelper
import com.lia.yello.linedine.Utils.BaseUtils
import com.lia.yello.linedine.Utils.DialogUtils
import com.lia.yello.linedine.Utils.UiUtils
import com.lia.yello.linedine.ViewModels.MapViewModel
import com.lia.yello.linedine.ViewModels.ProfileViewModel
import kotlinx.android.synthetic.main.activity_login.*
import kotlinx.android.synthetic.main.fragment_home.*
import kotlinx.android.synthetic.main.fragment_profile.*
import kotlinx.android.synthetic.main.fragment_profile.root
import kotlinx.android.synthetic.main.popup_filter.*
import java.io.File
import java.util.*
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import com.lia.yello.linedine.Utils.NetworkUtils
import com.lia.yello.linedine.activity.NoonPaymentActivity
import com.lia.yello.linedine.adapter.*
import com.lia.yello.linedine.databinding.*
import kotlinx.android.synthetic.main.carticon.*
import kotlinx.android.synthetic.main.fragment_payment.*
import kotlinx.android.synthetic.main.fragment_subhome.*
import kotlinx.android.synthetic.main.fragment_suborders.*
import kotlinx.android.synthetic.main.fragment_suborders.placeorder
import kotlinx.android.synthetic.main.header.*


class PaymentFragment : Fragment(R.layout.fragment_payment) {
    var amt:Double=0.0
    private var orderSucessFragment: OrderSucessFragment? = null
    var orderid:Int=0
    private lateinit var fragmentTransaction: FragmentTransaction
    var subtotal: Double = 0.0
    var vat:Double=0.0
    public var sharedHelper: SharedHelper? = null

    var binding: FragmentPaymentBinding? = null
    private var mapViewModel: MapViewModel? = null
    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        binding = DataBindingUtil.findBinding(view)
        sharedHelper = SharedHelper(requireContext())

        mapViewModel = ViewModelProvider(this).get(MapViewModel::class.java)
        val head = view.findViewById<TextView>(R.id.head)
        head.setText(getString(R.string.payment))
        back.setOnClickListener(View.OnClickListener {
            requireActivity().supportFragmentManager.popBackStack()
        })
        if (!NetworkUtils.isNetworkConnected(requireContext())) {
            UiUtils.showSnack(
                view, "no_internet_connection"
            )
        }

        if (sharedHelper!!.choose.equals("finedine")||sharedHelper!!.choose.equals("inqueue")|| sharedHelper!!.cartcount == 0){

            //var cartframe: FrameLayout =findViewById(R.id.cartframe)
            cartframe.visibility=View.GONE
        }
        amt = arguments?.getDouble("amount")!!
        subtotal = arguments?.getDouble("subtotal")!!
        vat = arguments?.getDouble("vat")!!

        noonpaymentgetewwy.setOnClickListener {
            startActivityForResult(
                Intent(requireContext(), NoonPaymentActivity::class.java)
                    .putExtra(
                        "amount",
                        amt
                    ), 100
            )
        }

    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (requestCode == 100) {
            if (resultCode == Activity.RESULT_OK) {

             //   placeorder(amt,"card")
                //  sendPaymentDetails(data!!.getStringExtra("paymentorderid").toString(),data!!.getStringExtra("paymenttransactionid").toString())

            }
        }
    }

/*    fun placeorder(d:Double,s: String) {
        DialogUtils.showLoader(requireContext())
        mapViewModel?.getplaceorder(
            requireContext(),
            s,
            subtotal,
            d,
            vat
            // instruction.text.toString()
        )
            ?.observe(viewLifecycleOwner,
                Observer {
                    DialogUtils.dismissLoader()

                    it?.let {
                        it.error?.let { error ->
                            if (error) {
                                it.message?.let { msg ->
                                    UiUtils.showSnack(root_suborder, msg)
                                    Toast.makeText(requireContext(), msg, Toast.LENGTH_SHORT)
                                        .show()
                                }
                            } else {
                                cart_badge.text = "0"
                                it.data?.let { data ->
                                    orderid = data.insert_data!!
                                    val bundle = Bundle()
                                    bundle.putInt("orderid", orderid)

                                    orderSucessFragment = OrderSucessFragment()
                                    orderSucessFragment!!.arguments = bundle
                                    fragmentTransaction =
                                        requireActivity().supportFragmentManager.beginTransaction()
                                    fragmentTransaction.setCustomAnimations(
                                        R.anim.screen_in,
                                        R.anim.screen_out
                                    )
                                    fragmentTransaction.replace(
                                        R.id.container,
                                        orderSucessFragment!!
                                    )
                                    fragmentTransaction.addToBackStack(null)
                                    fragmentTransaction.commit()

                                }
                            }
                        }

                    }
                })

    }*/



}