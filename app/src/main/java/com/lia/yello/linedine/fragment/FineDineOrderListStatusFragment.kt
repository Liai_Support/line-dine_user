package com.lia.yello.linedine.fragment

import android.annotation.SuppressLint
import android.app.Activity
import android.app.AlertDialog
import android.content.Context
import android.content.Intent
import android.content.res.ColorStateList
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.location.Geocoder
import android.os.Bundle
import android.text.Editable
import android.text.TextWatcher
import android.text.format.Time
import android.util.DisplayMetrics
import android.util.Log
import android.view.*
import android.view.inputmethod.InputMethodManager
import androidx.fragment.app.Fragment
import android.widget.TextView
import androidx.activity.OnBackPressedCallback
import androidx.core.content.ContextCompat
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.FragmentTransaction
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.LinearLayoutManager
import com.google.android.material.snackbar.BaseTransientBottomBar.LENGTH_SHORT
import com.google.android.material.snackbar.Snackbar
import com.lia.yello.linedine.Models.PromocodeAmount
import com.lia.yello.linedine.R
import com.lia.yello.linedine.Session.SharedHelper
import com.lia.yello.linedine.Utils.BaseUtils
import com.lia.yello.linedine.Utils.DialogUtils
import com.lia.yello.linedine.Utils.NetworkUtils
import com.lia.yello.linedine.Utils.UiUtils
import com.lia.yello.linedine.ViewModels.MapViewModel
import com.lia.yello.linedine.activity.NoonPaymentActivity
import com.lia.yello.linedine.adapter.FineDineOrderedMenuAdapter
import com.lia.yello.linedine.databinding.FragmentFineDineOrderListStatusBinding
import kotlinx.android.synthetic.main.carticon.*
import kotlinx.android.synthetic.main.fragment_fine_dine3.*
import kotlinx.android.synthetic.main.fragment_fine_dine_order_list_status.*
import kotlinx.android.synthetic.main.fragment_fine_dine_order_list_status.addmenu
import kotlinx.android.synthetic.main.fragment_fine_dine_order_list_status.finedineorderid
import kotlinx.android.synthetic.main.fragment_fine_dine_order_list_status.imgorderprocessing
import kotlinx.android.synthetic.main.fragment_fine_dine_order_list_status.menushowrecycler
import kotlinx.android.synthetic.main.fragment_fine_dine_order_list_status.name
import kotlinx.android.synthetic.main.fragment_fine_dine_order_list_status.root_finedineorderstatus
import kotlinx.android.synthetic.main.fragment_fine_dine_order_list_status.statusfdorderlist
import kotlinx.android.synthetic.main.fragment_order_processing.*
import kotlinx.android.synthetic.main.fragment_suborders.*
import kotlinx.android.synthetic.main.fragment_wallet.*
import kotlinx.android.synthetic.main.header.*
import kotlinx.android.synthetic.main.placeorder.view.*
import kotlinx.android.synthetic.main.pop_up_movetopay.view.*
import kotlinx.android.synthetic.main.popup_cancelorder.*
import kotlinx.android.synthetic.main.popup_cancelorder.linear_cancel_order
import kotlinx.android.synthetic.main.popup_cancelorder.others
import kotlinx.android.synthetic.main.popup_cancelorder.popup_cancel_order_close
import kotlinx.android.synthetic.main.popup_cancelorder.radio1
import kotlinx.android.synthetic.main.popup_cancelorder.radio2
import kotlinx.android.synthetic.main.popup_cancelorder.radio3
import kotlinx.android.synthetic.main.popup_cancelorder.radioGroup
import kotlinx.android.synthetic.main.popup_ordercancelfinedine.*
import kotlinx.android.synthetic.main.popup_orderprocess.*
import kotlinx.android.synthetic.main.rulepagepickup.view.*
import kotlinx.android.synthetic.main.rulepagepickup.view.check1
import kotlinx.android.synthetic.main.rulespage.view.*
import java.text.ParseException
import java.text.SimpleDateFormat
import java.time.LocalDate
import java.time.LocalDateTime
import java.time.Period
import java.time.format.DateTimeFormatter
import java.util.*


class FineDineOrderListStatusFragment : Fragment(R.layout.fragment_fine_dine_order_list_status),View.OnClickListener {

    var orderid:Int=0
    var totalamt:Double=0.0
    var paytotal:Double=0.0
    var resid:Int=0
    var status:String=""
    var bookingstatus:String=""
    var hotelname:String=""
    var resimage:String=""
    var fcity:String=""
    var descrip:String=""
    var discountamt:Double=0.0
    var tmt:Double=0.0
    var finaldiscountamt:Double=0.0
    var promocodearray:ArrayList<PromocodeAmount>? = ArrayList<PromocodeAmount>()

    var binding: FragmentFineDineOrderListStatusBinding? = null
    private var mapViewModel: MapViewModel? = null
    public var sharedHelper: SharedHelper? = null
    private var homeFragment : HomeFragment? = null
    private var myOrderMenuListFragment : MyOrderMenuListFragment? = null
    private var myOrderFragment : MyOrderFragment? = null
    private lateinit var fragmentTransaction: FragmentTransaction
    private var fineDineMenuList : FineDineMenuListFragment? = null
    private var myLat: Double? = null
    private var myLng: Double? = null
    private var paymenttype: String? = ""
    private var statuspaid: String? = ""
    public  var ispayment = false
    var promocode:String=""
    var promocodelist: MutableList<String> = mutableListOf<String>()
    var minimumanount:Double=0.0
    var refundamt:Double=0.0
    var paymentorderid:String=""
    var StartDate:String=""
    var EndDate:String=""
    var finaldatetime:String=""
    var finalbookingtime:Int=0
    var discountid:Int=0
    var uptoamount :Double=0.0
    var diss:Double=0.0
    var ordersubtotal:Double=0.0
    var vattt:Double=0.0
    var bookingamt:Double=0.0
    var finaltotal:Double=0.0

    @SuppressLint("NewApi")

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        binding = DataBindingUtil.findBinding(view)
        mapViewModel = ViewModelProvider(this).get(MapViewModel::class.java)
        val head = view.findViewById<TextView>(R.id.head)
        sharedHelper = SharedHelper(requireContext())
        orderid = arguments?.getInt("orderid")!!
        resid = arguments?.getInt("resid")!!
        status = arguments?.getString("status")!!
        bookingstatus=status
        statuspaid = arguments?.getString("paidstatus")!!
        resimage = arguments?.getString("resimg")!!
        hotelname = arguments?.getString("resname")!!
        myLat = arguments?.getDouble("lat")!!
        myLng = arguments?.getDouble("lng")!!
        paymenttype = arguments?.getString("paymenttype")!!
        Log.d("statuspaid",""+statuspaid)
        Log.d("orderid",""+orderid)
        Log.d("paymenggttype",""+paymenttype)
        if (sharedHelper!!.choose.equals("finedine")||sharedHelper!!.choose.equals("inqueue")){

            //var cartframe: FrameLayout =findViewById(R.id.cartframe)
            cartframe.visibility=View.INVISIBLE
        }
        if (!NetworkUtils.isNetworkConnected(requireContext())) {
            UiUtils.showSnack(
                view, "no_internet_connection"
            )
        }
        if (statuspaid.equals("paid") && paymenttype.equals("cod")){
            movetopay.visibility=View.GONE
            cancelreservation.visibility=View.GONE
            Log.d("asdfghjkl","yes")
        }else if (statuspaid.equals("unpaid") && paymenttype.equals("card")&&status.equals("processing")){

            movetopay.visibility=View.VISIBLE
            couponfdlayout.visibility=View.VISIBLE
            cancelreservation.visibility=View.GONE

            Log.d("wedfcvgbhn","no")

        }else if (statuspaid.equals("unpaid") && paymenttype.equals("wallet")&&status.equals("processing")){
            movetopay.visibility=View.VISIBLE
            couponfdlayout.visibility=View.VISIBLE
            cancelreservation.visibility=View.GONE



/*

                movetopay.visibility=View.GONE
                cancelreservation.visibility=View.GONE

                Log.d("wedfcvgbhn","no")
*/

        }else if (statuspaid.equals("paid") && paymenttype.equals("card")){

            movetopay.visibility=View.GONE
            cancelreservation.visibility=View.GONE

            Log.d("wedfcvgbhn","no")

        }else if (statuspaid.equals("paid") && paymenttype.equals("wallet")){

            movetopay.visibility=View.GONE
            cancelreservation.visibility=View.GONE

            Log.d("wedfcvgbhn","no")

        }else if (statuspaid.equals("unpaid") && paymenttype.equals("card")&&status.equals("booked")){

            movetopay.visibility=View.GONE
            couponfdlayout.visibility=View.GONE
            cancelreservation.visibility=View.VISIBLE

            Log.d("wedfcvgbhn","no")

        }else if (statuspaid.equals("unpaid") && paymenttype.equals("card")&&status.equals("booked")){

            movetopay.visibility=View.GONE
            couponfdlayout.visibility=View.GONE
            cancelreservation.visibility=View.VISIBLE

            Log.d("wedfcvgbhn","no")

        }

        if (status.equals("pending")){

            addmenu.visibility=View.GONE
            movetopay.visibility=View.GONE
            val builder = AlertDialog.Builder(requireContext())
            builder.setTitle(R.string.alert)
            builder.setMessage(R.string.wouldyouliketoconfirmyourbooking)
            // Set a positive button and its click listener on alert dialog
            builder.setPositiveButton("YES") { dialog, which ->
                //  checkformenu.visibility = View.VISIBLE
                //  getcomfirmbooking()

                /*   startActivityForResult(
                    Intent(requireContext(), NoonPaymentActivity::class.java)
                        .putExtra(
                            "amount",
                            50.0
                        ), 100
                )*/
                val mDialogView =
                    LayoutInflater.from(requireContext()).inflate(R.layout.rulespage, null)
                val mBuilder =
                    AlertDialog.Builder(requireContext()).setView(mDialogView).setCancelable(false)
                val mAlertDialog = mBuilder.show()
                val layoutParams = WindowManager.LayoutParams()
                val displayMetrics = DisplayMetrics()
                requireActivity().windowManager.defaultDisplay.getMetrics(displayMetrics)
                val displayWidth = displayMetrics.widthPixels
                val displayHeight = displayMetrics.heightPixels
                layoutParams.copyFrom(mAlertDialog.window?.attributes)
                layoutParams.width = ((displayWidth * 0.9f).toInt())
                mAlertDialog.window?.setAttributes(layoutParams)
                mAlertDialog.window?.setGravity(Gravity.CENTER)
                mAlertDialog.window?.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))

                mDialogView.check1.setOnCheckedChangeListener { buttonView, isChecked ->
                    if (isChecked) {
                        mDialogView.check1.buttonTintList =
                            ColorStateList.valueOf(resources.getColor(R.color.colorPrimary))
                    } else {
                        mDialogView.check1.buttonTintList =
                            ColorStateList.valueOf(resources.getColor(R.color.grey2))
                    }
                }
                mDialogView.check2.setOnCheckedChangeListener { buttonView, isChecked ->
                    if (isChecked) {
                        mDialogView.check2.buttonTintList =
                            ColorStateList.valueOf(resources.getColor(R.color.colorPrimary))
                    } else {
                        mDialogView.check2.buttonTintList =
                            ColorStateList.valueOf(resources.getColor(R.color.grey2))
                    }
                }
                /*  mDialogView.check3.setOnCheckedChangeListener { buttonView, isChecked ->
                      if (isChecked) {
                          mDialogView.check3.buttonTintList =
                              ColorStateList.valueOf(resources.getColor(R.color.colorPrimary))
                      } else {
                          mDialogView.check3.buttonTintList =
                              ColorStateList.valueOf(resources.getColor(R.color.grey2))
                      }
                  }*/


                mDialogView.proccedfd.setOnClickListener {

                    if (mDialogView.check1.isChecked && mDialogView.check2.isChecked ) {
                        mAlertDialog.dismiss()


                        val mDialogView =
                            LayoutInflater.from(requireContext()).inflate(R.layout.placeorder, null)
                        val mBuilder = AlertDialog.Builder(requireContext()).setView(mDialogView)
                            .setCancelable(true)
                        val mAlertDialog = mBuilder.show()
                        val layoutParams = WindowManager.LayoutParams()
                        val displayMetrics = DisplayMetrics()
                        requireActivity().windowManager.defaultDisplay.getMetrics(displayMetrics)
                        val displayWidth = displayMetrics.widthPixels
                        val displayHeight = displayMetrics.heightPixels
                        layoutParams.copyFrom(mAlertDialog.getWindow()?.getAttributes())
                        layoutParams.width = ((displayWidth * 0.9f).toInt())
                        mAlertDialog.getWindow()?.setAttributes(layoutParams)
                        mAlertDialog.getWindow()?.setGravity(Gravity.CENTER)
                        mAlertDialog.getWindow()
                            ?.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
                        mDialogView.checkoutcard.setOnClickListener {
                            startActivityForResult(
                                Intent(requireContext(), NoonPaymentActivity::class.java)
                                    .putExtra(
                                        "amount",
                                        50.0
                                    ), 100
                            )
                            mAlertDialog.dismiss()

                        }
                        mDialogView.checkoutwallet.setOnClickListener {
                            getwalletamount(50.0)
                            mAlertDialog.dismiss()

                        }


                    } else {
                        UiUtils.showSnack(root_finedineorderstatus, getString(R.string.plscheckthebox))
                        //  mAlertDialog.dismiss()


                    }


                }
                mDialogView.backfd.setOnClickListener {

                    mAlertDialog.dismiss()


                }
            }


            builder.setNegativeButton("No") { dialog, which ->
                builder.setCancelable(true)
                //
                // finalreserve.visibility = View.VISIBLE
                myOrderFragment = MyOrderFragment()
                fragmentTransaction =
                    requireActivity().supportFragmentManager.beginTransaction()
                fragmentTransaction.setCustomAnimations(
                    R.anim.screen_in,
                    R.anim.screen_out
                )
                fragmentTransaction.replace(R.id.container, myOrderFragment!!)
                fragmentTransaction.commit()


            }
            val dialog: AlertDialog = builder.create()
            dialog.show()
            dialog.setCancelable(false)


        }
        else if (status.equals("booked")){


            if (finalbookingtime<20){
                if (finalbookingtime<0) {
                    Log.d("hghghgh","llll")
                    addmenu.visibility=View.VISIBLE
                }else{
                    Log.d("hghghgh","ll1")

                    addmenu.visibility=View.VISIBLE
                    cancelreservation.visibility=View.VISIBLE
                    couponfdlayout.visibility=View.GONE
                }

            }else{
                Log.d("nvnvnvn","mxmxmx")

                addmenu.visibility=View.VISIBLE
                cancelreservation.visibility=View.VISIBLE


            }




        }

        activity?.onBackPressedDispatcher?.addCallback(this, object : OnBackPressedCallback(true) {
            override fun handleOnBackPressed() {

                /* val bundle = Bundle()
                 bundle.putInt("bookingid",bookingid)
                 bundle.putInt("tableid",value)
                 bundle.putString("booked","true")
                 bundle.putString("resimg",resimage)*/
                myOrderFragment = MyOrderFragment()
                //    myOrderFragment!!.arguments = bundle
                fragmentTransaction = requireActivity().supportFragmentManager.beginTransaction()
                fragmentTransaction.setCustomAnimations(R.anim.screen_in, R.anim.screen_out)
                fragmentTransaction.replace(R.id.container, myOrderFragment!!)
                fragmentTransaction.addToBackStack(null)
                fragmentTransaction.commit()

            }
        })


        head.setText(getString(R.string.myordersdetails))
        cart_badge.text = ""+ sharedHelper!!.cartcount
        resimage.let {
            UiUtils.loadImage(
                imgorderprocessing,
                it,
                ContextCompat.getDrawable(
                    requireContext(),
                    R.drawable.dummylogo
                )!!
            )
        }
        val gcd = Geocoder(context, Locale.getDefault())
        val addresses = gcd.getFromLocation(myLat!!, myLng!!, 1)
        if (addresses.size > 0) {
            println(addresses[0].locality)
            Log.d("adedretrgygyuihu", "" + addresses[0].locality)
            fcity=addresses[0].locality
            loccationnnn.text= ": $fcity"
        } else {
            // do your stuff
        }
        name.text=hotelname
        back.setOnClickListener(View.OnClickListener {
            //  requireActivity().supportFragmentManager.popBackStack()

            myOrderFragment = MyOrderFragment()
            //    myOrderFragment!!.arguments = bundle
            fragmentTransaction = requireActivity().supportFragmentManager.beginTransaction()
            fragmentTransaction.setCustomAnimations(R.anim.screen_in, R.anim.screen_out)
            fragmentTransaction.replace(R.id.container, myOrderFragment!!)
            fragmentTransaction.addToBackStack(null)
            fragmentTransaction.commit()
        })


        if (sharedHelper!!.language=="ar"){
            if (status.equals("booked")){
                statusfdorderlist.text="حجز"

            }else if (status=="processing"){
                statusfdorderlist.text="تحت المعالجة"

            }
        }else{
            statusfdorderlist.text=status

        }


        cancelreservation.setOnClickListener(View.OnClickListener {


            if(status.equals("booked")){
                linear_cancel_orderfinedine.visibility=View.VISIBLE


            }

        })


        refund.setOnClickListener(View.OnClickListener  {

            //   refundamouunt()

        })


        getbookinglist("active")


        finedineorderlinearclick.setOnClickListener(View.OnClickListener {
            val bundle = Bundle()
            bundle.putInt("orderid",orderid )
            bundle.putString("status",status )
            myOrderMenuListFragment = MyOrderMenuListFragment()
            myOrderMenuListFragment!!.arguments = bundle
            fragmentTransaction = requireActivity().supportFragmentManager.beginTransaction()
            fragmentTransaction.setCustomAnimations(R.anim.screen_in, R.anim.screen_out)
            fragmentTransaction.replace(R.id.container, myOrderMenuListFragment!!)
            fragmentTransaction.addToBackStack(null)
            fragmentTransaction.commit()
        })


        if (sharedHelper!!.language == "ar"){
            back.rotation= 180F
        }

        getlistoffers(resid)

        couponcodefinedine.addTextChangedListener(object : TextWatcher {
            override fun beforeTextChanged(s: CharSequence, start: Int, count: Int, after: Int) {

            }

            override fun onTextChanged(s: CharSequence, start: Int, before: Int, count: Int) {
            }

            override fun afterTextChanged(s: Editable) {


                promocode=couponcodefinedine.text.toString()
                Log.d("kanikakjakajk",""+promocode)
            }
        })

        applycouponfinedine.setOnClickListener {

            if (promocode.equals("")){
                UiUtils.showSnack(root_finedineorderstatus,getString(R.string.pleaseenterthecouponcode))

            }else{
                getvalidateoffer(promocode,resid)

            }

            getvalidateoffer(promocode,resid)

            /*for (i in 0 until promocodearray!!.size){
                Log.d("sdfghjkl",promocodelist[i] )
                Log.d("xcvbncxvb",promocode )
                val promo=promocodelist[i]
                if (promocodearray!![i].promocode==promocode){
                *//*    if (promocode.equals("")){
                        UiUtils.showSnack(root_finedineorderstatus,getString(R.string.pleaseenterthecouponcode))

                    }else{*//*
                        if (totalamt>promocodearray!![i].amount){
                            getvalidateoffer(promocode,resid)

                        }else{
                            val imm: InputMethodManager =
                                activity!!.getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
                            imm.hideSoftInputFromWindow(view!!.windowToken, 0)
                            UiUtils.showSnack(root_finedineorderstatus,getString(R.string.minimummorderamountislow))
                        }
                   // }
                }else{

                      if (promocode.equals("")){
                        UiUtils.showSnack(root_finedineorderstatus,getString(R.string.pleaseenterthecouponcode))

                    }else{
                         // UiUtils.showSnack(root_finedineorderstatus,getString(R.string.minimummorderamountislow))

                      }




                }
            }*/












        }



        popup_cancel_order_close.setOnClickListener(this)

        radioGroup.setOnClickListener(this)
        radio1.setOnClickListener(this)
        radio2.setOnClickListener(this)
        radio3.setOnClickListener(this)
        others.setOnClickListener(this)
        popup_cancel_orderfinedine.setOnClickListener(this)


    }



    public fun getbookinglist(status:String){
        DialogUtils.showLoader(requireContext())
        mapViewModel?.getbookinglist(requireContext(),status)
            ?.observe(viewLifecycleOwner,
                Observer {
                    DialogUtils.dismissLoader()

                    it?.let {
                        it.error?.let { error ->
                            if (error) {
                                it.message?.let { msg ->

                                    UiUtils.showSnack(root_finedineorderstatus, msg)
                                }
                            } else {
                                it.data?.let { data ->

                                    for (i in 0 until data!!.size) {
                                        if (data[i].id!! == orderid) {

                                            discountlevelfd.visibility=View.VISIBLE
                                            if (data[i].discount_amount!!.equals("0")){
                                                discountlevelfd.visibility=View.GONE
                                                productotalfd.visibility=View.GONE


                                            }else{
                                                productotalfd.visibility=View.VISIBLE
                                                discountlevelfd.visibility=View.VISIBLE
                                                discountfd.text= " - "+String.format(Locale("en", "US"),"%.2f",data[i].discount_amount!!.toDouble()) +" SAR"
                                                finedineproducttotal.text= String.format(Locale("en", "US"),"%.2f",data[i].discount_amount!!.toDouble()+data[i].subtotal!!.toDouble()) +" SAR"


                                            }

                                            //  statuspaid=data[i].paidstatus
                                            // Log.d("aqwsdrfcvgbnm",""+statuspaid)
                                            paymentorderid=data[i].Payment_orderid!!
                                            StartDate= data[i].date!!
                                            EndDate=data[i].timeslot.toString()
                                            EndDate= EndDate.split(":").toString()
                                            EndDate= data[i].timeslot!!.substring(0,2)

                                            finaldatetime= StartDate + " " +EndDate+":"+"00"
                                            Log.d("nbdzcbncdbn",EndDate)
                                            Log.d("nbdzcbncdbn",finaldatetime)
                                            gettimee()
                                            /* Log.d("qwertyuiop",""+StartDate)
                                             Log.d("xcvbnm,.",""+data[i].timeslot!!.substring(0,2))
                                             Log.d("qwertyuiop",""+EndDate.substringBefore(":").removeSuffix(","))
                                                 //var timeslot=String[ ]
                                             Log.d("nmcnmcvnmcvnmvcnmm",""+EndDate.split("-"))*/
                                            if (data[i].menudetails!!.size != 0) {
                                                //  movetopay.visibility=View.VISIBLE
                                                finedinesubtotal.text=String.format(Locale("en", "US"),"%.2f", data[i].subtotal)+ " SAR"
                                                ordersubtotal=data[i].subtotal!!
                                                finedinevat.text=String.format(Locale("en", "US"),"%.2f", data[i].vat)+ " SAR"
                                                finedinetotal.text=String.format(Locale("en", "US"),"%.2f", data[i].amount)+ " SAR"
                                                vattt=data[i].subtotal!!/data[i].vat!!
                                                /*finedinesubtotal.text =
                                                    data[i].subtotal.toString() + " SAR"
                                                finedinevat.text =
                                                   data[i].vat.toString() + " SAR"
                                                finedinetotal.text =
                                                 data[i].amount.toString() + " SAR"
*/
                                                /*   paytotal= data[i].amount!!
   */
                                                paytotal=   String.format(Locale("en", "US"),"%.2f",data[i].amount!!).toDouble()
                                                finaltotal=paytotal
/*
                                                finedinebookingamt.text =
                                                  data[i].initial_amount.toString() + " SAR"*/

                                                Log.d("qawsedrftgyhujikol;",""+  data[i].initial_amount.toString() )
                                                finedinebookingamt.text =" -" + String.format(Locale("en", "US"),"%.2f",data[i].initial_amount)+ " SAR"
                                                totalamt= data[i].subtotal!!
                                                bookingamt= data[i].initial_amount!!
                                                // finaltotal=totalamt

                                                totalamt = String.format(Locale("en", "US"),"%.2f",totalamt).toDouble()

                                                addmenu.visibility=View.GONE
                                                linear_ordermenushow.visibility=View.VISIBLE
                                                finedineorderlinearclick.isEnabled=true
                                                finedineorderid.text =
                                                    getString(R.string.orderid) + " : " + data[i].id.toString()

                                                menushowrecycler.layoutManager =
                                                    LinearLayoutManager(context)
                                                menushowrecycler.adapter =
                                                    FineDineOrderedMenuAdapter(
                                                        this,
                                                        requireContext(),
                                                        data[i].menudetails
                                                    )
                                            }else{
                                                linear_ordermenushow.visibility=View.GONE
                                                finedineorderlinearclick.isEnabled=false

                                                if (finalbookingtime<20){
                                                    Log.d("jnjnj","mnmbnb")
                                                    if (finalbookingtime<0) {
                                                        addmenu.visibility=View.VISIBLE
                                                    }else {
                                                        Log.d("lmlm","kkkk")
                                                        addmenu.visibility=View.VISIBLE

                                                    }
                                                }else{
                                                    addmenu.visibility=View.VISIBLE
                                                    Log.d("ccccvv","ccdcc")

                                                }
                                                movetopay.visibility=View.GONE
                                                addmenu.setOnClickListener(View.OnClickListener {

                                                    val bundle = Bundle()
                                                    bundle.putInt("resid",resid )
                                                    bundle.putInt("orderid", orderid)
                                                    bundle.putString("isfd","false")
                                                    bundle.putString("inqueue","false")
                                                    bundle.putSerializable("menulist",null)
                                                    bundle.putString("status",bookingstatus)
                                                    bundle.putString("resname",hotelname)
                                                    bundle.putString("resimg",resimage)
                                                    bundle.putDouble("lat", myLat!!)
                                                    bundle.putDouble("lng", myLng!!)
                                                    bundle.putString("paidstatus",statuspaid)
                                                    bundle.putString("paymenttype",paymenttype)
                                                    Log.d("orderid",""+orderid)
                                                    fineDineMenuList = FineDineMenuListFragment()
                                                    fineDineMenuList!!.arguments=bundle
                                                    fragmentTransaction = requireActivity().supportFragmentManager.beginTransaction()
                                                    fragmentTransaction.setCustomAnimations(R.anim.screen_in,R.anim.screen_out)
                                                    fragmentTransaction.replace(R.id.container, fineDineMenuList!!)
                                                    fragmentTransaction.addToBackStack(null)
                                                    fragmentTransaction.commit()


                                                })
                                            }


/*


                                            if (paymenttype.equals("card")){
                                                movetopay.visibility=View.VISIBLE
                                            }
*/

                                            movetopay.setOnClickListener(View.OnClickListener {

                                                // getbookingpayment()
                                                val mDialogView = LayoutInflater.from(requireContext()).inflate(R.layout.pop_up_movetopay, null)
                                                val mBuilder = AlertDialog.Builder(requireContext()).setView(mDialogView).setCancelable(false)
                                                val mAlertDialog = mBuilder.show()
                                                val layoutParams = WindowManager.LayoutParams()
                                                val displayMetrics = DisplayMetrics()
                                                requireActivity().windowManager.defaultDisplay.getMetrics(displayMetrics)
                                                val displayWidth = displayMetrics.widthPixels
                                                val displayHeight = displayMetrics.heightPixels
                                                layoutParams.copyFrom(mAlertDialog.getWindow()?.getAttributes())
                                                layoutParams.width = ((displayWidth * 0.9f).toInt())
                                                mAlertDialog.getWindow()?.setAttributes(layoutParams)
                                                mAlertDialog.getWindow()?.setGravity(Gravity.CENTER)
                                                mAlertDialog.getWindow()?.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
                                                mDialogView.card.setOnClickListener {

                                                    ispayment=true
                                                    startActivityForResult(
                                                        Intent(requireContext(), NoonPaymentActivity::class.java)
                                                            .putExtra(
                                                                "amount",
                                                                finaltotal
                                                            ), 100
                                                    )
                                                    // getbookingpayment()
                                                    // pop_up_movetopay.visibility = View.GONE
                                                    mAlertDialog.dismiss()

                                                }
                                                mDialogView.wallet.setOnClickListener {
                                                    //  getbookingpayment()
                                                    ispayment=true

                                                    getwalletamount(finaltotal)
                                                    mAlertDialog.dismiss()

                                                }

                                            })



                                        }

                                    }


                                }
                            }
                        }

                    }
                })
    }


    private fun cancelbooking(paymentype:String,id:Int,refundamt:Double){
        DialogUtils.showLoader(requireContext())

        mapViewModel?.cancelbooking(requireContext(),id, paymentype,refundamt,descrip)
            ?.observe(viewLifecycleOwner,
                Observer {
                    DialogUtils.dismissLoader()
                    it?.let {
                        it.error?.let { error ->
                            if (error) {
                                it.message?.let { msg ->
                                    UiUtils.showSnack(root_finedineorderstatus, msg)

                                }
                            }
                            else {
                                it.message?.let { msg ->
                                    UiUtils.showSnack(root_finedineorderstatus, msg)

                                    if (paymenttype.equals("card") && status.equals("booked")  ){
                                        refundamouunt(refundamt, paymentorderid)
                                    }
                                    else{
                                        homeFragment = HomeFragment()
                                        fragmentTransaction = requireActivity().supportFragmentManager.beginTransaction()
                                        fragmentTransaction.setCustomAnimations(R.anim.screen_in,R.anim.screen_out)
                                        fragmentTransaction.replace(R.id.container, homeFragment!!)
                                        fragmentTransaction.addToBackStack(null)
                                        fragmentTransaction.commit()
                                    }



                                }
                            }
                        }

                    }
                })

    }




    public fun getbookingpayment(totalamt: Double, s: String,payid: String,transid: String) {
        DialogUtils.showLoader(requireContext())
        mapViewModel?.bookingpayment(requireContext(),orderid, totalamt,"paid",s,payid,transid,discountid,finaldiscountamt)
            ?.observe(viewLifecycleOwner, androidx.lifecycle.Observer {
                DialogUtils.dismissLoader()

                it?.let {
                    it.error?.let { error ->
                        if (error) {
                            it.message?.let { msg ->
                                movetopay.visibility=View.VISIBLE
                                couponfdlayout.visibility=View.VISIBLE


                                UiUtils.showSnack(root_finedineorderstatus, msg)
                            }
                        } else {
                            it.message?.let { msg ->
                                UiUtils.showSnack(root_finedineorderstatus, msg)
                                couponfdlayout.visibility=View.GONE
                                movetopay.visibility=View.GONE
/*
                                homeFragment = HomeFragment()
                                fragmentTransaction =
                                    requireActivity().supportFragmentManager.beginTransaction()
                                fragmentTransaction.setCustomAnimations(R.anim.screen_in,
                                    R.anim.screen_out)
                                fragmentTransaction.replace(R.id.container, homeFragment!!)
                                fragmentTransaction.commit()*/

                            }
                        }

                    }
                }
            })


    }


    public fun getcomfirmbooking(s: String, payorderid: String, transid: String) {
        DialogUtils.showLoader(requireContext())
        mapViewModel?.getcomfirmbooking(requireContext(), orderid, 50.0, "booked", s,payorderid,transid)
            ?.observe(viewLifecycleOwner, androidx.lifecycle.Observer {
                DialogUtils.dismissLoader()

                it?.let {
                    it.error?.let { error ->
                        if (error) {
                            it.message?.let { msg ->

                                UiUtils.showSnack(root_finedineorderstatus, msg)
                            }
                        } else {
                            it.message?.let { msg ->
                                UiUtils.showSnack(root_finedineorderstatus, msg)

                                movetopay.visibility=View.GONE

                                myOrderFragment = MyOrderFragment()
                                fragmentTransaction =
                                    requireActivity().supportFragmentManager.beginTransaction()
                                fragmentTransaction.setCustomAnimations(
                                    R.anim.screen_in,
                                    R.anim.screen_out
                                )
                                fragmentTransaction.replace(R.id.container, myOrderFragment!!)
                                fragmentTransaction.commit()



                            }
                        }

                    }
                }
            })


    }



    /*statusfdorderlist.setOnClickListener(View.OnClickListener {
               // pop_up_movetopay.visibility = View.VISIBLE



            })*/




    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (requestCode == 100) {
            if (resultCode == Activity.RESULT_OK) {

                if (ispayment==false){
                    getcomfirmbooking("card",data!!.getStringExtra("paymentorderid").toString(),data!!.getStringExtra("paymenttransactionid").toString())

                }else{

                    getbookingpayment(finaltotal,"card",data!!.getStringExtra("paymentorderid").toString(),data!!.getStringExtra("paymenttransactionid").toString())
                }
                //  sendPaymentDetails(data!!.getStringExtra("paymentorderid").toString(),data!!.getStringExtra("paymenttransactionid").toString())

            }
        }
    }



    private fun getwalletamount(totalamt: Double) {
        DialogUtils.showLoader(requireContext())

        mapViewModel?.getwalletamount(requireContext())
            ?.observe(viewLifecycleOwner, androidx.lifecycle.Observer {
                DialogUtils.dismissLoader()

                it?.let {
                    it.error?.let { error ->
                        if (error) {
                            it.message?.let { msg ->
                                UiUtils.showSnack(root_finedineorderstatus, msg)
                            }
                        } else {
                            it.data?.let { data ->
                                //  getbookingpayment()
                                if(data.amount!! >= totalamt){
                                    if (ispayment==true){
                                        getbookingpayment(totalamt,"wallet","","")

                                    }else{
                                        getcomfirmbooking("wallet","","")

                                    }
                                    //getbookingpayment(totalamt,"wallet","","")
                                }else{
                                    UiUtils.showSnack(root_finedineorderstatus, getString(R.string.yourwalletamountislowpleaseuplateyourwalletamount))

                                    // Snackbar.make(root_finedineorderstatus,getString(R.string.yourwalletamountislowpleaseuplateyourwalletamount),LENGTH_SHORT)
                                    getbookinglist("active")
                                }
                                /*Log.d("amount", data.amount.toString())
                                var amount:Double
                                amount= data.amount!!
                                walletamount.text = amount.toString()+" SAR"
                                Log.d("sdxcfvgbnm",""+requireActivity().walletamount.text)*/

                            }

                        }
                    }

                }
            })

    }


    private fun refundamouunt(amt:Double,orderid:String){
        DialogUtils.showLoader(requireContext())

        mapViewModel?.refundpayment(requireContext(), amt,orderid)
            ?.observe(viewLifecycleOwner,
                Observer {
                    DialogUtils.dismissLoader()
                    it?.let {
                        it.rcode?.let { rcode ->
                            if (rcode!=0) {
                                it.message?.let { msg ->
                                    UiUtils.showSnack(root_finedineorderstatus, msg)

                                }
                            }
                            else {
                                it.message?.let { msg ->
                                    UiUtils.showSnack(root_finedineorderstatus, msg)
                                    Log.d("ertyul;",""+msg)
                                    homeFragment = HomeFragment()
                                    fragmentTransaction = requireActivity().supportFragmentManager.beginTransaction()
                                    fragmentTransaction.setCustomAnimations(R.anim.screen_in,R.anim.screen_out)
                                    fragmentTransaction.replace(R.id.container, homeFragment!!)
                                    fragmentTransaction.addToBackStack(null)
                                    fragmentTransaction.commit()

                                }
                            }
                        }

                    }
                })

    }
    fun gettimee(){


        val sdf= SimpleDateFormat("dd/MM/yyyy HH:mm")
        val dtf = DateTimeFormatter.ofPattern("dd-MM-yyyy HH:mm")
        val date=Date()
        println(sdf.format(date))
        Log.d("t1",sdf.format(date))

        val cal= Calendar.getInstance()
        println(sdf.format(cal.time))
        Log.d("t2",""+sdf.format(cal.time))
        val currentdatetime=sdf.format(cal.time)
        val now=LocalDateTime.now()
        println(dtf.format(now))
        Log.d("ertyuiop",""+dtf.format(now))

        val localdate=LocalDate.now()
        println(DateTimeFormatter.ofPattern("dd/MM/yyyy").format(localdate))
        Log.d("cvbnmcvb",""+localdate)
        Log.d("vffvfvf",""+DateTimeFormatter.ofPattern("dd/MM/yyyy").format(localdate))



        try {
/*        val date1: Date = sdf.parse("20/10/2021 17:05:10")
        val date2: Date = sdf.parse("20/10/2021 17:35:10")*/
            val date1: Date = sdf.parse(currentdatetime)
            val date2: Date = sdf.parse(finaldatetime)
            printDifference(date1, date2)
        } catch (e: ParseException) {
            e.printStackTrace()
        }

        val current = LocalDateTime.now()

        val formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm")
        val formatted = current.format(formatter)

        BaseUtils.getFormatedDateUtc(formatted, "yyyy-MM-dd HH:mm", "dd/MM/yyyy hh:mm a")
        println("Current Date and Time is: $formatted")



    }

    public  fun printDifference(startDate: Date, endDate: Date) {
        var different: Long = endDate.getTime() - startDate.getTime()

        System.out.println("startDate : " + startDate);
        System.out.println("endDate : "+ endDate);
        System.out.println("different : " + different);


        val secondsInMilli: Long = 1000
        val minutesInMilli = secondsInMilli * 60
        val hoursInMilli = minutesInMilli * 60
        val daysInMilli = hoursInMilli * 24


        val elapsedDays = different / daysInMilli
        different = different % daysInMilli

        val elapsedHours = different / hoursInMilli
        different = different % hoursInMilli

        val elapsedMinutes = different / minutesInMilli
        different = different % minutesInMilli

        val elapsedSeconds = different / secondsInMilli
        finalbookingtime= (elapsedHours*60+elapsedMinutes).toInt()

        Log.d("uiehffuuerhgur","oiuuytrre"+elapsedHours*60)
        Log.d("uiehffuuerhgur","bnbnbb"+elapsedMinutes)
        Log.d("uiehffuuerhgur","okok"+elapsedHours)
        Log.d("uiehffuuerhgur","sdfgh"+elapsedSeconds)
        Log.d("finalbookingtime",""+finalbookingtime)

    }


    private fun getlistoffers(restuarntid:Int) {
        DialogUtils.showLoader(requireContext())

        mapViewModel?.getListoffers(requireContext(), restuarntid)
            ?.observe(viewLifecycleOwner, androidx.lifecycle.Observer {
                DialogUtils.dismissLoader()

                it?.let {
                    it.error?.let { error ->
                        if (error) {
                            it.message?.let { msg ->
                                UiUtils.showSnack(root_finedineorderstatus, msg)
                            }
                        } else {
                            it.data?.let { data ->
                                for (i in 0 until data.size){
                                    promocodearray!!.add(PromocodeAmount(data[i].promocode!!,data[i].minimum_order_amount!!.toDouble()))

                                    promocodelist.add(data[i].promocode!!)
                                    // minimumanount=data[i].minimum_order_amount!!.toDouble()
                                }
                                Log.d("promocodelist.size",""+promocodelist.size)

                            }

                        }
                    }

                }
            })

    }
    private fun getvalidateoffer(promocode: String,restuarntid:Int) {
        DialogUtils.showLoader(requireContext())

        mapViewModel?.getvalidateoffer(requireContext(), restuarntid,promocode)
            ?.observe(viewLifecycleOwner, androidx.lifecycle.Observer {
                DialogUtils.dismissLoader()

                it?.let {
                    it.error?.let { error ->
                        if (error) {
                            it.message?.let { msg ->

                                UiUtils.showSnack(root_finedineorderstatus, msg)

                            }
                        }else {
                            it.data?.let { data ->

                                minimumanount=data[0].minimum_order_amount!!.toDouble()
                                if (totalamt<minimumanount){
                                    UiUtils.showSnack(root_finedineorderstatus,getString(R.string.minimummorderamountislow))

                                }else{
                                    UiUtils.showSnack(root_finedineorderstatus,getString(R.string.promocodeapplied))
                                    applycouponfinedine.setEnabled(false)
                                    couponcodefinedine.setEnabled(false)
                                    uptoamount=data[0].upto_amt!!.toDouble()
                                    productotalfd.visibility=View.VISIBLE
                                    discountlevelfd.visibility=View.VISIBLE
                                    totalfinedine.visibility=View.VISIBLE
                                    finedineproducttotal.text= String.format(Locale("en", "US"),"%.2f",ordersubtotal)+" SAR"

                                    if (data[0].percentage.equals("0")){
                                        // discountpickup.text= data[0].sar.toString()+" SAR"
                                        discountid= data[0].id!!


                                        discountfd.text= "- "+String.format(Locale("en", "US"),"%.2f",data[0].sar!!.toDouble())+" SAR"
                                        val saramt=data[0].sar!!.toDouble()
                                        finaldiscountamt=data[0].sar!!.toDouble()
                                        val subtt=ordersubtotal-saramt
                                        finedinesubtotal.text=String.format(Locale("en", "US"),"%.2f", subtt)+ " SAR"

                                        val vat = (subtt / 100.0f) * vattt
                                        finedinevat.text=String.format(Locale("en", "US"),"%.2f", vat)+ " SAR"

                                        //  Log.d("ertyui9op[]",""+paytotal)
                                        //paytotal -= data[0].sar!!.toDouble()
                                        //discountamt=data[0].star!!.toDouble()
                                        //Log.d("ertyui9op[]",""+paytotal)
                                        //Log.d("ertyui9op[]",""+discountamt)
                                        val tt=subtt+vat

                                        tltfd.text= String.format(Locale("en", "US"),"%.2f",tt) +" SAR"
                                        finedinebookingamt.text= "- "+String.format(Locale("en", "US"),"%.2f",bookingamt) +" SAR"
                                        tltfd.text= String.format(Locale("en", "US"),"%.2f",tt) +" SAR"
                                        val grandtotal:Double=tt-bookingamt
                                        finaltotal=grandtotal
                                        finedinetotal.text= String.format(Locale("en", "US"),"%.2f",grandtotal) +" SAR"
                                    }else{
                                        discountid= data[0].id!!
                                        val dis=data[0].percentage!!.toInt()

                                        diss=ordersubtotal * dis/100.0f
                                        //tmt=ordersubtotal-diss


                                        if(diss<uptoamount){
                                            //330<100
                                            Log.d("wertyhujkl",""+dis)
                                            // discountamt=diss

                                            // paytotal-=diss
                                            // Log.d("wertyhujkl",""+diss)
                                            //Log.d("wertyhujkl",""+discountamt)
                                            tmt=ordersubtotal-diss
                                            // discountpickup.text= diss.toString()+" SAR"
                                            discountfd.text= "- "+String.format(Locale("en", "US"),"%.2f",diss)+" SAR"
                                            finaldiscountamt=diss
                                            finedinesubtotal.text=String.format(Locale("en", "US"),"%.2f", tmt)+ " SAR"
                                            val vat = (tmt / 100.0f) * vattt

                                            finedinevat.text=String.format(Locale("en", "US"),"%.2f", vat)+ " SAR"

                                            val tt=tmt+vat

                                            tltfd.text= String.format(Locale("en", "US"),"%.2f",tt) +" SAR"
                                            finedinebookingamt.text= "- "+String.format(Locale("en", "US"),"%.2f",bookingamt) +" SAR"
                                            val grandtotal:Double=tt-bookingamt
                                            finaltotal=grandtotal

                                            finedinetotal.text= String.format(Locale("en", "US"),"%.2f",grandtotal) +" SAR"


                                        }else{
                                            discountfd.text= "- "+String.format(Locale("en", "US"),"%.2f",uptoamount)+" SAR"
                                            ordersubtotal-=uptoamount
                                            finaldiscountamt=uptoamount
                                            val vat = (ordersubtotal / 100.0f) * vattt
                                            finedinevat.text=String.format(Locale("en", "US"),"%.2f", vat)+ " SAR"

                                            finedinesubtotal.text=String.format(Locale("en", "US"),"%.2f", ordersubtotal)+ " SAR"
                                            val tt=ordersubtotal+vat
                                            tltfd.text= String.format(Locale("en", "US"),"%.2f",tt) +" SAR"

                                            finedinebookingamt.text= "- "+String.format(Locale("en", "US"),"%.2f",bookingamt) +" SAR"
                                            val grandtotal:Double=tt-bookingamt
                                            finaltotal=grandtotal

                                            finedinetotal.text= String.format(Locale("en", "US"),"%.2f",grandtotal) +" SAR"

                                        }

                                    }








                                }

                            }

                        }
                    }

                }
            })

    }


    override fun onClick(v: View?) {

        if (v==popup_cancel_order_close){
            linear_cancel_orderfinedine.visibility=View.GONE
        } else if (v==others){
            Log.d("sdfghjkl","zxcvbgnjm,k.")
            instructionfinedine.visibility=View.VISIBLE

            instructionfinedine.addTextChangedListener(object : TextWatcher {
                override fun beforeTextChanged(s: CharSequence, start: Int, count: Int, after: Int) {

                }

                override fun onTextChanged(s: CharSequence, start: Int, before: Int, count: Int) {
                }

                override fun afterTextChanged(s: Editable) {
                    descrip=instructionfinedine.text.toString()
                    Log.d("sdfghjkl",""+descrip)
                }
            })




        }
        else if (v==radio1){
            descrip="Delivery time is too long"
        }
        else if (v==radio2){
            descrip = "Duplicate order"


        }
        else if (v==radio3){
            descrip = "Change of mind/order"

        }else if (v==popup_cancel_orderfinedine) {

            linear_cancel_orderfinedine.visibility=View.GONE
            if (finalbookingtime>20){
                cancelbooking(paymenttype!!,orderid,50.0)

            }else if (finalbookingtime<20){
                if (finalbookingtime<0){

                    val builder = AlertDialog.Builder(context)
                    builder.setTitle(R.string.oops)
                    builder.setMessage(R.string.youbookingtimeisstertedcouldnotcancelthebooking)
                    builder.setCancelable(true)
                    builder.setPositiveButton(android.R.string.ok) { dialog, which ->

                        builder.setCancelable(true)
                    }
                    builder.show()


                    // UiUtils.showSnack(root_finedineorderstatus, getString(R.string.couldnotcancelthbooking))

                }else{
                    val builder = AlertDialog.Builder(context)
                    builder.setTitle(R.string.alert)
                    builder.setMessage(R.string.ifyoucancelyourbooking5percentwillbeduductedfromyourbookingamount)
                    builder.setCancelable(true)
                    builder.setPositiveButton(android.R.string.yes) { dialog, which ->

                        val deduct = 50.0*5/100.0f
                        val dedductamt=50.0-deduct

                        Log.d("wertyuiop[",""+deduct)
                        cancelbooking(paymenttype!!,orderid,dedductamt)

                    }

                    builder.setNegativeButton(android.R.string.no) { dialog, which ->
                        builder.setCancelable(true)
                    }
                    builder.show()
                }


            }else if (finalbookingtime<0){
                val builder = AlertDialog.Builder(context)
                builder.setTitle(R.string.oops)
                builder.setMessage(R.string.youbookingtimeisstertedcouldnotcancelthebooking)
                builder.setCancelable(true)
                builder.setPositiveButton(android.R.string.ok) { dialog, which ->

                    builder.setCancelable(true)


                }

                /* builder.setNegativeButton(android.R.string.no) { dialog, which ->
                     builder.setCancelable(true)
                 }*/
                builder.show()



                //   UiUtils.showSnack(root_finedineorderstatus, getString(R.string.couldnotcancelthbooking))
            }
        }
    }


}








