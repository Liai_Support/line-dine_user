package com.lia.yello.linedine.Models

import com.google.gson.annotations.SerializedName
import java.io.Serializable

class NotifyStatusResponse {


    @SerializedName("error")
    var error: Boolean? = null

    @SerializedName("Status updated")
    var message: String? = null


    @SerializedName("data")
    var data: Notify? = null
}

class Notify : Serializable {

    @SerializedName("notify_status")
    var notify_status: String? = null


}