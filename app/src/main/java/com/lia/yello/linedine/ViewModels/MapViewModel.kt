package com.lia.yello.linedine.ViewModels

import android.app.Application
import android.content.Context
import android.util.Log
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.LiveData

import com.lia.yello.linedine.Models.*
import com.lia.yello.linedine.Repository.MapRepository
import com.lia.yello.linedine.Session.SharedHelper
import com.lia.yello.linedine.Utils.Constants
import com.lia.yello.linedine.activity.HomeResponse
import com.lia.yello.linedine.network.ApiInput
import com.lia.yello.linedine.network.UrlHelper
import org.json.JSONArray
import org.json.JSONException
import org.json.JSONObject

class MapViewModel(application: Application) : AndroidViewModel(application) {

    var repository: MapRepository = MapRepository.getInstance()
    var applicationIns: Application = application
    var sharedHelper: SharedHelper? = SharedHelper(applicationIns.applicationContext)

    private fun getApiParams(
        context: Context,
        jsonObject: JSONObject?,
        methodName: String
    ): ApiInput {

        val header: MutableMap<String, String> = HashMap()
        sharedHelper?.let { header[Constants.ApiKeys.LANG] = it.language }
        sharedHelper?.let { header[Constants.ApiKeys.AUTHORIZATION] = it.token }
        //header[Constants.ApiKeys.ROLE] = Constants.ApiKeys.PATIENT


        val apiInputs = ApiInput()
        apiInputs.context = context
        apiInputs.jsonObject = jsonObject
        apiInputs.headers = header
        apiInputs.url = methodName

        Log.d("Headers:", "" + header)

        return apiInputs
    }





    fun getAddress(context: Context, input: ApiInput): LiveData<AutoCompleteResponse>? {

        return repository.getAddress(input)
    }




    private fun getApiParamsgate(
        context: Context,
        jsonObject: JSONObject?,
        methodName: String
    ): ApiInput {

        val header: MutableMap<String, String> = HashMap()
        // sharedHelper?.let { header[Constants.ApiKeys.LANG] = it.language }
        // header.put("Authorization", BuildConfig.PAYMENT_KEY);
        header["Authorization"] = UrlHelper.PAYMENT_KEY
        //  header["Content-Type"] = "application/json; charset=utf-8"
        //header[Constants.ApiKeys.ROLE] = Constants.ApiKeys.PATIENT


        val apiInputs = ApiInput()
        apiInputs.context = context
        apiInputs.jsonObject = jsonObject
        apiInputs.headers = header
        apiInputs.url = methodName
        Log.d("paymentgatewayHeaders:", "" + header)

        Log.d("returnstaement","apiInputs"+apiInputs)
        return apiInputs


    }


    fun gethomeDetails(context: Context): LiveData<HomeResponse>? {
        val jsonObject = JSONObject()
        sharedHelper?.let { jsonObject.put(Constants.ApiKeys.ID, it.id) }
        return repository.gethomedetails(getApiParams(context, jsonObject, UrlHelper.GETHOME))
    }

    fun getupdateaddressid(context: Context): LiveData<CommonResponse>? {
        val jsonObject = JSONObject()
        sharedHelper?.let { jsonObject.put("userid", it.id) }
        jsonObject.put("addressid", sharedHelper!!.selectedaddressid)

        return repository.getupdateaddressid(
            getApiParams(
                context,
                jsonObject,
                UrlHelper.UPDATEADDRESSID
            )
        )
    }


    // for list resturant
    fun getlistresturant(
        context: Context, id: Int, cuisine: ArrayList<Int>, iscuisine: String, all: String

    ): LiveData<ListResturantResponse>? {

        val jsonObject = JSONObject()

        jsonObject.put("category", id)
        jsonObject.put("cuisine", cuisine)
        jsonObject.put("iscuisine", iscuisine)
        jsonObject.put("lat", sharedHelper!!.currentLat)
        jsonObject.put("lng", sharedHelper!!.currentLng)
        jsonObject.put("filter", all)
        jsonObject.put("services", sharedHelper!!.choose)
        jsonObject.put("userid", sharedHelper!!.id)

        return repository.getlistresturant(
            getApiParams(
                context,
                jsonObject,
                UrlHelper.LISTRESTURANT
            )
        )
    }


    // for itemdetail

    fun getItemDetail(
        context: Context, id: Int,
        resid: Int

    ): LiveData<ItemDetailResponse>? {

        val jsonObject = JSONObject()

        jsonObject.put("itemid", id)
        jsonObject.put("resid", resid)
        return repository.getitemdetail(getApiParams(context, jsonObject, UrlHelper.ITEMDETAILS))
    }

    // menu detail
    fun getMenuDetail(
        context: Context, resid: Int,
        category: String

    ): LiveData<MenuDetailResponse>? {

        val jsonObject = JSONObject()

        jsonObject.put("resid", resid)
        jsonObject.put("category", category)
        jsonObject.put("userid", sharedHelper?.id)

        return repository.getmenudetail(getApiParams(context, jsonObject, UrlHelper.MENUDETAILS))
    }


    fun getAddCart(
        context: Context, menuid: Int, quantity: Int, cost: Double, resid: Int,specialsins: String

    ): LiveData<AddCartResponse>? {

        val jsonObject = JSONObject()

        jsonObject.put("resid", resid)
        jsonObject.put("menuid", menuid)
        jsonObject.put("quantity", quantity)
        jsonObject.put("cost", cost)
        jsonObject.put("userid", sharedHelper?.id)
        jsonObject.put("special_instruction", specialsins)

        return repository.getaddcart(getApiParams(context, jsonObject, UrlHelper.ADDCART))
    }


    fun getfavorites(
        context: Context


    ): LiveData<FavoriteResponse>? {

        val jsonObject = JSONObject()

        jsonObject.put("userid", sharedHelper?.id)
        return repository.getfavdetail(getApiParams(context, jsonObject, UrlHelper.Fav))
    }


    fun getorderlist(
        context: Context
    ): LiveData<OrderListResponse>? {

        val jsonObject = JSONObject()

        jsonObject.put("userid", sharedHelper?.id)

        return repository.getorderlists(getApiParams(context, jsonObject, UrlHelper.ORDERLIST))
    }

    fun getdelete(
        context: Context,
        id: Int
    ): LiveData<DeleteOrderResponse>? {

        val jsonObject = JSONObject()
        jsonObject.put("id", id)

        return repository.getdeleteorder(getApiParams(context, jsonObject, UrlHelper.DELETEORDER))
    }


    fun getlistadress(
        context: Context
    ): LiveData<ListAdressResponse>? {

        val jsonObject = JSONObject()
        jsonObject.put("userid", sharedHelper?.id)

        return repository.getlistaddress(getApiParams(context, jsonObject, UrlHelper.LISTADDRESS))
    }

    fun getdeleteaddress(
        context: Context,
        addressid: Int
    ): LiveData<DeleteAddressResponse>? {

        val jsonObject = JSONObject()
        jsonObject.put("addressid", addressid)

        return repository.getdeleteadress(
            getApiParams(
                context,
                jsonObject,
                UrlHelper.DELETEADDRESS
            )
        )
    }

    fun getaddderss(
        context: Context,
        address: String,
        lat: Double,
        lng: Double,
        type: String,
        city: String
    ): LiveData<AddAdressResponse>? {

        val jsonObject = JSONObject()
        jsonObject.put("userid", sharedHelper?.id)
        jsonObject.put("address", address)
        jsonObject.put("lat", lat)
        jsonObject.put("lng", lng)
        jsonObject.put("type", type)
        // jsonObject.put("city",city)

        return repository.getaddadress(getApiParams(context, jsonObject, UrlHelper.ADDADDRESS))
    }


    fun getupdateadress(
        context: Context,
        addressid: Int,
        address: String,
        lat: Double,
        lng: Double,
        type: String,
        city: String
    ): LiveData<UpdateAdressResponse>? {

        val jsonObject = JSONObject()
        jsonObject.put("userid", sharedHelper?.id)
        jsonObject.put("addressid", addressid)
        jsonObject.put("address", address)
        jsonObject.put("lat", lat)
        jsonObject.put("lng", lng)
        jsonObject.put("type", type)
        // jsonObject.put("city",city)

        return repository.getupdateaddress(
            getApiParams(
                context,
                jsonObject,
                UrlHelper.UPDATEADDRESS
            )
        )
    }


    fun getaddwallet(
        context: Context,
        transactionid: String,
        amount: Double

    ): LiveData<AddWalletResponse>? {

        val jsonObject = JSONObject()
        jsonObject.put("userid", sharedHelper?.id)
        jsonObject.put("transactionid", transactionid)
        jsonObject.put("amount", amount)

        return repository.getaddwallet(getApiParams(context, jsonObject, UrlHelper.ADDWALLET))
    }


    fun getwallethistory(
        context: Context
    ): LiveData<WalletHistory>? {

        val jsonObject = JSONObject()
        jsonObject.put("userid", sharedHelper?.id)

        return repository.getwallethistry(getApiParams(context, jsonObject, UrlHelper.WALLETHIS))
    }

    fun getwalletamount(
        context: Context
    ): LiveData<GetWalletResponse>? {

        val jsonObject = JSONObject()
        jsonObject.put("userid", sharedHelper?.id)

        return repository.getwalletamount(
            getApiParams(
                context,
                jsonObject,
                UrlHelper.GETWALLETAMOUNT
            )
        )
    }


    fun addfav(
        context: Context, itemid: Int,
        resid: Int,
        isfavourite: String


    ): LiveData<AddFavResponse>? {

        val jsonObject = JSONObject()

        jsonObject.put("itemid", itemid)
        jsonObject.put("resid", resid)
        jsonObject.put("isfavourite", isfavourite)
        jsonObject.put("userid", sharedHelper?.id)

        return repository.getaddfav(getApiParams(context, jsonObject, UrlHelper.ADDFAV))
    }

    fun listnotification(
        context: Context


    ): LiveData<ListNotificationResponse>? {

        val jsonObject = JSONObject()

        jsonObject.put("userid", sharedHelper?.id)

        return repository.getlistnoti(getApiParams(context, jsonObject, UrlHelper.LISTNOTI))
    }

    fun clearnoti(
        context: Context
    ): LiveData<DeleteAddressResponse>? {

        val jsonObject = JSONObject()

        jsonObject.put("userid", sharedHelper?.id)

        return repository.clearnotification(getApiParams(context, jsonObject, UrlHelper.DELETENOTI))
    }


    fun getupdatecart(
        context: Context,
        id: Int,
        quantity: Int,
        specialins: String
    ): LiveData<UpdateCartResponse>? {

        val jsonObject = JSONObject()
        jsonObject.put("id", id)
        jsonObject.put("quantity", quantity)
        jsonObject.put("special_instruction", specialins)

        return repository.getupdatecarrt(getApiParams(context, jsonObject, UrlHelper.UPDATECART))
    }


    fun getplaceorder(
        context: Context,
        paymenttype: String,
        subtotal: Double,
        total: Double,
        vat: Double,
        trans_id: String,
        paymentorder_id: String,
        splins:String,
        discountid:Int,
        discountamt:Double
    ): LiveData<PlaceOrderResponse>? {

        val jsonObject = JSONObject()
        jsonObject.put("userid", sharedHelper?.id)
        jsonObject.put("subtotal", subtotal)
        jsonObject.put("total", total)
        jsonObject.put("vat", vat)
        jsonObject.put("splins", splins)
        jsonObject.put("paymenttype", paymenttype)
        jsonObject.put("trans_id", trans_id)
        jsonObject.put("paymentorder_id", paymentorder_id)
        jsonObject.put("discount_id", discountid)
        jsonObject.put("discount_amount", discountamt)

        return repository.getplaceorder(getApiParams(context, jsonObject, UrlHelper.PLACEORDER))
    }


    fun getplaceorderlist(
        context: Context,
        status: String

    ): LiveData<MyOrderResponse>? {

        val jsonObject = JSONObject()
        jsonObject.put("userid", sharedHelper?.id)
        jsonObject.put("status", status)

        return repository.getorderlist(
            getApiParams(
                context,
                jsonObject,
                UrlHelper.PLACEORDERORDERLIST
            )
        )
    }


    fun getcancelorder(
        context: Context,
        orderid: Int,
        descrip: String,
        paymenttype: String,
        refundamt: Double

    ): LiveData<CancelOrderResponse>? {

        val jsonObject = JSONObject()

        jsonObject.put("orderid", orderid)
        jsonObject.put("cancel_desc", descrip)
        jsonObject.put("paymenttype", paymenttype)
        jsonObject.put("refundamt", refundamt)

        return repository.getcancelorder(getApiParams(context, jsonObject, UrlHelper.CANCELORDER))
    }

    fun getreorder(
        context: Context,
        orderid: Int,
        paymenttype: String,
        paymentorder_id: String,

        trans_id: String

    ): LiveData<PlaceOrderResponse>? {

        val jsonObject = JSONObject()

        jsonObject.put("orderid", orderid)
        jsonObject.put("paymenttype", paymenttype)
        jsonObject.put("paymentorder_id", paymentorder_id)
        jsonObject.put("trans_id", trans_id)

        return repository.getplaceorder(getApiParams(context, jsonObject, UrlHelper.REORDER))
    }


    fun getreview(
        context: Context,
        orderid: Int,
        menurating: Float,
        resrating: Float,
        menuid: Int

    ): LiveData<CancelOrderResponse>? {

        val jsonObject = JSONObject()

        jsonObject.put("orderid", orderid)
        jsonObject.put("res_rate", resrating)
        jsonObject.put("menu_rate", menurating)
        jsonObject.put("menuid", menuid)

        return repository.getreorder(getApiParams(context, jsonObject, UrlHelper.REVIEW))
    }


    fun getdeleteall(
        context: Context

    ): LiveData<CancelOrderResponse>? {

        val jsonObject = JSONObject()

        jsonObject.put("userid", sharedHelper?.id)

        return repository.getdeleteall(getApiParams(context, jsonObject, UrlHelper.DELETECART))
    }


    fun getdayslot(
        context: Context, resid: Int


    ): LiveData<DaySlotResponse>? {

        val jsonObject = JSONObject()

        jsonObject.put("resid", resid)

        return repository.getdayslot(getApiParams(context, jsonObject, UrlHelper.DAYSLOT))
    }

    fun gettimeslot(
        context: Context, resid: Int,
        category: String,
        day: String


    ): LiveData<TimeSlotResponse>? {

        val jsonObject = JSONObject()

        jsonObject.put("resid", resid)
        jsonObject.put("day", day)
        jsonObject.put("category", category)

        return repository.gettimeslot(getApiParams(context, jsonObject, UrlHelper.TIMESLOT))
    }


    fun getreservetable(
        context: Context, resid: Int,
        slot: String,
        date: String,
        seatcount: Int,
        tabletype: String


    ): LiveData<ReserveTableModel>? {

        val jsonObject = JSONObject()

        jsonObject.put("date", date)
        jsonObject.put("slot", slot)
        jsonObject.put("resid", resid)
        jsonObject.put("seatcount", seatcount)
        jsonObject.put("tabletype", tabletype)
        jsonObject.put("userid", sharedHelper?.id)

        return repository.getreservetable(getApiParams(context, jsonObject, UrlHelper.RESERVETABLE))
    }


    fun cancelbooking(
        context: Context,
        id: Int,
        paymenttype: String,
        refundamt: Double,
        canceldescrip:String


    ): LiveData<CancelBookingResponse>? {

        val jsonObject = JSONObject()

        jsonObject.put("bookingid", id)
        jsonObject.put("cancel_desc", id)
        jsonObject.put("paymenttype", paymenttype)
        jsonObject.put("refundamt", refundamt)
        jsonObject.put("cancelbooking", canceldescrip)

        return repository.cancelbooking(getApiParams(context, jsonObject, UrlHelper.CANCELBOOKING))
    }


    fun getbookinglist(
        context: Context, status: String

    ): LiveData<BookingListResponse>? {

        val jsonObject = JSONObject()


        jsonObject.put("userid", sharedHelper?.id)
        jsonObject.put("status", status)

        return repository.bookinglist(getApiParams(context, jsonObject, UrlHelper.BOOKINGLIST))
    }


    fun gettablebookingmenu(
        context: Context,
        bookingid: Int,
        jsonArray: String

    ): LiveData<TableBookingMenuResponse>? {

        val jsonObject = JSONObject()

        jsonObject.put("bookingid", bookingid)
        jsonObject.put("menu", jsonArray)

        return repository.tablebookingmenu(
            getApiParams(
                context,
                jsonObject,
                UrlHelper.TABLEBOOKINGMENU
            )
        )
    }



    fun getqueuecount(
        context: Context,
        bookingid: Int,
        resid: Int,
        inqueue: String,
        date: String,
        service:String


    ): LiveData<GetQueueCountResponse>? {

        val jsonObject = JSONObject()

        jsonObject.put("bookingid", bookingid)
        jsonObject.put("resid", resid)
        jsonObject.put("inqueue", inqueue)
        jsonObject.put("date", date)
        jsonObject.put("service", service)
        jsonObject.put("userid", sharedHelper!!.id)

        return repository.queuecount(getApiParams(context, jsonObject, UrlHelper.GETQUEUECOUNT))
    }


    fun addinqueue(
        context: Context,
        seatcount: Int,
        resid: Int,
        queuecount: Int,
        tabletype: String,
        date: String,
        slot: String


    ): LiveData<AddInQueueResponse>? {

        val jsonObject = JSONObject()

        jsonObject.put("date", date)
        jsonObject.put("tabletype", tabletype)
        jsonObject.put("resid", resid)
        jsonObject.put("seatcount", seatcount)
        jsonObject.put("queuecount", queuecount)
        jsonObject.put("slot", slot)
        jsonObject.put("userid", sharedHelper?.id)
        return repository.addinqueue(getApiParams(context, jsonObject, UrlHelper.ADDINQUEUE))
    }


    fun getfaqq(
        context: Context

    ): LiveData<FaqModelResponse>? {


        return repository.getfaq(getApiParams(context, null, UrlHelper.FAQ))
    }



    fun bookingpayment(
        context: Context,
        bookingid: Int,
        totalamount: Double,
        status: String,
        paymenttype: String,
        paymenttransid: String,
        transactionid: String,
        discount_id:Int,
        discount_amount:Double

    ): LiveData<BookingPaymentResponse>? {

        val jsonObject = JSONObject()

        jsonObject.put("bookingid", bookingid)
        jsonObject.put("totalamount", totalamount)
        jsonObject.put("status", status)
        jsonObject.put("paymenttype", paymenttype)
        jsonObject.put("userid", sharedHelper?.id)
        jsonObject.put("trans_id", transactionid)
        jsonObject.put("paymentorder_id", paymenttransid)
        jsonObject.put("discount_id", discount_id)
        jsonObject.put("discount_amount", discount_amount)


        return repository.bookingpayment(getApiParams(context, jsonObject, UrlHelper.BOOKINGPAYMENT))
    }



    fun getcomfirmbooking(
        context: Context,
        bookingid: Int,
        amount: Double,
        status: String,
        paymenttype: String,
        paymenttransid: String,
        transactionid: String

    ): LiveData<ComfirmBookingResponse>? {

        val jsonObject = JSONObject()

        jsonObject.put("bookingid", bookingid)
        jsonObject.put("amount", amount)
        jsonObject.put("status", status)
        jsonObject.put("paymenttype", paymenttype)
        jsonObject.put("trans_id", transactionid)
        jsonObject.put("paymentorder_id", paymenttransid)
        jsonObject.put("userid", sharedHelper?.id)

        return repository.comfirmbooking(getApiParams(context, jsonObject, UrlHelper.CONFIRMBOOKING))
    }







    fun payment(context: Context, amount: Double): LiveData<PaymentGatewayResponse>?  {

        var jsonObject = JSONObject()

        jsonObject.put("apiOperation", "INITIATE")

        var jsonObject1 = JSONObject()
        jsonObject1.put("reference", "Ref#1234")
        jsonObject1.put("amount", amount)
        jsonObject1.put("currency", "SAR")
        jsonObject1.put("name", "Sample order name")
        jsonObject1.put("channel", "Mobile")
        jsonObject1.put("category", "PAY")

        jsonObject.put("order", jsonObject1)

        var jsonObject2 = JSONObject()
        jsonObject2.put("tokenizeCc", "true")
        jsonObject2.put("returnUrl", "https://google.com/")
        jsonObject2.put("locale",  sharedHelper?.language)

        jsonObject.put("configuration", jsonObject2)
        Log.d("returnstaement","2")

        return repository.payment(
            getApiParamsgate(
                context,
                jsonObject,
                UrlHelper.PAYMENT_URL
            )
        )

    }

    fun payment1(context: Context, orderid: String): LiveData<PaymentGatewayResponse>?  {
        Log.d("returnstaement","1")

        return repository.payment1(
            getApiParamsgate(
                context,
                null,
                UrlHelper.PAYMENT_URL+"/"+orderid
            )
        )

    }

    fun payment2(context: Context, orderid: String): LiveData<PaymentGatewayResponse1>?  {

        val jsonObject = JSONObject()
        val order = JSONObject()
        try {
            jsonObject.put("apiOperation", "SALE")
            order.put("id", orderid)
            jsonObject.put("order", order)
        } catch (e: JSONException) {
            e.printStackTrace()
        }
        Log.d("returnstaement","3")


        return repository.payment2(
            getApiParamsgate(
                context,
                jsonObject,
                UrlHelper.PAYMENT_URL
            )
        )

    }




    fun notifystatus(
        context: Context,
        status: String): LiveData<NotifyStatusResponse>? {

        val jsonObject = JSONObject()

        sharedHelper?.let { jsonObject.put("userid", it.id) }
        jsonObject.put("status", status)

        return repository.notifystatus(getApiParams(context, jsonObject, UrlHelper.NOTIFY_STATUS))
    }

    fun getcheckpendingstatus(
        context: Context

    ): LiveData<CheckPendingOrders>? {

        val jsonObject = JSONObject()
        sharedHelper?.let { jsonObject.put("userid", it.id) }

        return repository.checkpengingg(
            getApiParams(
                context,
                jsonObject,
                UrlHelper.CHECK_PENDING
            )
        )
    }




    fun getvalidateoffer(
        context: Context,
        resid:Int,
        promocode:String

    ): LiveData<ValidateOfferResponse>? {

        val jsonObject = JSONObject()
        sharedHelper?.let { jsonObject.put("userid", it.id) }
        sharedHelper?.let { jsonObject.put("offerfor", it.choose) }
        jsonObject.put("resid", resid)
        jsonObject.put("promocode", promocode)


        return repository.validateoffer(
            getApiParams(
                context,
                jsonObject,
                UrlHelper.VALIDATEOFFER
            )
        )
    }




    fun getpointResponsee(
        context: Context

    ): LiveData<GetPointResponse>? {

        val jsonObject = JSONObject()
        sharedHelper?.let { jsonObject.put("userid", it.id) }

        return repository.getpointResponse(
            getApiParams(
                context,
                jsonObject,
                UrlHelper.GetPointResponse
            )
        )
    }

    fun getpointhistory(
        context: Context

    ): LiveData<PointsHistory>? {

        val jsonObject = JSONObject()
        sharedHelper?.let { jsonObject.put("userid", it.id) }

        return repository.getpointHistoryResponse(
            getApiParams(
                context,
                jsonObject,
                UrlHelper.PointsHistory
            )
        )
    }



    fun getListoffers(
        context: Context,
        resid:Int

    ): LiveData<ListOfferResponse>? {

        val jsonObject = JSONObject()
        sharedHelper?.let { jsonObject.put("offerfor", it.choose) }
        jsonObject.put("resid", resid)
        return repository.getListOfferResponse(
            getApiParams(
                context,
                jsonObject,
                UrlHelper.LISTOFFERS
            )
        )
    }


    fun refundpayment(context: Context, amount: Double,orderid: String): LiveData<PaymentRefundResponse>?  {

        var jsonObject = JSONObject()
        jsonObject.put("apiOperation", "REFUND")

        var jsonObject1 = JSONObject()
        jsonObject1.put("Id", orderid)

        jsonObject.put("order", jsonObject1)

        var jsonObject2 = JSONObject()
        jsonObject2.put("amount", amount.toString())
        jsonObject2.put("currency", "SAR")

        jsonObject.put("transaction", jsonObject2)
        return repository.refundpayment(
            getApiParamsgate(
                context,
                jsonObject,
                UrlHelper.PAYMENT_URL
            )
        )

    }




    fun getaddresturantqueue(
        context: Context,
        queuecount: Int,
        resid: Int,
        seatcount: Int


    ): LiveData<AddRestuarantInqueue>? {

        val jsonObject = JSONObject()

        jsonObject.put("resid", resid)
        jsonObject.put("seatcount", seatcount)
        jsonObject.put("queuecount",queuecount )
        jsonObject.put("userid", sharedHelper!!.id)

        return repository.addrestuarantInqueue(getApiParams(context, jsonObject, UrlHelper.ADDRESTUARANTQUEUE))
    }



    fun getcheckFavRest(
        context: Context,
        resid: Int


    ): LiveData<CheckFavrouiteRestuarant>? {

        val jsonObject = JSONObject()

        jsonObject.put("resid", resid)
        jsonObject.put("lat", sharedHelper!!.currentLat)
        jsonObject.put("lng",sharedHelper!!.currentLng )

        return repository.checkfavrestuarannt(getApiParams(context, jsonObject, UrlHelper.CHECKFAVRESTUARNT))
    }



}

