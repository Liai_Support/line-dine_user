package com.lia.yello.linedine.Models

import com.google.gson.annotations.SerializedName
import java.io.Serializable

class GetQueueCountResponse: Serializable {

    @SerializedName("error")
    var error: Boolean? = null

    @SerializedName("message")
    var message: String? = null

    @SerializedName("data")
    var data: Count? = null

}

class Count : Serializable {

    @SerializedName("totcount")
    var totcount: Int? = null


    @SerializedName("user_queuecount")
    var user_queuecount: Int? = null

}