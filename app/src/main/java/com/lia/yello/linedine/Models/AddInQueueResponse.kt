package com.lia.yello.linedine.Models

import com.google.gson.annotations.SerializedName
import java.io.Serializable

class AddInQueueResponse: Serializable {

    @SerializedName("error")
    var error: Boolean? = null

    @SerializedName("message")
    var message: String? = null

    @SerializedName("data")
    var data: QueueCount? = null

}

class QueueCount : Serializable {

    @SerializedName("Queue count")
    var queuecount: Int? = null

    @SerializedName("bookingid")
    var bookingid: Int? = null

}
