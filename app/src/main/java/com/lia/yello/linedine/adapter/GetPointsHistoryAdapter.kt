package com.lia.yello.linedine.adapter

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.content.ContextCompat
import androidx.recyclerview.widget.RecyclerView
import com.lia.yello.linedine.Models.Walletata
import com.lia.yello.linedine.Models.walletdata
import com.lia.yello.linedine.R
import com.lia.yello.linedine.databinding.CardWalletBinding
import com.lia.yello.linedine.fragment.WalletFragment
import java.util.*
import kotlin.collections.ArrayList

class GetPointsHistoryAdapter (
    var walletFragment: WalletFragment,
    var context: Context,
    var list: ArrayList<Walletata>?
) :
    RecyclerView.Adapter<GetPointsHistoryAdapter.HomeHeaderViewHolder>() {
    var row_index: Int = -1
    lateinit var txt: String


    inner class HomeHeaderViewHolder(view: View) : RecyclerView.ViewHolder(view) {
        var binding: CardWalletBinding = CardWalletBinding.bind(view)

    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): HomeHeaderViewHolder {
        return HomeHeaderViewHolder(
            LayoutInflater.from(context).inflate(
                R.layout.card_wallet,
                parent,
                false
            )
        )
    }

    override fun getItemCount(): Int {
        return list!!.size
    }

    override fun onBindViewHolder(holder: HomeHeaderViewHolder, position: Int) {


        holder.binding.amount.text ="+" + String.format(Locale("en", "US"),"%.2f",list!![position].point!!.toDouble())

        holder.binding.amount.setTextColor(
            ContextCompat.getColor(
                context,
                R.color.green
            )
        )


        if (list!![position].order_id.equals("0")){
            holder.binding.trancid.text =
                list!![position].offered_by

        }else{
            holder.binding.trancid.text =
                list!![position].offered_by +" - " + context.getString(R.string.transactionid) + ":" + list!![position].order_id

        }


        holder.binding.date.text = list!![position].date.toString()


    }


}

