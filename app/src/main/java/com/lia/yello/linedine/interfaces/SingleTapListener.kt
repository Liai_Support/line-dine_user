package com.lia.yello.linedine.interfaces

interface SingleTapListener {
    fun singleTap()
}