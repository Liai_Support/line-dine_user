package com.lia.yello.linedine.adapter

import android.app.AlertDialog
import android.content.Context
import android.os.Bundle
import android.util.Log
import android.view.*
import androidx.fragment.app.FragmentTransaction
import androidx.recyclerview.widget.RecyclerView
import com.lia.yello.linedine.Models.menudetails
import com.lia.yello.linedine.R
import com.lia.yello.linedine.Utils.DialogUtils
import com.lia.yello.linedine.Utils.UiUtils
import com.lia.yello.linedine.ViewModels.MapViewModel
import com.lia.yello.linedine.databinding.CardSubsubhome2Binding
import com.lia.yello.linedine.fragment.SubsubHomeFragment
import com.lia.yello.linedine.fragment.SubsubsubHomeFragment
import java.util.*
import kotlin.collections.ArrayList

class SubsubHomeAdapter(
    var subsubHomeFragment: SubsubHomeFragment,
    var context: Context,
    var list: ArrayList<menudetails>?,
    var iscolsed: Boolean,
    var menuquatity: MutableList<Int>?,
    var menuids: MutableList<Int>
) :
    RecyclerView.Adapter<SubsubHomeAdapter.HomeHeaderViewHolder>() {
    var id:Int=0
    var resid:Int=0
    var menuid:Int=0
    var isfav:String=""
    private var mapViewModel: MapViewModel? = null
    private lateinit var fragmentTransaction: FragmentTransaction
    private var subsubsubHomeFragment: SubsubsubHomeFragment? = null
    var menuidss: MutableList<Int> = mutableListOf<Int>()


    inner class HomeHeaderViewHolder(view: View) : RecyclerView.ViewHolder(view) {

        var binding: CardSubsubhome2Binding = CardSubsubhome2Binding.bind(view)



    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): HomeHeaderViewHolder {

        return HomeHeaderViewHolder(
            LayoutInflater.from(context).inflate(
                R.layout.card_subsubhome2,
                parent,
                false
            )
        )
    }

    override fun getItemCount(): Int {

        return list!!.size
    }

    override fun onBindViewHolder(holder: HomeHeaderViewHolder, position: Int) {

        for (i in 0 until menuids.size){

            for (i in 0 until menuquatity!!.size){
                Log.d("i value",""+i+1)
                Log.d("arraysize",""+menuquatity!!.size)
                Log.d("arrayvalue",""+menuquatity!!)
                Log.d("new,","check: "+menuids[i]+" == "+list!![position].menu_id)
                if (menuids[i]==list!![position].menu_id){
                    if (menuquatity!![i]==0){

                    }else{
                        holder.binding.menuquantitytxt.visibility=View.VISIBLE
                        holder.binding.menuquantitytxt.text= menuquatity!![i].toString()
                        // Log.d("sdfghnm,",""+menuquatity!![i])
                        //Log.d("rtyhbnmko;",""+menuids[i])
                        Log.d("awertgbhju;",""+list!![position].menu_id)
                    }


                }
            }
        }


        holder.binding.title.text = list!![position].item

        holder.binding.ratingitemdet.text= list!![position].menurating.toString()+"/"+"5"

        resid = list!![position].res_id!!
        //  Log.d("residdssd",""+resid)
        holder.binding.price.text= String.format(Locale("en", "US"),"%.2f",  list!![position].price)+ " SAR"
/*
        holder.binding.price.text= list!![position].price.toString()+" SAR"
*/
        list!![position].images?.let { imageUrl -> UiUtils.loadImage(holder.binding.itemimg, imageUrl)
        }
        // to go to subsubsubfragment
        holder.binding.itemimg.setOnClickListener(View.OnClickListener {

            val bundle = Bundle()
            bundle.putInt("id", list!![position].menu_id!!)
            bundle.putInt("resid", list!![position].res_id!!)
            bundle.putDouble("menurating", list!![position].menurating!!)
            bundle.putString("promocode", subsubHomeFragment.promocode)

            subsubHomeFragment.imgid?.let { it1 -> bundle.putInt("imgid", it1) }
            bundle.putString("type", subsubHomeFragment.typee)
            //      bundle.putBoolean("fav",subsubHomeFragment.fav)
            Log.d("menuid", "" + list!![position].menu_id!!)
            Log.d("menuid", "" + list!![position].menu_id!!)
            Log.d("asdfghjkl", "" + subsubHomeFragment.fav)
            subsubsubHomeFragment = SubsubsubHomeFragment()
            subsubsubHomeFragment!!.arguments = bundle
            fragmentTransaction = subsubHomeFragment.requireActivity().supportFragmentManager.beginTransaction()
            fragmentTransaction.setCustomAnimations(R.anim.screen_in, R.anim.screen_out)
            fragmentTransaction.replace(R.id.container, subsubsubHomeFragment!!)
            fragmentTransaction.addToBackStack(null)
            fragmentTransaction.commit()
        })
        // for adding into fav
        holder.binding.fav.setOnClickListener(View.OnClickListener {
            holder.binding.fav2.setVisibility(View.VISIBLE)
            holder.binding.fav.setVisibility(View.GONE)
            isfav = "true"
            menuid = list!![position].menu_id!!
            resid = list!![position].res_id!!
            getvalues()
            Log.d("tdtvcyudbciiujjw", "gdvgtqvduyqwidsd")

        })
        if(list!![position].favourite.equals("true")){
            holder.binding.fav2.setVisibility(View.VISIBLE)
            holder.binding.fav.setVisibility(View.GONE)
        }
        // to remove from fav
        holder.binding.fav2.setOnClickListener(View.OnClickListener {
            holder.binding.fav.setVisibility(View.VISIBLE)
            holder.binding.fav2.setVisibility(View.GONE)
            isfav = "false"
            menuid = list!![position].menu_id!!
            resid = list!![position].res_id!!
            getvalues()


        })
        // to go to subsubsubfragment
        holder.binding.addcart.setOnClickListener(View.OnClickListener {

            if(subsubHomeFragment.cartlist.isNullOrEmpty()){
                subsubHomeFragment.menuid2 = list!![position].menu_id!!
                subsubHomeFragment.resid2 = list!![position].res_id!!
                subsubHomeFragment.cost2 = list!![position].price!!
                subsubHomeFragment.getaddcart()


            }
            else{
                var hd:Boolean = false
                for (i in 0 until subsubHomeFragment.cartlist!!.size){
                    //   holder.binding.menuquantity.visibility=View.VISIBLE
                    // holder.binding.menuquantity.text= "1"
                    Log.d("dbvhfd",""+list!![position].res_id+"==="+ subsubHomeFragment.cartlist!![i].res_id)
                    if (list!![position].res_id != subsubHomeFragment.cartlist!![i].res_id){
                        hd = true
                        break
                    }
                    else{
                        hd = false
                    }
                }

                if(hd == true){
                    val builder = AlertDialog.Builder(context)
                    builder.setTitle(R.string.alert)
                    builder.setMessage(R.string.youareaddingfromdiffresturant)
                    builder.setPositiveButton(android.R.string.yes) { dialog, which ->
                        subsubHomeFragment.menuid2 = list!![position].menu_id!!
                        subsubHomeFragment.resid2 = list!![position].res_id!!
                        subsubHomeFragment.cost2 = list!![position].price!!
                        subsubHomeFragment.getdeleteall()
                    }

                    builder.setNegativeButton(android.R.string.no) { dialog, which ->
                        builder.setCancelable(true)
                    }
                    builder.show()
                }
                else{
                    subsubHomeFragment.menuid2 = list!![position].menu_id!!
                    subsubHomeFragment.resid2 = list!![position].res_id!!
                    subsubHomeFragment.cost2 = list!![position].price!!
                    subsubHomeFragment.getaddcart()
                }
            }





            /*   subsubsubHomeFragment = SubsubsubHomeFragment()
               fragmentTransaction = subsubHomeFragment.requireActivity().supportFragmentManager.beginTransaction()
               fragmentTransaction.setCustomAnimations(R.anim.screen_in, R.anim.screen_out)
               fragmentTransaction.replace(R.id.container, subsubsubHomeFragment!!)
               fragmentTransaction.addToBackStack(null)
               fragmentTransaction.commit()*/
        })

        if(iscolsed == true){
            holder.binding.widgetIcon.alpha = 0.5F
            holder.binding.itemimg.isEnabled = false
            holder.binding.fav.isEnabled = false
            holder.binding.fav2.isEnabled = false
            holder.binding.addcart.isEnabled = false
        }

    }

    private fun getvalues() {
        Log.d("tdtvcyudbciiujjw", "gdvgtqvduyqwidsd")
        DialogUtils.showLoader(context)

        subsubHomeFragment.mapViewModel?.addfav(context, menuid, resid, isfav)
            ?.observe(subsubHomeFragment, androidx.lifecycle.Observer {
                DialogUtils.dismissLoader()

                it?.let {
                    it.error?.let { error ->
                        if (error) {
                            it.message?.let { msg ->
                                Log.d("response", msg)
                                Log.d("kanika", "kanika")

                            }
                        } else {

                            it.message?.let { msg ->
                                // Toast.makeText(context, msg, Toast.LENGTH_SHORT).show()

                                Log.d("response", msg)
                                Log.d("tdtvcyudbciiujjw", "gdvgtqvduyqwidsd")

                            }
                        }
                    }

                }
            })
    }

}
