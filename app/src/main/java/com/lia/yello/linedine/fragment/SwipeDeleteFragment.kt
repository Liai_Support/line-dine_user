package com.lia.yello.linedine.fragment
import android.graphics.Canvas
import android.os.Bundle
import android.util.Log
import android.view.View
import android.widget.TextView
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentTransaction
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.ItemTouchHelper
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import androidx.recyclerview.widget.RecyclerView.ItemDecoration
import com.google.android.material.bottomsheet.BottomSheetBehavior
import com.lia.yello.linedine.Models.OrderListData
import com.lia.yello.linedine.Models.orderlist
import com.lia.yello.linedine.R
import com.lia.yello.linedine.Session.SharedHelper
import com.lia.yello.linedine.Utils.DialogUtils
import com.lia.yello.linedine.Utils.UiUtils
import com.lia.yello.linedine.ViewModels.MapViewModel
import com.lia.yello.linedine.activity.DashBoardActivity
import com.lia.yello.linedine.adapter.CartAdapter
import com.lia.yello.linedine.adapter.CartAdapter1
import com.lia.yello.linedine.databinding.FragmentSubordersBinding
import kotlinx.android.synthetic.main.carticon.*
import kotlinx.android.synthetic.main.fragment_suborders.*
import kotlinx.android.synthetic.main.header.*
import java.util.*
class SwipeDeleteFragment() : Fragment(R.layout.fragment_swipe_delete) {
    var binding: FragmentSubordersBinding? = null
    var mapViewModel: MapViewModel? = null
    private lateinit var fragmentTransaction: FragmentTransaction
    private var orderSucessFragment: OrderSucessFragment? = null
    private var cartFragment: CartFragment? = CartFragment()
    private var homeFragment: HomeFragment? = null
    var subtotal: Double = 0.0
    var totalamt: Double = 0.0
    var vat: Double = 0.0
    public var sharedHelper: SharedHelper? = null
    var placeorderboolean: Boolean = false
    var orderid: Int = 0
    var swipeController: SwipeController? = null
    var menuavailable: Int = 0
    var stocknotavailable:Boolean=false
    lateinit var orderdata :ArrayList<OrderListData>
    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        binding = DataBindingUtil.findBinding(view)
        sharedHelper = SharedHelper(requireContext())
        mapViewModel = ViewModelProvider(this).get(MapViewModel::class.java)
        val head = view.findViewById<TextView>(R.id.head)
        head.setText(getString(R.string.mycart))
        (activity as DashBoardActivity?)!!.mBottomSheetBehavior2?.setState(BottomSheetBehavior.STATE_HIDDEN)
        (activity as DashBoardActivity?)!!.mBottomSheetBehavior?.setState(BottomSheetBehavior.STATE_HIDDEN)
        back.setOnClickListener(View.OnClickListener {
            requireActivity().supportFragmentManager.popBackStack()
        })
        getcartlist()
    }
    public fun getcartlist(){
        DialogUtils.showLoader(requireContext())
        mapViewModel?.getorderlist(requireContext())
            ?.observe(viewLifecycleOwner,
                Observer {
                    DialogUtils.dismissLoader()
                    it?.let {
                        it.error?.let { error ->
                            Log.d("hgdsh", "" + error)
                            if (error) {
                                it.message?.let { msg ->
                                    UiUtils.showSnack(root_suborder, msg)
                                    //snackbarToast(getString(R.string.cartisempty))
                                    cart_badge.text = "0"
                                    relative_suborder.visibility = View.GONE
                                    //   txtvisiblity.visibility = View.VISIBLE
                                }
                            } else {
                                it.data?.let { data ->
                                    cart_badge.text = "" + data.orderdata?.size
                                    Log.d("hhdgs", "" + data.orderdata?.size)
                                    var list: ArrayList<orderlist>? = data.orderdata
                                    for (i in 0 until list!!.size) {
                                        if (list[i].menuavilable == 1) {
                                            subtotal =
                                                subtotal + (list!![i].cost!! * list!![i].quantity!!)
                                        } else {
                                        }
                                    }
                                    vat = (subtotal / 100.0f) * sharedHelper!!.vat
                                    totalamt = subtotal + vat
                                    ordersubtotal.setText(
                                        "" + String.format(
                                            "%.2f",
                                            subtotal
                                        ) + " SAR"
                                    )
                                    totalmount.setText(
                                        "" + String.format(
                                            "%.2f",
                                            totalamt
                                        ) + " SAR"
                                    )
                                    vat_txt.setText("" + String.format("%.2f", vat) + " SAR")
                                    recycler.layoutManager = LinearLayoutManager(context)
                                    recycler.adapter = CartAdapter1(
                                        this,
                                        requireContext(),
                                        data.orderdata
                                    )
                                    swipeController = SwipeController(object : SwipeControllerActions() {
                                        override fun onRightClicked(position: Int) {
                                            Log.d("sdcvsbdvc","sdfcsd")
                                            DialogUtils.showLoader(requireContext())
                                            mapViewModel?.getdelete(requireContext(), data.orderdata!![position].id!!)
                                                ?.observe(viewLifecycleOwner, androidx.lifecycle.Observer {
                                                    DialogUtils.dismissLoader()
                                                    it?.let {
                                                        it.error?.let { error ->
                                                            if (error) {
                                                                it.message?.let { msg ->
                                                                    Log.d("zxcvfgbnm,", "" + msg)
                                                                    UiUtils.showSnack(recycler,msg)
                                                                }
                                                            } else {
                                                                it.message?.let { msg ->
                                                                    subtotal = 0.0
                                                                    vat = 0.0
                                                                    totalamt = 0.0
                                                                    Log.d("kanika,", "" + msg)
                                                                    stocknotavailable=false
                                                                    getcartlist()
                                                                }
                                                            }
                                                        }
                                                    }
                                                })
                                            //  recycler.adapter.players.remove(position)
                                            // (recycler.adapter as SwipeAdapter).notifyItemRemoved(position)
                                            // (recycler.adapter as SwipeAdapter).notifyItemRangeChanged(position, (recycler.adapter as SwipeAdapter).getItemCount())
                                        }
                                    })
                                    val itemTouchhelper = ItemTouchHelper(swipeController!!)
                                    itemTouchhelper.attachToRecyclerView(recycler)
                                    recycler.addItemDecoration(object : ItemDecoration() {
                                        override fun onDraw(c: Canvas, parent: RecyclerView, state: RecyclerView.State) {
                                            swipeController!!.onDraw(c)
                                        }
                                    })
                                }
                            }
                        }
                    }
                })
    }
}