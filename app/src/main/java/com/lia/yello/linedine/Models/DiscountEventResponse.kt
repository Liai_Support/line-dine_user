package com.lia.yello.linedine.Models

import com.google.gson.annotations.SerializedName
import com.lia.yello.linedine.activity.Banner
import java.io.Serializable

class DiscountEventResponse : Serializable {

    @SerializedName("error")
    var error: Boolean? = null

    @SerializedName("message")
    var message: String? = null

    @SerializedName("data")
    var data: DiscountEvents? = null

}

class DiscountEvents : Serializable {

    @SerializedName("live")
    var live: String? = null


    @SerializedName("events_data")
    var events_data: ArrayList<EventsData>? = null

}


class EventsData : Serializable {

    @SerializedName("eventid")
    var eventid: Int? = null

    @SerializedName("event")
    var event: String? = null

    @SerializedName("date")
    var date: String? = null

}


