package com.lia.yello.linedine.adapter

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.content.ContextCompat
import androidx.recyclerview.widget.RecyclerView
import com.lia.yello.linedine.R
import com.lia.yello.linedine.interfaces.OnClickListener
import com.lia.yello.linedine.interfaces.SingleTapListener
import com.lia.yello.linedine.activity.OnBoardActivity
import com.lia.yello.linedine.databinding.ChildOnboard1Binding

class OnBoardAdapter(
    var activity: OnBoardActivity,
    var context: Context,
    var list: ArrayList<Int>,
    var description: ArrayList<String>,
    var descriptionhead: ArrayList<String>,
    var next: OnClickListener,
    var skip: SingleTapListener
) :
    RecyclerView.Adapter<OnBoardAdapter.OnBoardViewHolder>() {


    inner class OnBoardViewHolder(view: View) : RecyclerView.ViewHolder(view) {
        var binding: ChildOnboard1Binding = ChildOnboard1Binding.bind(view)
    }

    override fun onCreateViewHolder(
        parent: ViewGroup,
        viewType: Int
    ): OnBoardViewHolder {
        return OnBoardViewHolder(
            LayoutInflater.from(context).inflate(
                R.layout.child_onboard1,
                parent,
                false
            )
        )
    }

    override fun getItemCount(): Int {
        return list.size
    }

    override fun onBindViewHolder(holder: OnBoardViewHolder, position: Int) {

        holder.binding.description.text = description[position]

        holder.binding.descriptionhead.text = descriptionhead[position]

       /* holder.binding.skip.setOnClickListener {
            skip.singleTap()
        }*/

        holder.binding.onboardImage.setImageDrawable(
            ContextCompat.getDrawable(
                context,
                list[position]
            )
        )

    }
}