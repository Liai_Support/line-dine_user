package com.lia.yello.linedine.Repository

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData

import com.google.gson.Gson
import com.lia.yello.linedine.Models.*
import com.lia.yello.linedine.interfaces.ApiResponseCallback
import com.lia.yello.linedine.network.Api
import com.lia.yello.linedine.network.ApiInput
import org.json.JSONObject

class ProfileRepository private constructor() {

    companion object {
        var repository: ProfileRepository? = null

        fun getInstance(): ProfileRepository {
            if (repository == null) {
                repository = ProfileRepository()
            }
            return repository as ProfileRepository
        }
    }


    fun getProfileDetails(input: ApiInput): LiveData<ProfileDetailsResponse>? {

        val apiResponse: MutableLiveData<ProfileDetailsResponse> = MutableLiveData()

        Api.postMethod(input, object : ApiResponseCallback {
            override fun setResponseSuccess(jsonObject: JSONObject) {
                val gson = Gson()
                var response: ProfileDetailsResponse =
                    gson.fromJson(jsonObject.toString(), ProfileDetailsResponse::class.java)
                apiResponse.value = response
            }

            override fun setErrorResponse(error: String) {
                var response = ProfileDetailsResponse()
                response.error = true
                response.message = error
                apiResponse.value = response
            }
        })

        return apiResponse
    }




    fun updateProfile(input: ApiInput): LiveData<CommonResponse>? {


        val apiResponse: MutableLiveData<CommonResponse> = MutableLiveData()

        Api.postMethod(input, object : ApiResponseCallback {
            override fun setResponseSuccess(jsonObject: JSONObject) {
                val gson = Gson()
                var response: CommonResponse =
                    gson.fromJson(jsonObject.toString(), CommonResponse::class.java)
                apiResponse.value = response
            }

            override fun setErrorResponse(error: String) {
                var response = CommonResponse()
                response.error = true
                response.message = error
                apiResponse.value = response
            }
        })

        return apiResponse
    }


    fun getupdatedevicetoken(input: ApiInput): LiveData<CommonResponseUpdateToken>? {

        val apiResponse: MutableLiveData<CommonResponseUpdateToken> = MutableLiveData()

        Api.postMethod(input, object : ApiResponseCallback {
            override fun setResponseSuccess(jsonObject: JSONObject) {
                val gson = Gson()
                var response: CommonResponseUpdateToken =
                    gson.fromJson(jsonObject.toString(), CommonResponseUpdateToken::class.java)
                apiResponse.value = response
            }

            override fun setErrorResponse(error: String) {
                var response = CommonResponseUpdateToken()
                response.error = true
                response.message = error
                apiResponse.value = response
            }
        })

        return apiResponse
    }


    fun getDiscountEvent(input: ApiInput): LiveData<DiscountEventResponse>? {

        val apiResponse: MutableLiveData<DiscountEventResponse> = MutableLiveData()

        Api.getMethod(input, object : ApiResponseCallback {
            override fun setResponseSuccess(jsonObject: JSONObject) {
                val gson = Gson()
                var response: DiscountEventResponse =
                    gson.fromJson(jsonObject.toString(), DiscountEventResponse::class.java)
                apiResponse.value = response
            }

            override fun setErrorResponse(error: String) {
                var response = DiscountEventResponse()
                response.error = true
                apiResponse.value = response
            }
        })

        return apiResponse
    }



    fun getDiscountEventDates(apiParams: ApiInput): MutableLiveData<DiscountEventDates> {
        val apiResponse: MutableLiveData<DiscountEventDates> = MutableLiveData()

        Api.postMethod(apiParams, object : ApiResponseCallback {
            override fun setResponseSuccess(jsonObject: JSONObject) {
                val gson = Gson()
                var response: DiscountEventDates =
                    gson.fromJson(jsonObject.toString(), DiscountEventDates::class.java)
                apiResponse.value = response
            }

            override fun setErrorResponse(error: String) {
                var response = DiscountEventDates()
                //  response.error = true
                response.message = error
                apiResponse.value = response
            }
        })

        return apiResponse

    }
}