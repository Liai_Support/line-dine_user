package com.lia.yello.linedine.Models

import com.google.gson.annotations.SerializedName
import java.io.Serializable

class MyOrderResponse : Serializable {


    @SerializedName("error")
    var error: Boolean? = null

    @SerializedName("message")
    var message: String? = null

    @SerializedName("data")
   var data:  ArrayList<Placeorderdata>? = null


       }



class Placeorderdata : Serializable {

    @SerializedName("order_id")
    var id: Int? = null

    @SerializedName("date")
    var date: String? = null

    @SerializedName("time")
    var time: String? = null

    @SerializedName("amount")
    var amount: Double? = null

    @SerializedName("res_id")
    var res_id: Int? = null

    @SerializedName("subtotal")
    var subtotal: Double? = null

    @SerializedName("vat")
    var vat: Double? = null


    @SerializedName("payment_type")
    var payment_type: String? = null


    @SerializedName("rating")
    var rating: Double? = null


    @SerializedName("resname")
    var resname: String? = null

    @SerializedName("resbanner")
    var resbanner: String? = null


    @SerializedName("status")
    var status: String? = null


    @SerializedName("splins")
    var splins: String? = null



    @SerializedName("lat")
    var lat: Double? = null


    @SerializedName("lng")
    var lng: Double? = null


    @SerializedName("reviewed")
    var reviewed: String? = null



    @SerializedName("Payment_orderid")
    var Payment_orderid: String? = null


    @SerializedName("transactionid")
    var transactionid: String? = null



    @SerializedName("discount_name")
    var discount_name: String? = null

    @SerializedName("discount_amount")
    var discount_amount: String? = null


    @SerializedName("menudetails")
    var menudetails: ArrayList<Menudetail>? = null

}

class Menudetail : Serializable {

    @SerializedName("id")
    var id: Int? = null

    @SerializedName("menu_id")
    var menu_id: Int? = null

    @SerializedName("menu_name")
    var menu_name: String? = null

    @SerializedName("quantity")
    var quantity: Int? = null

    @SerializedName("amount")
    var amount: Double? = null


    @SerializedName("rating")
    var rating: Double? = null

    @SerializedName("image")
    var image: String? = null

    @SerializedName("special_instruction")
    var splins: String? = null


}
