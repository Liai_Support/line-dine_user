package com.lia.yello.linedine.Models

import com.google.gson.annotations.SerializedName
import java.io.Serializable

class PaymentGatewayResponse {
    @SerializedName("resultCode")
    var rcode: Int? = null

    @SerializedName("message")
    var message: String? = null


    @SerializedName("result")
    var data: PResult? = null
}
class PResult : Serializable {

    @SerializedName("order")
    var order: Order? = null

    @SerializedName("checkoutData")
    var cdata: CData? = null

}

class Order : Serializable {

    @SerializedName("bookingId")
    var bookingId: Int? = null

    @SerializedName("status")
    var status: String? = null

}


class CData : Serializable {

    @SerializedName("postUrl")
    var posturl: String? = null

    @SerializedName("jsUrl")
    var jsurl: String? = null

}




