package com.lia.yello.linedine.adapter

import android.app.AlertDialog
import android.content.Context
import android.graphics.Color
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.RelativeLayout
import android.widget.TextView
import androidx.cardview.widget.CardView
import androidx.fragment.app.FragmentTransaction
import androidx.recyclerview.widget.RecyclerView
import com.google.android.material.bottomsheet.BottomSheetBehavior
import com.lia.yello.linedine.Models.listaddress
import com.lia.yello.linedine.R
import com.lia.yello.linedine.Utils.DialogUtils
import com.lia.yello.linedine.Utils.UiUtils
import com.lia.yello.linedine.activity.DashBoardActivity
import com.lia.yello.linedine.fragment.AddAdressFragment
import com.lia.yello.linedine.fragment.HomeFragment
import kotlinx.android.synthetic.main.fragment_home.*
import androidx.lifecycle.Observer
import com.lia.yello.linedine.databinding.AddresscardBinding
import com.lia.yello.linedine.databinding.DayslotBinding
import kotlinx.android.synthetic.main.carticon.*

class HomeListAddressAdapter(
    var homeFragment: HomeFragment,
    var context: Context,
    var list: ArrayList<listaddress>?
) :
    RecyclerView.Adapter<HomeListAddressAdapter.HomeHeaderViewHolder>() {
    private lateinit var fragmentTransaction: FragmentTransaction
    private var addAdressFragment: AddAdressFragment? = null
    lateinit var view:View
    var row_index:Int=-1

    inner class HomeHeaderViewHolder(view: View) : RecyclerView.ViewHolder(view) {

        var binding: AddresscardBinding = AddresscardBinding.bind(view)

  /*      lateinit var address:TextView
       lateinit var type:TextView
       lateinit var add:ImageView
       lateinit var delete:ImageView
       lateinit var cardaddress:CardView
       lateinit var relative_address:RelativeLayout
        init {
            address=view.findViewById(R.id.address)
            add=view.findViewById(R.id.add)
            delete=view.findViewById(R.id.delete)
            type=view.findViewById(R.id.type)
            cardaddress=view.findViewById(R.id.card_address)
            relative_address=view.findViewById(R.id.relative_address)

        }*/
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): HomeHeaderViewHolder {
        return HomeHeaderViewHolder(
            LayoutInflater.from(context).inflate(
                R.layout.addresscard,
                parent,
                false
            )
        )
    }

    override fun getItemCount(): Int {
        return list!!.size
    }

    override fun onBindViewHolder(holder: HomeHeaderViewHolder, position: Int) {

//        holder.cardaddress.setOnClickListener(View.OnClickListener {
//            row_index = position
//            notifyDataSetChanged()
//        })
//        if (row_index === position) {
//            holder.cardaddress.setBackgroundColor(Color.parseColor("#567845"))
//            holder.tv1.setTextColor(Color.parseColor("#ffffff"))
//        } else {
//            holder.row_linearlayout.setBackgroundColor(Color.parseColor("#ffffff"))
//            holder.tv1.setTextColor(Color.parseColor("#000000"))
//        }



        holder.binding.address.text = list!![position].address
        holder.binding.type.text = list!![position].type
           holder.binding.address.setOnClickListener(View.OnClickListener {
               if(homeFragment.sharedHelper!!.cartcount==0){
                   row_index=position;
                   notifyDataSetChanged();
                   holder.binding.relativeAddress.setBackgroundColor(Color.parseColor("#ffffff"));
                   holder.binding.address.setBackgroundColor(Color.parseColor("#ffffff"));
                   homeFragment.sharedHelper!!.selectedaddressid = list!![position].id!!
                   homeFragment.getupdateaddressid()
                   homeFragment.sharedHelper?.iscurrentlocationseleted = false
                   homeFragment.requireActivity().location.setText(""+list!![position].type)
                   homeFragment.sharedHelper?.currentlocation = list!![position].address.toString()
                   homeFragment.sharedHelper!!.currentLat= list!![position].lat.toString()
                   homeFragment.sharedHelper!!.currentLng= list!![position].lng.toString()
                   Log.d("currentlat", homeFragment.sharedHelper!!.currentLat)
                   Log.d("currentlng", homeFragment.sharedHelper!!.currentLng)

                   (context as DashBoardActivity?)!!.mBottomSheetBehavior2?.setState(
                       BottomSheetBehavior.STATE_HIDDEN)
               }
               else{
                   if(homeFragment.sharedHelper!!.selectedaddressid == list!![position].id){
                       row_index=position;
                       notifyDataSetChanged();
                       homeFragment.sharedHelper!!.selectedaddressid = list!![position].id!!
                       homeFragment.getupdateaddressid()
                       homeFragment.sharedHelper?.iscurrentlocationseleted = false
                       homeFragment.requireActivity().location.setText(""+list!![position].type)
                       homeFragment.sharedHelper?.currentlocation = list!![position].address.toString()
                       homeFragment.sharedHelper!!.currentLat= list!![position].lat.toString()
                       homeFragment.sharedHelper!!.currentLng= list!![position].lng.toString()

                       Log.d("currentlat", homeFragment.sharedHelper!!.currentLat)
                       Log.d("currentlng", homeFragment.sharedHelper!!.currentLng)
                       (context as DashBoardActivity?)!!.mBottomSheetBehavior2?.setState(
                           BottomSheetBehavior.STATE_HIDDEN)
                       holder.binding.relativeAddress.setBackgroundColor(Color.parseColor("#ffffff"));
                       holder.binding.address.setBackgroundColor(Color.parseColor("#ffffff"));
                   }
                   else{
                       val builder = AlertDialog.Builder(context)
                       builder.setTitle(R.string.alert)
                       builder.setMessage(R.string.ifyoucanchangeaddressyourcartproductwillremoveautomatically)
                       builder.setCancelable(true)
                       builder.setPositiveButton(android.R.string.yes) { dialog, which ->
                           getdeleteall(list!![position].id,list!![position].address,list!![position].lat,list!![position].lng,list!![position].type)
                       }

                       builder.setNegativeButton(android.R.string.no) { dialog, which ->
                           builder.setCancelable(true)
                       }
                       builder.show()
                   }
               }
           })


        holder.binding.add.setOnClickListener {
            val bundle = Bundle()
            list!![position].lat?.let { it1 -> bundle.putDouble("flat", it1) }
            list!![position].lng?.let { it1 -> bundle.putDouble("flng", it1) }
            bundle.putString("faddr", list!![position].address)
            bundle.putString("fatype", list!![position].type)
            bundle.putString("fcity","")
            bundle.putBoolean("update", true)
            bundle.putInt("faid", list!![position].id!!)

            addAdressFragment = AddAdressFragment()
            addAdressFragment!!.arguments = bundle

            fragmentTransaction =
                homeFragment.requireActivity().supportFragmentManager.beginTransaction()
            fragmentTransaction.setCustomAnimations(R.anim.screen_in, R.anim.screen_out)
            fragmentTransaction.replace(R.id.container, addAdressFragment!!)
            fragmentTransaction.addToBackStack(null)
            fragmentTransaction.commit()
        }


        holder.binding.delete.setOnClickListener {

            if (holder.binding.address.text.equals(homeFragment.location.text.toString())){

                row_index=position;
                notifyDataSetChanged();


                UiUtils.showSnack(holder.binding.delete,context.getString(R.string.deliverylocationcannotbedeleted))
            }else{
                Log.d("addressid2",""+list!![position].id!!)
                DialogUtils.showLoader(context)

                homeFragment.mapViewModel?.getdeleteaddress(context, list!![position].id!!)
                    ?.observe(homeFragment,
                        androidx.lifecycle.Observer {
                            DialogUtils.dismissLoader()

                            it?.let {
                                it.error?.let { error ->
                                    if (error) {
                                        it.message?.let { msg ->
                                            homeFragment.getaddressvalue()
                                            Log.d("response", msg)
                                        }
                                    } else {
                                        it.message?.let { msg ->

                                            Log.d("response", msg)
                                            homeFragment.getaddressvalue()
                                        }

                                    }
                                }
                            }

                        })

            }



        }

        if(row_index==position){
            holder.binding.relativeAddress.setBackgroundColor(Color.parseColor("#FFC0CB"))
            holder.binding.address.setBackgroundColor(Color.parseColor("#FFC0CB"))
        }
        else
        {
            holder.binding.relativeAddress.setBackgroundColor(Color.parseColor("#ffffff"));
            holder.binding.address.setBackgroundColor(Color.parseColor("#ffffff"));
          //  holder.tv1.setTextColor(Color.parseColor("#000000"));
        }
    }

    public fun getdeleteall(
        addressid: Int?,
        address: String?,
        lat: Double?,
        lng: Double?,
        type: String?
    ) {
        DialogUtils.showLoader(context)

        homeFragment.mapViewModel?.getdeleteall(context)
            ?.observe(homeFragment,
                Observer {
                    DialogUtils.dismissLoader()

                    it?.let {
                        it.error?.let { error ->
                            if (error) {
                                it.message?.let { msg ->
                                    //UiUtils.showSnack(root1, msg)
                                    // snackbarToast(msg)
                                }
                            } else {
                                it.message?.let { msg ->

                                    homeFragment.sharedHelper!!.selectedaddressid = addressid!!
                                    homeFragment.getupdateaddressid()
                                    homeFragment.cart_badge.text = "0"
                                    homeFragment.sharedHelper!!.cartcount = 0
                                    homeFragment.sharedHelper?.iscurrentlocationseleted = false
                                   // homeFragment.requireActivity().location.setText(""+address)
                                    homeFragment.location.setText(""+type)
                                    homeFragment.sharedHelper?.currentlocation = address.toString()
                                    homeFragment.sharedHelper!!.currentLat= lat.toString()
                                    homeFragment.sharedHelper!!.currentLng= lng.toString()

                                    Log.d("currentlat", homeFragment.sharedHelper!!.currentLat)
                                    Log.d("currentlng", homeFragment.sharedHelper!!.currentLng)

                                    (context as DashBoardActivity?)!!.mBottomSheetBehavior2?.setState(
                                        BottomSheetBehavior.STATE_HIDDEN)
                                      }
                            }
                        }

                    }
                })



    }






}
