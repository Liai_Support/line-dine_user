package com.lia.yello.linedine.Models

import com.google.gson.annotations.SerializedName
import java.io.Serializable

class CheckFavrouiteRestuarant  : Serializable {

    @SerializedName("error")
    var error: Boolean? = null

    @SerializedName("message")
    var message: String? = null

    @SerializedName("data")
    var data: Rest? = null

}

class Rest : Serializable {

    @SerializedName("resid")
    var resid: Int? = null

}