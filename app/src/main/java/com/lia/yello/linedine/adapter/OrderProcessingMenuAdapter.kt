package com.lia.yello.linedine.adapter

import android.content.Context
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.lia.yello.linedine.Models.Menudetail
import com.lia.yello.linedine.R
import com.lia.yello.linedine.databinding.CardOrdersBinding
import com.lia.yello.linedine.databinding.OrderprocessinngmenuadapterBinding
import com.lia.yello.linedine.fragment.MyActiveOrderFragment
import kotlinx.android.synthetic.main.fragment_order_processing.*
import kotlinx.android.synthetic.main.fragment_suborders.*
import kotlinx.android.synthetic.main.fragment_suborders.ordersubtotal
import java.util.*
import kotlin.collections.ArrayList

class OrderProcessingMenuAdapter(
    var myActiveOrderFragment: MyActiveOrderFragment,
    var context: Context,
    var list: ArrayList<Menudetail>?
) :
    RecyclerView.Adapter<OrderProcessingMenuAdapter.HomeHeaderViewHolder>() {



    inner class HomeHeaderViewHolder(view: View) : RecyclerView.ViewHolder(view) {

        var binding: OrderprocessinngmenuadapterBinding = OrderprocessinngmenuadapterBinding.bind(view)

    }


    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): HomeHeaderViewHolder {
        return HomeHeaderViewHolder(
            LayoutInflater.from(parent.context).inflate(
                R.layout.orderprocessinngmenuadapter,
                parent,
                false
            )
        )
    }

    override fun getItemCount(): Int {
        return list!!.size
    }


    override fun onBindViewHolder(holder: HomeHeaderViewHolder, position: Int) {

        // myActiveOrderFragment.subtotalcount_total+=list!![position].amount!!
        Log.d("wedrftghyjkl",""+myActiveOrderFragment.subtotalcount)
        Log.d("wedrftghyjkl",""+myActiveOrderFragment.subtotalcount_total)
        holder.binding.menuname.text = ""+list!![position].quantity +  " * "+ list!![position].menu_name
        holder.binding.menutotalcost.text =    String.format(Locale("en", "US"),"%.2f", list!![position].amount)+" SAR"


        // myActiveOrderFragment.subtotal1.text =String.format(Locale("en", "US"),"%.2f", myActiveOrderFragment.subtotalcount_total)+ " SAR"
        // myActiveOrderFragment.finaltotal=myActiveOrderFragment.subtotalcount_total+myActiveOrderFragment.vattocal
        //myActiveOrderFragment.total1.text =String.format(Locale("en", "US"),"%.2f", myActiveOrderFragment.finaltotal-myActiveOrderFragment.discoumtamtt)+ " SAR"

        /*      if (myActiveOrderFragment.discoumtamtt==0.0){
                  Log.d("yes","yes")
                  myActiveOrderFragment.total1.text =String.format(Locale("en", "US"),"%.2f", myActiveOrderFragment.finaltotal)+ " SAR"

              }else{
                   Log.d("no","no")
                  myActiveOrderFragment.total1.text =String.format(Locale("en", "US"),"%.2f", myActiveOrderFragment.finaltotal-myActiveOrderFragment.discoumtamtt)+ " SAR"


              }*/



    }

}












