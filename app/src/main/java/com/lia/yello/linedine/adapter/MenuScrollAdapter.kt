package com.lia.yello.linedine.adapter

import android.graphics.Color
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.LinearLayout
import android.widget.TextView
import androidx.cardview.widget.CardView
import androidx.core.content.ContextCompat
import androidx.recyclerview.widget.RecyclerView
import com.facebook.FacebookSdk
import com.facebook.FacebookSdk.getApplicationContext
import com.lia.yello.linedine.R
import com.lia.yello.linedine.databinding.Notification1Binding
import com.lia.yello.linedine.databinding.ScrollistBinding
import com.lia.yello.linedine.fragment.SubsubHomeFragment
import kotlinx.android.synthetic.main.fragment_subsubhome.*

class MenuScrollAdapter(
    var subsubHomeFragment: SubsubHomeFragment) :
    RecyclerView.Adapter<MenuScrollAdapter.MyViweHolder>() {
    var intt: Int = 0
    val fname = arrayOf(subsubHomeFragment.getString(R.string.breakfast),subsubHomeFragment.getString(R.string.lunch),subsubHomeFragment.getString(R.string.dinner))
    val pics =
        intArrayOf(R.drawable.ic_breakfast, R.drawable.ic_egg, R.drawable.dinner)
    var row_index:Int=-1



    class MyViweHolder(view: View) : RecyclerView.ViewHolder(view) {

        var binding: ScrollistBinding = ScrollistBinding.bind(view)

        /*     lateinit var text: TextView
             lateinit var img: ImageView
             lateinit var card: CardView
             lateinit var linear:LinearLayout


             init {
                 text = view.findViewById(R.id.txtscroll)
                 img = view.findViewById(R.id.imgscroll)
                 card = view.findViewById(R.id.cardlunch)
                 linear = view.findViewById(R.id.linearlunchh)
             }*/
    }

    override fun onCreateViewHolder(
        parent: ViewGroup,
        viewType: Int
    ): MenuScrollAdapter.MyViweHolder {

        return MyViweHolder(
            LayoutInflater.from(parent.context).inflate(R.layout.scrollist, parent, false)
        )
    }

    override fun getItemCount(): Int {
        return fname.size
    }

    override fun onBindViewHolder(holder: MenuScrollAdapter.MyViweHolder, position: Int) {

        holder.binding.txtscroll.text = fname[position].toString()
        holder.binding.imgscroll.setImageResource(pics[position])
        holder.binding.cardlunch.setOnClickListener(View.OnClickListener {
            row_index = position;
            subsubHomeFragment.allseleted = false
            notifyDataSetChanged();


        })
        if (row_index==position){
            subsubHomeFragment.all.backgroundTintList = ContextCompat.getColorStateList(FacebookSdk.getApplicationContext(), R.color.white)
            subsubHomeFragment.all.setTextColor(Color.BLACK)
            subsubHomeFragment.all.background = ContextCompat.getDrawable(FacebookSdk.getApplicationContext(),R.drawable.border_layoutred)
            holder.binding.linearlunchh.setBackgroundResource(R.drawable.border_layoutred)
            holder.binding.txtscroll.setTextColor(Color.WHITE)
            /* subsubHomeFragment.txt= holder.text.text as String
             Log.d("sadfghjkl",""+ subsubHomeFragment.txt)*/
            subsubHomeFragment.all.setTextColor(Color.BLACK)
            subsubHomeFragment.all.background = ContextCompat.getDrawable(
                FacebookSdk.getApplicationContext(),
                R.drawable.border_layoutred
            )
            subsubHomeFragment.all.backgroundTintList = ContextCompat.getColorStateList(
                getApplicationContext(),
                R.color.white
            )
            if (holder.binding.txtscroll.text.toString()=="فطور"){
                subsubHomeFragment.getvalues("Break Fast")

            }else if (holder.binding.txtscroll.text.toString()=="غداء"){
                subsubHomeFragment.getvalues("Lunch")

            }else if (holder.binding.txtscroll.text.toString()=="عشاء"){
                subsubHomeFragment.getvalues("Dinner")

            }else{
                subsubHomeFragment.getvalues(holder.binding.txtscroll.text.toString())

            }


        }else
        {
            if(subsubHomeFragment.allseleted == true){
                holder.binding.linearlunchh.setBackgroundColor(Color.parseColor("#ffffff"))
                holder.binding.txtscroll.setTextColor(Color.parseColor("#000000"));
                subsubHomeFragment.all.backgroundTintList = ContextCompat.getColorStateList(FacebookSdk.getApplicationContext(), R.color.colorPrimary)
                subsubHomeFragment.all.setTextColor(Color.WHITE)
                subsubHomeFragment.all.background = ContextCompat.getDrawable(FacebookSdk.getApplicationContext(),R.drawable.border_layoutred)

            }
            else {
                holder.binding.linearlunchh.setBackgroundColor(Color.parseColor("#ffffff"))
                holder.binding.txtscroll.setTextColor(Color.parseColor("#000000"));
                subsubHomeFragment.all.setTextColor(Color.BLACK)
                subsubHomeFragment.all.background = ContextCompat.getDrawable(
                    FacebookSdk.getApplicationContext(),
                    R.drawable.border_layoutred
                )
                subsubHomeFragment.all.backgroundTintList = ContextCompat.getColorStateList(
                    FacebookSdk.getApplicationContext(),
                    R.color.white
                )
            }
        }


    }
}







