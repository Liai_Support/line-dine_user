package com.lia.yello.linedine.Models

import com.google.gson.annotations.SerializedName
import java.io.Serializable

class ProfileDetailsResponse : Serializable {

    @SerializedName("error")
    var error: Boolean? = null

    @SerializedName("message")
    var message: String? = null

    @SerializedName("data")
    var data: ProfileDetails? = null

}




class ProfileDetails : Serializable {


    @SerializedName("name")
    var name: String? = null

    @SerializedName("email")
    var email: String? = null

    @SerializedName("id")
    var Id: Int? = null

    @SerializedName("countrycode")
    var countryCode: String? = null

    @SerializedName("phone")
    var mobileNumber: String? = null

    @SerializedName("gender")
    var gender: String? = null

    @SerializedName("dob")
    var DOB: String? = null


    @SerializedName("address_id")
    var addressid: Int? = null

   @SerializedName("events")
   var events: ArrayList<EventsArray>? = null

}



class EventsArray : Serializable {

    @SerializedName("eventid")
    var eventid: Int? = null

    @SerializedName("event")
    var event: String? = null

    @SerializedName("date")
    var date: String? = null

}