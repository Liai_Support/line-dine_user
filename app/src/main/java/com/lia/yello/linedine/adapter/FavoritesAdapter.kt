package com.lia.yello.linedine.adapter

import android.content.Context
import android.os.Build
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.annotation.RequiresApi
import androidx.fragment.app.FragmentTransaction
import androidx.recyclerview.widget.RecyclerView
import com.lia.yello.linedine.Models.listfav
import com.lia.yello.linedine.R
import com.lia.yello.linedine.Utils.UiUtils

import com.lia.yello.linedine.databinding.CardFavoritesBinding
import com.lia.yello.linedine.fragment.FavoritesFragment
import com.lia.yello.linedine.fragment.SubsubHomeFragment
import kotlinx.android.synthetic.main.fragment_subsubsubhome.*
import java.util.*
import kotlin.collections.ArrayList

class FavoritesAdapter(
    var favoritesFragment: FavoritesFragment,
    var context: Context,
    var list: ArrayList<listfav>?
) :
    RecyclerView.Adapter<FavoritesAdapter.HomeHeaderViewHolder>() {


    inner class HomeHeaderViewHolder(view: View) : RecyclerView.ViewHolder(view) {
        var binding: CardFavoritesBinding = CardFavoritesBinding.bind(view)
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): HomeHeaderViewHolder {
        return HomeHeaderViewHolder(
            LayoutInflater.from(context).inflate(
                R.layout.card_favorites,
                parent,
                false
            )
        )
    }

    override fun getItemCount(): Int {
        return list!!.size
    }

    @RequiresApi(Build.VERSION_CODES.M)
    override fun onBindViewHolder(holder: HomeHeaderViewHolder, position: Int) {


        if(list!![position].menuavilable==0){
           // holder.binding.card.foreground.alpha=0
            holder.binding.favlin.alpha=0.5f
        }else{
            holder.binding.title.text = list!![position].item_name

            holder.binding.price.text=String.format(Locale("en", "US"),"%.2f",list!![position].cost)+ " SAR"
            holder.binding.resname.text = list!![position].name

            holder.binding.favrating.text= list!![position].rating.toString()
            list!![position].images?.let { imageUrl ->
                UiUtils.loadImage(holder.binding.imagefav, imageUrl)
            }
        }



        holder.binding.addfav.setOnClickListener(View.OnClickListener {
            favoritesFragment.checkfavroite(list!![position].res_id!!)
       /*   */



        })


    }
}