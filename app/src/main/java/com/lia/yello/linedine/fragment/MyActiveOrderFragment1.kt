package com.lia.yello.linedine.fragment

import android.os.Bundle
import android.util.Log
import android.view.View
import android.widget.*
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentTransaction
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.lia.yello.linedine.R
import com.lia.yello.linedine.Session.SharedHelper
import com.lia.yello.linedine.Utils.DialogUtils
import com.lia.yello.linedine.Utils.NetworkUtils
import com.lia.yello.linedine.Utils.UiUtils
import com.lia.yello.linedine.ViewModels.MapViewModel
import com.lia.yello.linedine.adapter.MyActiveOrdersAdapter
import com.lia.yello.linedine.databinding.FragmentMyActiveOrdersBinding
import kotlinx.android.synthetic.main.carticon.*
import kotlinx.android.synthetic.main.fragment_my_active_orders.*
import kotlinx.android.synthetic.main.fragment_profile.*
import kotlinx.android.synthetic.main.header.*
import java.util.*

class MyActiveOrderFragment1 : Fragment(R.layout.fragment_my_active_orders) {

    var binding: FragmentMyActiveOrdersBinding? = null
    var mapViewModel: MapViewModel? = null
    lateinit var adapter: RecyclerView.Adapter<RecyclerView.ViewHolder>
    lateinit var layoutManager: RecyclerView.LayoutManager
    var orderid:Int=0
    lateinit var ordersubtotal: TextView
    lateinit var vat_txt: TextView
    lateinit var totalmount: TextView
    var status: String=""
    var discoumtamtt:Double=0.0
    lateinit var discountreorder:TextView
    lateinit var discountlevel:RelativeLayout
    var menuid:Int=0
    lateinit var bot: LinearLayout
    lateinit var linaer_activeorder2: LinearLayout
    lateinit var reorder: Button
    lateinit var review: Button

    public var sharedHelper: SharedHelper? = null
    private var myActiveOrderFragment: MyActiveOrderFragment? = null
    private lateinit var fragmentTransaction: FragmentTransaction
    private var reviewFragment: ReviewFragment? = null
    var subtotalcount:Double=0.0
    var subtotalcount_total:Double=0.0
    var vattocal:Double=0.0
    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        binding = DataBindingUtil.findBinding(view)
        sharedHelper = SharedHelper(requireContext())
        if (!NetworkUtils.isNetworkConnected(requireContext())) {
            UiUtils.showSnack(
                view, "no_internet_connection"
            )
        }

        if (sharedHelper!!.language == "ar"){
            back.rotation= 180F
        }
        mapViewModel = ViewModelProvider(this).get(MapViewModel::class.java)
        val head = view.findViewById<TextView>(R.id.head)

        back.setOnClickListener(View.OnClickListener {
            requireActivity().supportFragmentManager.popBackStack()
        })
        ordersubtotal = view.findViewById<TextView>(R.id.ordersubtotal)
        vat_txt = view.findViewById<TextView>(R.id.vat_txt)
        totalmount = view.findViewById<TextView>(R.id.totalmount)
        bot = view.findViewById<LinearLayout>(R.id.bot)
        reorder = view.findViewById<Button>(R.id.reorder)
        review = view.findViewById<Button>(R.id.review)
        discountreorder = view.findViewById<Button>(R.id.discountreorder)
        discountlevel = view.findViewById<RelativeLayout>(R.id.discountlevel)
        reorder.visibility = View.GONE
        review.visibility = View.GONE

        head.setText(getString(R.string.myactiveorders))
        cart_badge.text = ""+ sharedHelper!!.cartcount

        orderid = arguments?.getInt("orderid")!!
        status = arguments?.getString("status")!!
        Log.d("wedrfghnjm,",""+orderid)
        Log.d("wedrfghnjm,",""+status)

        if (status.equals("cancelled")){
            head.setText(getString(R.string.mycompleteorders))

        }

        getpending()
        if (sharedHelper!!.choose.equals("finedine")||sharedHelper!!.choose.equals("inqueue")|| sharedHelper!!.cartcount == 0){

            //var cartframe: FrameLayout =findViewById(R.id.cartframe)
            cartframe.visibility=View.GONE
        }
    }

    private fun getpending(){
        DialogUtils.showLoader(requireContext())
        mapViewModel?.getplaceorderlist(requireContext(),status)
            ?.observe(viewLifecycleOwner,
                Observer {
                    DialogUtils.dismissLoader()

                    it?.let {
                        it.error?.let { error ->
                            if (error) {
                                it.message?.let { msg ->
                                    UiUtils.showSnack(rootmyactiveorders, msg)
                                }
                            } else {
                                it.data?.let { data ->

                                    for (i in 0 until data!!.size) {
                                        Log.d("xcfghj",""+data[i].id)
                                        Log.d("jjjjjj",""+orderid)
                                        if (data[i].id!! ==  orderid) {


                                            if (data[i].discount_amount=="0"){
                                                discountlevel.visibility=View.GONE
                                            }else{
                                                discoumtamtt=data[i].discount_amount!!.toDouble()

                                                discountlevel.visibility=View.VISIBLE

                                                discountreorder.text =" - " +String.format(Locale("en", "US"),"%.2f", data[i].discount_amount!!.toDouble())+ " SAR"

                                            }
                                            Log.d("qwedrfghjnm,",""+menuid)

                                            Log.d("qwedrfghjnm,",""+data[i].subtotal.toString()+ " SAR")
                                            subtotalcount=0.0
                                            subtotalcount_total=0.0
                                            vattocal=data[i].vat!!
                                            // ordersubtotal.text =String.format(Locale("en", "US"),"%.2f", data[i].subtotal)+ " SAR"
                                            vat_txt.text =String.format(Locale("en", "US"),"%.2f", data[i].vat)+ " SAR"
                                            //totalmount.text =String.format(Locale("en", "US"),"%.2f", data[i].amount)+ " SAR"
                                            /*                               ordersubtotal.text =data[i].subtotal.toString()+ " SAR"
                                                                           vat_txt.text= data[i].vat.toString()+ " SAR"
                                                                           totalmount.text= data[i].amount.toString()+ " SAR"*/


                                            menurecycler.layoutManager =
                                                LinearLayoutManager(context)
                                            menurecycler.adapter = MyActiveOrdersAdapter(
                                                this,
                                                requireContext(),
                                                data[i].menudetails
                                            )
                                        }

                                    }


                                }
                            }
                        }

                    }
                })
    }


}