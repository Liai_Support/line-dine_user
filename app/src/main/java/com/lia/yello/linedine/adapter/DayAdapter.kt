package com.lia.yello.linedine.adapter

import android.content.Context
import android.graphics.Color
import android.os.Build
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.LinearLayout
import android.widget.RelativeLayout
import android.widget.TextView
import androidx.annotation.RequiresApi
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentTransaction
import androidx.recyclerview.widget.RecyclerView
import com.google.android.material.bottomsheet.BottomSheetBehavior
import com.lia.yello.linedine.Models.ListDay
import com.lia.yello.linedine.Models.listfav
import com.lia.yello.linedine.R
import com.lia.yello.linedine.Utils.UiUtils
import com.lia.yello.linedine.databinding.*

import com.lia.yello.linedine.fragment.FavoritesFragment
import com.lia.yello.linedine.fragment.FineDineFragment1
import kotlinx.android.synthetic.main.fragment_fine_dine2.*

class DayAdapter(
    var fineDineFragment1: FineDineFragment1,
    var context: Context,
    var list: ArrayList<ListDay>?
) :
    RecyclerView.Adapter<DayAdapter.HomeHeaderViewHolder>() {

    var row_index:Int=-1
    inner class HomeHeaderViewHolder(view: View) : RecyclerView.ViewHolder(view) {
        var binding: DayslotBinding = DayslotBinding.bind(view)

    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): HomeHeaderViewHolder {
        return HomeHeaderViewHolder(
            LayoutInflater.from(context).inflate(
                R.layout.dayslot,
                parent,
                false
            )
        )
    }

    override fun getItemCount(): Int {
        return list!!.size
    }

    @RequiresApi(Build.VERSION_CODES.M)
    override fun onBindViewHolder(holder: HomeHeaderViewHolder, position: Int) {
        /*  if (list!![position].status.equals("closed")){
              Log.d("hdhdhjfhdfhdh","bhbdhbdhbbdjh")
              holder.binding.day.setTextColor(Color.parseColor("#000000"))
              holder.binding.date.setTextColor(Color.parseColor("#000000"))
              holder.binding.linrDateSlot.setBackgroundColor(Color.parseColor("#AEAEAE"))


          }else{
             *//* holder.binding.day.setTextColor(Color.parseColor("#000000"))
            holder.binding.date.setTextColor(Color.parseColor("#000000"))
            holder.binding.linrDateSlot.setBackgroundColor(Color.parseColor("#A41620"))*//*
        }*/
        holder.binding.day.text= list!![position].day
        holder.binding.date.text= list!![position].date
        //  fineDineFragment1.month.text=list!![position].month
        Log.d("zsedfgtyhjuik",""+list!![position].month)
        //fineDineFragment1.montthh= list!![position].month!!
        holder.binding.linrDateSlot.setOnClickListener {
            if(position == 0){
                fineDineFragment1.istoday = true
                if (fineDineFragment1.isclosed == true) {
                    Log.d("asdfghjkl","bnbnbnbn")
                    holder.binding.linrDateSlot.isClickable = false
                    fineDineFragment1.work.visibility=View.GONE
                    fineDineFragment1.time.text= context.getString(R.string.closed)
                    fineDineFragment1.recycler_time.visibility=View.GONE
                    fineDineFragment1.linear_bld_slot.visibility=View.GONE
                    fineDineFragment1.continuereserve.visibility=View.VISIBLE
                    fineDineFragment1.textclosesorry.visibility=View.VISIBLE
                    fineDineFragment1.imgcloseicon.visibility=View.VISIBLE
                    fineDineFragment1.chooseanavail.visibility=View.GONE

                    fineDineFragment1.continuereserve.background=context.getDrawable(R.drawable.border_layoutgreyy)
                    fineDineFragment1.continuereserve.isClickable=false
                    row_index = position;
                    notifyDataSetChanged();
                }
                else {
                    if (list!![position].status.equals("closed")){
                        Log.d("xmxmxm","bvbvb")

                        fineDineFragment1.work.visibility=View.GONE
                        fineDineFragment1.time.text= context.getString(R.string.closed)
                        fineDineFragment1.recycler_time.visibility=View.GONE
                        fineDineFragment1.linear_bld_slot.visibility=View.GONE
                        fineDineFragment1.continuereserve.visibility=View.VISIBLE
                        fineDineFragment1.textclosesorry.visibility=View.VISIBLE
                        fineDineFragment1.imgcloseicon.visibility=View.VISIBLE
                        fineDineFragment1.chooseanavail.visibility=View.GONE
                        fineDineFragment1.continuereserve.background=context.getDrawable(R.drawable.border_layoutgreyy)
                        fineDineFragment1.continuereserve.isClickable=false
                        row_index = position;
                        notifyDataSetChanged();
                    }else{
                        Log.d("wertyuio","jnjnnjjn")
                        fineDineFragment1.breakfast.isEnabled = true
                        fineDineFragment1.lunch.isEnabled = true
                        fineDineFragment1.dinner.isEnabled = true
                        fineDineFragment1.work.visibility = View.VISIBLE
                        fineDineFragment1.time.visibility = View.VISIBLE
                        fineDineFragment1.day = list!![position].fulday!!
                        fineDineFragment1.date = list!![position].date!!
                        fineDineFragment1.fulldate = list!![position].fulldate!!
                        // fineDineFragment1.montthh=list!![position].month!!
                        //fineDineFragment1.montthh=list!![position].month!!
                        Log.d("monthhhhhh", "" + list!![position].month!!)
                        fineDineFragment1.month.text = list!![position].month!!
                        Log.d("dayyyyy", "" + list!![position].fulday!!)
                        Log.d("month", "" + list!![position].month!!)
                        fineDineFragment1.linear_bld_slot.visibility = View.VISIBLE
                        fineDineFragment1.chooseanavail.visibility = View.VISIBLE
                        fineDineFragment1.continuereserve.isClickable=true
                        fineDineFragment1.getvalues("All")
                        row_index = position;
                        notifyDataSetChanged();
                    }


                }
            }
            else {
                fineDineFragment1.istoday = false
                if (fineDineFragment1.isclosed == true) {
                    Log.d("asasas","vbvb")
                    holder.binding.linrDateSlot.isClickable = true

                    fineDineFragment1.continuereserve.visibility=View.VISIBLE
                    fineDineFragment1.textclosesorry.visibility=View.GONE
                    fineDineFragment1.imgcloseicon.visibility=View.GONE
                    holder.binding.linrDateSlot.isClickable = true
                    fineDineFragment1.breakfast.isEnabled = true
                    fineDineFragment1.continuereserve.isClickable = true
                    fineDineFragment1.lunch.isEnabled = true
                    fineDineFragment1.dinner.isEnabled = true
                    fineDineFragment1.isclosed = true

                    fineDineFragment1.day = list!![position].fulday!!
                    fineDineFragment1.date = list!![position].date!!
                    fineDineFragment1.fulldate = list!![position].fulldate!!
                    // fineDineFragment1.montthh=list!![position].month!!
                    //fineDineFragment1.montthh=list!![position].month!!
                    Log.d("monthhhhhh", "" + list!![position].month!!)
                    fineDineFragment1.month.text = list!![position].month!!
                    Log.d("dayyyyy", "" + list!![position].fulday!!)
                    Log.d("month", "" + list!![position].month!!)
                    fineDineFragment1.linear_bld_slot.visibility = View.VISIBLE
                    fineDineFragment1.chooseanavail.visibility = View.VISIBLE

                    row_index = position;
                    notifyDataSetChanged();
                    //  fineDineFragment1.getvalues("All")
                    // row_index = position;
                    //notifyDataSetChanged();

                }
                else {
                    if (list!![position].status.equals("closed")){
                        Log.d("hshshhs","nxnxnxn")
                        fineDineFragment1.work.visibility=View.GONE
                        fineDineFragment1.time.text= context.getString(R.string.closed)
                        fineDineFragment1.isclosed = false
                        fineDineFragment1.recycler_time.visibility=View.GONE
                        fineDineFragment1.linear_bld_slot.visibility=View.GONE
                        fineDineFragment1.continuereserve.visibility=View.VISIBLE
                        fineDineFragment1.textclosesorry.visibility=View.VISIBLE
                        fineDineFragment1.imgcloseicon.visibility=View.VISIBLE
                        fineDineFragment1.chooseanavail.visibility=View.GONE

                        fineDineFragment1.continuereserve.background=context.getDrawable(R.drawable.border_layoutgreyy)
                        fineDineFragment1.continuereserve.isClickable=false
                        row_index = position;
                        notifyDataSetChanged();
                    }else{
                        Log.d("hdhdgdgdg","llllll")
                        fineDineFragment1.breakfast.isEnabled = true
                        fineDineFragment1.lunch.isEnabled = true
                        fineDineFragment1.dinner.isEnabled = true
                        fineDineFragment1.isclosed = false
                        fineDineFragment1.work.visibility = View.VISIBLE
                        fineDineFragment1.time.visibility = View.VISIBLE
                        fineDineFragment1.continuereserve.isClickable = true
                        fineDineFragment1.day = list!![position].fulday!!
                        fineDineFragment1.date = list!![position].date!!
                        fineDineFragment1.fulldate = list!![position].fulldate!!
                        // fineDineFragment1.montthh=list!![position].month!!
                        //fineDineFragment1.montthh=list!![position].month!!
                        Log.d("monthhhhhh", "" + list!![position].month!!)
                        fineDineFragment1.month.text = list!![position].month!!
                        Log.d("dayyyyy", "" + list!![position].fulday!!)
                        Log.d("month", "" + list!![position].month!!)
                        fineDineFragment1.linear_bld_slot.visibility = View.VISIBLE
                        fineDineFragment1.chooseanavail.visibility = View.VISIBLE
                        fineDineFragment1.textclosesorry.visibility=View.GONE
                        fineDineFragment1.imgcloseicon.visibility=View.GONE
                        fineDineFragment1.getvalues("All")

                        row_index = position;
                        notifyDataSetChanged();
                    }





                }
            }
        }
        if(row_index==position)
        {
            if (list!![position].status.equals("closed")){
                Log.d("hdhdhjfhdfhdh","bvbvbbvvbvbvb")
                holder.binding.day.setTextColor(Color.parseColor("#000000"))
                holder.binding.date.setTextColor(Color.parseColor("#000000"))
                holder.binding.relativeDaySlot.setBackgroundColor(Color.parseColor("#AEAEAE"))

                fineDineFragment1.work.visibility=View.GONE
                fineDineFragment1.time.text= context.getString(R.string.closed)
                fineDineFragment1.recycler_time.visibility=View.GONE
                fineDineFragment1.linear_bld_slot.visibility=View.GONE
                fineDineFragment1.continuereserve.visibility=View.VISIBLE
                fineDineFragment1.textclosesorry.visibility=View.VISIBLE
                fineDineFragment1.imgcloseicon.visibility=View.VISIBLE
                fineDineFragment1.chooseanavail.visibility=View.GONE

                fineDineFragment1.continuereserve.background=context.getDrawable(R.drawable.border_layoutgreyy)
                fineDineFragment1.continuereserve.isClickable=false
            }else{
                Log.d("jjjj","jjkjkjk")
                holder.binding.relativeDaySlot.setBackgroundColor(Color.parseColor("#A41620"))
                holder.binding.day.setTextColor(Color.parseColor("#FFFFFF"))
                holder.binding.date.setTextColor(Color.parseColor("#FFFFFF"))

                fineDineFragment1.timetext.visibility=View.GONE
                fineDineFragment1. viewbreakfast.visibility=View.INVISIBLE
                fineDineFragment1.viewlunch.visibility=View.INVISIBLE



                /*fineDineFragment1.recycler_time.visibility=View.GONE
                fineDineFragment1.timetext.visibility=View.GONE
                fineDineFragment1. viewbreakfast.visibility=View.INVISIBLE
                fineDineFragment1.viewlunch.visibility=View.INVISIBLE
                fineDineFragment1. viewdinner.visibility=View.INVISIBLE*/
            }
        }

        else
        {
            Log.d("bnnnnb","jjkjkjk")
            if (list!![position].status.equals("closed")){
                Log.d("hdhdhjfhdfhdh","ncncncnc")
                holder.binding.day.setTextColor(Color.parseColor("#000000"))
                holder.binding.date.setTextColor(Color.parseColor("#000000"))
                holder.binding.relativeDaySlot.setBackgroundColor(Color.parseColor("#AEAEAE"))


            }else{
                holder.binding.relativeDaySlot.setBackgroundColor(Color.parseColor("#ffffff"))
                holder.binding.day.setTextColor(Color.parseColor("#707070"))
                holder.binding.date.setTextColor(Color.parseColor("#707070"))
                /* fineDineFragment1.recycler_time.visibility=View.GONE
                 fineDineFragment1.timetext.visibility=View.GONE
                 fineDineFragment1.viewbreakfast.visibility=View.INVISIBLE
                 fineDineFragment1. viewlunch.visibility=View.INVISIBLE
                 fineDineFragment1.viewdinner.visibility=View.INVISIBLE*/
            }

        }

    }


}
