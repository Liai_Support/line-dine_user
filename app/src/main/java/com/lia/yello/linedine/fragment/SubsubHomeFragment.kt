package com.lia.yello.linedine.fragment

import android.content.Intent
import android.graphics.Color
import android.net.Uri
import android.os.Bundle
import android.util.Log
import android.view.View
import android.widget.TextView
import androidx.activity.OnBackPressedCallback
import androidx.core.content.ContextCompat
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentTransaction
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.facebook.FacebookSdk.getApplicationContext
import com.lia.yello.linedine.Models.orderlist
import com.lia.yello.linedine.R
import com.lia.yello.linedine.Session.SharedHelper
import com.lia.yello.linedine.Utils.DialogUtils
import com.lia.yello.linedine.Utils.NetworkUtils
import com.lia.yello.linedine.Utils.UiUtils
import com.lia.yello.linedine.ViewModels.MapViewModel
import com.lia.yello.linedine.adapter.MenuScrollAdapter
import com.lia.yello.linedine.adapter.SubsubHomeAdapter
import com.lia.yello.linedine.databinding.*
import kotlinx.android.synthetic.main.carticon.*
import kotlinx.android.synthetic.main.fragment_subhome.recycler
import kotlinx.android.synthetic.main.fragment_subsubhome.*
import kotlinx.android.synthetic.main.header.*
import java.util.*


class SubsubHomeFragment : Fragment(R.layout.fragment_subsubhome) {

    var binding: FragmentSubsubhomeBinding? = null
    public var mapViewModel: MapViewModel? = null
    public var id:Int? = 0
    public var sharedHelper: SharedHelper? = null
    var lat:Double=0.0
    var lng:Double=0.0
    lateinit var adapter: RecyclerView.Adapter<RecyclerView.ViewHolder>
    lateinit var layoutManager: RecyclerView.LayoutManager
    var fav:Boolean = false
    private lateinit var fragmentTransaction: FragmentTransaction
    private var cartFragment: CartFragment? = null
    private var subHomeFragment: SubHomeFragment? = null
    private var favoritesFragment: FavoritesFragment? = null
    var subtotal: Double = 0.0
    var totalamt:Double=0.0
    var vat:Double=0.0
    var promocode=""
    public var cartlist: ArrayList<orderlist>? = null
  //  public var menuids: ArrayList<Int>? = null
    var menuids: MutableList<Int> = mutableListOf<Int>()
    var menuids2: MutableList<Int> = mutableListOf<Int>()
    var menuquantity: MutableList<Int> = mutableListOf<Int>()
    var menuquantity2: MutableList<Int> = mutableListOf<Int>()
    var resid2:Int=0
    var menuid2:Int=0
    var cost2:Double=0.0
    var resturantid:Int=0
     var txt:String=""
    var allseleted: Boolean = false
    var isclosed = false
    var ratingmenu:Double=0.0
    var imgid:Int=0
    var typee:String=""
    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        binding = DataBindingUtil.findBinding(view)
        sharedHelper = SharedHelper(requireContext())
        subtotal = 0.0
        totalamt = 0.0
        vat = 0.0
        mapViewModel = ViewModelProvider(this).get(MapViewModel::class.java)
        val head = view.findViewById<TextView>(R.id.head)
        head.setText(getString(R.string.menu))
        if (sharedHelper!!.language == "ar"){
            back.rotation= 180F
        }

        allseleted = true
        if (!NetworkUtils.isNetworkConnected(requireContext())) {
            UiUtils.showSnack(
                view, "no_internet_connection"
            )
        }

        if (sharedHelper!!.choose.equals("finedine")||sharedHelper!!.choose.equals("inqueue")|| sharedHelper!!.cartcount == 0){

            //var cartframe: FrameLayout =findViewById(R.id.cartframe)
            cartframe.visibility=View.GONE
        }else{
            cartframe.visibility=View.VISIBLE
        }

        id = arguments?.getInt("id")
        imgid = arguments?.getInt("imgid")!!
        typee = arguments?.getString("type")!!
        promocode = arguments?.getString("promocode")!!
        back.setOnClickListener(View.OnClickListener {
            //   requireActivity().supportFragmentManager.popBackStack()
             if(imgid==0){
                 favoritesFragment = FavoritesFragment()
                 fragmentTransaction = requireActivity().supportFragmentManager.beginTransaction()
                 fragmentTransaction.setCustomAnimations(R.anim.screen_in, R.anim.screen_out)
                 fragmentTransaction.replace(R.id.container, favoritesFragment!!)
                 fragmentTransaction.addToBackStack(null)
                 fragmentTransaction.commit()
             }else{
                 val bundle = Bundle()
                 bundle.putString("iscuisine", "false")
                 bundle.putInt("imgid", imgid)
                 bundle.putString("type", typee)
                 bundle.putString("promocode", promocode)



                 subHomeFragment = SubHomeFragment()
                 subHomeFragment!!.arguments = bundle
                 fragmentTransaction = requireActivity().supportFragmentManager.beginTransaction()
                 fragmentTransaction.setCustomAnimations(R.anim.screen_in, R.anim.screen_out)
                 fragmentTransaction.replace(R.id.container, subHomeFragment!!)
                 fragmentTransaction.addToBackStack(null)
                 fragmentTransaction.commit()
             }


        })

        Log.d("asdfghjk,", "" + id)
        cart_badge.text = ""+ sharedHelper!!.cartcount

        layoutManager= LinearLayoutManager(requireContext(), LinearLayoutManager.HORIZONTAL, false)
        recycler_lunch.layoutManager =layoutManager
        recycler_lunch.adapter = MenuScrollAdapter(this)
        getcartlist()

        all.backgroundTintList = ContextCompat.getColorStateList(
            getApplicationContext(),
            R.color.colorPrimary
        )
        all.setTextColor(Color.WHITE)
        all.background = ContextCompat.getDrawable(
            getApplicationContext(),
            R.drawable.border_layoutred
        )
        all.setOnClickListener(View.OnClickListener {
            // all.setBackgroundResource(R.drawable.border_layout)
            allseleted = true
            all.backgroundTintList = ContextCompat.getColorStateList(
                getApplicationContext(),
                R.color.colorPrimary
            )
            all.setTextColor(Color.WHITE)
            all.background = ContextCompat.getDrawable(
                getApplicationContext(),
                R.drawable.border_layoutred
            )
            getvalues("All")
            layoutManager = LinearLayoutManager(
                requireContext(),
                LinearLayoutManager.HORIZONTAL,
                false
            )
            recycler_lunch.layoutManager = layoutManager
            recycler_lunch.adapter = MenuScrollAdapter(this)
        })

            location.setOnClickListener(View.OnClickListener {
                val uri = String.format(Locale.ENGLISH,
                "google.navigation:q=$lat,$lng")
                 val gmmIntentUri = Uri.parse(uri)
                  val mapIntent = Intent(Intent.ACTION_VIEW, gmmIntentUri)
                   mapIntent.setPackage("com.google.android.apps.maps")
                   requireActivity().startActivity(mapIntent)
        })


        activity?.onBackPressedDispatcher?.addCallback(this, object : OnBackPressedCallback(true) {
            override fun handleOnBackPressed() {

                if(imgid==0){
                    favoritesFragment = FavoritesFragment()
                    fragmentTransaction = requireActivity().supportFragmentManager.beginTransaction()
                    fragmentTransaction.setCustomAnimations(R.anim.screen_in, R.anim.screen_out)
                    fragmentTransaction.replace(R.id.container, favoritesFragment!!)
                    fragmentTransaction.addToBackStack(null)
                    fragmentTransaction.commit()
                }else{
                    val bundle = Bundle()
                    bundle.putString("iscuisine", "false")
                    bundle.putInt("imgid", imgid)
                    bundle.putString("type", typee)
                    bundle.putString("promocode", promocode)


                    subHomeFragment = SubHomeFragment()
                    subHomeFragment!!.arguments = bundle
                    fragmentTransaction = requireActivity().supportFragmentManager.beginTransaction()
                    fragmentTransaction.setCustomAnimations(R.anim.screen_in, R.anim.screen_out)
                    fragmentTransaction.replace(R.id.container, subHomeFragment!!)
                    fragmentTransaction.addToBackStack(null)
                    fragmentTransaction.commit()
                }

/*
                val bundle = Bundle()
                bundle.putString("iscuisine", "false")
                bundle.putInt("imgid", imgid)
                bundle.putString("type", typee)


                subHomeFragment = SubHomeFragment()
                subHomeFragment!!.arguments = bundle
                fragmentTransaction = requireActivity().supportFragmentManager.beginTransaction()
                fragmentTransaction.setCustomAnimations(R.anim.screen_in, R.anim.screen_out)
                fragmentTransaction.replace(R.id.container, subHomeFragment!!)
                fragmentTransaction.addToBackStack(null)
                fragmentTransaction.commit()*/
            }
        })

    }

    public fun getvalues(txt: String){
        DialogUtils.showLoader(requireContext())
        mapViewModel?.getMenuDetail(requireContext(), id!!, txt)
            ?.observe(viewLifecycleOwner,
                Observer {
                    DialogUtils.dismissLoader()

                    it?.let {
                        it.error?.let { error ->
                            if (error) {
                                it.message?.let { msg ->
                                    UiUtils.showSnack(root_subsubhome, msg)
                                    recycler.visibility = View.GONE

                                    //  Toast.makeText(context, msg, Toast.LENGTH_SHORT).show()

                                }
                            } else {
                                recycler.visibility = View.VISIBLE
                                it.message?.let { msg ->
                                    // UiUtils.showSnack(root_subsubhome, msg)

                                    // Toast.makeText(context, msg, Toast.LENGTH_SHORT).show()
                                }

                                it.data?.let { data ->
                                    Log.d("asxdcfvgb", "" + data.restdetail)
                                    Log.d("asxdcfvgb", "" + data.data2)
                                    Log.d("asxdcfvgb", "" + data.menudetail)
                                    data.restdetail?.res_name?.let { menuname.text = it }
                                    sharedHelper?.vat=data.restdetail?.vat!!.toFloat()
                                    Log.d("kanikasjhajaj",""+sharedHelper?.vat)
                                             if( data.restdetail!!.rating.equals("")){
                                                 rating.text = "0"+"/" + "5"

                                             }else{
                                                 rating.text = data.restdetail!!.rating+"/" + "5"

                                             }



                                    data.restdetail?.res_name?.let { head.text = it }
                                    data.restdetail?.description?.let { menu_description.text = it }
                                    if (data.data2!![0].closed.equals("0")) {
                                        recycler.visibility = View.VISIBLE
                                        if (data.data2!!.size != 0) {

                                            var time1: String? = data.data2!![0].openingTime
                                            var time2: String? = data.data2!![0].closingTime
                                            var time3: String? = " " + time1 + " - " + time2 + " "
                                            Log.d("sdfghjkl", "" + time3)
                                            work.visibility = View.VISIBLE
                                            time.text = time3
                                        } else {
                                            time.text = getString(R.string.invalidtime)
                                        }
                                    } else {
                                        work.visibility = View.GONE
                                        time.text = getString(R.string.closed)
                                        isclosed = true
                                        //recycler.visibility=View.GONE

                                    }

                                    lat = data.restdetail?.lat!!
                                    lng = data.restdetail?.lng!!
                                    data.restdetail?.bannerimg?.let {
                                        UiUtils.loadImage(
                                            bannerimg,
                                            it,
                                            ContextCompat.getDrawable(
                                                requireContext(),
                                                R.drawable.maskgroup46
                                            )!!
                                        )
                                    }

                                    recycler.layoutManager =
                                        GridLayoutManager(
                                            context,
                                            2
                                        )
                                    recycler.adapter = SubsubHomeAdapter(
                                        this,
                                        requireContext(), data.menudetail, isclosed,menuquantity2,menuids2
                                    )

                                }
                            }
                        }

                    }
                })
    }

    public fun getaddcart() {
        DialogUtils.showLoader(requireContext())
        menuid2?.let {
            cost2?.let { it1 ->
                resid2?.let { it2 ->
                    mapViewModel?.getAddCart(requireContext(), it, 1, it1, it2,"")
                        ?.observe(viewLifecycleOwner,
                            Observer {
                                DialogUtils.dismissLoader()

                                it?.let {
                                    it.error?.let { error ->
                                        if (error) {
                                            it.message?.let { msg ->
                                                UiUtils.showSnack(root_subsubhome, msg)
                                                // Toast.makeText(requireContext(), msg, Toast.LENGTH_SHORT).show()
                                            }
                                        } else {
                                            it.message?.let { msg ->
                                                //  UiUtils.showSnack(root_subsubhome,msg)
                                                subtotal = 0.0
                                                totalamt = 0.0
                                                vat = 0.0
                                                // Toast.makeText(requireContext(), msg, Toast.LENGTH_SHORT).show()
                                                getcartlist()
                                                /*          viewurcart.setVisibility(View.VISIBLE)
                                                          viewurcart.setOnClickListener{
                                                              subOrderFragment = SubOrderFragment()
                                                              fragmentTransaction = requireActivity().supportFragmentManager.beginTransaction()
                                                              fragmentTransaction.setCustomAnimations(R.anim.screen_in,R.anim.screen_out)
                                                              fragmentTransaction.replace(R.id.container, subOrderFragment!!)
                                                              fragmentTransaction.addToBackStack(null)
                                                              fragmentTransaction.commit()

                                                              Log.d("zxcvfgbnm,",""+msg)
          */
                                            }


                                        }


                                    }
                                }


                            })
                }
            }
        }
    }

    public fun getcartlist(){
        DialogUtils.showLoader(requireContext())

        mapViewModel?.getorderlist(requireContext())
            ?.observe(viewLifecycleOwner,
                Observer {
                    DialogUtils.dismissLoader()
                    menuids.clear()
                    menuids2.clear()
                    menuquantity.clear()
                    menuquantity2.clear()
                    it?.let {
                        it.error?.let { error ->
                            if (error) {
                                it.message?.let { msg ->
                                    Log.d("cvnjcn", "cbhxv")
                                    // UiUtils.showSnack(root_subsubhome, msg)
                                    cart_badge.text = "0"
                                    sharedHelper!!.cartcount = 0

                                    cartframe.visibility=View.GONE
                                    Log.d("ordersize",""+ sharedHelper!!.cartcount)
                                    getvalues("All")

                                    // snackbarToast(msg)
                                }
                            } else {
                                it.data?.let { data ->
                                    if (data.orderdata!!.size != 0) {

                                        for(i in 0 until data.orderdata!!.size){

                                            menuids!!.add(data.orderdata!![i].menu_id!!)
                                            menuids2=menuids.distinct().toMutableList()
                                            menuquantity!!.add(data.orderdata!![i].quantity!!)
                                           // menuquantity2=menuquantity.distinct().toMutableList()
                                            menuquantity2=menuquantity
                                            Log.d("menuids",""+menuids2)
                                            Log.d("menuquantity",""+menuquantity2)
                                        }

                                        Log.d("arraylist",""+menuids)
                                        Log.d("menuquantity",""+menuquantity)
                                        sharedHelper!!.cartcount = data.orderdata?.size!!
                                        Log.d("ordersize",""+ sharedHelper!!.cartcount)
                                        cart_badge.text = "" + data.orderdata?.size
                                        cartlist = data.orderdata
                                        Log.d("resturantid", "" + resturantid)

                                        for (i in 0 until data.orderdata!!.size) {

                                            if (data.orderdata!![i].menuavilable==1){
                                                subtotal = subtotal + (data.orderdata!![i].cost!! * data.orderdata!![i].quantity!!)
                                            }

                                        }
                                        vat = (subtotal / 100.0f) * sharedHelper!!.vat
                                        totalamt = subtotal + vat


                                        viewcart.visibility = View.VISIBLE
                                        Log.d("yhdxgyusdxusydx",""+subtotal)

                                        /*txt_price_subsubhome!!.text = "$subtotal SAR"*/
                                        txt_price_subsubhome!!.text= String.format(Locale("en", "US"),"%.2f",  subtotal)+ " SAR"

                                        viewcart.setOnClickListener {


                                            val bundle = Bundle()

                                            bundle.putString("promocode", promocode)

                                            cartFragment = CartFragment()
                                            cartFragment!!.arguments = bundle

                                            fragmentTransaction = requireActivity().supportFragmentManager.beginTransaction()
                                            fragmentTransaction.setCustomAnimations(
                                                R.anim.screen_in,
                                                R.anim.screen_out
                                            )
                                            fragmentTransaction.replace(
                                                R.id.container,
                                                cartFragment!!
                                            )
                                            fragmentTransaction.addToBackStack(null)
                                            fragmentTransaction.commit()


                                        }
                                        // var list: ArrayList<orderlist>? = data.orderdata
                                    }
                                    getvalues("All")
                                }
                            }
                        }

                    }
                })



    }

    public fun getdeleteall(){
        DialogUtils.showLoader(requireContext())

        mapViewModel?.getdeleteall(requireContext())
            ?.observe(viewLifecycleOwner,
                Observer {
                    DialogUtils.dismissLoader()

                    it?.let {
                        it.error?.let { error ->
                            if (error) {
                                it.message?.let { msg ->
                                    UiUtils.showSnack(root_subsubhome, msg)
                                    // snackbarToast(msg)
                                }
                            } else {
                                it.message?.let { msg ->
                                    getaddcart()
                                    UiUtils.showSnack(root_subsubhome, msg)
                                    // snackbarToast(msg)
                                }
                            }
                        }

                    }
                })



    }

}


