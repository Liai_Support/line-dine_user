package com.lia.yello.linedine.fragment

import android.content.Intent
import android.graphics.Color
import android.net.Uri
import android.os.Bundle
import android.util.Log
import android.view.View
import android.widget.TextView
import androidx.activity.OnBackPressedCallback
import androidx.core.content.ContextCompat
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentTransaction
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.facebook.FacebookSdk
import com.fasterxml.jackson.module.kotlin.jacksonObjectMapper
import com.google.android.material.snackbar.BaseTransientBottomBar
import com.google.android.material.snackbar.Snackbar
import com.lia.yello.linedine.Models.MenuDetailJson
import com.lia.yello.linedine.R
import com.lia.yello.linedine.Session.SharedHelper
import com.lia.yello.linedine.Utils.DialogUtils
import com.lia.yello.linedine.Utils.NetworkUtils
import com.lia.yello.linedine.Utils.UiUtils
import com.lia.yello.linedine.ViewModels.MapViewModel
import com.lia.yello.linedine.adapter.FineDineMenuListAdapter
import com.lia.yello.linedine.adapter.MenuScrollFineDineAdapter
import com.lia.yello.linedine.databinding.FragmentFineDineMenuListBinding
import kotlinx.android.synthetic.main.carticon.*
import kotlinx.android.synthetic.main.fragment_fine_dine_menu_list.*
import kotlinx.android.synthetic.main.fragment_subsubhome.*
import kotlinx.android.synthetic.main.fragment_subsubhome.all
import kotlinx.android.synthetic.main.fragment_subsubhome.location
import kotlinx.android.synthetic.main.header.*
import org.json.JSONArray
import java.util.*
import kotlin.collections.ArrayList


class FineDineMenuListFragment : Fragment(R.layout.fragment_fine_dine_menu_list) {

    var binding: FragmentFineDineMenuListBinding? = null
    private var fineDineOrderListStatus: FineDineOrderListStatusFragment? = null
    public var mapViewModel: MapViewModel? = null
    public var resid: Int? = 0
    public var orderid: Int? = 0
    public var sharedHelper: SharedHelper? = null
    val mapper = jacksonObjectMapper()
    var homeFragment:HomeFragment?=null
    var myOrderFragment:MyOrderFragment?=null
    var lat: Double = 0.0
    var lng: Double = 0.0
    lateinit var adapter: RecyclerView.Adapter<RecyclerView.ViewHolder>
    lateinit var layoutManager: RecyclerView.LayoutManager
    private lateinit var fragmentTransaction: FragmentTransaction
    var fav: Boolean = false
    var subtotal: Double = 0.0
    var vat: Double = 0.0
    var menuids: MutableList<Int> = mutableListOf<Int>()
    var menuids2: MutableList<Int> = mutableListOf<Int>()
    var allseleted: Boolean = false
    var isclosed = false
    // val menulist = mutableListOf<User>()
    var menulist:ArrayList<MenuDetailJson>? = ArrayList<MenuDetailJson>()
     var backres:String?=""



    var inqueue:String=""

    var totalamt:Double=0.0
    var paytotal:Double=0.0
    var status:String=""
    var hotelname:String=""
    var resimage:String=""
    public var myLat: Double? = null
    public var myLng: Double? = null
    public var paymenttype: String? = ""
    public var statuspaid: String? = ""
    public var isnewinqueue: String? = ""

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        binding = DataBindingUtil.findBinding(view)
        sharedHelper = SharedHelper(requireContext())
        mapViewModel = ViewModelProvider(this).get(MapViewModel::class.java)
        val head = view.findViewById<TextView>(R.id.head)
        head.setText(getString(R.string.menu))
        if (!NetworkUtils.isNetworkConnected(requireContext())) {
            UiUtils.showSnack(
                view, "no_internet_connection"
            )
        }

        if (sharedHelper!!.language == "ar"){
            back.rotation= 180F
        }
        if (sharedHelper!!.choose.equals("finedine")||sharedHelper!!.choose.equals("inqueue")|| sharedHelper!!.cartcount == 0){

            //var cartframe: FrameLayout =findViewById(R.id.cartframe)
            cartframe.visibility=View.GONE
        }


        backres = arguments?.getString("isfd")
        orderid = arguments?.getInt("orderid")!!
        resid = arguments?.getInt("resid")!!
        status = arguments?.getString("status")!!
        statuspaid = arguments?.getString("paidstatus")!!
        resimage = arguments?.getString("resimg")!!
        hotelname = arguments?.getString("resname")!!
        myLat = arguments?.getDouble("lat")!!
        myLng = arguments?.getDouble("lng")!!
        paymenttype = arguments?.getString("paymenttype")!!
        isnewinqueue = arguments?.getString("isnewinqueue")!!
        inqueue = arguments?.getString("inqueue")!!

        Log.d("rtyui",status)
        back.setOnClickListener(View.OnClickListener {


            if (backres!!.equals("true")){
                homeFragment = HomeFragment()
                fragmentTransaction =requireActivity().supportFragmentManager.beginTransaction()
                fragmentTransaction.setCustomAnimations(R.anim.screen_in, R.anim.screen_out)
                fragmentTransaction.replace(R.id.container, homeFragment!!)
                fragmentTransaction.addToBackStack(null)
                fragmentTransaction.commit()
            }else{
                val bundle = Bundle()
                bundle.putInt("orderid",orderid!!)
                bundle.putString("status",status )
                bundle.putString("resname",hotelname)
                bundle.putString("resimg",resimage)
                bundle.putInt("resid", resid!!)
                bundle.putDouble("lat", myLat!!)
                bundle.putDouble("lng",myLng!!)
                bundle.putString("paidstatus",statuspaid)
                bundle.putString("paymenttype",paymenttype)
                fineDineOrderListStatus = FineDineOrderListStatusFragment()
                fineDineOrderListStatus!!.arguments = bundle
                fragmentTransaction = requireActivity().supportFragmentManager.beginTransaction()
                fragmentTransaction.setCustomAnimations(R.anim.screen_in, R.anim.screen_out)
                fragmentTransaction.replace(R.id.container, fineDineOrderListStatus!!)
                fragmentTransaction.addToBackStack(null)
                fragmentTransaction.commit()
              /*  myOrderFragment = MyOrderFragment()
                //    myOrderFragment!!.arguments = bundle
                fragmentTransaction = requireActivity().supportFragmentManager.beginTransaction()
                fragmentTransaction.setCustomAnimations(R.anim.screen_in, R.anim.screen_out)
                fragmentTransaction.replace(R.id.container, myOrderFragment!!)
                fragmentTransaction.addToBackStack(null)
                fragmentTransaction.commit()*/
            }
       /*    */
        })
        activity?.onBackPressedDispatcher?.addCallback(this, object : OnBackPressedCallback(true) {
            override fun handleOnBackPressed() {
                isEnabled = false


                if (backres!!.equals("true")) {
                    homeFragment = HomeFragment()
                    fragmentTransaction =
                        requireActivity().supportFragmentManager.beginTransaction()
                    fragmentTransaction.setCustomAnimations(R.anim.screen_in, R.anim.screen_out)
                    fragmentTransaction.replace(R.id.container, homeFragment!!)
                    fragmentTransaction.addToBackStack(null)
                    fragmentTransaction.commit()
                } else {
                    val bundle = Bundle()
                    bundle.putInt("orderid", orderid!!)
                    bundle.putString("status", status)
                    bundle.putString("resname", hotelname)
                    bundle.putString("resimg", resimage)
                    bundle.putInt("resid", resid!!)
                    bundle.putDouble("lat", myLat!!)
                    bundle.putDouble("lng", myLng!!)
                    bundle.putString("paidstatus", statuspaid)
                    bundle.putString("paymenttype", paymenttype)
                    fineDineOrderListStatus = FineDineOrderListStatusFragment()
                    fineDineOrderListStatus!!.arguments = bundle
                    fragmentTransaction = requireActivity().supportFragmentManager.beginTransaction()
                    fragmentTransaction.setCustomAnimations(R.anim.screen_in, R.anim.screen_out)
                    fragmentTransaction.replace(R.id.container, fineDineOrderListStatus!!)
                    fragmentTransaction.addToBackStack(null)
                    fragmentTransaction.commit()
                    /*               homeFragment = HomeFragment()
                fragmentTransaction =requireActivity().supportFragmentManager.beginTransaction()
                fragmentTransaction.setCustomAnimations(R.anim.screen_in, R.anim.screen_out)
                fragmentTransaction.replace(R.id.container, homeFragment!!)
                fragmentTransaction.addToBackStack(null)
                fragmentTransaction.commit()*/
                }
            }
        })

/*        resid = arguments?.getInt("resid")
        orderid = arguments?.getInt("orderid")*/


        Log.d("asdfghjk",""+orderid)
        if(arguments?.getSerializable("menulist")!= null){
            menulist = arguments?.getSerializable("menulist") as ArrayList<MenuDetailJson>
            Log.d("zsxdcfvgbnhmjj,.",""+menulist)
        }


        getvalues("All")

        cart_badge.text = "" + sharedHelper!!.cartcount

        layoutManager = LinearLayoutManager(requireContext(), LinearLayoutManager.HORIZONTAL, false)
        finedinemenu_descriptionrecycler_lunch.layoutManager = layoutManager
        finedinemenu_descriptionrecycler_lunch.adapter = MenuScrollFineDineAdapter(this)

        all.backgroundTintList = ContextCompat.getColorStateList(
            FacebookSdk.getApplicationContext(),
            R.color.colorPrimary
        )
        all.setTextColor(Color.WHITE)
        all.background = ContextCompat.getDrawable(
            FacebookSdk.getApplicationContext(),
            R.drawable.border_layout
        )

        all.setOnClickListener(View.OnClickListener {
            // all.setBackgroundResource(R.drawable.border_layout)
            allseleted = true
            all.backgroundTintList = ContextCompat.getColorStateList(
                FacebookSdk.getApplicationContext(),
                R.color.colorPrimary
            )
            all.setTextColor(Color.WHITE)
            all.background = ContextCompat.getDrawable(
                FacebookSdk.getApplicationContext(),
                R.drawable.border_layout
            )
            getvalues("All")
            layoutManager = LinearLayoutManager(
                requireContext(),
                LinearLayoutManager.HORIZONTAL,
                false
            )
            finedinemenu_descriptionrecycler_lunch.layoutManager = layoutManager
            finedinemenu_descriptionrecycler_lunch.adapter = MenuScrollFineDineAdapter(this)
        })

        location.setOnClickListener(View.OnClickListener {
            val uri = String.format(
                Locale.ENGLISH,
                "google.navigation:q=$lat,$lng"
            )
            val gmmIntentUri = Uri.parse(uri)
            val mapIntent = Intent(Intent.ACTION_VIEW, gmmIntentUri)
            mapIntent.setPackage("com.google.android.apps.maps")
            requireActivity().startActivity(mapIntent)
        })


        bookmenu.setOnClickListener(View.OnClickListener {
            if(menulist!!.isEmpty()==false && menulist!!.size!=0){
                val jsonArray = mapper.writeValueAsString(menulist)
                Log.d("chvb",""+jsonArray)
                getbookingtable(orderid!!,jsonArray )
            }
            else{
                UiUtils.showSnack(root_finedinemenu, getString(R.string.addsomemenu))

            }
        })
        val jsonArray = mapper.writeValueAsString(menulist)
        Log.d("chvb",""+jsonArray)

        /*  val user = User(102, 100)
        val userJson = mapper.writeValueAsString(user)
        println(userJson)*/

        /* val userList = mutableListOf<User>()
         userList.add(User(102, 54))
         userList.add(User(103, 76))
         val jsonArray = mapper.writeValueAsString(userList)
         println(jsonArray)*/


        /*  val userListFromJson: List<User> = mapper.readValue(jsonArray)
          val userList1 = mutableListOf<User>()
          userList1.clear()
          for (i in 0 until userListFromJson.size) {
              if (userListFromJson.get(i).menuid == 103) {
                  userList1.add(User(105, 55))
              } else {
                  userList1.add(User(userListFromJson.get(i).menuid, userListFromJson.get(i).qty))
              }
          }
          val jsonArray4 = mapper.writeValueAsString(userList1)
          println(jsonArray4)*/


        /*     val list = ArrayList<String?>()
             val jsonObject = null
             val jsonArray1 = jsonObject as JSONArray?
             val len = jsonArray!!.length
             if (jsonArray1 != null) {
                 for (i in 0 until len) {
                     list.add(jsonArray1[i].toString())
                     list.removeAt(i)
                 }
             }
             val jsArray = JSONArray(list)*/

    }


    /*  data class User (
        val menuid: Int,
        val qty: Int
    )*/


    public fun getvalues(txt: String) {
        DialogUtils.showLoader(requireContext())
        mapViewModel?.getMenuDetail(requireContext(), resid!!, txt)
            ?.observe(viewLifecycleOwner, androidx.lifecycle.Observer {
                DialogUtils.dismissLoader()

                it?.let {
                    it.error?.let { error ->
                        if (error) {
                            it.message?.let { msg ->
                                UiUtils.showSnack(root_finedinemenu, msg)
                                finedinemenu_descriptionrecycler.visibility = View.GONE
                            }
                        } else {
                            finedinemenu_descriptionrecycler.visibility = View.VISIBLE
                            it.message?.let { msg ->

                            }
                            it.data?.let { data ->


                                Log.d("asxdcfvgb", "" + data.restdetail)
                                Log.d("asxdcfvgb", "" + data.data2)
                                Log.d("asxdcfvgb", "" + data.menudetail)
                                data.restdetail?.res_name?.let { finedinemenuname.text = it }

                                if (data.restdetail!!.rating.equals("")){
                                    finedinemenurating.text = "0"+ "/" + "5"

                                }else{
                                    finedinemenurating.text = data.restdetail!!.rating + "/" + "5"

                                }


                                //  data.restdetail?.rating?.let { rating.text = it }
                                data.restdetail?.res_name?.let { head.text = it }
                                data.restdetail?.description?.let {
                                    finedinemenu_description.text = it
                                }
                                if (data.data2!![0].closed.equals("0")) {
                                    finedinemenu_descriptionrecycler.visibility = View.VISIBLE
                                    if (data.data2!!.size != 0) {

                                        var time1: String? = data.data2!![0].openingTime
                                        var time2: String? = data.data2!![0].closingTime
                                        var time3: String? = " " + time1 + " - " + time2 + " "
                                        Log.d("sdfghjkl", "" + time3)
                                        finedinemenu_descriptionwork.visibility = View.VISIBLE
                                        finedinemenu_descriptiontime.text = time3
                                    } else {
                                        time.text = getString(R.string.invalidtime)
                                    }
                                } else {
                                    finedinemenu_descriptionwork.visibility = View.GONE
                                    finedinemenu_descriptiontime.text = getString(R.string.closed)
                                    isclosed = true

                                }

                                lat = data.restdetail?.lat!!
                                lng = data.restdetail?.lng!!


                                data.restdetail?.bannerimg?.let {
                                    UiUtils.loadImage(
                                        finedinemenubannerimg,
                                        it,
                                        ContextCompat.getDrawable(
                                            requireContext(),
                                            R.drawable.maskgroup46
                                        )!!
                                    )
                                }

                                finedinemenu_descriptionrecycler.layoutManager =
                                    GridLayoutManager(
                                        context,
                                        2
                                    )
                                finedinemenu_descriptionrecycler.adapter = FineDineMenuListAdapter(
                                    this,
                                    requireContext(),
                                    data.menudetail,
                                    isclosed,
                                    menuids2
                                )

                            }
                        }
                    }

                }

            })
    }

    public fun getbookingtable(orderid: Int, jsonArray: String) {
        DialogUtils.showLoader(requireContext())
        mapViewModel?.gettablebookingmenu(requireContext(), orderid, jsonArray)
            ?.observe(viewLifecycleOwner, androidx.lifecycle.Observer {
                DialogUtils.dismissLoader()

                it?.let {
                    it.error?.let { error ->
                        if (error) {
                            it.message?.let { msg ->

                                UiUtils.showSnack(root_finedinemenu, msg)
                            }
                        } else {
                            it.message?.let { msg ->
                                UiUtils.showSnack(root_finedinemenu, msg)
                                homeFragment = HomeFragment()
                                fragmentTransaction = requireActivity().supportFragmentManager.beginTransaction()
                                fragmentTransaction.setCustomAnimations(R.anim.screen_in,R.anim.screen_out)
                                fragmentTransaction.replace(R.id.container, homeFragment!!)
                                fragmentTransaction.commit()
                                

                            }
                        }

                    }
                }
            })


    }


    public fun snackbarToast(msg: String) {
        var snackbar: Snackbar
        Snackbar.make(view!!, msg, Snackbar.LENGTH_SHORT)
            .setTextColor(ContextCompat.getColor(context!!, R.color.design_default_color_error))
            .setBackgroundTint(ContextCompat.getColor(context!!, R.color.white))
            .setDuration(BaseTransientBottomBar.LENGTH_LONG)
            .setAnimationMode(BaseTransientBottomBar.ANIMATION_MODE_SLIDE)
            .show()
    }
}