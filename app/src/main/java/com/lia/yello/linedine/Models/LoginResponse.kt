package com.lia.yello.linedine.activity

import com.google.gson.annotations.SerializedName
import java.io.Serializable

class LoginResponse : Serializable {

    @SerializedName("error")
    var error: Boolean? = null

    @SerializedName("message")
    var message: String? = null


    @SerializedName("data")
    var data: LoginData? = null


}


class LoginData : Serializable {

    @SerializedName("id")
    var id: Int? = null

    @SerializedName("name")
    var name: String? = null

    @SerializedName("email")
    var email: String? = null

    @SerializedName("phone")
    var mobileNumber: String? = null

    @SerializedName("accessToken")
    var token: String? = null

    @SerializedName("loginType")
    var loginType: String? = null

    @SerializedName("countrycode")
    var countryCode: String? = null

    @SerializedName("profilePic")
    var profilePic: String? = null


}