package com.lia.yello.linedine.Models

import com.google.gson.annotations.SerializedName
import java.io.Serializable

class AddAdressResponse : Serializable {

    @SerializedName("error")
    var error: Boolean? = null

    @SerializedName("message")
    var message: String? = null

    @SerializedName("data")
    var data: AddressData? = null
}

class AddressData : Serializable {

    @SerializedName("addressid")
    var id: Int? = null

}