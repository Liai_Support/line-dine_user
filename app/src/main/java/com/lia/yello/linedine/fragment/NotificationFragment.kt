package com.lia.yello.linedine.fragment


import android.content.Intent
import android.os.Bundle
import android.util.Log
import android.view.View
import android.widget.TextView
import androidx.activity.OnBackPressedCallback
import androidx.core.content.ContextCompat
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.LinearLayoutManager
import com.google.android.material.bottomsheet.BottomSheetBehavior
import com.google.android.material.snackbar.BaseTransientBottomBar
import com.google.android.material.snackbar.Snackbar
import com.lia.yello.linedine.R
import com.lia.yello.linedine.Session.SharedHelper
import com.lia.yello.linedine.Utils.DialogUtils
import com.lia.yello.linedine.Utils.NetworkUtils
import com.lia.yello.linedine.Utils.UiUtils
import com.lia.yello.linedine.ViewModels.MapViewModel
import com.lia.yello.linedine.activity.DashBoardActivity
import com.lia.yello.linedine.adapter.ListNotiAdapter
import com.lia.yello.linedine.databinding.FragmentNotificationBinding
import kotlinx.android.synthetic.main.carticon.*
import kotlinx.android.synthetic.main.fragment_fine_dine_order_list_status.*
import kotlinx.android.synthetic.main.fragment_notification.*
import kotlinx.android.synthetic.main.header.*

class NotificationFragment : Fragment(R.layout.fragment_notification) {

        var binding: FragmentNotificationBinding? = null;
        public var mapViewModel: MapViewModel? = null
        public var sharedHelper: SharedHelper? = null
        override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        binding = DataBindingUtil.findBinding(view)
        sharedHelper = SharedHelper(requireContext())
        sharedHelper = SharedHelper(requireContext())
        (activity as DashBoardActivity?)!!.mBottomSheetBehavior2?.setState(BottomSheetBehavior.STATE_HIDDEN)
        (activity as DashBoardActivity?)!!.mBottomSheetBehavior?.setState(BottomSheetBehavior.STATE_HIDDEN)
        val head = view.findViewById<TextView>(R.id.head)
        mapViewModel = ViewModelProvider(this).get(MapViewModel::class.java)


            if (sharedHelper!!.language == "ar"){
                back.rotation= 180F
            }
        back.setOnClickListener(View.OnClickListener {
           // requireActivity().supportFragmentManager.popBackStack()
            val intent = Intent(requireContext(), DashBoardActivity::class.java)
            startActivity(intent)
        })
        head.setText(getString(R.string.notification))
        cart_badge.text = ""+ sharedHelper!!.cartcount
            if (!NetworkUtils.isNetworkConnected(requireContext())) {
                UiUtils.showSnack(
                    view, "no_internet_connection"
                )
            }
        getvalues()

        (activity as DashBoardActivity?)!!.mBottomSheetBehavior2?.setState(BottomSheetBehavior.STATE_HIDDEN)
        (activity as DashBoardActivity?)!!.mBottomSheetBehavior?.setState(BottomSheetBehavior.STATE_HIDDEN)


        clear_notification.setOnClickListener(View.OnClickListener {
            getclearnoti()
        })

            if (sharedHelper!!.choose.equals("finedine")||sharedHelper!!.choose.equals("inqueue")|| sharedHelper!!.cartcount == 0){

                //var cartframe: FrameLayout =findViewById(R.id.cartframe)
                cartframe.visibility=View.GONE
            }


            activity?.onBackPressedDispatcher?.addCallback(this, object : OnBackPressedCallback(true) {
                override fun handleOnBackPressed() {
                    // this for single back press
                    // isEnabled = false
                    val intent = Intent(requireContext(), DashBoardActivity::class.java)
                    startActivity(intent)
                }
            })

        }


    private fun getvalues() {
        DialogUtils.showLoader(requireContext())

        mapViewModel?.listnotification(requireContext())
            ?.observe(viewLifecycleOwner,
                Observer {
                    DialogUtils.dismissLoader()

                    it?.let {
                        it.error?.let { error ->
                            if (error) {
                                Log.d("wdfvgbn", "wedrfgthjk")
                                nonotify.visibility = View.VISIBLE
                                clear_notification.visibility = View.GONE
                                listnotification_recycler.visibility=View.GONE

                            } else {

                                it.data?.let { data ->

                                    if (data.notification_data?.size == 0) {

                                        nonotify.visibility = View.VISIBLE
                                        clear_notification.visibility = View.GONE

                                    } else {
                                        listnotification_recycler.visibility=View.VISIBLE
                                        clear_notification.visibility = View.VISIBLE
                                        nonotify.visibility = View.GONE
                                        listnotification_recycler.layoutManager =
                                            LinearLayoutManager(
                                                context)
                                        listnotification_recycler.adapter = ListNotiAdapter(
                                            this,
                                            requireContext(),
                                            data.notification_data!!
                                        )

                                    }
                                }
                            }
                        }

                    }
                })
    }

    private fun getclearnoti(){
        DialogUtils.showLoader(requireContext())

        mapViewModel?.clearnoti(requireContext())
            ?.observe(viewLifecycleOwner,
                Observer {
                    DialogUtils.dismissLoader()

                    it?.let {
                        it.error?.let { error ->
                            if (error) {
                                it.message?.let { msg ->
                                    UiUtils.showSnack(root_notification, msg)
                                    //snackbarToast(getString(R.string.notificationnotcleared),view)


                                }
                            } else {
                                it.message?.let { msg ->
                                    getvalues()
                                    Log.d("message", msg)
                                }

                            }
                        }
                    }

                })
    }

    fun snackbarToast(msg: String?, view: View?) {
        Snackbar.make(view!!, msg!!, Snackbar.LENGTH_SHORT)
            .setTextColor(ContextCompat.getColor(context!!, R.color.design_default_color_error))
            .setBackgroundTint(ContextCompat.getColor(context!!, R.color.white))
            .setDuration(BaseTransientBottomBar.LENGTH_LONG)
            .setAnimationMode(BaseTransientBottomBar.ANIMATION_MODE_SLIDE)
            .show()
    }



    public fun getcomfirmbooking() {
        DialogUtils.showLoader(requireContext())
        mapViewModel?.getcomfirmbooking(requireContext(), 1, 50.0, "booked", "card","","")
            ?.observe(viewLifecycleOwner, androidx.lifecycle.Observer {
                DialogUtils.dismissLoader()

                it?.let {
                    it.error?.let { error ->
                        if (error) {
                            it.message?.let { msg ->

                                UiUtils.showSnack(root_notification, msg)
                            }
                        } else {
                            it.message?.let { msg ->
                                UiUtils.showSnack(root_notification, msg)


                            }
                        }

                    }
                }
            })


    }

}



