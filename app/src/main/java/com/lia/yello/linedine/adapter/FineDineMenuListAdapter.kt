package com.lia.yello.linedine.adapter
import android.content.Context
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.FragmentTransaction
import androidx.recyclerview.widget.RecyclerView
import com.lia.yello.linedine.Models.MenuDetailJson
import com.lia.yello.linedine.Models.menudetails
import com.lia.yello.linedine.R
import com.lia.yello.linedine.Utils.UiUtils
import com.lia.yello.linedine.databinding.FinedinemenuBinding
import com.lia.yello.linedine.fragment.FineDineItemDetailFragment
import com.lia.yello.linedine.fragment.FineDineMenuListFragment
import kotlinx.android.synthetic.main.fragment_fine_dine_menu_list.*
import kotlinx.android.synthetic.main.fragment_subsubhome.*
import java.io.Serializable
import java.util.*
import kotlin.collections.ArrayList

class FineDineMenuListAdapter(
    var fineDineMenuListFragment: FineDineMenuListFragment,
    var context: Context,
    var list: ArrayList<menudetails>?,
    var iscolsed: Boolean,
    var menuids: MutableList<Int>
   ) :
    RecyclerView.Adapter<FineDineMenuListAdapter.HomeHeaderViewHolder>() {
    private lateinit var fragmentTransaction: FragmentTransaction
    private var fineDineItemDetail: FineDineItemDetailFragment? = null
    inner class HomeHeaderViewHolder(view: View) : RecyclerView.ViewHolder(view){
        var binding: FinedinemenuBinding = FinedinemenuBinding.bind(view)
    }
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): HomeHeaderViewHolder {
        return HomeHeaderViewHolder(
            LayoutInflater.from(context).inflate(
                R.layout.finedinemenu,
                parent,
                false
            )
        )
    }
    override fun getItemCount(): Int {
        return list!!.size
    }
    override fun onBindViewHolder(holder: HomeHeaderViewHolder, position: Int) {
        for (i in 0 until fineDineMenuListFragment.menulist!!.size){
            if(fineDineMenuListFragment.menulist!![i].menuid == list!![position].menu_id){

                if (fineDineMenuListFragment.menulist!![i].quantity==0){

                }else{
                    holder.binding.finedinemenuquantitytxt.visibility = View.VISIBLE
                    holder.binding.finedinemenuquantitytxt.text = fineDineMenuListFragment.menulist!![i].quantity.toString()
                }

            }
        }
        holder.binding.finedinemenutitle.text = list!![position].item
        holder.binding.finedineratingitemdet.text= list!![position].menurating.toString()+"/"+"5"

        holder.binding.finedinemenuprice.text= String.format(Locale("en", "US"),"%.2f",  list!![position].price)+ " SAR"

       // holder.binding.finedinemenuprice.text= list!![position].price.toString()+" SAR"
        list!![position].images?.let { imageUrl -> UiUtils.loadImage(
            holder.binding.finedineitemimg,
            imageUrl)
        }


        if (fineDineMenuListFragment.inqueue.equals("true")){
            holder.binding.finedineaddcart.isEnabled=false
            holder.binding.finedineitemimg.isEnabled=false
            fineDineMenuListFragment.bookmenu.visibility=View.GONE
        }else{
            holder.binding.finedineaddcart.isEnabled=true
            holder.binding.finedineitemimg.isEnabled=true
        }
        holder.binding.finedineaddcart.setOnClickListener(View.OnClickListener {
            if (holder.binding.finedinemenuquantitytxt.visibility == View.GONE) {
                holder.binding.finedinemenuquantitytxt.visibility = View.VISIBLE
                holder.binding.finedinemenuquantitytxt.text = "1"
                fineDineMenuListFragment.menulist!!.add(MenuDetailJson(list!![position].menu_id!!, 1))
                val jsonArray = fineDineMenuListFragment.mapper.writeValueAsString(fineDineMenuListFragment.menulist)
                Log.d("ghb ",""+jsonArray)
            }
        /*    else {
                holder.binding.finedinemenuquantitytxt.visibility = View.GONE
                for (i in 0 until fineDineMenuListFragment.menulist!!.size) {
                    Log.d("asdfvgbhnjm,./", "" + fineDineMenuListFragment.menulist!!.size)
                    if (fineDineMenuListFragment.menulist!![i].menuid == list!![position].menu_id) {
                        fineDineMenuListFragment.menulist!!.removeAt(i)
                        val jsonArray = fineDineMenuListFragment.mapper.writeValueAsString(
                            fineDineMenuListFragment.menulist
                        )
                        Log.d("ghb ", "" + jsonArray)
                        break
                    }
                    //  fineDineMenuListFragment.menulist!!.remove(MenuDetailJson(list!![position].menu_id!!,1))
                    //val jsonArray = fineDineMenuListFragment.mapper.writeValueAsString(fineDineMenuListFragment.menulist)
                    //Log.d("ghb ",""+jsonArray)
                }
            }*/
        })
        // to go to subsubsubfragment
        holder.binding.finedineitemimg.setOnClickListener(View.OnClickListener {
            if (holder.binding.finedinemenuquantitytxt.visibility == View.VISIBLE) {
                val bundle = Bundle()
                bundle.putInt("id", list!![position].menu_id!!)
                bundle.putInt("resid", list!![position].res_id!!)
                bundle.putDouble("menurating", list!![position].menurating!!)
                bundle.putInt("orderid", fineDineMenuListFragment.orderid!!)
                bundle.putSerializable("menulist",fineDineMenuListFragment.menulist as Serializable)
                bundle.putBoolean("inarray", true)
                bundle.putString("status",  fineDineMenuListFragment.status)
                bundle.putString("resname",  fineDineMenuListFragment.hotelname)
                bundle.putString("resimg",  fineDineMenuListFragment.resimage)
                bundle.putDouble("lat",  fineDineMenuListFragment.myLat!!)
                bundle.putDouble("lng",  fineDineMenuListFragment.myLng!!)
                bundle.putString("paidstatus",  fineDineMenuListFragment.statuspaid)
                bundle.putString("paymenttype",  fineDineMenuListFragment.paymenttype)
                bundle.putString("isfd", fineDineMenuListFragment.backres)
                fineDineItemDetail = FineDineItemDetailFragment()
                fineDineItemDetail!!.arguments = bundle
                fragmentTransaction = fineDineMenuListFragment.requireActivity().supportFragmentManager.beginTransaction()
                fragmentTransaction.setCustomAnimations(R.anim.screen_in, R.anim.screen_out)
                fragmentTransaction.replace(R.id.container, fineDineItemDetail!!)
                fragmentTransaction.addToBackStack(null)
                fragmentTransaction.commit()
            }
            else {
                val bundle = Bundle()
                bundle.putInt("id", list!![position].menu_id!!)
                bundle.putInt("resid", list!![position].res_id!!)
                bundle.putDouble("menurating", list!![position].menurating!!)
                bundle.putInt("orderid", fineDineMenuListFragment.orderid!!)
                bundle.putSerializable("menulist",fineDineMenuListFragment.menulist as Serializable)
                bundle.putBoolean("inarray", false)
                bundle.putString("status",  fineDineMenuListFragment.status)
                bundle.putString("resname",  fineDineMenuListFragment.hotelname)
                bundle.putString("resimg",  fineDineMenuListFragment.resimage)
                bundle.putDouble("lat",  fineDineMenuListFragment.myLat!!)
                bundle.putDouble("lng",  fineDineMenuListFragment.myLng!!)
                bundle.putString("paidstatus",  fineDineMenuListFragment.statuspaid)
                bundle.putString("paymenttype",  fineDineMenuListFragment.paymenttype)
                bundle.putString("isfd", fineDineMenuListFragment.backres)

                fineDineItemDetail = FineDineItemDetailFragment()
                fineDineItemDetail!!.arguments = bundle
                fragmentTransaction = fineDineMenuListFragment.requireActivity().supportFragmentManager.beginTransaction()
                fragmentTransaction.setCustomAnimations(R.anim.screen_in, R.anim.screen_out)
                fragmentTransaction.replace(R.id.container, fineDineItemDetail!!)
                fragmentTransaction.addToBackStack(null)
                fragmentTransaction.commit()
            }
        })
    }
}
// for adding into fav
/*
        holder.binding.fav.setOnClickListener(View.OnClickListener {
            holder.binding.fav2.setVisibility(View.VISIBLE)
            holder.binding.fav.setVisibility(View.GONE)
            isfav = "true"
            menuid = list!![position].menu_id!!
            resid = list!![position].res_id!!
            getvalues()
            Log.d("tdtvcyudbciiujjw", "gdvgtqvduyqwidsd")
        })
*/
/*  if(list!![position].favourite.equals("true")){
      holder.binding.fav2.setVisibility(View.VISIBLE)
      holder.binding.fav.setVisibility(View.GONE)
  }
  // to remove from fav
  holder.binding.fav2.setOnClickListener(View.OnClickListener {
      holder.binding.fav.setVisibility(View.VISIBLE)
      holder.binding.fav2.setVisibility(View.GONE)
      isfav = "false"
      menuid = list!![position].menu_id!!
      resid = list!![position].res_id!!
      getvalues()
  })
  // to go to subsubsubfragment
  holder.binding.addcart.setOnClickListener(View.OnClickListener {
      if(subsubHomeFragment.cartlist.isNullOrEmpty()){
          subsubHomeFragment.menuid2 = list!![position].menu_id!!
          subsubHomeFragment.resid2 = list!![position].res_id!!
          subsubHomeFragment.cost2 = list!![position].price!!
          subsubHomeFragment.getaddcart()
      }
      else{
          var hd:Boolean = false
          for (i in 0 until subsubHomeFragment.cartlist!!.size){
              //   holder.binding.menuquantity.visibility=View.VISIBLE
              // holder.binding.menuquantity.text= "1"
              Log.d("dbvhfd",""+list!![position].res_id+"==="+ subsubHomeFragment.cartlist!![i].res_id)
              if (list!![position].res_id != subsubHomeFragment.cartlist!![i].res_id){
                  hd = true
                  break
              }
              else{
                  hd = false
              }
          }
          if(hd == true){
              val builder = AlertDialog.Builder(context)
              builder.setTitle(R.string.alert)
              builder.setMessage(R.string.youareaddingfromdiffresturant)
              builder.setPositiveButton(android.R.string.yes) { dialog, which ->
                  subsubHomeFragment.menuid2 = list!![position].menu_id!!
                  subsubHomeFragment.resid2 = list!![position].res_id!!
                  subsubHomeFragment.cost2 = list!![position].price!!
                  subsubHomeFragment.getdeleteall()
              }
              builder.setNegativeButton(android.R.string.no) { dialog, which ->
                  builder.setCancelable(true)
              }
              builder.show()
          }
          else{
              subsubHomeFragment.menuid2 = list!![position].menu_id!!
              subsubHomeFragment.resid2 = list!![position].res_id!!
              subsubHomeFragment.cost2 = list!![position].price!!
              subsubHomeFragment.getaddcart()
          }
      }
      *//*   subsubsubHomeFragment = SubsubsubHomeFragment()
               fragmentTransaction = subsubHomeFragment.requireActivity().supportFragmentManager.beginTransaction()
               fragmentTransaction.setCustomAnimations(R.anim.screen_in, R.anim.screen_out)
               fragmentTransaction.replace(R.id.container, subsubsubHomeFragment!!)
               fragmentTransaction.addToBackStack(null)
               fragmentTransaction.commit()*//*
        })
        if(iscolsed == true){
            holder.binding.widgetIcon.alpha = 0.5F
            holder.binding.itemimg.isEnabled = false
            holder.binding.fav.isEnabled = false
            holder.binding.fav2.isEnabled = false
            holder.binding.addcart.isEnabled = false
        }*/