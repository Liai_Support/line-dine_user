package com.lia.yello.linedine.fragment

import android.Manifest
import android.content.pm.PackageManager
import android.location.Geocoder
import android.os.Build
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import androidx.core.app.ActivityCompat
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentTransaction
import com.google.android.gms.location.*
import com.google.android.gms.maps.*
import com.google.android.gms.maps.model.LatLng
import com.google.android.material.bottomsheet.BottomSheetBehavior
import com.lia.yello.linedine.R
import com.lia.yello.linedine.Utils.Constants
import com.lia.yello.linedine.Utils.GpsUtils
import com.lia.yello.linedine.Utils.NetworkUtils
import com.lia.yello.linedine.Utils.UiUtils
import com.lia.yello.linedine.activity.DashBoardActivity
import kotlinx.android.synthetic.main.activity_dash_board.*
import kotlinx.android.synthetic.main.activity_map.*
import kotlinx.android.synthetic.main.header.*
import java.io.IOException
import java.util.*


class MapFragment : Fragment(),OnMapReadyCallback,View.OnClickListener {
    private lateinit var mMap: GoogleMap
    private var fusedLocationClient: FusedLocationProviderClient? = null
    private var locationRequest: LocationRequest? = null
    private var locationCallback: LocationCallback? = null
    private var myLat: Double? = null
    private var myLng: Double? = null
    var map: GoogleMap? = null
    var mapView: View? = null
    var supportMapFragment: SupportMapFragment? = null

    var flat: Double? = 0.0
    var flng: Double? = 0.0
    var updatelat: Double? = 0.0
    var updatelng: Double? = 0.0
    var fcity: String? = ""
    var faid: Int? = 0

    var faddr: String? = ""
    var fatype: String? = ""
    var address: String? = ""
    var lat:Double=0.0
    var lng:Double=0.0
    lateinit var save:Button
    var update:Boolean = false
    private lateinit var fragmentTransaction: FragmentTransaction
    private var addAdressFragment: AddAdressFragment? = null
    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {

        var view = inflater.inflate(R.layout.fragment_map, container, false)
        save=view.findViewById(R.id.save)
        // childFragmentManager.findFragmentById(R.id.map) as SupportMapFragment
        (context as DashBoardActivity?)!!.mBottomSheetBehavior2?.setState(BottomSheetBehavior.STATE_HIDDEN)
        (context as DashBoardActivity?)!!.bottom_navigation_main.setVisibility(View.GONE)
        (context as DashBoardActivity?)!!.iv_add.setVisibility(View.GONE)
        if (!NetworkUtils.isNetworkConnected(requireContext())) {
            UiUtils.showSnack(
                view, "no_internet_connection"
            )
        }
        supportMapFragment = childFragmentManager.findFragmentById(R.id.mapview) as SupportMapFragment?
        fusedLocationClient = LocationServices.getFusedLocationProviderClient(requireActivity())
        supportMapFragment?.getMapAsync(this)
        locationListner()
        askLocationPermission()





        update = arguments?.getBoolean("update")!!

        if (update==true){
            faid = arguments?.getInt("faid")
            address = arguments?.getString("faddr")
            updatelat = arguments?.getDouble("flat")
            updatelng = arguments?.getDouble("flng")
            fatype = arguments?.getString("fatype")
            fcity = arguments?.getString("fcity")
            //txt_address.text = address.toString()
        }else{
            flat = arguments?.getDouble("lat")!!
            flng = arguments?.getDouble("lng")!!

            Log.d("flat",""+flat)
            Log.d("flng",""+flng)
        }

       save.setOnClickListener(this)

        return view
    }


    override fun onMapReady(googleMap: GoogleMap) {
        map = googleMap

        // Add a marker in Sydney and move the camera
        val sydney = LatLng(23.6611815, 42.9249046)
//        mMap.addMarker(MarkerOptions().position(sydney).title("Marker in Sydney"))
//        mMap.moveCamera(CameraUpdateFactory.newLatLng(sydney))
        // map.moveCamera(CameraUpdateFactory.newLatLngZoom(sydney, 9.0f))

        if (ActivityCompat.checkSelfPermission(
                requireContext(),
                Manifest.permission.ACCESS_FINE_LOCATION
            ) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(
                requireContext(),
                Manifest.permission.ACCESS_COARSE_LOCATION
            ) != PackageManager.PERMISSION_GRANTED
        ) {
            // TODO: Consider calling
            //    ActivityCompat#requestPermissions
            // here to request the missing permissions, and then overriding
            //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
            //                                          int[] grantResults)
            // to handle the case where the user grants the permission. See the documentation
            // for ActivityCompat#requestPermissions for more details.
            return
        }
        map?.isMyLocationEnabled = true

       // map!!.setPadding(50, 50, 30, 105)

        /*  val locationButton = (mapView.findViewById("1".toInt()).getParent() as View).findViewById<View>("2".toInt())
          val rlp = locationButton.layoutParams as RelativeLayout.LayoutParams
          rlp.addRule(RelativeLayout.ALIGN_PARENT_TOP, 0)
          rlp.addRule(RelativeLayout.ALIGN_PARENT_TOP, RelativeLayout.TRUE)
          rlp.setMargins(0, 180, 180, 0)*/


        myLat?.let {
            myLng?.let {
                getLastKnownLocation()
            }
        }

        map?.setOnCameraIdleListener {

            var centerLatLng = map?.cameraPosition?.target
            flat = centerLatLng?.latitude
            flng = centerLatLng?.longitude


            Log.d("asdfghjnmk,", "" + centerLatLng)
            val geocoder = Geocoder(activity)
            try {
                val addresses = flat?.let {
                    flng?.let { it1 ->
                        geocoder.getFromLocation(
                            it,
                            it1, 1
                        )
                    }
                }
                if (addresses != null && addresses.size > 0) {
                    val address =
                        addresses[0].getAddressLine(0) // If any additional address line present than only, check with max available address lines by getMaxAddressLineIndex()
                    val city = addresses[0].locality
                    txt_address.text = address.toString()
                    faddr =  address.toString()
                    fcity=city

                    if(fcity!=null){
                        fcity = addresses[0].locality.toString()

                    }else{
                        fcity=""
                    }


                 //  fcity = addresses[0].locality.toString()
                    val state = addresses[0].adminArea
                    val country = addresses[0].countryName
                    val postalCode = addresses[0].postalCode
                    val knownName = addresses[0].featureName

                }
            } catch (e: IOException) {
                e.printStackTrace()
            }

        }

    }

    override fun onRequestPermissionsResult(
        requestCode: Int,
        permissions: Array<out String>,
        grantResults: IntArray
    ) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)

        if (requestCode == Constants.Permission.LOCATION_PERMISSION) {

            if (grantResults.size >= 2 && grantResults[0] == PackageManager.PERMISSION_GRANTED && grantResults[1] == PackageManager.PERMISSION_GRANTED) {
                getUserLocation()
            }
        }

    }

    private fun locationListner() {
        locationRequest = LocationRequest.create()
        locationRequest?.priority = LocationRequest.PRIORITY_HIGH_ACCURACY
        locationRequest?.interval = (20 * 1000).toLong()
        locationCallback = object : LocationCallback() {
            override fun onLocationResult(locationResult: LocationResult?) {
                if (locationResult == null) {
                    return
                }
                for (location in locationResult.locations) {
                    if (location != null) {
                        if (myLat == 0.0 && myLng == 0.0)
                            getLastKnownLocation()
                    }
                }
            }
        }

    }

    private fun askLocationPermission() {

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            if (requireContext().checkSelfPermission(Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && requireContext().checkSelfPermission(
                    Manifest.permission.ACCESS_COARSE_LOCATION
                ) != PackageManager.PERMISSION_GRANTED
            ) {
                requestPermissions(
                    Constants.Permission.LOCATION_PERMISSION_PERMISSON_LIST,
                    Constants.Permission.LOCATION_PERMISSION
                )
                return
            } else {
                getUserLocation()
            }
        } else {
            getUserLocation()
        }
    }

    private fun getUserLocation() {

        GpsUtils(requireActivity()).turnGPSOn(object : GpsUtils.OnGpsListener {
            override fun gpsStatus(isGPSEnable: Boolean) {
                if (isGPSEnable) {
                    getLastKnownLocation()
                }
            }
        })
    }

    private fun getLastKnownLocation() {

        if (ActivityCompat.checkSelfPermission(
                requireContext(),
                Manifest.permission.ACCESS_FINE_LOCATION
            ) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(
                requireContext(),
                Manifest.permission.ACCESS_COARSE_LOCATION
            ) != PackageManager.PERMISSION_GRANTED
        ) {
            // TODO: Consider calling
            //    ActivityCompat#requestPermissions
            // here to request the missing permissions, and then overriding
            //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
            //                                          int[] grantResults)
            // to handle the case where the user grants the permission. See the documentation
            // for ActivityCompat#requestPermissions for more details.
            return
        }
        fusedLocationClient?.lastLocation?.addOnSuccessListener { location ->
            // Got last known location. In some rare situations this can be null.
            if (location != null) {

                myLat = location.latitude
                myLng = location.longitude

                map?.let {
                    flat = myLat
                    flng = myLng
                    locateMyLocation()
                }


            } else {

                fusedLocationClient?.requestLocationUpdates(
                    locationRequest,
                    locationCallback,
                    null
                )

            }
        }

    }

    private fun locateMyLocation() {
     if (update==true){
         map?.animateCamera(
             CameraUpdateFactory.newLatLngZoom(
                 LatLng(updatelat!!, updatelng!!),
                 20.0f
             )
         )
     }
     else {

         Log.d("flat",""+flat)
         Log.d("flng",""+flng)
         flat?.let { lat ->
             flng?.let { lng ->
                 map?.animateCamera(CameraUpdateFactory.newLatLngZoom(LatLng(lat, lng), 20.0f))
             }
         }
     }

    }

    override fun onClick(v: View?) {

        if (v == save) {
             if(update==false){
                 val bundle = Bundle()
                 bundle.putDouble("flat", flat!!)
                 bundle.putDouble("flng", flng!!)
                 bundle.putString("faddr", faddr)
                 bundle.putString("fcity", fcity)
                 bundle.putString("fatype", "")
                 bundle.putBoolean("update", false)

                 Log.d("iddddddd", "" + flat)
                 Log.d("lmg", "" + flng)
                 Log.d("name", "" + faddr)
                 addAdressFragment = AddAdressFragment()
                 addAdressFragment!!.arguments=bundle
                 fragmentTransaction = requireActivity().supportFragmentManager.beginTransaction()
                 fragmentTransaction.setCustomAnimations(R.anim.screen_in, R.anim.screen_out)
                 fragmentTransaction.replace(R.id.container, addAdressFragment!!)
                 fragmentTransaction.addToBackStack(null)
                 fragmentTransaction.commit()
             }
             else{
                 val bundle = Bundle()
                 bundle.putInt("faid", faid!!)
                 bundle.putDouble("flat", updatelat!!)
                 bundle.putDouble("flng", updatelng!!)
                 bundle.putString("faddr", faddr)
                 bundle.putString("fcity", fcity)
                 bundle.putBoolean("update", true)
                 bundle.putString("fatype", fatype)

                 addAdressFragment = AddAdressFragment()
                 addAdressFragment!!.arguments=bundle

                 fragmentTransaction =
                     requireActivity().supportFragmentManager.beginTransaction()
                 fragmentTransaction.setCustomAnimations(R.anim.screen_in, R.anim.screen_out)
                 fragmentTransaction.replace(R.id.container, addAdressFragment!!)
                 fragmentTransaction.addToBackStack(null)
                 fragmentTransaction.commit()
             }
        }

    }
}


