package com.lia.yello.linedine.Models

import com.google.gson.annotations.SerializedName
import java.io.Serializable

class UpdateCartResponse: Serializable {


    @SerializedName("error")
    var error: Boolean? = null

    @SerializedName("message")
    var message: String? = null

      @SerializedName("data")
     var data:  ArrayList<updatecart>? = null

}


class updatecart : Serializable {


    @SerializedName("id")
    var id: Int? = null

    @SerializedName("quantity")
    var quantity: Int? = null
}
