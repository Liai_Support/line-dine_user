package com.lia.yello.linedine.adapter

import android.content.Context
import android.content.Intent
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.ImageView
import android.widget.TextView
import androidx.core.content.ContextCompat
import androidx.core.content.ContextCompat.startActivity
import androidx.viewpager.widget.PagerAdapter
import com.lia.yello.linedine.R
import com.lia.yello.linedine.Session.SharedHelper
import com.lia.yello.linedine.Utils.UiUtils
import com.lia.yello.linedine.activity.Banner
import com.lia.yello.linedine.activity.DashBoardActivity
import com.lia.yello.linedine.activity.LoginActivity
import com.lia.yello.linedine.activity.OnBoardActivity
import com.lia.yello.linedine.fragment.HomeFragment
import com.lia.yello.linedine.interfaces.OnClickListener
import com.lia.yello.linedine.interfaces.SingleTapListener

class OnBoardAdapter2(

    var activity: OnBoardActivity,
    var context: Context,
    var list: ArrayList<Int>,
    var description: ArrayList<String>,
    var descriptionhead: ArrayList<String>,
    var next: OnClickListener,
    var skip: SingleTapListener

): PagerAdapter() {

    override fun instantiateItem(container: ViewGroup, position: Int): Any {

        val inflater = LayoutInflater.from(context)
        val layout = inflater.inflate(R.layout.child_onboard1, container, false) as ViewGroup

        val imageView: ImageView = layout.findViewById(R.id.onboardImage)
        val text1: TextView = layout.findViewById(R.id.descriptionhead)
        val text2: TextView = layout.findViewById(R.id.description)
        val continuee: Button = layout.findViewById(R.id.continuee)
        text2.text = description[position]

     text1.text = descriptionhead[position]
        imageView.setImageDrawable(
            ContextCompat.getDrawable(
                context,
                list[position]
            )
        )

        continuee.setOnClickListener{


            if (descriptionhead[position].equals("Fine Dine")) {
                activity.sharedHelper!!.choose="finedine"
                skip.singleTap()

            }else if (descriptionhead[position].equals("In Queue")){
                activity.sharedHelper!!.choose="inqueue"
                skip.singleTap()

            }else{
                activity.sharedHelper!!.choose="pickup"
                skip.singleTap()

            }
        }

        container.addView(layout)


        return layout
    }


    override fun getCount(): Int {

        return list!!.size


    }

    override fun isViewFromObject(view: View, `object`: Any): Boolean {





        return view == `object`





    }

    override fun destroyItem(container: ViewGroup, position: Int, `object`: Any) {


        container.removeView(`object` as View)
    }








}