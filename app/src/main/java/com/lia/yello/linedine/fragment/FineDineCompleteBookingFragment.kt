package com.lia.yello.linedine.fragment

import android.annotation.SuppressLint
import android.location.Geocoder
import android.os.Bundle
import android.util.Log
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.core.content.ContextCompat
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.FragmentTransaction
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.LinearLayoutManager
import com.lia.yello.linedine.R
import com.lia.yello.linedine.Session.SharedHelper
import com.lia.yello.linedine.Utils.DialogUtils
import com.lia.yello.linedine.Utils.NetworkUtils
import com.lia.yello.linedine.Utils.UiUtils
import com.lia.yello.linedine.ViewModels.MapViewModel
import com.lia.yello.linedine.adapter.*
import com.lia.yello.linedine.databinding.FragmentFineDineCompleteBookingBinding
import com.lia.yello.linedine.databinding.FragmentFineDineOrderListStatusBinding
import com.lia.yello.linedine.databinding.FragmentMyActiveOrdersBinding
import kotlinx.android.synthetic.main.carticon.*
import kotlinx.android.synthetic.main.fragment_fine_dine_complete_booking.*
import kotlinx.android.synthetic.main.fragment_fine_dine_order_list_status.*
import kotlinx.android.synthetic.main.fragment_fine_dine_order_list_status.cancelreservation
import kotlinx.android.synthetic.main.fragment_fine_dine_order_list_status.finedineorderid
import kotlinx.android.synthetic.main.fragment_fine_dine_order_list_status.menushowrecycler
import kotlinx.android.synthetic.main.fragment_fine_dine_order_list_status.statusfdorderlist
import kotlinx.android.synthetic.main.fragment_order_processing.*
import kotlinx.android.synthetic.main.fragment_orders.*
import kotlinx.android.synthetic.main.fragment_profile.*
import kotlinx.android.synthetic.main.fragment_profile.root
import kotlinx.android.synthetic.main.fragment_subhome.*
import kotlinx.android.synthetic.main.header.*
import java.util.*


class FineDineCompleteBookingFragment : Fragment(R.layout.fragment_fine_dine_complete_booking) {

    var orderid:Int=0
    var status:String=""
    var binding: FragmentFineDineCompleteBookingBinding? = null
    private var mapViewModel: MapViewModel? = null
    public var sharedHelper: SharedHelper? = null
    private var homeFragment : HomeFragment? = null
    private lateinit var fragmentTransaction: FragmentTransaction
    private var myOrderMenuListFragment : MyOrderMenuListFragment? = null
    private var myLat: Double? = null
    private var myLng: Double? = null
    var fcity:String=""
    var resid:Int=0
    var hotelname:String=""
    var resimage:String=""
    var discoumtamtt:Double=0.0


    @SuppressLint("NewApi")

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        binding = DataBindingUtil.findBinding(view)
        mapViewModel = ViewModelProvider(this).get(MapViewModel::class.java)
        val head = view.findViewById<TextView>(R.id.head)
        sharedHelper = SharedHelper(requireContext())


        if (sharedHelper!!.language == "ar"){
            back.rotation= 180F
        }

        orderid = arguments?.getInt("orderid")!!
        status = arguments?.getString("status")!!
        myLat = arguments?.getDouble("lat")!!
        myLng = arguments?.getDouble("lng")!!
        hotelname = arguments?.getString("resname")!!
        resimage = arguments?.getString("resimg")!!
        Log.d("orderid",""+orderid)
        Log.d("status",""+status)
        Log.d("sdfgh",""+myLat)
        Log.d("sdfghj",""+myLng)
        head.setText(getString(R.string.myordersdetails))
        cart_badge.text = ""+ sharedHelper!!.cartcount
        completedresname.text=hotelname
        resimage.let {
            UiUtils.loadImage(
                completeimgorderprocessing,
                it,
                ContextCompat.getDrawable(
                    requireContext(),
                    R.drawable.dummylogo
                )!!
            )
        }

        if (sharedHelper!!.choose.equals("finedine")||sharedHelper!!.choose.equals("inqueue")|| sharedHelper!!.cartcount == 0){

            //var cartframe: FrameLayout =findViewById(R.id.cartframe)
            cartframe.visibility=View.GONE
        }
        if (!NetworkUtils.isNetworkConnected(requireContext())) {
            UiUtils.showSnack(
                view, "no_internet_connection"
            )
        }
        back.setOnClickListener(View.OnClickListener {
            requireActivity().supportFragmentManager.popBackStack()
        })

        statusfdorderlist.text=status
        /* cancelreservation.setOnClickListener(View.OnClickListener {

             cancelbooking(orderid)

         })
 */
        getbookinglist("history")

        finedinecompletedorder.setOnClickListener(View.OnClickListener {
            val bundle = Bundle()
            bundle.putInt("orderid",orderid )
            bundle.putString("status",status )
            myOrderMenuListFragment = MyOrderMenuListFragment()
            myOrderMenuListFragment!!.arguments = bundle
            fragmentTransaction = requireActivity().supportFragmentManager.beginTransaction()
            fragmentTransaction.setCustomAnimations(R.anim.screen_in, R.anim.screen_out)
            fragmentTransaction.replace(R.id.container, myOrderMenuListFragment!!)
            fragmentTransaction.addToBackStack(null)
            fragmentTransaction.commit()
        })

        val gcd = Geocoder(context, Locale.getDefault())
        val addresses = gcd.getFromLocation(myLat!!, myLng!!, 1)
        if (addresses.size > 0) {
            println(addresses[0].locality)
            Log.d("adedretrgygyuihu", "" + addresses[0].locality)

            if (sharedHelper!!.language.equals("ar")){
                fcity=addresses[0].locality
                finedinecompletedlocation.text= " $fcity :"
            }else{
                fcity=addresses[0].locality
                finedinecompletedlocation.text= ": $fcity"
            }

        } else {
            // do your stuff
        }
    }

    public fun getbookinglist(status:String){
        DialogUtils.showLoader(requireContext())
        mapViewModel?.getbookinglist(requireContext(),status)
            ?.observe(viewLifecycleOwner,
                Observer {
                    DialogUtils.dismissLoader()

                    it?.let {
                        it.error?.let { error ->
                            if (error) {
                                it.message?.let { msg ->
                                    UiUtils.showSnack(root_finedinecompleteorderstatus, msg)
                                }
                            } else {
                                it.data?.let { data ->

                                    for (i in 0 until data!!.size) {
                                        if (data[i].id!! == orderid) {



                                            if (data[i].discount_amount=="0"){
                                                discountlevelfdcom.visibility=View.GONE
                                            }else{
                                                discoumtamtt=data[i].discount_amount!!.toDouble()

                                                discountlevelfdcom.visibility=View.VISIBLE

                                                discountfdcomtxt.text =" - " +String.format(Locale("en", "US"),"%.2f", data[i].discount_amount!!.toDouble())+ " SAR"

                                            }
                                            finedinecompletesubtotal.text = String.format(Locale("en", "US"),"%.2f",
                                                data[i].subtotal) + " SAR"
                                            finedinecompletedvat.text =
                                                String.format(Locale("en", "US"),"%.2f", data[i].vat) + " SAR"
                                            finedinecompletedtotal.text =
                                                String.format(Locale("en", "US"),"%.2f", data[i].amount) + " SAR"

                                            finedinecomplebookingamount1.text =
                                                " - "+String.format(Locale("en", "US"),"%.2f", data[i].initial_amount) + " SAR"

                                            if (data[i].menudetails!!.size!=0){

                                                linear_complete_ordermenu.visibility=View.VISIBLE
                                                finedineorderid.text =
                                                    getString(R.string.orderid) + " : " + data[i].id.toString()
                                                finedinecompletedorder.isEnabled=true

                                                menushowrecycler.layoutManager =
                                                    LinearLayoutManager(context)
                                                menushowrecycler.adapter =
                                                    FineDineCompleteBookingAdapter(
                                                        this,
                                                        requireContext(),
                                                        data[i].menudetails
                                                    )
                                            }else{
                                                finedinecompletedorder.isEnabled=false

                                            }

                                        }

                                    }


                                }
                            }
                        }

                    }
                })
    }


/*
    private fun cancelbooking(id:Int){
        DialogUtils.showLoader(requireContext())

        mapViewModel?.cancelbooking(requireContext(),id)
            ?.observe(viewLifecycleOwner,
                Observer {
                    DialogUtils.dismissLoader()
                    it?.let {
                        it.error?.let { error ->
                            if (error) {
                                it.message?.let { msg ->
                                    UiUtils.showSnack(root_finedineorderstatus, msg)

                                }
                            }
                            else {
                                it.message?.let { msg ->
                                    UiUtils.showSnack(root_finedineorderstatus, msg)
                                    homeFragment = HomeFragment()

                                    fragmentTransaction = requireActivity().supportFragmentManager.beginTransaction()
                                    fragmentTransaction.setCustomAnimations(R.anim.screen_in,R.anim.screen_out)
                                    fragmentTransaction.replace(R.id.container, homeFragment!!)
                                    fragmentTransaction.addToBackStack(null)
                                    fragmentTransaction.commit()

                                }
                            }
                        }

                    }
                })

    }
*/

}