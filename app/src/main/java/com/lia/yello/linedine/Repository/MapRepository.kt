package com.lia.yello.linedine.Repository

import android.content.Context
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData

import com.google.gson.Gson
import com.lia.yello.linedine.Models.*
import com.lia.yello.linedine.Utils.Constants
import com.lia.yello.linedine.activity.HomeResponse
import com.lia.yello.linedine.interfaces.ApiResponseCallback
import com.lia.yello.linedine.network.Api
import com.lia.yello.linedine.network.ApiInput
import org.json.JSONObject

class MapRepository private constructor() {

    companion object {
        var repository: MapRepository? = null

        fun getInstance(): MapRepository {
            if (repository == null) {
                repository = MapRepository()
            }
            return repository as MapRepository
        }
    }




    fun getAddress(input: ApiInput): LiveData<AutoCompleteResponse>? {

        val apiResponse: MutableLiveData<AutoCompleteResponse> = MutableLiveData()

        Api.getMethod(input, object : ApiResponseCallback {
            override fun setResponseSuccess(jsonObject: JSONObject) {
                val gson = Gson()
                var response: AutoCompleteResponse =
                    gson.fromJson(jsonObject.toString(), AutoCompleteResponse::class.java)
                response.error = false
                response.errorMessage = ""
                apiResponse.value = response
            }

            override fun setErrorResponse(error: String) {
                var response = AutoCompleteResponse()
                response.error = true
                response.errorMessage = error
                apiResponse.value = response
            }
        })

        return apiResponse
    }


    fun gethomedetails(input: ApiInput): LiveData<HomeResponse>? {

        val apiResponse: MutableLiveData<HomeResponse> = MutableLiveData()

        Api.getMethod(input, object : ApiResponseCallback {
            override fun setResponseSuccess(jsonObject: JSONObject) {
                val gson = Gson()
                var response: HomeResponse =
                    gson.fromJson(jsonObject.toString(), HomeResponse::class.java)
                apiResponse.value = response
            }

            override fun setErrorResponse(error: String) {
                var response = HomeResponse()
                response.error = true
                apiResponse.value = response
            }
        })

        return apiResponse
    }

    fun getupdateaddressid(input: ApiInput): LiveData<CommonResponse>? {

        val apiResponse: MutableLiveData<CommonResponse> = MutableLiveData()

        Api.postMethod(input, object : ApiResponseCallback {
            override fun setResponseSuccess(jsonObject: JSONObject) {
                val gson = Gson()
                var response: CommonResponse =
                    gson.fromJson(jsonObject.toString(), CommonResponse::class.java)
                apiResponse.value = response
            }

            override fun setErrorResponse(error: String) {
                var response = CommonResponse()
                response.error = true
                apiResponse.value = response
            }
        })

        return apiResponse
    }


    fun getlistresturant(input: ApiInput): LiveData<ListResturantResponse>? {

        val apiResponse: MutableLiveData<ListResturantResponse> = MutableLiveData()

        Api.postMethod(input, object : ApiResponseCallback {
            override fun setResponseSuccess(jsonObject: JSONObject) {
                val gson = Gson()
                var response: ListResturantResponse =
                    gson.fromJson(jsonObject.toString(), ListResturantResponse::class.java)
                apiResponse.value = response
            }

            override fun setErrorResponse(error: String) {
                var response = ListResturantResponse()
                response.error = true
                apiResponse.value = response
            }
        })

        return apiResponse
    }

    fun getitemdetail(input: ApiInput): LiveData<ItemDetailResponse>? {

        val apiResponse: MutableLiveData<ItemDetailResponse> = MutableLiveData()

        Api.postMethod(input, object : ApiResponseCallback {
            override fun setResponseSuccess(jsonObject: JSONObject) {
                val gson = Gson()
                var response: ItemDetailResponse = gson.fromJson(jsonObject.toString(), ItemDetailResponse::class.java)
                apiResponse.value = response
            }

            override fun setErrorResponse(error: String) {
                var response = ItemDetailResponse()
                response.error = true
                apiResponse.value = response
            }
        })

        return apiResponse
    }



    fun getmenudetail(input: ApiInput): LiveData<MenuDetailResponse>? {

        val apiResponse: MutableLiveData<MenuDetailResponse> = MutableLiveData()

        Api.postMethod(input, object : ApiResponseCallback {
            override fun setResponseSuccess(jsonObject: JSONObject) {
                val gson = Gson()
                var response: MenuDetailResponse =
                    gson.fromJson(jsonObject.toString(), MenuDetailResponse::class.java)
                apiResponse.value = response
            }

            override fun setErrorResponse(error: String) {
                var response = MenuDetailResponse()
                response.error = true
                apiResponse.value = response
            }
        })

        return apiResponse
    }



    fun getaddcart(input: ApiInput): LiveData<AddCartResponse>? {

        val apiResponse: MutableLiveData<AddCartResponse> = MutableLiveData()

        Api.postMethod(input, object : ApiResponseCallback {
            override fun setResponseSuccess(jsonObject: JSONObject) {
                val gson = Gson()
                var response: AddCartResponse =
                    gson.fromJson(jsonObject.toString(), AddCartResponse::class.java)
                apiResponse.value = response
            }

            override fun setErrorResponse(error: String) {
                var response = AddCartResponse()
                response.error = true
                apiResponse.value = response
            }
        })

        return apiResponse
    }



    fun getfavdetail(input: ApiInput): LiveData<FavoriteResponse>? {

        val apiResponse: MutableLiveData<FavoriteResponse> = MutableLiveData()

        Api.postMethod(input, object : ApiResponseCallback {
            override fun setResponseSuccess(jsonObject: JSONObject) {
                val gson = Gson()
                var response: FavoriteResponse =
                    gson.fromJson(jsonObject.toString(), FavoriteResponse::class.java)
                apiResponse.value = response
            }

            override fun setErrorResponse(error: String) {
                var response = FavoriteResponse()
                response.error = true
                apiResponse.value = response
            }
        })

        return apiResponse
    }

    fun getorderlists(input: ApiInput): LiveData<OrderListResponse>? {

        val apiResponse: MutableLiveData<OrderListResponse> = MutableLiveData()

        Api.postMethod(input, object : ApiResponseCallback {
            override fun setResponseSuccess(jsonObject: JSONObject) {
                val gson = Gson()
                var response: OrderListResponse =
                    gson.fromJson(jsonObject.toString(), OrderListResponse::class.java)
                apiResponse.value = response
            }

            override fun setErrorResponse(error: String) {
                var response = OrderListResponse()
                response.error = true
                apiResponse.value = response
            }
        })

        return apiResponse
    }

    fun getdeleteorder(input: ApiInput): LiveData<DeleteOrderResponse>? {

        val apiResponse: MutableLiveData<DeleteOrderResponse> = MutableLiveData()

        Api.postMethod(input, object : ApiResponseCallback {
            override fun setResponseSuccess(jsonObject: JSONObject) {
                val gson = Gson()
                var response: DeleteOrderResponse =
                    gson.fromJson(jsonObject.toString(), DeleteOrderResponse::class.java)
                apiResponse.value = response
            }

            override fun setErrorResponse(error: String) {
                var response = DeleteOrderResponse()
                response.error = true
                apiResponse.value = response
            }
        })

        return apiResponse
    }

    fun getaddadress(input: ApiInput): LiveData<AddAdressResponse>? {

        val apiResponse: MutableLiveData<AddAdressResponse> = MutableLiveData()

        Api.postMethod(input, object : ApiResponseCallback {
            override fun setResponseSuccess(jsonObject: JSONObject) {
                val gson = Gson()
                var response: AddAdressResponse =
                    gson.fromJson(jsonObject.toString(), AddAdressResponse::class.java)
                apiResponse.value = response
            }

            override fun setErrorResponse(error: String) {
                var response = AddAdressResponse()
                response.error = true
                apiResponse.value = response
            }
        })

        return apiResponse
    }


    fun getupdateaddress(input: ApiInput): LiveData<UpdateAdressResponse>? {

        val apiResponse: MutableLiveData<UpdateAdressResponse> = MutableLiveData()

        Api.postMethod(input, object : ApiResponseCallback {
            override fun setResponseSuccess(jsonObject: JSONObject) {
                val gson = Gson()
                var response: UpdateAdressResponse =
                    gson.fromJson(jsonObject.toString(), UpdateAdressResponse::class.java)
                apiResponse.value = response
            }

            override fun setErrorResponse(error: String) {
                var response = UpdateAdressResponse()
                response.error = true
                apiResponse.value = response
            }
        })

        return apiResponse
    }

    fun getdeleteadress(input: ApiInput): LiveData<DeleteAddressResponse>? {

        val apiResponse: MutableLiveData<DeleteAddressResponse> = MutableLiveData()

        Api.postMethod(input, object : ApiResponseCallback {
            override fun setResponseSuccess(jsonObject: JSONObject) {
                val gson = Gson()
                var response: DeleteAddressResponse =
                    gson.fromJson(jsonObject.toString(), DeleteAddressResponse::class.java)
                apiResponse.value = response
            }

            override fun setErrorResponse(error: String) {
                var response = DeleteAddressResponse()
                response.error = true
                apiResponse.value = response
            }
        })

        return apiResponse
    }

    fun getlistaddress(input: ApiInput): LiveData<ListAdressResponse>? {

        val apiResponse: MutableLiveData<ListAdressResponse> = MutableLiveData()

        Api.postMethod(input, object : ApiResponseCallback {
            override fun setResponseSuccess(jsonObject: JSONObject) {
                val gson = Gson()
                var response: ListAdressResponse =
                    gson.fromJson(jsonObject.toString(), ListAdressResponse::class.java)
                apiResponse.value = response
            }

            override fun setErrorResponse(error: String) {
                var response = ListAdressResponse()
                response.error = true
                apiResponse.value = response
            }
        })

        return apiResponse
    }


    fun getaddwallet(input: ApiInput): LiveData<AddWalletResponse>? {

        val apiResponse: MutableLiveData<AddWalletResponse> = MutableLiveData()

        Api.postMethod(input, object : ApiResponseCallback {
            override fun setResponseSuccess(jsonObject: JSONObject) {
                val gson = Gson()
                var response: AddWalletResponse =
                    gson.fromJson(jsonObject.toString(), AddWalletResponse::class.java)
                apiResponse.value = response
            }

            override fun setErrorResponse(error: String) {
                var response = AddWalletResponse()
                response.error = true
                apiResponse.value = response
            }
        })

        return apiResponse
    }

    fun getwalletamount(input: ApiInput): LiveData<GetWalletResponse>? {

        val apiResponse: MutableLiveData<GetWalletResponse> = MutableLiveData()

        Api.postMethod(input, object : ApiResponseCallback {
            override fun setResponseSuccess(jsonObject: JSONObject) {
                val gson = Gson()
                var response: GetWalletResponse =
                    gson.fromJson(jsonObject.toString(), GetWalletResponse::class.java)
                apiResponse.value = response
            }

            override fun setErrorResponse(error: String) {
                var response = GetWalletResponse()
                response.error = true
                apiResponse.value = response
            }
        })

        return apiResponse
    }
    fun getwallethistry(input: ApiInput): LiveData<WalletHistory>? {

        val apiResponse: MutableLiveData<WalletHistory> = MutableLiveData()

        Api.postMethod(input, object : ApiResponseCallback {
            override fun setResponseSuccess(jsonObject: JSONObject) {
                val gson = Gson()
                var response: WalletHistory =
                    gson.fromJson(jsonObject.toString(), WalletHistory::class.java)
                apiResponse.value = response
            }

            override fun setErrorResponse(error: String) {
                var response = WalletHistory()
                response.error = true
                apiResponse.value = response
            }
        })

        return apiResponse
    }


    fun getaddfav(input: ApiInput): LiveData<AddFavResponse>? {

        val apiResponse: MutableLiveData<AddFavResponse> = MutableLiveData()

        Api.postMethod(input, object : ApiResponseCallback {
            override fun setResponseSuccess(jsonObject: JSONObject) {
                val gson = Gson()
                var response: AddFavResponse =
                    gson.fromJson(jsonObject.toString(), AddFavResponse::class.java)
                apiResponse.value = response
            }

            override fun setErrorResponse(error: String) {
                var response = AddFavResponse()
                response.error = true
                apiResponse.value = response
            }
        })

        return apiResponse
    }


    fun getlistnoti(input: ApiInput): LiveData<ListNotificationResponse>? {

        val apiResponse: MutableLiveData<ListNotificationResponse> = MutableLiveData()

        Api.postMethod(input, object : ApiResponseCallback {
            override fun setResponseSuccess(jsonObject: JSONObject) {
                val gson = Gson()
                var response: ListNotificationResponse =
                    gson.fromJson(jsonObject.toString(), ListNotificationResponse::class.java)
                apiResponse.value = response
            }

            override fun setErrorResponse(error: String) {
                var response = ListNotificationResponse()
                response.error = true
                apiResponse.value = response
            }
        })

        return apiResponse
    }



    fun clearnotification(input: ApiInput): LiveData<DeleteAddressResponse>? {

        val apiResponse: MutableLiveData<DeleteAddressResponse> = MutableLiveData()

        Api.postMethod(input, object : ApiResponseCallback {
            override fun setResponseSuccess(jsonObject: JSONObject) {
                val gson = Gson()
                var response: DeleteAddressResponse =
                    gson.fromJson(jsonObject.toString(), DeleteAddressResponse::class.java)
                apiResponse.value = response
            }

            override fun setErrorResponse(error: String) {
                var response = DeleteAddressResponse()
                response.error = true
                apiResponse.value = response
            }
        })

        return apiResponse
    }




    fun getupdatecarrt(input: ApiInput): LiveData<UpdateCartResponse>? {

        val apiResponse: MutableLiveData<UpdateCartResponse> = MutableLiveData()

        Api.postMethod(input, object : ApiResponseCallback {
            override fun setResponseSuccess(jsonObject: JSONObject) {
                val gson = Gson()
                var response: UpdateCartResponse =
                    gson.fromJson(jsonObject.toString(), UpdateCartResponse::class.java)
                apiResponse.value = response
            }

            override fun setErrorResponse(error: String) {
                var response = UpdateCartResponse()
                response.error = true
                apiResponse.value = response
            }
        })

        return apiResponse
    }

    fun getplaceorder(input: ApiInput): LiveData<PlaceOrderResponse>? {

        val apiResponse: MutableLiveData<PlaceOrderResponse> = MutableLiveData()

        Api.postMethod(input, object : ApiResponseCallback {
            override fun setResponseSuccess(jsonObject: JSONObject) {
                val gson = Gson()
                var response: PlaceOrderResponse =
                    gson.fromJson(jsonObject.toString(), PlaceOrderResponse::class.java)
                apiResponse.value = response
            }

            override fun setErrorResponse(error: String) {
                var response = PlaceOrderResponse()
                response.error = true
                apiResponse.value = response
            }
        })

        return apiResponse
    }

    fun getorderlist(input: ApiInput): LiveData<MyOrderResponse>? {

        val apiResponse: MutableLiveData<MyOrderResponse> = MutableLiveData()

        Api.postMethod(input, object : ApiResponseCallback {
            override fun setResponseSuccess(jsonObject: JSONObject) {
                val gson = Gson()
                var response: MyOrderResponse =
                    gson.fromJson(jsonObject.toString(), MyOrderResponse::class.java)
                apiResponse.value = response
            }

            override fun setErrorResponse(error: String) {
                var response = MyOrderResponse()
                response.error = true
                apiResponse.value = response
            }
        })

        return apiResponse
    }

    fun getcancelorder(input: ApiInput): LiveData<CancelOrderResponse>? {

        val apiResponse: MutableLiveData<CancelOrderResponse> = MutableLiveData()

        Api.postMethod(input, object : ApiResponseCallback {
            override fun setResponseSuccess(jsonObject: JSONObject) {
                val gson = Gson()
                var response: CancelOrderResponse =
                    gson.fromJson(jsonObject.toString(), CancelOrderResponse::class.java)
                apiResponse.value = response
            }

            override fun setErrorResponse(error: String) {
                var response = CancelOrderResponse()
                response.error = true
                apiResponse.value = response
            }
        })

        return apiResponse
    }

    fun getreorder(input: ApiInput): LiveData<CancelOrderResponse>? {

        val apiResponse: MutableLiveData<CancelOrderResponse> = MutableLiveData()

        Api.postMethod(input, object : ApiResponseCallback {
            override fun setResponseSuccess(jsonObject: JSONObject) {
                val gson = Gson()
                var response: CancelOrderResponse =
                    gson.fromJson(jsonObject.toString(), CancelOrderResponse::class.java)
                apiResponse.value = response
            }

            override fun setErrorResponse(error: String) {
                var response = CancelOrderResponse()
                response.error = true
                apiResponse.value = response
            }
        })

        return apiResponse
    }

    fun getdeleteall(input: ApiInput): LiveData<CancelOrderResponse>? {

        val apiResponse: MutableLiveData<CancelOrderResponse> = MutableLiveData()

        Api.postMethod(input, object : ApiResponseCallback {
            override fun setResponseSuccess(jsonObject: JSONObject) {
                val gson = Gson()
                var response: CancelOrderResponse =
                    gson.fromJson(jsonObject.toString(), CancelOrderResponse::class.java)
                apiResponse.value = response
            }

            override fun setErrorResponse(error: String) {
                var response = CancelOrderResponse()
                response.error = true
                apiResponse.value = response
            }
        })

        return apiResponse
    }



    fun getdayslot(input: ApiInput): LiveData<DaySlotResponse>? {

        val apiResponse: MutableLiveData<DaySlotResponse> = MutableLiveData()

        Api.postMethod(input, object : ApiResponseCallback {
            override fun setResponseSuccess(jsonObject: JSONObject) {
                val gson = Gson()
                var response: DaySlotResponse =
                    gson.fromJson(jsonObject.toString(), DaySlotResponse::class.java)
                apiResponse.value = response
            }

            override fun setErrorResponse(error: String) {
                var response = DaySlotResponse()
                response.error = true
                apiResponse.value = response
            }
        })

        return apiResponse
    }


    fun gettimeslot(input: ApiInput): LiveData<TimeSlotResponse>? {

        val apiResponse: MutableLiveData<TimeSlotResponse> = MutableLiveData()

        Api.postMethod(input, object : ApiResponseCallback {
            override fun setResponseSuccess(jsonObject: JSONObject) {
                val gson = Gson()
                var response: TimeSlotResponse =
                    gson.fromJson(jsonObject.toString(), TimeSlotResponse::class.java)
                apiResponse.value = response
            }

            override fun setErrorResponse(error: String) {
                var response = TimeSlotResponse()
                response.error = true
                apiResponse.value = response
            }
        })

        return apiResponse
    }

    fun getreservetable(input: ApiInput): LiveData<ReserveTableModel>? {

        val apiResponse: MutableLiveData<ReserveTableModel> = MutableLiveData()

        Api.postMethod(input, object : ApiResponseCallback {
            override fun setResponseSuccess(jsonObject: JSONObject) {
                val gson = Gson()
                var response: ReserveTableModel =
                    gson.fromJson(jsonObject.toString(), ReserveTableModel::class.java)
                apiResponse.value = response
            }

            override fun setErrorResponse(error: String) {
                var response = ReserveTableModel()
                response.error = true
                apiResponse.value = response
            }
        })

        return apiResponse
    }



    fun cancelbooking(input: ApiInput): LiveData<CancelBookingResponse>? {

        val apiResponse: MutableLiveData<CancelBookingResponse> = MutableLiveData()

        Api.postMethod(input, object : ApiResponseCallback {
            override fun setResponseSuccess(jsonObject: JSONObject) {
                val gson = Gson()
                var response: CancelBookingResponse =
                    gson.fromJson(jsonObject.toString(), CancelBookingResponse::class.java)
                apiResponse.value = response
            }

            override fun setErrorResponse(error: String) {
                var response = CancelBookingResponse()
                response.error = true
                apiResponse.value = response
            }
        })

        return apiResponse
    }



    fun bookinglist(input: ApiInput): LiveData<BookingListResponse>? {

        val apiResponse: MutableLiveData<BookingListResponse> = MutableLiveData()

        Api.postMethod(input, object : ApiResponseCallback {
            override fun setResponseSuccess(jsonObject: JSONObject) {
                val gson = Gson()
                var response: BookingListResponse =
                    gson.fromJson(jsonObject.toString(), BookingListResponse::class.java)
                apiResponse.value = response
            }

            override fun setErrorResponse(error: String) {
                var response = BookingListResponse()
                response.error = true
                apiResponse.value = response
            }
        })

        return apiResponse
    }


    fun tablebookingmenu(input: ApiInput): LiveData<TableBookingMenuResponse>? {

        val apiResponse: MutableLiveData<TableBookingMenuResponse> = MutableLiveData()

        Api.postMethod(input, object : ApiResponseCallback {
            override fun setResponseSuccess(jsonObject: JSONObject) {
                val gson = Gson()
                var response: TableBookingMenuResponse =
                    gson.fromJson(jsonObject.toString(), TableBookingMenuResponse::class.java)
                apiResponse.value = response
            }

            override fun setErrorResponse(error: String) {
                var response = TableBookingMenuResponse()
                response.error = true
                apiResponse.value = response
            }
        })

        return apiResponse
    }


    fun queuecount(input: ApiInput): LiveData<GetQueueCountResponse>? {

        val apiResponse: MutableLiveData<GetQueueCountResponse> = MutableLiveData()

        Api.postMethod(input, object : ApiResponseCallback {
            override fun setResponseSuccess(jsonObject: JSONObject) {
                val gson = Gson()
                var response: GetQueueCountResponse =
                    gson.fromJson(jsonObject.toString(), GetQueueCountResponse::class.java)
                apiResponse.value = response
            }

            override fun setErrorResponse(error: String) {
                var response = GetQueueCountResponse()
                response.error = true
                apiResponse.value = response
            }
        })

        return apiResponse
    }

    fun addrestuarantInqueue(input: ApiInput): LiveData<AddRestuarantInqueue>? {

        val apiResponse: MutableLiveData<AddRestuarantInqueue> = MutableLiveData()

        Api.postMethod(input, object : ApiResponseCallback {
            override fun setResponseSuccess(jsonObject: JSONObject) {
                val gson = Gson()
                var response: AddRestuarantInqueue =
                    gson.fromJson(jsonObject.toString(), AddRestuarantInqueue::class.java)
                apiResponse.value = response
            }

            override fun setErrorResponse(error: String) {
                var response = AddRestuarantInqueue()
                response.error = true
                apiResponse.value = response
            }
        })

        return apiResponse
    }


    fun checkfavrestuarannt(input: ApiInput): LiveData<CheckFavrouiteRestuarant>? {

        val apiResponse: MutableLiveData<CheckFavrouiteRestuarant> = MutableLiveData()

        Api.postMethod(input, object : ApiResponseCallback {
            override fun setResponseSuccess(jsonObject: JSONObject) {
                val gson = Gson()
                var response: CheckFavrouiteRestuarant =
                    gson.fromJson(jsonObject.toString(), CheckFavrouiteRestuarant::class.java)
                apiResponse.value = response
            }

            override fun setErrorResponse(error: String) {
                var response = CheckFavrouiteRestuarant()
                response.error = true
                apiResponse.value = response
            }
        })

        return apiResponse
    }



    fun addinqueue(input: ApiInput): LiveData<AddInQueueResponse>? {

        val apiResponse: MutableLiveData<AddInQueueResponse> = MutableLiveData()

        Api.postMethod(input, object : ApiResponseCallback {
            override fun setResponseSuccess(jsonObject: JSONObject) {
                val gson = Gson()
                var response: AddInQueueResponse =
                    gson.fromJson(jsonObject.toString(), AddInQueueResponse::class.java)
                apiResponse.value = response
            }

            override fun setErrorResponse(error: String) {
                var response = AddInQueueResponse()
                response.error = true
                apiResponse.value = response
            }
        })

        return apiResponse
    }



    fun getfaq(input: ApiInput): LiveData<FaqModelResponse>? {

        val apiResponse: MutableLiveData<FaqModelResponse> = MutableLiveData()

        Api.getMethod(input, object : ApiResponseCallback {
            override fun setResponseSuccess(jsonObject: JSONObject) {
                val gson = Gson()
                var response: FaqModelResponse =
                    gson.fromJson(jsonObject.toString(), FaqModelResponse::class.java)
                response.error = false
                apiResponse.value = response
            }

            override fun setErrorResponse(error: String) {
                var response = FaqModelResponse()
                response.error = true
                apiResponse.value = response
            }
        })

        return apiResponse
    }



    fun bookingpayment(input: ApiInput): LiveData<BookingPaymentResponse>? {

        val apiResponse: MutableLiveData<BookingPaymentResponse> = MutableLiveData()

        Api.postMethod(input, object : ApiResponseCallback {
            override fun setResponseSuccess(jsonObject: JSONObject) {
                val gson = Gson()
                var response: BookingPaymentResponse =
                    gson.fromJson(jsonObject.toString(), BookingPaymentResponse::class.java)
                apiResponse.value = response
            }

            override fun setErrorResponse(error: String) {
                var response = BookingPaymentResponse()
                response.error = true
                apiResponse.value = response
            }
        })

        return apiResponse
    }







    fun comfirmbooking(input: ApiInput): LiveData<ComfirmBookingResponse>? {

        val apiResponse: MutableLiveData<ComfirmBookingResponse> = MutableLiveData()

        Api.postMethod(input, object : ApiResponseCallback {
            override fun setResponseSuccess(jsonObject: JSONObject) {
                val gson = Gson()
                var response: ComfirmBookingResponse =
                    gson.fromJson(jsonObject.toString(), ComfirmBookingResponse::class.java)
                apiResponse.value = response
            }

            override fun setErrorResponse(error: String) {
                var response = ComfirmBookingResponse()
                response.error = true
                apiResponse.value = response
            }
        })

        return apiResponse
    }

    fun payment(apiParams: ApiInput): MutableLiveData<PaymentGatewayResponse> {
        val apiResponse: MutableLiveData<PaymentGatewayResponse> = MutableLiveData()

        Api.postMethod(apiParams, object : ApiResponseCallback {
            override fun setResponseSuccess(jsonObject: JSONObject) {
                val gson = Gson()
                var response: PaymentGatewayResponse =
                    gson.fromJson(jsonObject.toString(), PaymentGatewayResponse::class.java)
                apiResponse.value = response
            }

            override fun setErrorResponse(error: String) {
                var response = PaymentGatewayResponse()
                //response.error = true
                response.message = error
                apiResponse.value = response
            }
        })

        return apiResponse

    }


    fun payment1(apiParams: ApiInput): MutableLiveData<PaymentGatewayResponse> {
        val apiResponse: MutableLiveData<PaymentGatewayResponse> = MutableLiveData()

        Api.getMethod(apiParams, object : ApiResponseCallback {
            override fun setResponseSuccess(jsonObject: JSONObject) {
                val gson = Gson()
                var response: PaymentGatewayResponse =
                    gson.fromJson(jsonObject.toString(), PaymentGatewayResponse::class.java)
                apiResponse.value = response
            }

            override fun setErrorResponse(error: String) {
                var response = PaymentGatewayResponse()
                //response.error = true
                response.message = error
                apiResponse.value = response
            }
        })

        return apiResponse

    }

    fun payment2(apiParams: ApiInput): MutableLiveData<PaymentGatewayResponse1> {
        val apiResponse: MutableLiveData<PaymentGatewayResponse1> = MutableLiveData()

        Api.postMethod(apiParams, object : ApiResponseCallback {
            override fun setResponseSuccess(jsonObject: JSONObject) {
                val gson = Gson()
                var response: PaymentGatewayResponse1 =
                    gson.fromJson(jsonObject.toString(), PaymentGatewayResponse1::class.java)
                apiResponse.value = response
            }

            override fun setErrorResponse(error: String) {
                var response = PaymentGatewayResponse1()
                //response.error = true
                response.message = error
                apiResponse.value = response
            }
        })

        return apiResponse

    }



    fun refundpayment(apiParams: ApiInput): MutableLiveData<PaymentRefundResponse> {
        val apiResponse: MutableLiveData<PaymentRefundResponse> = MutableLiveData()

        Api.postMethod(apiParams, object : ApiResponseCallback {
            override fun setResponseSuccess(jsonObject: JSONObject) {
                val gson = Gson()
                var response: PaymentRefundResponse =
                    gson.fromJson(jsonObject.toString(), PaymentRefundResponse::class.java)
                apiResponse.value = response
            }

            override fun setErrorResponse(error: String) {
                var response = PaymentRefundResponse()
                //response.error = true
                response.message = error
                apiResponse.value = response
            }
        })

        return apiResponse

    }



    fun notifystatus(apiParams: ApiInput): MutableLiveData<NotifyStatusResponse> {
        val apiResponse: MutableLiveData<NotifyStatusResponse> = MutableLiveData()

        Api.postMethod(apiParams, object : ApiResponseCallback {
            override fun setResponseSuccess(jsonObject: JSONObject) {
                val gson = Gson()
                var response: NotifyStatusResponse =
                    gson.fromJson(jsonObject.toString(), NotifyStatusResponse::class.java)
                apiResponse.value = response
            }

            override fun setErrorResponse(error: String) {
                var response = NotifyStatusResponse()
                //response.error = true
                response.message = error
                apiResponse.value = response
            }
        })

        return apiResponse

    }



    fun checkpengingg(apiParams: ApiInput): MutableLiveData<CheckPendingOrders> {
        val apiResponse: MutableLiveData<CheckPendingOrders> = MutableLiveData()

        Api.postMethod(apiParams, object : ApiResponseCallback {
            override fun setResponseSuccess(jsonObject: JSONObject) {
                val gson = Gson()
                var response: CheckPendingOrders =
                    gson.fromJson(jsonObject.toString(), CheckPendingOrders::class.java)
                    apiResponse.value = response
            }

            override fun setErrorResponse(error: String) {
                var response = CheckPendingOrders()
              //  response.error = true
                response.message = error
                apiResponse.value = response
            }
        })

        return apiResponse

    }



    fun validateoffer(apiParams: ApiInput): MutableLiveData<ValidateOfferResponse> {
        val apiResponse: MutableLiveData<ValidateOfferResponse> = MutableLiveData()

        Api.postMethod(apiParams, object : ApiResponseCallback {
            override fun setResponseSuccess(jsonObject: JSONObject) {
                val gson = Gson()
                var response: ValidateOfferResponse =
                    gson.fromJson(jsonObject.toString(), ValidateOfferResponse::class.java)
                apiResponse.value = response
            }

            override fun setErrorResponse(error: String) {
                var response = ValidateOfferResponse()
                //  response.error = true
                response.message = error
                apiResponse.value = response
            }
        })

        return apiResponse

    }



    fun getpointResponse(apiParams: ApiInput): MutableLiveData<GetPointResponse> {
        val apiResponse: MutableLiveData<GetPointResponse> = MutableLiveData()

        Api.postMethod(apiParams, object : ApiResponseCallback {
            override fun setResponseSuccess(jsonObject: JSONObject) {
                val gson = Gson()
                var response: GetPointResponse =
                    gson.fromJson(jsonObject.toString(), GetPointResponse::class.java)
                apiResponse.value = response
            }

            override fun setErrorResponse(error: String) {
                var response = GetPointResponse()
                //  response.error = true
                response.message = error
                apiResponse.value = response
            }
        })

        return apiResponse

    }



    fun getpointHistoryResponse(apiParams: ApiInput): MutableLiveData<PointsHistory> {
        val apiResponse: MutableLiveData<PointsHistory> = MutableLiveData()

        Api.postMethod(apiParams, object : ApiResponseCallback {
            override fun setResponseSuccess(jsonObject: JSONObject) {
                val gson = Gson()
                var response: PointsHistory =
                    gson.fromJson(jsonObject.toString(), PointsHistory::class.java)
                apiResponse.value = response
            }

            override fun setErrorResponse(error: String) {
                var response = PointsHistory()
                //  response.error = true
                response.message = error
                apiResponse.value = response
            }
        })

        return apiResponse

    }





    fun getListOfferResponse(apiParams: ApiInput): MutableLiveData<ListOfferResponse> {
        val apiResponse: MutableLiveData<ListOfferResponse> = MutableLiveData()

        Api.postMethod(apiParams, object : ApiResponseCallback {
            override fun setResponseSuccess(jsonObject: JSONObject) {
                val gson = Gson()
                var response: ListOfferResponse =
                    gson.fromJson(jsonObject.toString(), ListOfferResponse::class.java)
                apiResponse.value = response
            }

            override fun setErrorResponse(error: String) {
                var response = ListOfferResponse()
                //  response.error = true
                response.message = error
                apiResponse.value = response
            }
        })

        return apiResponse

    }




}