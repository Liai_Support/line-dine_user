package com.lia.yello.linedine.Models

import com.google.gson.annotations.SerializedName
import java.io.Serializable

class GetPointResponse : Serializable {

    @SerializedName("error")
    var error: Boolean? = null

    @SerializedName("message")
    var message: String? = null

    @SerializedName("data")
    var data: Points? = null

}

class Points : Serializable {

    @SerializedName("points")
    var points: String? = null


    @SerializedName("userid")
    var userid: Int? = null


    @SerializedName("name")
    var name: String? = null

}