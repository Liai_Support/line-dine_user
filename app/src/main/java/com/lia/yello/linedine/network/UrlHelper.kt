package com.lia.yello.linedine.network

import com.lia.yello.linedine.BuildConfig
import com.lia.yello.linedine.Models.PointsHistory
import com.lia.yello.linedine.R
import com.lia.yello.linedine.activity.AppController


object UrlHelper {
 // private const val BASE = "http://192.168.1.12/"
 private const val BASE = "http://staging.lineandine.com"
 private const val BASE_URL = BASE + "/line_dine_api/customer_ver2/"
 private const val BASE_URL2 = BASE + "/customer_ver3/"
 //private const val BASE_URL = BASE + "/line_dine_api/customer/"

 //const val CREATE_ACCOUNT = BASE_URL + "customersignup"
 const val CREATE_ACCOUNT = BASE_URL2 + "customersignup"
 //const val LOGIN = BASE_URL + "custlogin"
 const val LOGIN = BASE_URL2 + "custlogin"
 //const val GET_PROFILE = BASE_URL + "viewprofile"
 const val GET_PROFILE = BASE_URL2 + "viewprofile"
 const val UPDATE_PROFILE = BASE_URL + "updateprofile"
 //const val CHECK_SOCIAL_LOGIN = BASE_URL + "checksocialmedialogin"
 const val CHECK_SOCIAL_LOGIN = BASE_URL2 + "checksocialmedialogin"
 //const val SOCIAL_SINGUP = BASE_URL + "socialmedia_signup"
 const val SOCIAL_SINGUP = BASE_URL2 + "socialmedia_signup"

 const val GOOGLE_LOGIN = BASE_URL + "socialmedia_signup"
 const val FACEBOOKLOGIN = BASE_URL + "facebookLogin"

 // added by kanika


 const val GETHOME =  BASE_URL2 + "initialvalues"
 const val LISTRESTURANT =  BASE_URL2 + "listrestaurant"
 const val ITEMDETAILS =  BASE_URL2+ "itemdet"
 const val MENUDETAILS =  BASE_URL2 + "menu"
 const val ADDCART =  BASE_URL2+"addcart"
 const val Fav =  BASE_URL2 + "list_fav"
 const val ORDERLIST = BASE_URL2  + "getcart"
 const val DELETEORDER =  BASE_URL2 + "deletecart"
 const val ADDADDRESS =  BASE_URL2 + "add_address"
 const val DELETEADDRESS =  BASE_URL2 + "delete_address"
 const val LISTADDRESS =  BASE_URL2+ "list_address"
 const val UPDATEADDRESS =  BASE_URL2  + "update_address"
 const val ADDWALLET =  BASE_URL2 + "updatewallet"
 const val GETWALLETAMOUNT =  BASE_URL2  + "getwallet"
 const val ADDFAV = BASE_URL2+ "addfav"
 const val WALLETHIS =  BASE_URL2 + "wallethistory"
 const val LISTNOTI =  BASE_URL2 + "listnotification"
 const val DELETENOTI =  BASE_URL2  + "clearnotification"
 const val UPDATECART =  BASE_URL2 + "updatecart"
 const val PLACEORDER =  BASE_URL2 + "placeorder"
 // const val PLACEORDER =   "https://lineanddine.com/line_dine_api/customer/placeorder"
 const val PLACEORDERORDERLIST =  BASE_URL2+ "orderlist"
 const val CANCELORDER =  BASE_URL2 + "cancelorder"
 const val REORDER = BASE_URL2 + "reorder"
 const val REVIEW =  BASE_URL2 + "rating"
 const val DELETECART =  BASE_URL2 + "removecart"
 const val FORGOTPASSWORD =  BASE_URL2 + "forgotpassword"
 const val UPDATEPASSWORD =  BASE_URL2 + "updatepassword"
 const val UPDATEADDRESSID =  BASE_URL2 + "updateaddressid"


 // FINE DINE API
 const val DAYSLOT =  BASE_URL2 + "listdates"
 const val TIMESLOT =  BASE_URL2  +  "listtimeslot"
 const val RESERVETABLE =  BASE_URL2 +  "tablebooking"
 const val UPDATETOKEN =  BASE_URL2 + "updateusertoken"
 const val CANCELBOOKING =  BASE_URL2 + "cancelbooking"
 const val BOOKINGLIST =  BASE_URL2 + "bookinglist"
 const val TABLEBOOKINGMENU =  BASE_URL2 + "tablebooking_menu"
 const val GETQUEUECOUNT =  BASE_URL2 + "getqueuecount"
 const val ADDINQUEUE =  BASE_URL2 + "addinqueue"
 const val BOOKINGPAYMENT =  BASE_URL2 + "bookingpayment"
 const val CONFIRMBOOKING =  BASE_URL2 + "confirmbooking"
 const val NOTIFY_STATUS =  BASE_URL2 + "notifystatus"
 const val CHECK_PENDING =  BASE_URL2 + "checkpending"
 const val FAQ =  BASE_URL2 + "faq"
 const val PAYMENT_URL = "https://api-test.noonpayments.com/payment/v1/order"
 const val PAYMENT_KEY ="Key_Test bGluZV9hbmRfZGluZS5MaW5lYW5kRGluZTpjNWJkYTc3ODRmNWQ0YmI5YWI5MWI3ZjBjZjM0MzFiOQ=="
 const val VALIDATEOFFER =  BASE_URL2 + "validateoffer"
 const val GetPointResponse =  BASE_URL2 + "getpoints"
 const val PointsHistory =  BASE_URL2 + "pointshistory"
 const val LISTOFFERS =  BASE_URL2 + "listoffers"
 const val VERIFYOTP =  BASE_URL2 + "verifyOtp"
 const val DISCOUNTEVENT =  BASE_URL2 + "discountevents"
 const val DISCOUNTEVENTDATES =  BASE_URL2 + "discountevent_dates"

 const val ADDRESTUARANTQUEUE =  BASE_URL2 + "add_restaurantqueue"
 const val CHECKFAVRESTUARNT =  BASE_URL2 + "checkfavrestaurant"

 /*  const val GETHOME = "https://lineanddine.com/line_dine_api/customer/initialvalues"
   const val LISTRESTURANT = "https://lineanddine.com/line_dine_api/customer/listrestaurant"
   const val ITEMDETAILS = "https://lineanddine.com/line_dine_api/customer/itemdet"
   const val MENUDETAILS = "https://lineanddine.com/line_dine_api/customer/menu"
   const val ADDCART = "https://lineanddine.com/line_dine_api/customer/addcart"
   const val Fav = "https://lineanddine.com/line_dine_api/customer/list_fav"
   const val ORDERLIST = "https://lineanddine.com/line_dine_api/customer/getcart"
   const val DELETEORDER = "https://lineanddine.com/line_dine_api/customer/deletecart"
   const val ADDADDRESS = "https://lineanddine.com/line_dine_api/customer/add_address"
   const val DELETEADDRESS = "https://lineanddine.com/line_dine_api/customer/delete_address"
   const val LISTADDRESS = "https://lineanddine.com/line_dine_api/customer/list_address"
   const val UPDATEADDRESS = "https://lineanddine.com/line_dine_api/customer/update_address"
   const val ADDWALLET = "https://lineanddine.com/line_dine_api/customer/updatewallet"
   const val GETWALLETAMOUNT = "https://lineanddine.com/line_dine_api/customer/getwallet"
   const val ADDFAV = "https://lineanddine.com/line_dine_api/customer/addfav"
   const val WALLETHIS = "https://lineanddine.com/line_dine_api/customer/wallethistory"
   const val LISTNOTI = "https://lineanddine.com/line_dine_api/customer/listnotification"
   const val DELETENOTI = "https://lineanddine.com/line_dine_api/customer/clearnotification"
   const val UPDATECART = "https://lineanddine.com/line_dine_api/customer/updatecart"
   const val PLACEORDER = "https://lineanddine.com/line_dine_api/customer/placeorder"
   const val PLACEORDERORDERLIST = "https://lineanddine.com/line_dine_api/customer/orderlist"
   const val CANCELORDER = "https://lineanddine.com/line_dine_api/customer/cancelorder"
   const val REORDER = "https://lineanddine.com/line_dine_api/customer/reorder"
   const val REVIEW = "https://lineanddine.com/line_dine_api/customer/rating"
   const val DELETECART = "https://lineanddine.com/line_dine_api/customer/removecart"
   const val FORGOTPASSWORD = "https://lineanddine.com/line_dine_api/customer/forgotpassword"
   const val UPDATEPASSWORD = "https://lineanddine.com/line_dine_api/customer/updatepassword"
   const val UPDATEADDRESSID = "https://lineanddine.com/line_dine_api/customer/updateaddressid"

   const val DAYSLOT = "https://lineanddine.com/line_dine_api/customer/listdates"
   const val TIMESLOT = "https://lineanddine.com/line_dine_api/customer/listtimeslot"
   const val RESERVETABLE = "https://lineanddine.com/line_dine_api/customer/tablebooking"
   const val UPDATETOKEN = "https://lineanddine.com/line_dine_api/customer/updateusertoken"
   const val CANCELBOOKING = "https://lineanddine.com/line_dine_api/customer/updateusertoken"
*/

 const val GOOGLE_API_BASE_URL = "https://maps.googleapis.com/maps/api/"
 const val GOOGLE_API_DIRECTION_BASE_URL = GOOGLE_API_BASE_URL + "directions/json?"
 const val GOOGLE_API_AUTOCOMPLETE_BASE_URL = GOOGLE_API_BASE_URL + "place/autocomplete/json?"
 const val GOOGLE_API_PLACE_DETAILS_BASE_URL = GOOGLE_API_BASE_URL + "place/details/json?"
 const val GOOGLE_API_STATIC_MAP_BASE_URL = GOOGLE_API_BASE_URL + "staticmap?"
 const val GOOGLE_API_GEOCODE_BASE_URL = GOOGLE_API_BASE_URL + "geocode/json?"


 fun getAddress(
  latitude: Double,
  longitude: Double
 ): String? {
  val lat = latitude.toString()
  val lngg = longitude.toString()
  return (GOOGLE_API_GEOCODE_BASE_URL + "latlng=" + lat + ","
          + lngg + "&sensor=true&key=" + AppController.getInstance().resources.getString(
   R.string.map_api_key
  ))
 }

}