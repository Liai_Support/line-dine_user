package com.lia.yello.linedine.fragment


import android.app.Activity
import android.app.AlertDialog
import android.content.Context
import android.content.Intent
import android.content.res.ColorStateList
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.os.Bundle
import android.text.Editable
import android.text.TextWatcher
import android.util.DisplayMetrics
import android.util.Log
import android.view.Gravity
import android.view.LayoutInflater
import android.view.View
import android.view.WindowManager
import android.view.inputmethod.InputMethodManager
import android.widget.TextView
import android.widget.Toast
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentTransaction
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.google.android.material.bottomsheet.BottomSheetBehavior
import com.lia.yello.linedine.Models.DiscountEventDateJson
import com.lia.yello.linedine.Models.PromocodeAmount
import com.lia.yello.linedine.Models.orderlist
import com.lia.yello.linedine.R
import com.lia.yello.linedine.Session.SharedHelper
import com.lia.yello.linedine.Utils.DialogUtils
import com.lia.yello.linedine.Utils.NetworkUtils
import com.lia.yello.linedine.Utils.UiUtils
import com.lia.yello.linedine.ViewModels.MapViewModel
import com.lia.yello.linedine.activity.DashBoardActivity
import com.lia.yello.linedine.activity.NoonPaymentActivity
import com.lia.yello.linedine.adapter.*
import com.lia.yello.linedine.databinding.*
import kotlinx.android.synthetic.main.card_suborders.*
import kotlinx.android.synthetic.main.carticon.*
import kotlinx.android.synthetic.main.fragment_fine_dine_order_list_status.*
import kotlinx.android.synthetic.main.fragment_subhome.recycler
import kotlinx.android.synthetic.main.fragment_suborders.*
import kotlinx.android.synthetic.main.fragment_subsubsubhome.*
import kotlinx.android.synthetic.main.fragment_wallet.*
import kotlinx.android.synthetic.main.header.*
import kotlinx.android.synthetic.main.header.back
import kotlinx.android.synthetic.main.placeorder.view.*
import kotlinx.android.synthetic.main.pop_up_movetopay.view.*
import kotlinx.android.synthetic.main.rulepagepickup.view.*
import kotlinx.android.synthetic.main.rulespage.*
import java.text.NumberFormat
import java.util.*


class CartFragment : Fragment(R.layout.fragment_suborders) {
    var binding: FragmentSubordersBinding? = null
    var mapViewModel: MapViewModel? = null
    private lateinit var fragmentTransaction: FragmentTransaction
    private var orderSucessFragment: OrderSucessFragment? = null
    private var homeFragment: HomeFragment? = null
    private var cartFragment: CartFragment? = null
    var subtotal: Double = 0.0
    var totalamt:Double=0.0
    var vat:Double=0.0
    var promocodearray:ArrayList<PromocodeAmount>? = ArrayList<PromocodeAmount>()

    var list: ArrayList<orderlist>? = null
    var sharedHelper: SharedHelper? = null
    var placeorderboolean:Boolean=false
    var orderid:Int=0
    var stocknotavailable:Boolean=false
    var descrip:String=""
    var promocode:String=""
    var restuarntid:Int=0
    var minimumanount:Double=0.0
    var uptoamount:Double=0.0
    var diss:Double=0.0
    var tmt:Double=0.0
    var ordersubtotaltt:Double=0.0
    var finaltotal:Double=0.0
    var promocodelist: MutableList<String> = mutableListOf<String>()

    var discountid:Int=0
    var discountamount:Double=0.0
    var finalvat:Int=0
    var isapplied:String="false"
    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        binding = DataBindingUtil.findBinding(view)
        sharedHelper = SharedHelper(requireContext())

        Log.d("ololo","kanikashanmugam")
        mapViewModel = ViewModelProvider(this).get(MapViewModel::class.java)
        val head = view.findViewById<TextView>(R.id.head)
        head.setText(getString(R.string.mycart))
        (activity as DashBoardActivity?)!!.mBottomSheetBehavior2?.setState(BottomSheetBehavior.STATE_HIDDEN)
        (activity as DashBoardActivity?)!!.mBottomSheetBehavior?.setState(BottomSheetBehavior.STATE_HIDDEN)
        back.setOnClickListener(View.OnClickListener {
            //requireActivity().supportFragmentManager.popBackStack()
            val intent = Intent(requireContext(), DashBoardActivity::class.java)
            startActivity(intent)
        })
        if (!NetworkUtils.isNetworkConnected(requireContext())) {
            UiUtils.showSnack(
                view, "no_internet_connection"
            )
        }


        if (sharedHelper!!.language == "ar"){
            back.rotation= 180F
            val i = 1
            val nf = NumberFormat.getInstance(Locale("hi", "IN"))
            nf.format(i.toLong())
        }

        placeorder.setOnClickListener(View.OnClickListener {

            if (stocknotavailable==true)
            {
                UiUtils.showSnack(root_suborder,getString(R.string.pleaseremoveunavailableitem))
            }
            else{
                val mDialogView = LayoutInflater.from(requireContext()).inflate(R.layout.rulepagepickup, null)
                val mBuilder = AlertDialog.Builder(requireContext()).setView(mDialogView).setCancelable(false)
                val mAlertDialog = mBuilder.show()
                val layoutParams = WindowManager.LayoutParams()
                val displayMetrics = DisplayMetrics()
                requireActivity().windowManager.defaultDisplay.getMetrics(displayMetrics)
                val displayWidth = displayMetrics.widthPixels
                val displayHeight = displayMetrics.heightPixels
                layoutParams.copyFrom(mAlertDialog.window?.attributes)
                layoutParams.width = ((displayWidth * 0.9f).toInt())
                mAlertDialog.window?.setAttributes(layoutParams)
                mAlertDialog.window?.setGravity(Gravity.CENTER)
                mAlertDialog.window?.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))

                mDialogView.check1.setOnCheckedChangeListener { buttonView, isChecked ->
                    if (isChecked) {
                        mDialogView.check1.buttonTintList= ColorStateList.valueOf(resources.getColor(R.color.colorPrimary))
                    }else{
                        mDialogView.check1.buttonTintList= ColorStateList.valueOf(resources.getColor(R.color.grey2))
                    }
                }
                mDialogView.procced.setOnClickListener {

                    if (mDialogView.check1.isChecked){
                        mAlertDialog.dismiss()


                        val mDialogView = LayoutInflater.from(requireContext()).inflate(R.layout.placeorder, null)
                        val mBuilder = AlertDialog.Builder(requireContext()).setView(mDialogView).setCancelable(true)
                        val mAlertDialog = mBuilder.show()
                        val layoutParams = WindowManager.LayoutParams()
                        val displayMetrics = DisplayMetrics()
                        requireActivity().windowManager.defaultDisplay.getMetrics(displayMetrics)
                        val displayWidth = displayMetrics.widthPixels
                        val displayHeight = displayMetrics.heightPixels
                        layoutParams.copyFrom(mAlertDialog.window?.getAttributes())
                        layoutParams.width = ((displayWidth * 0.9f).toInt())
                        mAlertDialog.window?.setAttributes(layoutParams)
                        mAlertDialog.window?.setGravity(Gravity.CENTER)
                        mAlertDialog.window?.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
                        mDialogView.checkoutcard.setOnClickListener {
                            startActivityForResult(
                                Intent(requireContext(), NoonPaymentActivity::class.java)
                                    .putExtra(
                                        "amount",
                                        finaltotal
                                    ), 100
                            )
                            mAlertDialog.dismiss()

                        }
                        mDialogView.checkoutwallet.setOnClickListener {
                            getwalletamount(finaltotal)
                            mAlertDialog.dismiss()

                        }

                    }else{
                        UiUtils.showSnack(root_suborder,getString(R.string.plscheckthebox))
                        //  mAlertDialog.dismiss()


                    }


                }
                mDialogView.back.setOnClickListener {

                    mAlertDialog.dismiss()

                }

            }

        })
        cancel.setOnClickListener(View.OnClickListener {

            homeFragment = HomeFragment()
            fragmentTransaction = requireActivity().supportFragmentManager.beginTransaction()
            fragmentTransaction.setCustomAnimations(R.anim.screen_in, R.anim.screen_out)
            fragmentTransaction.replace(R.id.container, homeFragment!!)
            fragmentTransaction.addToBackStack(null)
            fragmentTransaction.commit()



        })

        getcartlist()



        instruction.addTextChangedListener(object : TextWatcher {
            override fun beforeTextChanged(s: CharSequence, start: Int, count: Int, after: Int) {

            }

            override fun onTextChanged(s: CharSequence, start: Int, before: Int, count: Int) {
            }

            override fun afterTextChanged(s: Editable) {
                descrip=instruction.text.toString()
                Log.d("kanikakjakajk",""+descrip)
            }
        })
        couponcodepickup.addTextChangedListener(object : TextWatcher {
            override fun beforeTextChanged(s: CharSequence, start: Int, count: Int, after: Int) {

            }

            override fun onTextChanged(s: CharSequence, start: Int, before: Int, count: Int) {
            }

            override fun afterTextChanged(s: Editable) {


                promocode=couponcodepickup.text.toString()
                Log.d("kanikakjakajk",""+promocode)
            }
        })

        applycouponpickup.setOnClickListener {
            if(promocode.equals("")){
                UiUtils.showSnack(root_suborder,getString(R.string.pleaseenterthecouponcode))

            }else{
                getvalidateoffer(promocode,restuarntid)

            }




            /*  for (i in 0 until promocodearray!!.size){
                  Log.d("sdfghjkl",promocodelist[i] )
                  Log.d("xcvbncxvb",promocode )
                  val promo=promocodelist[i]

                  if (promocodearray!![i].promocode==promocode){

                    *//*  if(promocode.equals("")){
                          UiUtils.showSnack(root_suborder,getString(R.string.pleaseenterthecouponcode))

                      }else{*//*
                          if (totalamt>promocodearray!![i].amount){
                              getvalidateoffer(promocode,restuarntid)

                               break

                          }else{

                              val imm: InputMethodManager =
                                  activity!!.getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
                              imm.hideSoftInputFromWindow(view!!.windowToken, 0)
                              UiUtils.showSnack(root_suborder,getString(R.string.minimummorderamountislow))
                          }
                     // }

                  }else{


                      if(promocode.equals("")){
                          UiUtils.showSnack(root_suborder,getString(R.string.pleaseenterthecouponcode))

                      }else{
                         // UiUtils.showSnack(root_suborder,getString(R.string.invalidcouponcode))

                      }



                  }




                  }*/




        }
    }

    public fun getcartlist(){
        DialogUtils.showLoader(requireContext())
        mapViewModel?.getorderlist(requireContext())
            ?.observe(viewLifecycleOwner,
                Observer {
                    DialogUtils.dismissLoader()
                    it?.let {
                        it.error?.let { error ->
                            Log.d("hgdsh", "" + error)
                            if (error) {
                                it.message?.let { msg ->
                                    UiUtils.showSnack(root_suborder, msg)
                                    //snackbarToast(getString(R.string.cartisempty))
                                    cart_badge.text = "0"
                                    sharedHelper!!.cartcount = 0
                                    relative_suborder.visibility = View.GONE
                                    cartframe.visibility=View.GONE

                                    // sharedHelper!!.cartcount = 0

                                    //   txtvisiblity.visibility = View.VISIBLE
                                }
                            } else {
                                it.data?.let { data ->
                                    subtotal = 0.0
                                    vat = 0.0
                                    totalamt = 0.0
                                    cart_badge.text = ""+data.orderdata?.size
                                    //   sharedHelper!!.cartcount = data.orderdata?.size!!


                                    Log.d("hhdgs", "" + data.orderdata?.size)

                                    list = data.orderdata

                                    for (i in 0 until list!!.size ) {
                                        restuarntid= data.orderdata!![i].res_id!!
                                        finalvat= data.orderdata!![i].vat!!
                                        sharedHelper!!.vat= data.orderdata!![i].vat!!.toFloat()
                                        Log.d("restuarntid", "" + restuarntid)

                                        if (list!![i].menuavilable==1){
                                            subtotal += (list!![i].cost!! * list!![i].quantity!!)
                                            subtotal=String.format(Locale("en", "US"),"%.2f",subtotal).toDouble()


                                        }else{

                                            stocknotavailable=true

                                        }
                                    }
                                    vattxtt.text= getString(R.string.vat)+"( " + finalvat+" %"+ " )"

                                    getlistoffers(restuarntid)

                                    vat = (subtotal / 100.0f) * finalvat
                                    vat=String.format(Locale("en", "US"),"%.2f",vat).toDouble()


                                    totalamt = subtotal + vat


                                    //   totalamt=String.format(Locale("en", "US"),"%.2f",totalamt).toDouble()

                                    Log.d("wertyuio",""+totalamt)
                                    // ordersubtotal.text = "$subtotal SAR"
                                    /*String.format("%.2f", data[i].initial_amount) */
                                    ordersubtotaltt=subtotal
                                    ordersubtotal.text=String.format(Locale("en", "US"),"%.2f",subtotal)+ " SAR"

                                    //  totalmount.text = "$totalamt SAR"
                                    finaltotal=totalamt
                                    totalmount.text = String.format(Locale("en", "US"),"%.2f",totalamt)+ " SAR"
                                    vat_txt.text = String.format(Locale("en", "US"),"%.2f",vat)+ " SAR"

                                    productotal.text= String.format(Locale("en", "US"),"%.2f",totalamt.toDouble())+" SAR"

                                    recycler.layoutManager = LinearLayoutManager(context)
                                    recycler.adapter = CartAdapter(
                                        this,
                                        requireContext(),
                                        data.orderdata
                                    )



                                    if(sharedHelper!!.language.equals("ar")){
                                        val swipeHelper: SwipeHelper2 = object :
                                            SwipeHelper2(requireContext(), recycler,data.orderdata) {
                                            override fun instantiateUnderlayButton(
                                                viewHolder: RecyclerView.ViewHolder?,
                                                underlayButtons: ArrayList<UnderlayButton>?
                                            ) {
                                                underlayButtons!!.add(UnderlayButton(
                                                    this@CartFragment,
                                                    data.orderdata,
                                                    getString(R.string.delete),
                                                    0,
                                                    Color.parseColor("#A41620"),
                                                    null
                                                ))
                                            }


                                        }
                                        swipeHelper.attachSwipe()
                                    }else{
                                        val swipeHelper: SwipeHelper = object :
                                            SwipeHelper(requireContext(), recycler,data.orderdata) {
                                            override fun instantiateUnderlayButton(
                                                viewHolder: RecyclerView.ViewHolder?,
                                                underlayButtons: ArrayList<UnderlayButton>?
                                            ) {
                                                underlayButtons!!.add(UnderlayButton(
                                                    this@CartFragment,
                                                    data.orderdata,
                                                    getString(R.string.delete),
                                                    0,
                                                    Color.parseColor("#A41620"),
                                                    null
                                                ))
                                            }


                                        }
                                        swipeHelper.attachSwipe()
                                    }


                                }

                            }
                        }

                    }
                })
    }


    public fun deletecart(pos: Int) {
        Log.d("scvsgdhghtf",""+pos)
        Log.d("scvsjhbhbbhgdhghtf",""+requireContext())
        Log.d("scvsgd",""+list!![pos].id!!)


        // this!!.context?.let { DialogUtils.showLoader(it) }
        DialogUtils.showLoader(requireContext())
        mapViewModel?.getdelete(requireContext(), list!![pos].id!!)
            ?.observe(viewLifecycleOwner, androidx.lifecycle.Observer {
                DialogUtils.dismissLoader()

                it?.let {
                    it.error?.let { error ->
                        if (error) {
                            it.message?.let { msg ->

                                Log.d("zxcvfgbnm,", "" + msg)
                                UiUtils.showSnack(root_suborder,msg)

                            }
                        } else {

                            it.message?.let { msg ->
                                /*  subtotal = 0.0
                                  vat = 0.0
                                  totalamt = 0.0
                                  Log.d("kanika,", "" + msg)
                                  getcartlist()*/
                                cartFragment = CartFragment()
                                fragmentTransaction = requireActivity().supportFragmentManager.beginTransaction()
                                fragmentTransaction.setCustomAnimations(R.anim.screen_in, R.anim.screen_out)
                                fragmentTransaction.replace(R.id.container, cartFragment!!)
                                fragmentTransaction.commit()
                            }


                        }
                    }
                }

            })
    }




    private fun getwalletamount(totalamt: Double) {
        DialogUtils.showLoader(requireContext())

        mapViewModel?.getwalletamount(requireContext())
            ?.observe(viewLifecycleOwner, androidx.lifecycle.Observer {
                DialogUtils.dismissLoader()

                it?.let {
                    it.error?.let { error ->
                        if (error) {
                            it.message?.let { msg ->
                                UiUtils.showSnack(root_suborder, msg)
                            }
                        } else {
                            it.data?.let { data ->
                                //  getbookingpayment()
                                if(data.amount!! >= totalamt){
                                    placeorder(totalamt,"wallet","","",descrip)
                                }else{

                                    UiUtils.showSnack(root_suborder, getString(R.string.yourwalletamountislowpleaseuplateyourwalletamount))

                                    // getbookinglist("active")
                                }
                                /*Log.d("amount", data.amount.toString())
                                var amount:Double
                                amount= data.amount!!
                                walletamount.text = amount.toString()+" SAR"
                                Log.d("sdxcfvgbnm",""+requireActivity().walletamount.text)*/

                            }

                        }
                    }

                }
            })

    }







    fun placeorder(d:Double,s: String,transid:String,payorderid:String,descrip:String) {
        DialogUtils.showLoader(requireContext())
        mapViewModel?.getplaceorder(
            requireContext(),
            s,
            subtotal,
            finaltotal,
            vat,transid,payorderid,descrip,discountid,discountamount
            // instruction.text.toString()
        )
            ?.observe(viewLifecycleOwner,
                Observer {
                    DialogUtils.dismissLoader()

                    it?.let {
                        it.error?.let { error ->
                            if (error) {
                                it.message?.let { msg ->
                                    UiUtils.showSnack(root_suborder, msg)
                                    Toast.makeText(requireContext(), msg, Toast.LENGTH_SHORT)
                                        .show()
                                }
                            } else {
                                cart_badge.text = "0"
                                it.data?.let { data ->
                                    orderid = data.insert_data!!
                                    val bundle = Bundle()
                                    bundle.putInt("orderid", orderid)

                                    orderSucessFragment = OrderSucessFragment()
                                    orderSucessFragment!!.arguments = bundle
                                    fragmentTransaction =
                                        requireActivity().supportFragmentManager.beginTransaction()
                                    fragmentTransaction.setCustomAnimations(
                                        R.anim.screen_in,
                                        R.anim.screen_out
                                    )
                                    fragmentTransaction.replace(
                                        R.id.container,
                                        orderSucessFragment!!
                                    )
                                    fragmentTransaction.addToBackStack(null)
                                    fragmentTransaction.commit()

                                }
                            }
                        }

                    }
                })

    }




    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (requestCode == 100) {
            if (resultCode == Activity.RESULT_OK) {
                Log.d("qwertyu7i8op","cfgbvgvggvvb")
                placeorder(finaltotal,"card",data!!.getStringExtra("paymentorderid").toString(),data!!.getStringExtra("paymenttransactionid").toString(),descrip)
                //  sendPaymentDetails(data!!.getStringExtra("paymentorderid").toString(),data!!.getStringExtra("paymenttransactionid").toString())

            }
        }
    }


    private fun getlistoffers(restuarntid:Int) {
        DialogUtils.showLoader(requireContext())

        mapViewModel?.getListoffers(requireContext(), restuarntid)
            ?.observe(viewLifecycleOwner, androidx.lifecycle.Observer {
                DialogUtils.dismissLoader()

                it?.let {
                    it.error?.let { error ->
                        if (error) {
                            it.message?.let { msg ->
                                //  UiUtils.showSnack(root_suborder, msg)
                            }
                        } else {
                            it.data?.let { data ->
                                for (i in 0 until data.size){

                                    promocodearray!!.add(PromocodeAmount(data[i].promocode!!,data[i].minimum_order_amount!!.toDouble()))
                                    promocodelist.add(data[i].promocode!!)
                                    //  minimumanount=data[i].minimum_order_amount!!.toDouble()
                                    Log.d("promocodelist.size",""+promocodelist.size)
                                    Log.d("b nccbncbcxnb.size",""+uptoamount)
                                    Log.d("b nccbncbcxnb.size",""+minimumanount)
                                    Log.d("b nccbncbcxnb.size",""+promocodearray)

                                }


                            }

                        }
                    }

                }
            })

    }

    private fun getvalidateoffer(promocode: String,restuarntid:Int) {
        DialogUtils.showLoader(requireContext())

        mapViewModel?.getvalidateoffer(requireContext(), restuarntid,promocode)
            ?.observe(viewLifecycleOwner, androidx.lifecycle.Observer {
                DialogUtils.dismissLoader()

                it?.let {
                    it.error?.let { error ->
                        if (error) {
                            it.message?.let { msg ->

                                UiUtils.showSnack(root_suborder, msg)

                            }
                        }else {
                            it.data?.let { data ->

                                minimumanount=data[0].minimum_order_amount!!.toDouble()
                                if(totalamt < minimumanount){
                                    val imm: InputMethodManager =
                                        activity!!.getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
                                    imm.hideSoftInputFromWindow(view!!.windowToken, 0)
                                    UiUtils.showSnack(root_suborder,getString(R.string.minimummorderamountislow))

                                }else{
                                    isapplied="true"
                                    discountlevel.visibility=View.VISIBLE
                                    UiUtils.showSnack(root_suborder,getString(R.string.promocodeapplied))
                                    applycouponpickup.setEnabled(false)
                                    couponcodepickup.setEnabled(false)
                                    productotalrelative.visibility=View.VISIBLE
                                    uptoamount=data[0].upto_amt!!.toDouble()

                                    if (data[0].percentage.equals("0")){
                                        productotalrelative.visibility=View.VISIBLE

                                        discountid= data[0].id!!
                                        // discountpickup.text= data[0].sar.toString()+" SAR"
                                        var saramt:Double=data[0].sar!!.toDouble()
                                        val ptsar:Double=ordersubtotaltt - saramt
                                        productotal.text= String.format(Locale("en", "US"),"%.2f",ordersubtotaltt)+" SAR"
                                        Log.d("nknknk",""+data[0].sar+ordersubtotaltt)
                                        Log.d("nknknk",""+ptsar)
                                        ordersubtotal.text=String.format(Locale("en", "US"),"%.2f",ptsar)+ " SAR"
                                        val vatt = (ptsar / 100.0f) * sharedHelper!!.vat
                                        vat=String.format(Locale("en", "US"),"%.2f",vatt).toDouble()
                                        vat_txt.text = String.format(Locale("en", "US"),"%.2f",vatt)+ " SAR"
                                        discountamount=data[0].sar!!.toDouble()
                                        discountpickup.text= " - " +String.format(Locale("en", "US"),"%.2f",data[0].sar!!.toDouble())+" SAR"
                                        ordersubtotaltt -= data[0].sar!!.toDouble()
                                        var totall=vat+ptsar
                                        finaltotal=totall
                                        Log.d("qwertyuiop",""+totall)
                                        totalmount.text= String.format(Locale("en", "US"),"%.2f",totall) +" SAR"
                                    }else{
                                        discountid= data[0].id!!
                                        productotalrelative.visibility=View.VISIBLE
                                        val dis=data[0].percentage!!.toInt()
                                        Log.d("wertyhujkl",""+dis)
                                        diss=ordersubtotaltt * dis/100.0f
                                        Log.d("zxdcfvghjkl",""+diss)
                                        productotal.text= String.format(Locale("en", "US"),"%.2f",ordersubtotaltt)+" SAR"

                                        if (diss<uptoamount){
                                            Log.d("wertyhujkl",""+diss)
                                            tmt=ordersubtotaltt-diss

                                            ordersubtotal.text=String.format(Locale("en", "US"),"%.2f",tmt)+ " SAR"
                                            val vatt = (tmt / 100.0f) * sharedHelper!!.vat
                                            vat=String.format(Locale("en", "US"),"%.2f",vatt).toDouble()
                                            //  vat=vatt
                                            vat_txt.text = String.format(Locale("en", "US"),"%.2f",vatt.toDouble())+ " SAR"

                                            // discountpickup.text= diss.toString()+" SAR"
                                            val productotall=diss+subtotal
                                            Log.d("kkkk",""+productotall)
                                            discountamount=diss
                                            discountpickup.text= " - " +String.format(Locale("en", "US"),"%.2f",diss.toDouble())+" SAR"
                                            //   vat = (ordersubtotaltt / 100.0f) * sharedHelper!!.vat
                                            //   ordersubtotaltt -= diss
                                            //  ordersubtotaltt+=vat
                                            val tttt=vat+tmt
                                            finaltotal=tttt
                                            Log.d("qwertyuiop",""+finaltotal)

                                            totalmount.text= String.format(Locale("en", "US"),"%.2f",tttt.toDouble()) +" SAR"

                                        }else{

                                            Log.d("wertyhujkl",""+diss)
                                            val ordersubtota:Double=ordersubtotaltt-uptoamount
                                            ordersubtotal.text=String.format(Locale("en", "US"),"%.2f",ordersubtota)+ " SAR"
                                            val vatt = (ordersubtota / 100.0f) * sharedHelper!!.vat
                                            vat=String.format(Locale("en", "US"),"%.2f",vatt).toDouble()
                                            vat_txt.text = String.format(Locale("en", "US"),"%.2f",vatt.toDouble())+ " SAR"

                                            // discountpickup.text= diss.toString()+" SAR"
                                            val productotall=diss+subtotal
                                            Log.d("kkkk",""+productotall)
                                            discountamount=uptoamount

                                            discountpickup.text= " - " +String.format(Locale("en", "US"),"%.2f",uptoamount)+" SAR"
                                            //   vat = (ordersubtotaltt / 100.0f) * sharedHelper!!.vat
                                            //   ordersubtotaltt -= diss
                                            //  ordersubtotaltt+=vat
                                            val tttt=vat+ordersubtota
                                            finaltotal=tttt
                                            Log.d("qwertyuiop",""+finaltotal)

                                            totalmount.text= String.format(Locale("en", "US"),"%.2f",tttt.toDouble()) +" SAR"


                                        }



                                        /* val ptt=diss+subtotal

                                          Log.d("qwertyui",""+totalamt)
                                         Log.d("kkkk",""+ptt)

                                         discountpickup.text= String.format(Locale("en", "US"),"%.2f",uptoamount)+" SAR"
                                         ordersubtotaltt=ordersubtotaltt-data[0].upto_amt!!.toDouble()
                                         productotal.text= String.format(Locale("en", "US"),"%.2f",ptt)+" SAR"
                                         ordersubtotal.text=String.format(Locale("en", "US"),"%.2f",subtotal)+ " SAR"

                                         Log.d("ggggg",""+ordersubtotaltt)
                                          totalmount.text= String.format(Locale("en", "US"),"%.2f",totalamt) +" SAR"
 */
                                    }



                                }

                            }

                        }
                    }

                }
            })

    }

}