package com.lia.yello.linedine.Models

import com.google.gson.annotations.SerializedName
import java.io.Serializable

class ListAdressResponse : Serializable {


    @SerializedName("error")
    var error: Boolean? = null

    @SerializedName("message")
    var message: String? = null

    @SerializedName("data")
    var data: ListAdd? = null
}


class ListAdd : Serializable {

    @SerializedName("address_data")
    var listaddrs: ArrayList<listaddress>? = null

}
class listaddress : Serializable {

    @SerializedName("id")
    var id: Int? = null

    @SerializedName("address")
    var address: String? = null

    @SerializedName("lat")
    var lat: Double? = null

    @SerializedName("lng")
    var lng: Double? = null

    @SerializedName("type")
    var type: String? = null

    @SerializedName("tag")
    var tag: String? = null


}