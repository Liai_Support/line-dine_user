package com.lia.yello.linedine.adapter

import android.content.Context
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.FragmentTransaction
import androidx.recyclerview.widget.RecyclerView
import com.lia.yello.linedine.Models.Menudetail
import com.lia.yello.linedine.Models.Placeorderdata
import com.lia.yello.linedine.R
import com.lia.yello.linedine.Utils.UiUtils
import com.lia.yello.linedine.databinding.CardOrdersBinding
import com.lia.yello.linedine.fragment.MyCompleteOrderFragment
import com.lia.yello.linedine.fragment.MyOrderFragment
import com.lia.yello.linedine.fragment.MyActiveOrderFragment
import java.util.*
import kotlin.collections.ArrayList


class OrderAdapter(
    var myOrderFragment: MyOrderFragment,
    var context: Context,
    var list: ArrayList<Placeorderdata>?

) :
    RecyclerView.Adapter<OrderAdapter.HomeHeaderViewHolder>() {

    var id:Int=0
    var price:Double=0.0
    var img:String=""
    var menu_name:String=""
    var splins:String=""
    var quantity:Int=0
    var list2: ArrayList<Menudetail>? = null

    private lateinit var fragmentTransaction: FragmentTransaction
    private var myCompleteOrderFragment: MyCompleteOrderFragment? = null
    private var myActiveOrderFragment: MyActiveOrderFragment? = null
    inner class HomeHeaderViewHolder(view: View) : RecyclerView.ViewHolder(view) {
        var binding: CardOrdersBinding = CardOrdersBinding.bind(view)
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): HomeHeaderViewHolder {
        return HomeHeaderViewHolder(
            LayoutInflater.from(context).inflate(
                R.layout.card_orders,
                parent,
                false
            )
        )
    }

    override fun getItemCount(): Int {
        return list!!.size
    }

    override fun onBindViewHolder(holder: HomeHeaderViewHolder, position: Int) {

        //  holder.binding.amount.text = list!![position].amount.toString()
        myOrderFragment.totalfinal=0.0
        for (i in 0 until list!![position].menudetails!!.size){
            //   myOrderFragment.total1=list!![position].menudetails!![i].amount!!.toDouble()  * list!![position].menudetails!![i]?.quantity!!
            myOrderFragment.totalfinal+=list!![position].menudetails!![i].amount!!.toDouble()
            Log.d("totalfinal",""+(myOrderFragment.totalfinal ))

            // Log.d("totalamount",""+total)
        }
        Log.d("totalfinal",""+(myOrderFragment.totalfinal + list!![position].vat!!))
        Log.d("totalfinal",""+(myOrderFragment.totalfinal ))



        holder.binding.time.text = list!![position].time
        holder.binding.date.text = list!![position].date
        holder.binding.resName.text = list!![position].resname
        holder.binding.orderid.text = "ID "+ list!![position].id.toString()
/*  val total=list!![position].amount!!.toDouble()  * list!![position].menudetails!![position]?.quantity!!
Log.d("totalamount",""+total)*/

        holder.binding.bookingamount.text =  String.format(Locale("en", "US"),"%.2f", list!![position].amount)+ " SAR"

// holder.binding.bookingamount.text = list!![position].amount.toString() + " SAR"
        Log.d("asdfghjkl.;/",""+list!![position].amount.toString())
        Log.d("bcbbccbcb",""+(myOrderFragment.totalfinal + list!![position].vat!! ))
        list2 = list!![position].menudetails

        Log.d("array",""+list2)
        Log.d("statuss",""+list!![position].status!!)
        Log.d("payment_type",""+list!![position].payment_type)



        list!![position].resbanner?.let { imageUrl ->
            UiUtils.loadImage(holder.binding.img, imageUrl)
        }

        holder.binding.card.setOnClickListener(View.OnClickListener {

            if (list!![position].status!!.equals("delivered")){

                val bundle = Bundle()
                bundle.putInt("orderid",list!![position].id!! )
                bundle.putString("status",list!![position].status!! )

                myCompleteOrderFragment = MyCompleteOrderFragment()
                myCompleteOrderFragment!!.arguments = bundle
                fragmentTransaction = myOrderFragment.requireActivity().supportFragmentManager.beginTransaction()
                fragmentTransaction.setCustomAnimations(R.anim.screen_in, R.anim.screen_out)
                fragmentTransaction.replace(R.id.container, myCompleteOrderFragment!!)
                fragmentTransaction.addToBackStack(null)
                fragmentTransaction.commit()

            }
            else  if (list!![position].status!!.equals("cancelled")){

                val bundle = Bundle()
                bundle.putInt("orderid",list!![position].id!! )
                bundle.putString("status",list!![position].status!! )
                bundle.putString("paymentype",list!![position].payment_type!! )

//  bundle.putString("status","processing" )
//   bundle.putString("status","readytopickup" )
                myActiveOrderFragment = MyActiveOrderFragment()
                myActiveOrderFragment!!.arguments = bundle
                fragmentTransaction = myOrderFragment.requireActivity().supportFragmentManager.beginTransaction()
                fragmentTransaction.setCustomAnimations(R.anim.screen_in, R.anim.screen_out)
                fragmentTransaction.replace(R.id.container, myActiveOrderFragment!!)
                fragmentTransaction.addToBackStack(null)
                fragmentTransaction.commit()

            }else if(list!![position].status!!.equals("ready_to_pickup")){

                val bundle = Bundle()
                bundle.putInt("orderid",list!![position].id!! )
                bundle.putString("status",list!![position].status!! )
                bundle.putString("paymentype",list!![position].payment_type!! )

                //  bundle.putString("status","processing" )
                //   bundle.putString("status","readytopickup" )
                myActiveOrderFragment = MyActiveOrderFragment()
                myActiveOrderFragment!!.arguments = bundle
                fragmentTransaction = myOrderFragment.requireActivity().supportFragmentManager.beginTransaction()
                fragmentTransaction.setCustomAnimations(R.anim.screen_in, R.anim.screen_out)
                fragmentTransaction.replace(R.id.container, myActiveOrderFragment!!)
                fragmentTransaction.addToBackStack(null)
                fragmentTransaction.commit()

            }



            else{
                Log.d("payment_type",""+list!![position].payment_type)

                val bundle = Bundle()
                bundle.putInt("orderid",list!![position].id!! )
                bundle.putString("status",list!![position].status!! )
                bundle.putString("paymentype",list!![position].payment_type!! )
//  bundle.putString("status","processing" )
//   bundle.putString("status","readytopickup" )
                myActiveOrderFragment = MyActiveOrderFragment()
                myActiveOrderFragment!!.arguments = bundle
                fragmentTransaction = myOrderFragment.requireActivity().supportFragmentManager.beginTransaction()
                fragmentTransaction.setCustomAnimations(R.anim.screen_in, R.anim.screen_out)
                fragmentTransaction.replace(R.id.container, myActiveOrderFragment!!)
                fragmentTransaction.addToBackStack(null)
                fragmentTransaction.commit()

            }


        })


    }
}