package com.lia.yello.linedine.Models

data class MenuDetailJson (
    val menuid: Int,
    val quantity: Int
)