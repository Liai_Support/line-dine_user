package com.lia.yello.linedine.activity

import android.app.AlertDialog
import android.content.Context
import android.content.Intent
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.text.Editable
import android.text.TextWatcher
import android.util.DisplayMetrics
import android.util.Log
import android.view.*
import android.widget.EditText
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import com.lia.yello.linedine.GenericTextWatcher
import com.lia.yello.linedine.Session.SharedHelper
import com.lia.yello.linedine.R
import com.lia.yello.linedine.Utils.BaseUtils
import com.lia.yello.linedine.Utils.DialogUtils
import com.lia.yello.linedine.Utils.UiUtils
import com.lia.yello.linedine.ViewModels.RegisterViewModel
import kotlinx.android.synthetic.main.activity_login.*
import kotlinx.android.synthetic.main.activity_signup.*
import kotlinx.android.synthetic.main.activity_signup.root
import kotlinx.android.synthetic.main.popup_mobile.view.*
import kotlinx.android.synthetic.main.popup_otp.*
import kotlinx.android.synthetic.main.popup_otp.view.*

class SignupActivity : BaseActivity() {
    private var viewmodel: RegisterViewModel? = null
    var sharedHelper: SharedHelper? = null


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_signup)
        viewmodel = ViewModelProvider(this).get(RegisterViewModel::class.java)
        sharedHelper = SharedHelper(this)

    }

    private fun isValidInputs(): Boolean {
        when {
            edt_name.text.toString().trim() == "" -> {
                UiUtils.showSnack(root, getString(R.string.pleaseenteryourname))
                return false
            }
            edt_email.text.toString().trim() == "" -> {
                UiUtils.showSnack(root, getString(R.string.pleaseenteryouremailid))
                return false
            }!BaseUtils.isValidEmail(edt_email.text.toString()) -> {
            UiUtils.showSnack(root, getString(R.string.pleaseentervalidemailid))
            return false
        }
            edt_mobile.text.toString().trim() == "" -> {
                UiUtils.showSnack(root, getString(R.string.pleaseenteryourmobilenumber))
                return false
            }
            edt_password.text.toString().trim() == "" -> {
                UiUtils.showSnack(root, getString(R.string.pleaseenteryourpassword))
                return false
            }
            edt_conf_password.text.toString().trim() == "" -> {
                UiUtils.showSnack(root, getString(R.string.pleaseenteryourconfirmpassword))
                return false
            }
            edt_conf_password.text.toString().trim() != edt_password.text.toString().trim() -> {
                UiUtils.showSnack(root, getString(R.string.confirmpasswordshouldbesameaspassword))
                return false
            }

            else -> return true
        }

        return true
    }

    fun Createaccount(view: View) {
        if (isValidInputs()) {
            DialogUtils.showLoader(this)
            viewmodel?.createAccount(
                edt_name.text.toString().trim(),
                edt_email.text.toString().trim(),
                "91",
                edt_mobile.text.toString().trim(),
                edt_password.text.toString().trim()
            )?.observe(this, Observer {
                DialogUtils.dismissLoader()
                it?.let {
                    it.error?.let { error ->
                        if (error) {
                            it.message?.let { msg ->
                                UiUtils.showSnack(root, msg)
                            }
                        } else {
                            it.message?.let { msg ->
                                if (msg.equals("mobile no or email id already exist. please login ")){
                                    UiUtils.showSnack(root, msg)

                                    /*  val intent =
                                          Intent(this, LoginActivity::class.java)
                                      startActivity(intent)
                                      finish()*/
                                }else{
                                    it.data?.let { data ->
                                        Log.d("hhggh",""+data);


                                        handleResponse(data)


                                        //showDialog2()
                                    }
                                }
                            }
                        }
                    }
                }
            })
        }
    }

    private fun handleResponse(data: RegisterData) {

        data.email?.let { sharedHelper?.email = it }
        data.id?.let { sharedHelper?.id = it }
        data.name?.let { sharedHelper?.name = it }
        data.mobileNumber?.let { sharedHelper?.mobileNumber = it }
        data.token?.let { sharedHelper?.token = it }

        if (data.isotpverified.equals("false")){
            showDialog2()
        }else{
            val intent =
                Intent(this, DashBoardActivity::class.java)
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP)
            intent.flags =
                Intent.FLAG_ACTIVITY_NEW_TASK or Intent.FLAG_ACTIVITY_CLEAR_TASK
            startActivity(intent)
            finish()
        }

        /*   // onSignInClicked(dontHaveAcc1)
          */

    }

    override fun onBackPressed() {
        super.onBackPressed()
    }

    private fun showDialog2() {
        val mDialogView =
            LayoutInflater.from(this as Context).inflate(R.layout.popup_otp, null as ViewGroup?)
        val mBuilder =
            AlertDialog.Builder(this as Context).setView(mDialogView).setCancelable(false)
        val mAlertDialog = mBuilder.show()
        val layoutParams = WindowManager.LayoutParams()
        val displayMetrics = DisplayMetrics()
        val windowManager = this.windowManager
        windowManager.defaultDisplay.getMetrics(displayMetrics)
        val displayWidth = displayMetrics.widthPixels
        val displayHeight = displayMetrics.heightPixels
        val window = mAlertDialog.window
        layoutParams.copyFrom(window?.attributes)
        layoutParams.width = (displayWidth.toFloat() * 0.9f).toInt()
        window!!.attributes = layoutParams
        window.setGravity(Gravity.CENTER)
        window.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))




        if (sharedHelper!!.language=="ar"){
            mDialogView.linearotp.rotation= 180F
            mDialogView.otp1.rotation= 180F
            mDialogView.otp2.rotation= 180F
            mDialogView.otp3.rotation= 180F
            mDialogView.otp4.rotation= 180F
        }
        val otp1 = mDialogView.findViewById<EditText>(R.id.otp1)
        val otp2 = mDialogView.findViewById<EditText>(R.id.otp2)
        val otp3 = mDialogView.findViewById<EditText>(R.id.otp3)
        val otp4 = mDialogView.findViewById<EditText>(R.id.otp4)
        otp1.requestFocus()
        otp1.addTextChangedListener(object : TextWatcher {
            override fun beforeTextChanged(charSequence: CharSequence, i: Int, i1: Int, i2: Int) {}
            override fun onTextChanged(charSequence: CharSequence, i: Int, i1: Int, i2: Int) {}
            override fun afterTextChanged(editable: Editable) {
                if (editable.toString() != "") {
                    otp2.isFocusable = true
                    otp2.requestFocus()
                }
            }
        })
        otp2.addTextChangedListener(object : TextWatcher {
            override fun beforeTextChanged(charSequence: CharSequence, i: Int, i1: Int, i2: Int) {}
            override fun onTextChanged(charSequence: CharSequence, i: Int, i1: Int, i2: Int) {}
            override fun afterTextChanged(editable: Editable) {
                if (editable.toString() != "") {
                    otp3.isFocusable = true
                    otp3.requestFocus()
                } else {
                    otp1.isFocusable = true
                    otp1.requestFocus()
                }
            }
        })
        otp3.addTextChangedListener(object : TextWatcher {
            override fun beforeTextChanged(charSequence: CharSequence, i: Int, i1: Int, i2: Int) {}
            override fun onTextChanged(charSequence: CharSequence, i: Int, i1: Int, i2: Int) {}
            override fun afterTextChanged(editable: Editable) {
                if (editable.toString() != "") {
                    otp4.isFocusable = true
                    otp4.requestFocus()
                } else {
                    otp2.isFocusable = true
                    otp2.requestFocus()
                }
            }
        })
        otp4.addTextChangedListener(object : TextWatcher {
            override fun beforeTextChanged(charSequence: CharSequence, i: Int, i1: Int, i2: Int) {}
            override fun onTextChanged(charSequence: CharSequence, i: Int, i1: Int, i2: Int) {}
            override fun afterTextChanged(editable: Editable) {
                if (editable.toString() != "") {
                    otp4.isFocusable = true
                    otp4.requestFocus()
                } else {
                    otp3.isFocusable = true
                    otp3.requestFocus()
                }
            }
        })
        otp2.setOnKeyListener { view, i, keyEvent ->
            if (i == KeyEvent.KEYCODE_DEL) {
                if (otp2.text.toString().isEmpty()) {
                    otp1.setText("")
                    otp1.requestFocus()
                }
            }
            false
        }
        otp3.setOnKeyListener { view, i, keyEvent ->
            if (i == KeyEvent.KEYCODE_DEL) {
                if (otp3.text.toString().isEmpty()) {
                    otp2.setText("")
                    otp2.requestFocus()
                }
            }
            false
        }
        otp4.setOnKeyListener { view, i, keyEvent ->
            if (i == KeyEvent.KEYCODE_DEL) {
                if (otp4.text.toString().isEmpty()) {
                    otp3.setText("")
                    otp3.requestFocus()
                }
            }
            false
        }


        mDialogView.confirmotp.setOnClickListener {
            if ( otp1.text.toString() != "" && otp2.text.toString() != "" && otp3.text.toString() != "" && otp4.text.toString() != ""){
                mAlertDialog.dismiss()
                var otp: String = mAlertDialog.otp1.text.toString()+mAlertDialog.otp2.text.toString()+mAlertDialog.otp3.text.toString()+mAlertDialog.otp4.text.toString()
                Log.d("dsvcdsv", "" + otp)

                getverifyotp(otp.toDouble())
            }else{
                UiUtils.showSnack(root, getString(R.string.entervalidotp))
            }


            //  verifyAuthentication(verificationID, otp)
            // val mail = mDialogView.mobile.text.toString()

        }



    }



    private fun getverifyotp(
        OTP: Double,
    ){


        DialogUtils.showLoader(this)

        viewmodel?.getverifyOtp(OTP)
            ?.observe(this,
                Observer {
                    DialogUtils.dismissLoader()
                    it?.let {
                        it.error?.let { error ->
                            if (error) {

                                it.message?.let { msg ->
                                    UiUtils.showSnack(root, msg)
                                }

                            } else {
                                it.message?.let { msg ->
                                    UiUtils.showSnack(root, msg)
                                    sharedHelper?.loggedIn = true

                                    val intent =
                                        Intent(this, DashBoardActivity::class.java)
                                    intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP)
                                    intent.flags =
                                        Intent.FLAG_ACTIVITY_NEW_TASK or Intent.FLAG_ACTIVITY_CLEAR_TASK
                                    startActivity(intent)
                                    finish()
                                }

                            }
                        }

                    }
                })
    }


}