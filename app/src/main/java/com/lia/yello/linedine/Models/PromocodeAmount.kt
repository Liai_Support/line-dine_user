package com.lia.yello.linedine.Models

 data class PromocodeAmount
     (val promocode: String,
      val amount: Double)