package com.lia.yello.linedine.fragment

import android.os.Bundle
import android.util.Log
import android.view.View
import android.widget.Button
import android.widget.RatingBar
import androidx.core.content.ContextCompat
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentTransaction
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import com.google.android.material.snackbar.BaseTransientBottomBar
import com.google.android.material.snackbar.Snackbar
import com.lia.yello.linedine.R
import com.lia.yello.linedine.Session.SharedHelper
import com.lia.yello.linedine.Utils.DialogUtils
import com.lia.yello.linedine.Utils.NetworkUtils
import com.lia.yello.linedine.Utils.UiUtils
import com.lia.yello.linedine.ViewModels.MapViewModel
import kotlinx.android.synthetic.main.carticon.*
import kotlinx.android.synthetic.main.header.*

class ReviewFragment : Fragment(R.layout.fragment_review) {
    var binding: ReviewFragment? = null
    var mapViewModel: MapViewModel? = null
    var orderid:Int=0
    var menuid:Int=0
    lateinit var btn_submit_review:Button
    var ratingresturant: Float = 0.0f
    var ratingmenu: Float = 0.0f
    public var sharedHelper: SharedHelper? = null

    private lateinit var fragmentTransaction: FragmentTransaction
    private var homeFragment: HomeFragment? = null
    lateinit var rating1:RatingBar
    lateinit var rating2:RatingBar
    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        btn_submit_review=view.findViewById(R.id.btn_submit_review)
        sharedHelper = SharedHelper(requireContext())

        mapViewModel = ViewModelProvider(this).get(MapViewModel::class.java)
        orderid = arguments?.getInt("orderid")!!
        menuid = arguments?.getInt("menuid")!!
        rating1 = view.findViewById<RatingBar>(R.id.ratingres)
        rating2 = view.findViewById<RatingBar>(R.id.ratingmenu)

        if (sharedHelper!!.choose.equals("finedine")||sharedHelper!!.choose.equals("inqueue")|| sharedHelper!!.cartcount == 0){

            //var cartframe: FrameLayout =findViewById(R.id.cartframe)
            cartframe.visibility=View.GONE
        }
        if (!NetworkUtils.isNetworkConnected(requireContext())) {
            UiUtils.showSnack(
                view, "no_internet_connection"
            )
        }

        if (sharedHelper!!.language == "ar"){
            back.rotation= 180F
        }

        head.setText(getString(R.string.review))

        back.setOnClickListener(View.OnClickListener {
            requireActivity().supportFragmentManager.popBackStack()
        })
        ratingresturant= rating1.getRating()
        val msg = rating2.rating.toString()

        Log.d("asdfghjk", "" + ratingresturant)
        Log.d("qwertyu", "" + ratingresturant)
        Log.d("qwertyu", "" + msg)

        btn_submit_review.setOnClickListener(View.OnClickListener {
            Log.d("dsgvsdh",""+rating1.rating)
            Log.d("dsgvsdh",""+rating1.rating.toString())

            if (rating1.rating.toString().equals("0.0") || rating2.rating.toString().equals("0.0")){

                snackbarToast(getString(R.string.enterallfields))
                Log.d("asdcfvgbnm,","xcvfgbnm,.")
            }else{
                ratingresturant = rating1.getRating()
                ratingmenu = rating2.getRating()
                val msg = rating2.rating.toString()
                Log.d("asdfghjk", "" + ratingresturant)
                Log.d("qwertyu", "" + ratingresturant)
                Log.d("qwertyu", "" + msg)
                getreview(rating1.rating.toString(), rating2.rating.toString())
            }

        })


    }


    private fun getreview(ratingresturant: String, ratingmenu: String) {
        DialogUtils.showLoader(requireContext())
        mapViewModel?.getreview(requireContext(), orderid, ratingmenu.toFloat(), ratingresturant.toFloat(),menuid)
            ?.observe(viewLifecycleOwner,
                Observer {
                    DialogUtils.dismissLoader()

                    it?.let {
                        it.error?.let { error ->
                            if (error) {
                                it.message?.let { msg ->

                                    snackbarToast(msg)
                                }
                            } else {

                                it.message?.let { msg ->
                                    btn_submit_review.visibility=View.GONE
                                    snackbarToast(msg)
                                    homeFragment = HomeFragment()
                                    fragmentTransaction =requireActivity().supportFragmentManager.beginTransaction()
                                    fragmentTransaction.setCustomAnimations(R.anim.screen_in, R.anim.screen_out)
                                    fragmentTransaction.replace(R.id.container, homeFragment!!)
                                    fragmentTransaction.commit()
                                }
                            }
                        }

                    }
                })
    }



    private fun snackbarToast(msg: String) {
        var snackbar: Snackbar
        Snackbar.make(view!!, msg, Snackbar.LENGTH_SHORT)
            .setTextColor(ContextCompat.getColor(context!!, R.color.design_default_color_error))
            .setBackgroundTint(ContextCompat.getColor(context!!, R.color.white))
            .setDuration(BaseTransientBottomBar.LENGTH_LONG)
            .setAnimationMode(BaseTransientBottomBar.ANIMATION_MODE_SLIDE)
            .show()
    }



}