package com.lia.yello.linedine.ViewModels

import android.app.Application
import android.content.Context
import android.util.Log
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.LiveData
import com.lia.yello.linedine.Models.*

import com.lia.yello.linedine.Repository.MapRepository
import com.lia.yello.linedine.Repository.ProfileRepository
import com.lia.yello.linedine.Session.SharedHelper
import com.lia.yello.linedine.Utils.Constants
import com.lia.yello.linedine.network.ApiInput
import com.lia.yello.linedine.network.UrlHelper
import org.json.JSONObject

class ProfileViewModel(application: Application) : AndroidViewModel(application) {

    var repository: ProfileRepository = ProfileRepository.getInstance()
    var applicationIns: Application = application
    var sharedHelper: SharedHelper? = SharedHelper(applicationIns.applicationContext)


    private fun getApiParams(context: Context,jsonObject: JSONObject?, methodName: String): ApiInput {

        val header: MutableMap<String, String> = HashMap()
        sharedHelper?.let { header[Constants.ApiKeys.LANG] = "en" }
        sharedHelper?.let { header[Constants.ApiKeys.AUTHORIZATION] = it.token }
        header[Constants.ApiKeys.ROLE] = Constants.ApiKeys.PATIENT


        val apiInputs = ApiInput()
        apiInputs.context = context
        apiInputs.jsonObject = jsonObject
        apiInputs.headers = header
        apiInputs.url = methodName

        return apiInputs
    }


    fun updateDeviceToken(context: Context): LiveData<CommonResponseUpdateToken>? {

        var jsonObject = JSONObject()

        jsonObject.put(Constants.ApiKeys.TYPE, Constants.ApiKeys.ANDROID)
        jsonObject.put(Constants.ApiKeys.TOKEN, sharedHelper?.token)
        jsonObject.put("userid", sharedHelper?.id)


        return  ProfileRepository.repository!!.getupdatedevicetoken(getApiParams(context, jsonObject, UrlHelper.UPDATETOKEN))

        Log.d("asdfghjm",""+jsonObject)

    }


    fun getProfileDetails(context : Context): LiveData<ProfileDetailsResponse>? {
        val jsonObject = JSONObject()
        sharedHelper?.let { jsonObject.put(Constants.ApiKeys.ID, it.id) }
        return repository.getProfileDetails(getApiParams(context,jsonObject, UrlHelper.GET_PROFILE))
    }

    fun updateProfile(context : Context,
        name: String,
        email: String,
        cCode: String,
        mobile: String,
        gender: String,
        dob: String
       // profilePic: String?
    ): LiveData<CommonResponse>? {

        val jsonObject = JSONObject()
        sharedHelper?.let { jsonObject.put(Constants.ApiKeys.ID, it.id) }
        jsonObject.put(Constants.ApiKeys.NAME, name)
        jsonObject.put(Constants.ApiKeys.EMAIL, email)
       // jsonObject.put(Constants.ApiKeys.COUNTRYCODE, cCode)
        jsonObject.put(Constants.ApiKeys.PHONE, mobile)
        jsonObject.put(Constants.ApiKeys.GENDER, gender)
        jsonObject.put(Constants.ApiKeys.DOB, dob)

      //  profilePic?.let { jsonObject.put(Constants.ApiKeys.PROFILEPIC, it) }


        return repository.updateProfile(getApiParams(context,jsonObject, UrlHelper.UPDATE_PROFILE))
    }

    fun getDiscountEvent(context: Context): LiveData<DiscountEventResponse>?  {
        Log.d("returnstaement","1")

        return repository.getDiscountEvent(
            getApiParams(
                context,
                null,
                UrlHelper.DISCOUNTEVENT
            )
        )

    }



    fun getDiscountEventDates(
        context: Context,
        jsonArray: String

    ): LiveData<DiscountEventDates>? {

        val jsonObject = JSONObject()
        sharedHelper?.let { jsonObject.put("userid", it.id) }
        jsonObject.put("event", jsonArray)


        return repository.getDiscountEventDates(
            getApiParams(
                context,
                jsonObject,
                UrlHelper.DISCOUNTEVENTDATES
            )
        )
    }

}