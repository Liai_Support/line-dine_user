package com.lia.yello.linedine.fragment


import android.os.Bundle
import android.view.View
import android.widget.TextView
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentTransaction
import com.lia.yello.linedine.R
import com.lia.yello.linedine.ViewModels.MapViewModel
import androidx.lifecycle.ViewModelProvider
import com.lia.yello.linedine.Session.SharedHelper
import com.lia.yello.linedine.Utils.DialogUtils
import com.lia.yello.linedine.Utils.NetworkUtils
import com.lia.yello.linedine.Utils.UiUtils
import com.lia.yello.linedine.databinding.*
import com.lia.yello.linedine.interfaces.SingleTapListener
import kotlinx.android.synthetic.main.carticon.*
import kotlinx.android.synthetic.main.fragment_paymentoption.*
import kotlinx.android.synthetic.main.header.*

class PaymentOptionFragment : Fragment(R.layout.fragment_paymentoption) {
    private lateinit var fragmentTransaction: FragmentTransaction
    private var walletFragment: WalletFragment? = null
    var binding: FragmentPaymentoptionBinding? = null
    private var mapViewModel: MapViewModel? = null
    public var sharedHelper: SharedHelper? = null

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        binding = DataBindingUtil.findBinding(view)
        mapViewModel = ViewModelProvider(this).get(MapViewModel::class.java)
        sharedHelper = SharedHelper(requireContext())

        val head = view.findViewById<TextView>(R.id.head)
        head.setText(getString(R.string.paymentoption))
        if (!NetworkUtils.isNetworkConnected(requireContext())) {
            UiUtils.showSnack(
                view, "no_internet_connection"
            )
        }


        if (sharedHelper!!.language == "ar"){
            back.rotation= 180F
        }

        if (sharedHelper!!.choose.equals("finedine")||sharedHelper!!.choose.equals("inqueue")|| sharedHelper!!.cartcount == 0){

            //var cartframe: FrameLayout =findViewById(R.id.cartframe)
            cartframe.visibility=View.GONE
        }

        back.setOnClickListener(View.OnClickListener {
            requireActivity().supportFragmentManager.popBackStack()
        })
        paymentoption_wallet.setOnClickListener {
            walletFragment = WalletFragment()
            fragmentTransaction = requireActivity().supportFragmentManager.beginTransaction()
            fragmentTransaction.setCustomAnimations(R.anim.screen_in, R.anim.screen_out)
            fragmentTransaction.replace(R.id.container, walletFragment!!)
            fragmentTransaction.addToBackStack(null)
            fragmentTransaction.commit()
        }

        deb.setOnClickListener {
            showComingSoon()
        }

    }


    fun showComingSoon() {

        DialogUtils.showAlertWithHeader(requireContext(), object : SingleTapListener {
            override fun singleTap() {

            }
        }, getString(R.string.comingsoon), getString(R.string.sorry))
    }


}