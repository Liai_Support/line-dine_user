package com.lia.yello.linedine.adapter

import android.content.Context
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.FragmentTransaction
import androidx.recyclerview.widget.RecyclerView
import com.amazonaws.auth.policy.Resource
import com.google.android.material.bottomsheet.BottomSheetBehavior
import com.lia.yello.linedine.R
import com.lia.yello.linedine.Utils.UiUtils
import com.lia.yello.linedine.activity.DashBoardActivity
import com.lia.yello.linedine.activity.ResType
import com.lia.yello.linedine.databinding.CardRestaurantsBinding
import com.lia.yello.linedine.fragment.HomeFragment
import com.lia.yello.linedine.fragment.SubHomeFragment


class HomeAdapter(
    var homeFragment: HomeFragment,
    var context: Context,
    var list: ArrayList<ResType>?
) :
    RecyclerView.Adapter<HomeAdapter.HomeHeaderViewHolder>() {
    private var subHomeFragment: SubHomeFragment? = null
    private lateinit var fragmentTransaction: FragmentTransaction

    var row_index:Int=-1

    inner class HomeHeaderViewHolder(view: View) : RecyclerView.ViewHolder(view) {
        var binding: CardRestaurantsBinding = CardRestaurantsBinding.bind(view)


    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): HomeHeaderViewHolder {
        return HomeHeaderViewHolder(
            LayoutInflater.from(context).inflate(
                R.layout.card_restaurants,
                parent,
                false
            )
        )
    }

    override fun getItemCount(): Int {
        return list!!.size
    }

    @ExperimentalStdlibApi
    override fun onBindViewHolder(holder: HomeHeaderViewHolder, position: Int) {


           if(homeFragment.sharedHelper!!.language.equals("ar")){
               holder.binding.seemore2.visibility=View.VISIBLE
               holder.binding.seemore1.visibility=View.GONE
           }



        holder.binding.title.text = list!![position].type
        list!![position].image?.let { imageUrl ->
            UiUtils.loadImage(holder.binding.imgdashboard, imageUrl)
        }
           holder.binding.filter.setOnClickListener(View.OnClickListener {
               homeFragment.imgid2 = list!![position].id
               homeFragment.type = list!![position].type
              (homeFragment.activity as DashBoardActivity?)!!.mBottomSheetBehavior?.setState(
                BottomSheetBehavior.STATE_COLLAPSED
            )
               (context as DashBoardActivity?)!!.mBottomSheetBehavior2?.setState(
                   BottomSheetBehavior.STATE_HIDDEN
               )
        })


        holder.binding.imgdashboard.setOnClickListener(View.OnClickListener {

            val bundle = Bundle()

          //  homeFragment.imgid2?.let { it1 -> bundle.putInt("imgid", it1) }
            homeFragment.iscuisine = false
            bundle.putString("iscuisine", homeFragment.iscuisine.toString())
            list!![position].id?.let { it1 -> bundle.putInt("imgid", it1) }
            list!![position].type?.let { it2 -> bundle.putString("type", it2) }
            subHomeFragment = SubHomeFragment()
            subHomeFragment!!.arguments = bundle
            fragmentTransaction =homeFragment.requireActivity().supportFragmentManager.beginTransaction()
            fragmentTransaction.setCustomAnimations(R.anim.screen_in, R.anim.screen_out)
            fragmentTransaction.replace(R.id.container, subHomeFragment!!)
            fragmentTransaction.addToBackStack(null)
            fragmentTransaction.commit()
            (homeFragment.activity as DashBoardActivity?)!!.mBottomSheetBehavior?.setState(
                BottomSheetBehavior.STATE_HIDDEN
            )

        })

    }
}