package com.lia.yello.linedine.adapter

import android.content.Context
import android.location.Geocoder
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.LinearLayout
import android.widget.TextView
import androidx.cardview.widget.CardView
import androidx.fragment.app.FragmentTransaction
import androidx.recyclerview.widget.RecyclerView
import com.lia.yello.linedine.Models.Placeorderdata
import com.lia.yello.linedine.R
import com.lia.yello.linedine.Utils.UiUtils
import com.lia.yello.linedine.databinding.CardOrderprocesingBinding
import com.lia.yello.linedine.databinding.ScrollistBinding
import com.lia.yello.linedine.fragment.MyActiveOrderFragment
import com.lia.yello.linedine.fragment.MyActiveOrderFragment1
import java.util.*

class OrderProcessingAdapter(
    var myActiveOrderFragment: MyActiveOrderFragment,
    var context: Context,
    var list: Placeorderdata
) :
    RecyclerView.Adapter<OrderProcessingAdapter.HomeHeaderViewHolder>() {

    private lateinit var fragmentTransaction: FragmentTransaction
    private var myActiveOrderFragment1: MyActiveOrderFragment1? = null

    inner class HomeHeaderViewHolder(view: View) : RecyclerView.ViewHolder(view) {

        var binding: CardOrderprocesingBinding = CardOrderprocesingBinding.bind(view)

       /* lateinit var text: TextView
        lateinit var status: TextView
        lateinit var locationpickup: TextView

        lateinit var img: ImageView
        lateinit var main: CardView
        lateinit var linearcardprocessing: LinearLayout



        init {
            text = view.findViewById(R.id.name)
            status = view.findViewById(R.id.status)

            img = view.findViewById(R.id.imgorderprocessing)
            main = view.findViewById(R.id.main)
            locationpickup = view.findViewById(R.id.location_pickup)
            linearcardprocessing = view.findViewById(R.id.linearcardprocessing)

        }*/
    }


    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): HomeHeaderViewHolder {
        return HomeHeaderViewHolder(
            LayoutInflater.from(parent.context).inflate(
                R.layout.card_orderprocesing,
                parent,
                false
            )
        )
    }

    override fun getItemCount(): Int {
        return 1
        // return itemCount
    }


    override fun onBindViewHolder(holder: HomeHeaderViewHolder, position: Int) {

        val gcd = Geocoder(context, Locale.getDefault())
        val addresses = gcd.getFromLocation(list.lat!!, list.lng!!, 1)
        if (addresses.size > 0) {
            println(addresses[0].locality)
            Log.d("adedretrgygyuihu", "" + addresses[0].locality)
           var fcity=addresses[0].locality
            if (myActiveOrderFragment.sharedHelper!!.language.equals("ar")){
             //   holder.linearcardprocessing.rotation=180F
                holder.binding.locationPickup.text= "  $fcity :"

            }else{
                holder.binding.locationPickup.text= " : $fcity"

            }
            Log.d("asdfghjk",""+fcity)
        } else {
            // do your stuff
        }

            holder.binding.name.text =list.resname


        if (myActiveOrderFragment.sharedHelper!!.language.equals("ar")){


            if (list.status!!.equals("confirmed")){
                holder.binding.status.text = "مؤكد"

            }else if(list.status!!.equals("process")){
                holder.binding.status.text = "تحت المعالجة"

            }
            else if(list.status!!.equals("ready_to_pickup")){
                holder.binding.status.text = "جاهز للاستلام"

            }else if(list.status!!.equals("cancelled")){
                holder.binding.status.text = "ألغيت"

            }else if(list.status!!.equals("pending")){
                holder.binding.status.text = "Pending"

            }

        }else{
            if (list.status!!.equals("confirmed")){
                holder.binding.status.text = "Confirmed"

            }else if(list.status!!.equals("process")){
                holder.binding.status.text = "Processing"

            }
            else if(list.status!!.equals("ready_to_pickup")){
                holder.binding.status.text = "Ready to Pickup"

            }else if(list.status!!.equals("cancelled")){
                holder.binding.status.text = "Cancelled"

            }else if(list.status!!.equals("pending")){
                holder.binding.status.text = "Pending"

            }
        }



        list!!.resbanner?.let { imageUrl ->
                UiUtils.loadImage(holder.binding.imgorderprocessing, imageUrl)
            }



        holder.binding.main.setOnClickListener(View.OnClickListener {

                val bundle = Bundle()
                bundle.putInt("orderid",list!!.id!! )
                bundle.putString("status",list!!.status!! )
                 Log.d("ertyuio",""+list!!.status)
                myActiveOrderFragment1 = MyActiveOrderFragment1()
                myActiveOrderFragment1!!.arguments = bundle
                fragmentTransaction = myActiveOrderFragment.requireActivity().supportFragmentManager.beginTransaction()
                fragmentTransaction.setCustomAnimations(R.anim.screen_in, R.anim.screen_out)
                fragmentTransaction.replace(R.id.container, myActiveOrderFragment1!!)
                fragmentTransaction.addToBackStack(null)
                fragmentTransaction.commit()


        })



    }

}












