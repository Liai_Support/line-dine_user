package com.lia.yello.linedine.fragment

import android.os.Bundle
import android.os.Handler
import android.view.View
import android.widget.ImageView
import android.widget.LinearLayout
import androidx.core.content.ContextCompat
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.viewpager.widget.PagerAdapter
import androidx.viewpager.widget.ViewPager
import com.facebook.FacebookSdk
import com.lia.yello.linedine.R
import com.lia.yello.linedine.Session.SharedHelper
import com.lia.yello.linedine.Utils.DialogUtils
import com.lia.yello.linedine.Utils.NetworkUtils
import com.lia.yello.linedine.Utils.UiUtils
import com.lia.yello.linedine.ViewModels.MapViewModel
import com.lia.yello.linedine.activity.HomeData
import com.lia.yello.linedine.adapter.HomeBannerFragment2
import com.lia.yello.linedine.databinding.FragmentHomeBinding
import kotlinx.android.synthetic.main.carticon.*
import kotlinx.android.synthetic.main.fragment_home.root1
import kotlinx.android.synthetic.main.fragment_home_banner.*
import kotlinx.android.synthetic.main.header.*
import java.util.*

class HomeBannerFragment : Fragment(R.layout.fragment_home_banner) {
    public var mapViewModel: MapViewModel? = null
    var binding: FragmentHomeBinding? = null;

    public var sharedHelper: SharedHelper? = null
   // var viewPager: ViewPager? = null

    var timer: Timer? = null
    var timerTask: TimerTask? = null
    private var banner: HomeData? = null


    var currentPage = 0
    val DELAY_MS: Long = 5000 //delay in milliseconds before task is to be executed
    val PERIOD_MS: Long = 5000 // time in milliseconds between successive task executions.
    var count = 0
    public var dotscount = 0
    private var dots: Array<ImageView?>? = null

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        binding = DataBindingUtil.findBinding(view)
        sharedHelper = SharedHelper(requireContext())
        mapViewModel = ViewModelProvider(this).get(MapViewModel::class.java)
        if (!NetworkUtils.isNetworkConnected(requireContext())) {
            UiUtils.showSnack(
                view, "no_internet_connection"
            )
        }
        getvalues()

        if (sharedHelper!!.language == "ar"){
            back.rotation= 180F
        }
        if (sharedHelper!!.choose.equals("finedine")||sharedHelper!!.choose.equals("inqueue")|| sharedHelper!!.cartcount == 0){

            //var cartframe: FrameLayout =findViewById(R.id.cartframe)
            cartframe.visibility=View.GONE
        }

    }


    private fun getvalues(){
        DialogUtils.showLoader(requireContext())
        mapViewModel?.gethomeDetails(requireContext())
            ?.observe(viewLifecycleOwner,
                Observer {
                    DialogUtils.dismissLoader()

                    it?.let {
                        it.error?.let { error ->
                            if (error) {
                                it.message?.let { msg ->
                                    UiUtils.showSnack(root1, msg)
                                }
                            } else {
                                it.data?.let { data ->

                                    /*  recycler1.layoutManager = LinearLayoutManager(context, RecyclerView.HORIZONTAL, false)
                                   requireActivity().recycler1.adapter = HomeBannerAdapter(this,
                                        requireContext(),
                                        data.banner
                                    )
*/

                                  /*  val adapter3: PagerAdapter = HomeBannerFragment2(
                                        this,
                                        requireContext(),
                                        data.banner
                                    )
                                    viewpager!!.adapter = adapter3
                                    dotscount = adapter3.count

                                    dots = arrayOfNulls<ImageView>(dotscount)

                                    for (i in 0 until dotscount) {
                                        dots!![i] = ImageView(context)
                                        dots!![i]!!.setImageDrawable(
                                            ContextCompat.getDrawable(
                                                FacebookSdk.getApplicationContext(),
                                                R.drawable.non_active_dot
                                            )
                                        )
                                        val params = LinearLayout.LayoutParams(
                                            LinearLayout.LayoutParams.WRAP_CONTENT,
                                            LinearLayout.LayoutParams.WRAP_CONTENT
                                        )
                                        params.setMargins(8, 0, 8, 0)
                                        SliderDotss.addView(dots!![i], params)
                                    }

                                    dots!![0]?.setImageDrawable(
                                        ContextCompat.getDrawable(
                                            FacebookSdk.getApplicationContext(),
                                            R.drawable.active_dot
                                        )
                                    )

                                    viewpager.addOnPageChangeListener(object :
                                        ViewPager.OnPageChangeListener {
                                        override fun onPageScrolled(
                                            position: Int,
                                            positionOffset: Float,
                                            positionOffsetPixels: Int
                                        ) {
                                        }

                                        override fun onPageSelected(position: Int) {
                                            for (i in 0 until dotscount) {
                                                dots!![i]!!.setImageDrawable(
                                                    ContextCompat.getDrawable(
                                                        FacebookSdk.getApplicationContext(),
                                                        R.drawable.non_active_dot
                                                    )
                                                )
                                            }
                                            dots!![position]?.setImageDrawable(
                                                ContextCompat.getDrawable(
                                                    FacebookSdk.getApplicationContext(),
                                                    R.drawable.active_dot
                                                )
                                            )
                                        }

                                        override fun onPageScrollStateChanged(state: Int) {}
                                    })

                                    *//*After setting the adapter use the timer *//*
                                    val handler = Handler()
                                    val Update = Runnable {
                                        if (currentPage === data.banner!!.size - 1) {
                                            currentPage = 0
                                        }
                                        viewpager.setCurrentItem(currentPage++, true)
                                    }

                                    timer = Timer() // This will create a new Thread

                                    timer!!.schedule(object : TimerTask() {
                                        // task to be scheduled
                                        override fun run() {
                                            handler.post(Update)
                                        }
                                    }, DELAY_MS, PERIOD_MS)
*/

                                    /*    val speedScroll = 1000
                                    val handler = Handler()
                                    val runnable: Runnable = object : Runnable {
                                        override fun run() {
                                            if (count < data.banner!!.size) {
                                                recycler1.scrollToPosition(count++)
                                                handler.postDelayed(this, speedScroll.toLong())
                                            } else {
                                                count = 0
                                                run()
                                            }
                                        }
                                    }

                                    handler.postDelayed(runnable, speedScroll.toLong())*/

                                }
                            }

                        }
                    }
                })

    }



}

