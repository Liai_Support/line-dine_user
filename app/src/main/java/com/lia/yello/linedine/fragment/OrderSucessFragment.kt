package com.lia.yello.linedine.fragment


import android.content.Intent
import android.os.Bundle
import android.os.Handler
import android.os.Looper
import android.util.Log
import android.view.View
import android.widget.TextView
import androidx.activity.OnBackPressedCallback
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentTransaction
import androidx.lifecycle.LifecycleOwner
import com.lia.yello.linedine.R
import com.lia.yello.linedine.Session.SharedHelper
import com.lia.yello.linedine.Utils.NetworkUtils
import com.lia.yello.linedine.Utils.UiUtils
import com.lia.yello.linedine.activity.ChooseLanguageActivity
import com.lia.yello.linedine.activity.DashBoardActivity
import com.lia.yello.linedine.activity.OnBoardActivity
import com.lia.yello.linedine.databinding.FragmentOrdercompleteBinding
import kotlinx.android.synthetic.main.carticon.*
import kotlinx.android.synthetic.main.fragment_ordercomplete.*
import kotlinx.android.synthetic.main.header.*

class OrderSucessFragment : Fragment(R.layout.fragment_ordercomplete) {
     var orderrid:Int=0
    var binding: FragmentOrdercompleteBinding? = null;
    public var sharedHelper: SharedHelper? = null
    private lateinit var fragmentTransaction: FragmentTransaction
    private var homeFragment: HomeFragment? = null
    private var myOrderFragment: MyOrderFragment? = null

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        binding = DataBindingUtil.findBinding(view)
        sharedHelper = SharedHelper(requireContext())
        val head = view.findViewById<TextView>(R.id.head)
        head.setText("")

        sharedHelper!!.cartcount=0
        if (!NetworkUtils.isNetworkConnected(requireContext())) {
            UiUtils.showSnack(
                view, "no_internet_connection"
            )
        }

        if (sharedHelper!!.choose.equals("finedine")||sharedHelper!!.choose.equals("inqueue")|| sharedHelper!!.cartcount == 0){

            //var cartframe: FrameLayout =findViewById(R.id.cartframe)
            cartframe.visibility=View.GONE
        }
        if (sharedHelper!!.language == "ar"){
            back.rotation= 180F
        }
        orderrid = arguments?.getInt("orderid")!!

        orderid.setText("" + orderrid)
        cart_badge.text = "0"
        activity?.onBackPressedDispatcher?.addCallback(this, object : OnBackPressedCallback(true) {
            override fun handleOnBackPressed() {
                homeFragment = HomeFragment()
                fragmentTransaction =requireActivity().supportFragmentManager.beginTransaction()
                fragmentTransaction.setCustomAnimations(R.anim.screen_in, R.anim.screen_out)
                fragmentTransaction.replace(R.id.container, homeFragment!!)
                fragmentTransaction.addToBackStack(null)
                fragmentTransaction.commit()
            }
        })



        Handler(Looper.getMainLooper()).postDelayed({
/*
            val intent = Intent(requireContext(), DashBoardActivity::class.java)
            intent.putExtra("fromnotification", "pickup")
            startActivity(intent)*/

            myOrderFragment = MyOrderFragment()
            fragmentTransaction = requireActivity().supportFragmentManager.beginTransaction()
            fragmentTransaction.setCustomAnimations(R.anim.screen_in, R.anim.screen_out)
            fragmentTransaction.replace(R.id.container, myOrderFragment!!)
            fragmentTransaction.addToBackStack(null)
            fragmentTransaction.commit()

        }, 3000)

    }






}