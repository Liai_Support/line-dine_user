package com.lia.yello.linedine.Models

import com.google.gson.annotations.SerializedName
import com.lia.yello.linedine.activity.LoginData
import java.io.Serializable

class CheckPendingOrders : Serializable {

    @SerializedName("error")
    var error: Boolean? = null

    @SerializedName("message")
    var message: String? = null

    @SerializedName("data")
    var data: Orderid? = null

      }

    class Orderid : Serializable {

    @SerializedName("order_id")
    var order_id: String? = null


     }