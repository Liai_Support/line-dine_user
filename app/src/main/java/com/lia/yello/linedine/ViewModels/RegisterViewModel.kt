package com.lia.yello.linedine.ViewModels

import android.app.Application
import android.content.Context
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.LiveData
import com.app.hakeemUser.models.SocialLoginResponse
import com.app.hakeemUser.models.SocialRegisterResponse

import com.lia.yello.linedine.Models.ForgotpasswordResponse
import com.lia.yello.linedine.Models.OtpResponseModel
import com.lia.yello.linedine.Models.UpdatePasswordResponse
import com.lia.yello.linedine.Repository.RegisterRepository
import com.lia.yello.linedine.Session.SharedHelper
import com.lia.yello.linedine.Utils.Constants
import com.lia.yello.linedine.activity.LoginResponse
import com.lia.yello.linedine.activity.RegisterResponse
import com.lia.yello.linedine.network.ApiInput
import com.lia.yello.linedine.network.UrlHelper
import org.json.JSONObject

class RegisterViewModel(application: Application) : AndroidViewModel(application) {
    var repository: RegisterRepository = RegisterRepository.getInstance()
    var applicationIns: Application = application
    var sharedHelper: SharedHelper? = SharedHelper(applicationIns.applicationContext)


    fun createAccount(
        name: String,
        emailId: String,
        countryCode: String,
        mobileNumber: String,
        password: String
    ): LiveData<RegisterResponse>? {

        val jsonObject = JSONObject()
        jsonObject.put(Constants.ApiKeys.FIRSTNAME, name)
        jsonObject.put(Constants.ApiKeys.LASTNAME, "")
        jsonObject.put(Constants.ApiKeys.EMAIL, emailId)
        jsonObject.put(Constants.ApiKeys.COUNTRYCODE, countryCode)
        jsonObject.put(Constants.ApiKeys.PHONE, mobileNumber)
        jsonObject.put(Constants.ApiKeys.PASSWORD, password)
        //jsonObject.put(Constants.ApiKeys.PROFILEPIC, pic)

        return repository.createAccount(
            getApiParams(
                jsonObject,
                UrlHelper.CREATE_ACCOUNT
            )
        )
    }

    private fun getApiParams(jsonObject: JSONObject, methodName: String): ApiInput {

        val header: MutableMap<String, String> = HashMap()
        sharedHelper?.let { header[Constants.ApiKeys.LANG] = "en" }
        sharedHelper?.let { header[Constants.ApiKeys.AUTHORIZATION] = it.token }
        header[Constants.ApiKeys.ROLE] = Constants.ApiKeys.PATIENT


        val apiInputs = ApiInput()
        apiInputs.context = applicationIns.applicationContext
        apiInputs.jsonObject = jsonObject
        apiInputs.headers = header
        apiInputs.url = methodName

        return apiInputs
    }

    fun signin(email: String, password: String): LiveData<LoginResponse>? {

        val jsonObject = JSONObject()
        jsonObject.put(Constants.ApiKeys.EMAIL, email)
        jsonObject.put(Constants.ApiKeys.PASSWORD, password)

        return repository.signIn(
            getApiParams(
                jsonObject,
                UrlHelper.LOGIN
            )
        )
    }

    fun socialLogin(id: String?, type: String,mail: String): LiveData<SocialLoginResponse>? {

        val jsonObject = JSONObject()
        jsonObject.put("token", id)
        jsonObject.put("socialmedia", type)
        jsonObject.put("email", mail)


        return repository.socialLogin(
            getApiParams(
                jsonObject,
                UrlHelper.CHECK_SOCIAL_LOGIN
            )
        )
    }

    fun socialRegister(
        email: String,
        name: String,
        socialToken: String,
        selectedCountryCode: String?,
        phoneNumber: String, socialType: String
    ): LiveData<SocialRegisterResponse>? {
        val jsonObject = JSONObject()
        jsonObject.put("email", email)
        jsonObject.put("firstname", name)
        jsonObject.put("lastname","")
        jsonObject.put("token", socialToken)
        jsonObject.put("countrycode", selectedCountryCode)
        jsonObject.put("phone", phoneNumber)
        jsonObject.put("socialmedia", socialType)


        return repository.socilaRegisterDetails(
            getApiParams(
                jsonObject,
                UrlHelper.SOCIAL_SINGUP
            )
        )

    }



    fun forgotpassword(mobileno: String): LiveData<ForgotpasswordResponse>? {

        val jsonObject = JSONObject()
        jsonObject.put(Constants.ApiKeys.PHONE, mobileno)

        return repository.forgotpassword(
            getApiParams(
                jsonObject,
                UrlHelper.FORGOTPASSWORD
            )
        )
    }

    fun resetpassword(password: String): LiveData<UpdatePasswordResponse>? {

        val jsonObject = JSONObject()
        jsonObject.put("userid", sharedHelper?.id)
        jsonObject.put(Constants.ApiKeys.PASSWORD, password)

        return repository.updatepassword(
            getApiParams(
                jsonObject,
                UrlHelper.UPDATEPASSWORD
            )
        )
    }

    fun getverifyOtp(
        otp:Double

    ): LiveData<OtpResponseModel>? {

        val jsonObject = JSONObject()
        sharedHelper?.let { jsonObject.put("userId", it.id) }
        jsonObject.put("otp", otp)
        return repository.getverifyotp(
            getApiParams(
                jsonObject,
                UrlHelper.VERIFYOTP
            )
        )
    }

}