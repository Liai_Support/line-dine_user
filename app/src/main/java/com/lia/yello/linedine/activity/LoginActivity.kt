package com.lia.yello.linedine.activity

import android.Manifest
import android.app.AlertDialog
import android.content.Context
import android.content.Intent
import android.content.pm.PackageManager
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.net.Uri
import android.os.Bundle
import android.os.Handler
import android.telephony.TelephonyManager
import android.text.Editable
import android.text.TextWatcher
import android.util.DisplayMetrics
import android.util.Log
import android.view.*
import android.widget.EditText
import android.widget.ImageView
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.core.app.ActivityCompat
import androidx.core.content.ContextCompat
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import com.facebook.*
import com.facebook.login.LoginManager
import com.facebook.login.LoginResult
import com.google.android.gms.auth.api.Auth
import com.google.android.gms.auth.api.signin.GoogleSignInAccount
import com.google.android.gms.auth.api.signin.GoogleSignInOptions
import com.google.android.gms.auth.api.signin.GoogleSignInResult
import com.google.android.gms.common.ConnectionResult
import com.google.android.gms.common.api.GoogleApiClient
import com.google.android.gms.tasks.OnCompleteListener
import com.google.android.gms.tasks.Task
import com.google.android.material.snackbar.BaseTransientBottomBar
import com.google.android.material.snackbar.Snackbar
import com.google.firebase.FirebaseException
import com.google.firebase.auth.*
import com.google.firebase.auth.PhoneAuthProvider.ForceResendingToken
import com.lia.yello.linedine.GenericTextWatcher
import com.lia.yello.linedine.R
import com.lia.yello.linedine.Session.SharedHelper
import com.lia.yello.linedine.Utils.*
import com.lia.yello.linedine.ViewModels.RegisterViewModel
import kotlinx.android.synthetic.main.activity_login.*
import kotlinx.android.synthetic.main.activity_login.root
import kotlinx.android.synthetic.main.fragment_orders.*
import kotlinx.android.synthetic.main.fragment_profile.*
import kotlinx.android.synthetic.main.fragment_subhome.*
import kotlinx.android.synthetic.main.popup_mobile.view.*
import kotlinx.android.synthetic.main.popup_mobile.view.confirm
import kotlinx.android.synthetic.main.popup_otp.*
import kotlinx.android.synthetic.main.popup_otp.view.*
import kotlinx.android.synthetic.main.popup_password.*
import kotlinx.android.synthetic.main.popup_password.view.*
import org.json.JSONException
import org.json.JSONObject
import java.util.concurrent.TimeUnit

class LoginActivity : AppCompatActivity(),GoogleApiClient.OnConnectionFailedListener{
    private var viewmodel: RegisterViewModel? = null
    private  var sharedHelper: SharedHelper? = null

    var finishAct = false
    var mAuth: FirebaseAuth? = null
    var mobileNumber: String = ""
    var newpass1: String = ""
    var newpass2: String = ""
    var finalpass: String = ""
    var verificationID: String = ""
    var token_: String = ""
    private var mGoogleSignInClient: GoogleApiClient? = null
    lateinit var callbackManager: CallbackManager
    var type=""
    var doubleBackToExitPressedOnce :Boolean = false
    var ids:Int=0

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_login)
        sharedHelper = SharedHelper(this)

        FacebookSdk.sdkInitialize(applicationContext)
//        AppEventsLogger.activateApp(this);


        viewmodel = ViewModelProvider(this).get(RegisterViewModel::class.java)

        mAuth = FirebaseAuth.getInstance();

        initFacebookLogin()
        initGoogleLogin()

        Log.d("languagee",""+sharedHelper!!.language)
        if (sharedHelper!!.language == "ar"){
            googleimg.scaleX=-1F
            fbimg.scaleX=-1F
        }

    }

    private fun initGoogleLogin() {

        val gso = GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
            .requestIdToken("342670983437-orcui9h4n51rmp2cq4k901bb7hd52g07.apps.googleusercontent.com")//you can also use R.string.default_web_client_id
            .requestEmail()
            .build()

        mGoogleSignInClient = GoogleApiClient.Builder(this)
            .enableAutoManage(this, this)
            .addApi(Auth.GOOGLE_SIGN_IN_API, gso)
            .build()
    }

    private fun googlesignIn() {

        val signInIntent = Auth.GoogleSignInApi.getSignInIntent(mGoogleSignInClient)
        startActivityForResult(signInIntent, 100)
        Log.d("jvjv ", "goolelogiedfn");

    }


    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        Log.d("jvjv ", "requestcode" + requestCode);
        if (data != null) {
            Log.d("jvjdvdfvdfvv ", "resultcode" + resultCode);

            if (requestCode == 100 || data.getData()!=null) {
                val result = Auth.GoogleSignInApi.getSignInResultFromIntent(data)
                Log.d("jvjvcv ", "goolelogin" + result);

                result?.let {
                    mGoogleSignInClient?.clearDefaultAccountAndReconnect()
                    handleSignInResult(it)
                }

            } else {
                callbackManager.onActivityResult(requestCode, resultCode, data)
            }
        }
        else{
            UiUtils.showSnack(root, "Invd")
        }
    }


    private fun get_data_for_facebook(loginResult: LoginResult) {

        val request = GraphRequest.newMeRequest(loginResult.accessToken) { value, response ->
            handleFacebookSuccessResponse(
                value,
                response
            )
        }
        val parameters = Bundle()
        parameters.putString("fields", "id, first_name, last_name, email,gender, birthday, locale ")
        request.parameters = parameters
        request.executeAsync()
    }


    private fun handleFacebookSuccessResponse(value: JSONObject, response: GraphResponse) {

        val email_json = value.optString("email")
        val first_name = value.optString("first_name")
        val last_name = value.optString("last_name")
        val id = value.optString("id")
        type = "facebook"
        val imageUrl = "https://graph.facebook.com/$id/picture?type=large"

        Log.d("jfasdv", "" + email_json + "" + first_name);
        LoginManager.getInstance().logOut()


        viewmodel?.socialLogin(id, type, email_json)?.observe(this, Observer {
            DialogUtils.dismissLoader()
            it?.let {
                it.error?.let { error ->
                    if (error) {
                        it.message?.let { msg ->
                            UiUtils.showSnack(root, msg)
                            if (msg.equals("please verify your mobile number")){

                                showDialog5( value)
                            }

                        }
                    } else {
                        it.data?.let { data ->
                            data.userExists?.let { exist ->
                                if (exist) {
                                    data.email?.let { value -> sharedHelper?.email = value }
                                    data.id?.let { value -> sharedHelper?.id = value }
                                    data.name?.let { value -> sharedHelper?.name = value }
                                    data.mobileNumber?.let { value ->
                                        sharedHelper?.mobileNumber = value
                                    }
                                    data.token?.let { value -> sharedHelper?.token = value }
                                    data.countryCode?.let { value ->
                                        sharedHelper?.countryCode = value
                                    }
                                    /*data.profilePic?.let { value ->
                                        sharedHelper?.userImage = value
                                    }*/



                                    sharedHelper?.loggedIn = true
                                    if (finishAct) {
                                        finish()
                                    } else {
                                        if ( !check1.isChecked()){
                                            snackbarToast(getString(R.string.pleasecheckthebox));

                                        }else{
                                            val intent =
                                                Intent(this, DashBoardActivity::class.java)
                                            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP)
                                            intent.flags =
                                                Intent.FLAG_ACTIVITY_NEW_TASK or Intent.FLAG_ACTIVITY_CLEAR_TASK
                                            startActivity(intent)
                                            finish()
                                        }

                                    }

                                } else {
                                    val signInAccount: GoogleSignInAccount?=null


                                    Log.d("asdfghjkl;",""+signInAccount)
                                    showDialog5( value)


                                }
                            }


                        }
                    }
                }
            }
        })


    }

    private fun handleSignInResult(result: GoogleSignInResult) {
        Log.d("result ", "goin" + result);
        var ha = true
        if (result.isSuccess) {
            Log.d("kanika","shanmugam")
            val googleSignInAccount = result.signInAccount
            val email = googleSignInAccount!!.email
            val first_name: String
            val last_name: String
            type = "google"
            val id = googleSignInAccount.id
            val imageUrl = googleSignInAccount.photoUrl.toString()
            val full_name = googleSignInAccount.displayName
            if (full_name!!.contains(" ")) {
                val names =
                    full_name.split(" ".toRegex()).dropLastWhile { it.isEmpty() }.toTypedArray()
                first_name = names[0]
                last_name = names[1]
            } else {
                first_name = full_name
                last_name = ""
            }

            Auth.GoogleSignInApi.signOut(mGoogleSignInClient)
            DialogUtils.showLoader(this)
            viewmodel?.socialLogin(id, type, googleSignInAccount!!.email!!)?.observe(
                this,
                Observer {
                    DialogUtils.dismissLoader()
                    it?.let {
                        it.error?.let { error ->
                            if (error) {
                                it.message?.let { msg ->
                                    UiUtils.showSnack(root, msg)

                                    if (msg.equals("please verify your mobile number")){
                                        // UiUtils.showSnack(root, msg)
                                        var value: JSONObject? = JSONObject()
                                        showDialog3(googleSignInAccount, value!!)
                                    }

                                }
                            } else {
                                it.data?.let { data ->
                                    data.userExists?.let { exist ->
                                        if (exist) {
                                            data.email?.let { value -> sharedHelper?.email = value }
                                            data.id?.let { value -> sharedHelper?.id = value }
                                            data.name?.let { value -> sharedHelper?.name = value }
                                            data.mobileNumber?.let { value ->
                                                sharedHelper?.mobileNumber = value
                                            }
                                            data.token?.let { value -> sharedHelper?.token = value }
                                            data.countryCode?.let { value ->
                                                sharedHelper?.countryCode = value
                                            }
                                            /* data.profilePic?.let { value ->
                                            sharedHelper?.userImage = value
                                        }*/

                                            sharedHelper?.loggedIn = true
                                            if (finishAct) {
                                                finish()
                                            } else {
                                                if ( !check1.isChecked()){
                                                    snackbarToast(getString(R.string.pleasecheckthebox));

                                                }else{
                                                    val intent =
                                                        Intent(this, DashBoardActivity::class.java)
                                                    intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP)
                                                    intent.flags =
                                                        Intent.FLAG_ACTIVITY_NEW_TASK or Intent.FLAG_ACTIVITY_CLEAR_TASK
                                                    startActivity(intent)
                                                    finish()
                                                }


                                            }

                                        } else {

                                            var value: JSONObject? = JSONObject()
                                            showDialog3(googleSignInAccount, value!!)


                                            Log.d("dfghjkl", "" + googleSignInAccount.email)
                                            Log.d("dfghjkl", "" + googleSignInAccount.displayName)
                                            Log.d("dfghjkl", "" + googleSignInAccount.idToken)
                                            Log.d("dfghjkl", "" + googleSignInAccount.id)


                                            /*    startActivity(
                                            Intent(this, SocialLoginDetailsActivity::class.java)
                                                .putExtra(Constants.IntentKeys.EMAIL, email)
                                                .putExtra(Constants.IntentKeys.NAME, first_name)
                                                .putExtra(
                                                    Constants.IntentKeys.PROFILE_PICTURE,
                                                    imageUrl
                                                )
                                                .putExtra(Constants.IntentKeys.SOCIALTOKEN, id)
                                                .putExtra(
                                                    Constants.IntentKeys.LOGINTYPE,
                                                    Constants.SocialLogin.GOOGLE
                                                )
                                        )*/

                                        }
                                    }
                                }
                            }
                        }
                    }
                })


        }
        else{

            //   UiUtils.showSnack(root, getString(R.string.invalllid))
        }
    }

    private fun initFacebookLogin() {
        callbackManager = CallbackManager.Factory.create()

        //loginButtonFacebook.setReadPermissions("email")
        loginButtonFacebook.setReadPermissions("email", "public_profile", "user_friends")
        LoginManager.getInstance().registerCallback(callbackManager, object :
            FacebookCallback<LoginResult> {
            override fun onSuccess(loginResult: LoginResult) {

                get_data_for_facebook(loginResult)
            }

            override fun onCancel() {
                UiUtils.showLog("facebook", "")
            }

            override fun onError(exception: FacebookException) {
                UiUtils.showLog("facebook", exception.toString())
                LoginManager.getInstance().logOut()
            }
        })
    }


    fun onFacebookClicked(view: View) {
        if (NetworkUtils.isNetworkConnected(this))
            loginButtonFacebook.performClick()
        else
            UiUtils.showSnack(
                findViewById(android.R.id.content), "no_internet_connection"
            )

    }

    fun OnGoogleClicked(view: View) {
        googlesignIn()
    }

    private fun tryAndPrefillPhoneNumber(): String? {
        if (checkCallingOrSelfPermission(Manifest.permission.READ_PHONE_STATE) == PackageManager.PERMISSION_GRANTED) {
            val manager = getSystemService(TELEPHONY_SERVICE) as TelephonyManager
            //mPhoneNumber.setText(manager.line1Number)
            var hgh = ""+manager.line1Number
            return hgh
        } else {
            ActivityCompat.requestPermissions(
                this,
                arrayOf(Manifest.permission.READ_PHONE_STATE,Manifest.permission.READ_PHONE_NUMBERS,Manifest.permission.READ_SMS),
                0
            )
        }
        return tryAndPrefillPhoneNumber()
    }

    override fun onRequestPermissionsResult(
        requestCode: Int,
        permissions: Array<String?>,
        grantResults: IntArray
    ) {
        if (grantResults.size > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
            tryAndPrefillPhoneNumber()
        } else {
            /*  Toast.makeText(
                  this,
                  getString(R.string.thisapplicationneedspermissiontoreadyourphonenumbertoautomatically)
                          + getString(R.string.prefillit),
                  Toast.LENGTH_SHORT
              ).show()*/
            /*if (ActivityCompat.shouldShowRequestPermissionRationale(this, permissions[0]!!)) {

            }*/
        }
    }

    private fun loginTask() {

        var mCallBacks = object : PhoneAuthProvider.OnVerificationStateChangedCallbacks() {

            override fun onVerificationCompleted(p0: PhoneAuthCredential) {
                if (p0 != null) {
                    signInWithPhoneAuthCredential(p0)
                }
            }

            override fun onVerificationFailed(p0: FirebaseException) {
                UiUtils.showSnack(root, "Erroe")
            }

            /* override fun onVerificationCompleted(credential: PhoneAuthCredential?) {
                 if (credential != null) {
                     signInWithPhoneAuthCredential(credential)
                 }
             }

             override fun onVerificationFailed(p0: FirebaseException?) {
                // progressBar.visibility = View.GONE
                // toast("Invalid phone number or verification failed.")
             }
 */
            override fun onCodeSent(p0: String, p1: ForceResendingToken) {
                super.onCodeSent(p0, p1)
                //progressBar.visibility = View.GONE
                verificationID = p0.toString()
                token_ = p1.toString()

                //etNumber.setText("")

                //etNumber.setHint("Enter OTP ")
                //btnSignIn.setText("Verify OTP")

                /*  btnSignIn.setOnClickListener {
                      progressBar.visibility = View.VISIBLE
                      verifyAuthentication(verificationID, etNumber.text.toString())
                  }*/

                Log.e("Login : verificationId ", verificationID)
                Log.e("Login : token ", token_)
                showDialog2()
            }

            override fun onCodeAutoRetrievalTimeOut(p0: String) {
                super.onCodeAutoRetrievalTimeOut(p0)
                // progressBar.visibility = View.GONE
                // toast("Time out")
            }


        }

        PhoneAuthProvider.getInstance().verifyPhoneNumber(
            mobileNumber,            // Phone number to verify
            30,                  // Timeout duration
            TimeUnit.SECONDS,        // Unit of timeout
            this,                // Activity (for callback binding)
            mCallBacks
        );

    }

    private fun signInWithPhoneAuthCredential(credential: PhoneAuthCredential) {

        mAuth!!.signInWithCredential(credential)
            .addOnCompleteListener(this@LoginActivity, object : OnCompleteListener<AuthResult> {
                override fun onComplete(task: Task<AuthResult>) {
                    if (task.isSuccessful()) {
                        val user = task.getResult()?.getUser()
                        //  progressBar.visibility = View.GONE
                        // startActivity(Intent(this@LoginActivity, MainActivity::class.java))
                        //showDialog1(user)

                    } else {
                        if (task.getException() is FirebaseAuthInvalidCredentialsException) {
                            //  progressBar.visibility = View.GONE
                            //  toast("Invalid OPT")
                        }
                    }
                }
            })
    }

    private fun verifyAuthentication(verificationID: String, otpText: String) {

        val phoneAuthCredential = PhoneAuthProvider.getCredential(verificationID, otpText) as PhoneAuthCredential
        signInWithPhoneAuthCredential(phoneAuthCredential)

    }

    /* private fun showDialog2() {

         val mDialogView = LayoutInflater.from(this).inflate(R.layout.popup_otp, null)
         val mBuilder = AlertDialog.Builder(this).setView(mDialogView).setCancelable(false)
         val  mAlertDialog = mBuilder.show()
         val layoutParams = WindowManager.LayoutParams()
         val displayMetrics = DisplayMetrics()
         windowManager.defaultDisplay.getMetrics(displayMetrics)
         val displayWidth = displayMetrics.widthPixels
         val displayHeight = displayMetrics.heightPixels
         layoutParams.copyFrom(mAlertDialog.getWindow()?.getAttributes())
         layoutParams.width = ((displayWidth * 0.9f).toInt())
         mAlertDialog.getWindow()?.setAttributes(layoutParams)
         mAlertDialog.getWindow()?.setGravity(Gravity.CENTER)
         mAlertDialog.getWindow()?.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))

         val edit =
             arrayOf<EditText>(
                 mDialogView.otp1,
                 mDialogView.otp2,
                 mDialogView.otp3,
                 mDialogView.otp4,
               //  mDialogView.otp5,
                 //mDialogView.otp6
             )
         mDialogView.otp1.addTextChangedListener(GenericTextWatcher(mDialogView.otp1, edit))
         mDialogView.otp2.addTextChangedListener(GenericTextWatcher(mDialogView.otp2, edit))
         mDialogView.otp3.addTextChangedListener(GenericTextWatcher(mDialogView.otp3, edit))
         mDialogView.otp4.addTextChangedListener(GenericTextWatcher(mDialogView.otp4, edit))


         mDialogView.confirmotp.setOnClickListener {
             mAlertDialog.dismiss()
             var otp: String = mAlertDialog.otp1.text.toString()+mAlertDialog.otp2.text.toString()+mAlertDialog.otp3.text.toString()+mAlertDialog.otp4.text.toString()
             Log.d("dsvcdsv", "" + otp)

             getverifyotp(otp.toDouble())
           //  verifyAuthentication(verificationID, otp)
            // val mail = mDialogView.mobile.text.toString()

         }
     }*/




    private fun showDialog4() {

        val mDialogView = LayoutInflater.from(this).inflate(R.layout.popup_password, null)
        val mBuilder = AlertDialog.Builder(this).setView(mDialogView).setCancelable(true)
        val mAlertDialog = mBuilder.show()
        val layoutParams = WindowManager.LayoutParams()
        val displayMetrics = DisplayMetrics()
        windowManager.defaultDisplay.getMetrics(displayMetrics)
        val displayWidth = displayMetrics.widthPixels
        val displayHeight = displayMetrics.heightPixels
        layoutParams.copyFrom(mAlertDialog.getWindow()?.getAttributes())
        layoutParams.width = ((displayWidth * 0.9f).toInt())
        mAlertDialog.getWindow()?.setAttributes(layoutParams)
        mAlertDialog.getWindow()?.setGravity(Gravity.CENTER)
        mAlertDialog.getWindow()?.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))



        mDialogView.resetpassconfirm.setOnClickListener {

            newpass1 = mDialogView.pass1.text.toString()
            newpass2 = mDialogView.pass2.text.toString()

            if (mDialogView.pass1.text.toString().length==0 && mDialogView.pass2.text.toString().length==0){
                mDialogView.pass1.setError(getString(R.string.enterallfields))
                mDialogView.pass2.setError(getString(R.string.enterallfields))
            }
            else if (mDialogView.pass1.text.toString().length==0 || mDialogView.pass2.text.toString().length==0){
                if( mDialogView.pass1.text.toString().length==0){
                    mDialogView.pass1.setError(getString(R.string.enterallfields))
                }
                else if(mDialogView.pass2.text.toString().length==0){
                    mDialogView.pass2.setError(getString(R.string.enterallfields))
                }
            }
            else{
                if(mDialogView.pass1.text.toString()!=mDialogView.pass2.text.toString()){
                    UiUtils.showSnack(root, getString(R.string.passwordmismatch))

                }else{
                    resetpassword(newpass1)
                    mAlertDialog.dismiss()

                }
            }

        }
    }



    private fun showDialog5( value: JSONObject) {
        val mDialogView = LayoutInflater.from(this).inflate(R.layout.popup_mobile, null)
        val mBuilder = AlertDialog.Builder(this).setView(mDialogView)
        val  mAlertDialog = mBuilder.show()
        val layoutParams = WindowManager.LayoutParams()
        val displayMetrics = DisplayMetrics()
        windowManager.defaultDisplay.getMetrics(displayMetrics)
        val displayWidth = displayMetrics.widthPixels
        val displayHeight = displayMetrics.heightPixels
        layoutParams.copyFrom(mAlertDialog.getWindow()?.getAttributes())
        layoutParams.width = ((displayWidth * 0.8f).toInt())
        mAlertDialog.getWindow()?.setAttributes(layoutParams)
        mAlertDialog.getWindow()?.setGravity(Gravity.CENTER)
        mAlertDialog.getWindow()?.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))



        mDialogView.confirm.setOnClickListener {
            // mAlertDialog.dismiss()
            mobileNumber = mDialogView.mobile.text.toString()
            //mobileNumber = "+918870927538"
            if (mobileNumber.length > 0) {
                // progressBar.visibility = View.VISIBLE
                // Log.d("cbcvb", "" + mobileNumber);
                mAlertDialog.dismiss()
                socialRegister2(mobileNumber, value)



                //   loginTask()
            } else {
                mAlertDialog.dismiss()

                mDialogView.mobile.setError(getString(R.string.entervalidphonenumber))            }        }
    }

    private fun showDialog3(googleSignInAccount: GoogleSignInAccount, value: JSONObject) {
        val mDialogView = LayoutInflater.from(this).inflate(R.layout.popup_mobile, null)
        val mBuilder = AlertDialog.Builder(this).setView(mDialogView)
        val  mAlertDialog = mBuilder.show()
        val layoutParams = WindowManager.LayoutParams()
        val displayMetrics = DisplayMetrics()
        windowManager.defaultDisplay.getMetrics(displayMetrics)
        val displayWidth = displayMetrics.widthPixels
        val displayHeight = displayMetrics.heightPixels
        layoutParams.copyFrom(mAlertDialog.getWindow()?.getAttributes())
        layoutParams.width = ((displayWidth * 0.8f).toInt())
        mAlertDialog.getWindow()?.setAttributes(layoutParams)
        mAlertDialog.getWindow()?.setGravity(Gravity.CENTER)
        mAlertDialog.getWindow()?.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))



        mDialogView.confirm.setOnClickListener {
            // mAlertDialog.dismiss()
            mobileNumber = mDialogView.mobile.text.toString()
            //mobileNumber = "+918870927538"
            if (mobileNumber.length > 0) {
                // progressBar.visibility = View.VISIBLE
                // Log.d("cbcvb", "" + mobileNumber);
                mAlertDialog.dismiss()
                socialRegister(mobileNumber, googleSignInAccount, value)
                Log.d("dfgh", "" + googleSignInAccount.email)
                Log.d("dfghj", "" + googleSignInAccount.displayName)
                Log.d("wertyuio", "" + googleSignInAccount.idToken)
                Log.d("wertyuiop", "" + googleSignInAccount.id)


                //   loginTask()
            } else {
                mDialogView.mobile.setError(getString(R.string.entervalidphonenumber))            }        }
    }


    private fun showDialog1(mob: String) {
        val mDialogView = LayoutInflater.from(this).inflate(R.layout.popup_forget, null)
        val mBuilder = AlertDialog.Builder(this).setView(mDialogView).setCancelable(true)
        val  mAlertDialog = mBuilder.show()
        //mAlertDialog.getWindow()?.setLayout(100,40)
        val layoutParams = WindowManager.LayoutParams()
        val displayMetrics = DisplayMetrics()
        windowManager.defaultDisplay.getMetrics(displayMetrics)
        val displayWidth = displayMetrics.widthPixels
        val displayHeight = displayMetrics.heightPixels
        layoutParams.copyFrom(mAlertDialog.getWindow()?.getAttributes())
        /* layoutParams.width = WindowManager.LayoutParams.WRAP_CONTENT
        layoutParams.height = WindowManager.LayoutParams.WRAP_CONTENT*/
        layoutParams.width = ((displayWidth * 0.8f).toInt())
        //layoutParams.height = ((displayHeight * 0.5f).toInt())
        mAlertDialog.getWindow()?.setAttributes(layoutParams)
        mAlertDialog.getWindow()?.setGravity(Gravity.CENTER)
        mAlertDialog.getWindow()?.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))

        if (mob=="null"){
            mDialogView.mobile.setText("")

        }else{
            mDialogView.mobile.setText(mob)
            Log.d("gdhdchhd","nncnccnnc")
        }
        mDialogView.confirm.setOnClickListener {
            mobileNumber = mDialogView.mobile.text.toString()
            //mobileNumber = "+918870927538"
            if (mobileNumber.length > 0) {
                // progressBar.visibility = View.VISIBLE
                // Log.d("cbcvb", "" + mobileNumber);
                mAlertDialog.dismiss()
                forgotpassword(mobileNumber)

                //   loginTask()
            } else {
                mDialogView.mobile.setError(getString(R.string.entervalidphonenumber))            }
        }
    }

    fun Forget(view: View) {
        var mob = tryAndPrefillPhoneNumber()
        Log.d("dfghyujiko",""+mob)
        if (mob != null) {
            showDialog1(mob)
        }else{

        }
    }

    fun signup(view: View) {
        startActivity(Intent(this@LoginActivity, SignupActivity::class.java))
    }

    fun Login(view: View) {
        if (isValidInputs()){
            if(!check1.isChecked()){
                snackbarToast(getString(R.string.pleasecheckthebox));
            }else{

                DialogUtils.showLoader(this)
                viewmodel?.signin(emailtxt.text.toString().trim(), pswdtxt.text.toString().trim())
                    ?.observe(this,
                        Observer {
                            DialogUtils.dismissLoader()
                            it?.let {
                                it.error?.let { error ->
                                    if (error) {
                                        it.message?.let { msg ->
                                            UiUtils.showSnack(root, getString(R.string.invalidcredentails))
                                        }
                                    } else {
                                        it.data?.let { data ->
                                            handleResponse(data)
                                        }
                                    }
                                }

                            }
                        })


            }
        }


    }

    private fun handleResponse(data: LoginData) {

        data.email?.let { sharedHelper?.email = it }
        data.id?.let { sharedHelper?.id = it }
        data.name?.let { sharedHelper?.name = it }
        data.mobileNumber?.let { sharedHelper?.mobileNumber = it }
        data.token?.let { sharedHelper?.token = it }
        data.countryCode?.let { value -> sharedHelper?.countryCode = value }
        data.profilePic?.let { value -> sharedHelper?.userImage = value }

        sharedHelper?.loggedIn = true

        if (finishAct) {
            finish()
        } else {
            val intent =
                Intent(this, DashBoardActivity::class.java)
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP)
            intent.flags =
                Intent.FLAG_ACTIVITY_NEW_TASK or Intent.FLAG_ACTIVITY_CLEAR_TASK
            startActivity(intent)
            finish()
        }

    }


    private fun isValidInputs(): Boolean {
        when {

            emailtxt.text.toString().trim() == "" -> {
                UiUtils.showSnack(root, getString(R.string.pleaseenteryouremailid))
                return false
            }
            !BaseUtils.isValidEmail(emailtxt.text.toString()) -> {
                UiUtils.showSnack(root, getString(R.string.pleaseentervalidemailid))
                return false
            }
            pswdtxt.text.toString().trim() == "" -> {
                UiUtils.showSnack(root, getString(R.string.pleaseenteryourpassword))
                return false
            }


            else -> return true
        }


    }

    override fun onConnectionFailed(p0: ConnectionResult) {
        TODO("Not yet implemented")
    }

    private fun snackbarToast(msg: String) {
        var snackbar: Snackbar
        //contextView = findViewById(R.id.contextview);
        val view = findViewById<View>(R.id.signinbtn)
        Snackbar.make(view, msg, Snackbar.LENGTH_SHORT)
            .setTextColor(
                ContextCompat.getColor(
                    this@LoginActivity,
                    R.color.design_default_color_error
                )
            )
            .setBackgroundTint(ContextCompat.getColor(this@LoginActivity, R.color.white))
            .setDuration(BaseTransientBottomBar.LENGTH_LONG)
            .setAnimationMode(BaseTransientBottomBar.ANIMATION_MODE_SLIDE)
            .show()
    }



    private fun forgotpassword(mobileno: String){
        DialogUtils.showLoader(this)
        viewmodel?.forgotpassword(mobileno)
            ?.observe(this,
                Observer {
                    DialogUtils.dismissLoader()

                    it?.let {
                        it.error?.let { error ->
                            if (error) {

                                it.message?.let { msg ->
                                    UiUtils.showSnack(root, msg)
                                }

                            } else {
                                it.message?.let { msg ->
                                    //  UiUtils.showSnack(root, msg)
                                    sharedHelper?.id = it.data!!.userid!!
                                    sharedHelper?.Otp = it.data!!.OTP!!
                                    showDialog3()
                                }


                            }
                        }

                    }
                })
    }


    private fun resetpassword(finalpassword: String){
        DialogUtils.showLoader(this)
        viewmodel?.resetpassword(finalpassword)
            ?.observe(this,
                Observer {
                    DialogUtils.dismissLoader()

                    it?.let {
                        it.error?.let { error ->
                            if (error) {

                                it.message?.let { msg ->
                                    UiUtils.showSnack(root, msg)
                                }

                            } else {
                                it.message?.let { msg ->
                                    UiUtils.showSnack(root, msg)
                                }

                            }
                        }

                    }
                })
    }

    private fun socialRegister(
        mobileno: String,
        googleSignInAccount: GoogleSignInAccount,
        value: JSONObject
    ){
        Log.d("asdfghjkl;", "" + googleSignInAccount.email)
        Log.d("sdfghjkl", "" + googleSignInAccount.displayName)
        Log.d("cvgbbhjkolp", "" + googleSignInAccount.idToken)
        Log.d("wsedfghnm", "" + googleSignInAccount.id)

        DialogUtils.showLoader(this)
        if (googleSignInAccount!=null){

            Log.d("mssmksjsudh;", "" + googleSignInAccount.email)
            Log.d("sgwysgywus", "" + googleSignInAccount.displayName)
            Log.d("sywgswhsjiwwos", "" + googleSignInAccount.idToken)
            Log.d("hswwgswjsjiw", "" + googleSignInAccount.id)

            viewmodel?.socialRegister(
                googleSignInAccount?.email!!.toString(),
                googleSignInAccount?.displayName!!.toString(),
                googleSignInAccount?.id!!.toString(),
                "966",
                mobileno,
                type
            )
                ?.observe(this,
                    Observer {
                        DialogUtils.dismissLoader()
                        it?.let {
                            it.error?.let { error ->
                                if (error) {

                                    it.message?.let { msg ->
                                        UiUtils.showSnack(root, msg)
                                    }

                                } else {

                                    it.message?.let { msg ->
                                        if (msg.equals("mobile no or email id already exist. please login ") ){
                                            UiUtils.showSnack(root, msg)
                                            Log.d("sdfghjkl;","hjkhj")
                                        }else{
                                            showDialog2()
                                            it.data?.let { data->
                                                Log.d("wertyui","sdfghjkl;")
                                                sharedHelper!!.id=data.id!!

                                            }
                                        }

                                    }

                                }
                            }

                        }
                    })
        }else{
            viewmodel?.socialRegister(
                value.optString("email"),
                value.optString("first_name"),
                value.optString("id"),
                "966",
                mobileno,
                type
            )
                ?.observe(this,
                    Observer {
                        DialogUtils.dismissLoader()
                        it?.let {
                            it.error?.let { error ->
                                if (error) {

                                    it.message?.let { msg ->
                                        UiUtils.showSnack(root, msg)
                                    }

                                } else {
                                    it.data?.let { data->
                                        sharedHelper!!.id=data.id!!

                                    }
                                    it.message?.let { msg ->
                                        UiUtils.showSnack(root, msg)

                                        showDialog2()
                                    }


                                }
                            }

                        }
                    })
        }

    }


    private fun socialRegister2(
        mobileno: String,
        value: JSONObject
    ){


        DialogUtils.showLoader(this)

        viewmodel?.socialRegister(
            value.optString("email"),
            value.optString("first_name"),
            value.optString("id"),
            "966",
            mobileno,
            type
        )
            ?.observe(this,
                Observer {
                    DialogUtils.dismissLoader()
                    it?.let {
                        it.error?.let { error ->
                            if (error) {

                                it.message?.let { msg ->
                                    UiUtils.showSnack(root, msg)
                                }

                            } else {
                                it.data?.let { data->
                                    sharedHelper!!.id=data.id!!

                                }
                                it.message?.let { msg ->
                                    UiUtils.showSnack(root, msg)
                                    showDialog2()

                                }

                            }
                        }

                    }
                })
    }


    private fun getverifyotp(
        OTP: Double
    ){


        DialogUtils.showLoader(this)

        viewmodel?.getverifyOtp(OTP)
            ?.observe(this,
                Observer {
                    DialogUtils.dismissLoader()
                    it?.let {
                        it.error?.let { error ->
                            if (error) {

                                it.message?.let { msg ->
                                    UiUtils.showSnack(root, msg)
                                }

                            } else {
                                it.message?.let { msg ->
                                    UiUtils.showSnack(root, msg)
                                    val intent =
                                        Intent(this, DashBoardActivity::class.java)
                                    intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP)
                                    intent.flags =
                                        Intent.FLAG_ACTIVITY_NEW_TASK or Intent.FLAG_ACTIVITY_CLEAR_TASK
                                    startActivity(intent)
                                    finish()
                                }

                            }
                        }

                    }
                })
    }

    override fun onBackPressed() {
        val intent = Intent(this, OnBoardActivity::class.java)
        startActivity(intent)

        /*   if (doubleBackToExitPressedOnce) {
               val intent = Intent(Intent.ACTION_MAIN)
               intent.addCategory(Intent.CATEGORY_HOME)
               startActivity(intent)
               return
           }
           doubleBackToExitPressedOnce = true
           Toast.makeText(this, "Please click BACK again to exit", Toast.LENGTH_SHORT).show()
           Handler().postDelayed({ doubleBackToExitPressedOnce = false }, 2000)*/
    }


    fun Termscondition(view: View) {

        val intent = Intent(
            "android.intent.action.VIEW",
            Uri.parse("https://lineandine.com/termsconditions.php")
        )
        startActivity(intent)


    }

    fun Termscondition1(view: View) {

        check1.isChecked=true


    }


    private fun showDialog2() {
        val mDialogView =
            LayoutInflater.from(this as Context).inflate(R.layout.popup_otp, null as ViewGroup?)
        val mBuilder =
            AlertDialog.Builder(this as Context).setView(mDialogView).setCancelable(false)
        val mAlertDialog = mBuilder.show()
        val layoutParams = WindowManager.LayoutParams()
        val displayMetrics = DisplayMetrics()
        val windowManager = this.windowManager
        windowManager.defaultDisplay.getMetrics(displayMetrics)
        val displayWidth = displayMetrics.widthPixels
        val displayHeight = displayMetrics.heightPixels
        val window = mAlertDialog.window
        layoutParams.copyFrom(window?.attributes)
        layoutParams.width = (displayWidth.toFloat() * 0.9f).toInt()
        window!!.attributes = layoutParams
        window.setGravity(Gravity.CENTER)
        window.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))


        if (sharedHelper!!.language=="ar"){
            mDialogView.linearotp.rotation= 180F
            mDialogView.otp1.rotation= 180F
            mDialogView.otp2.rotation= 180F
            mDialogView.otp3.rotation= 180F
            mDialogView.otp4.rotation= 180F
        }
        val otp1 = mDialogView.findViewById<EditText>(R.id.otp1)
        val otp2 = mDialogView.findViewById<EditText>(R.id.otp2)
        val otp3 = mDialogView.findViewById<EditText>(R.id.otp3)
        val otp4 = mDialogView.findViewById<EditText>(R.id.otp4)
        otp1.requestFocus()
        otp1.addTextChangedListener(object : TextWatcher {
            override fun beforeTextChanged(charSequence: CharSequence, i: Int, i1: Int, i2: Int) {}
            override fun onTextChanged(charSequence: CharSequence, i: Int, i1: Int, i2: Int) {}
            override fun afterTextChanged(editable: Editable) {
                if (editable.toString() != "") {
                    otp2.isFocusable = true
                    otp2.requestFocus()
                }
            }
        })
        otp2.addTextChangedListener(object : TextWatcher {
            override fun beforeTextChanged(charSequence: CharSequence, i: Int, i1: Int, i2: Int) {}
            override fun onTextChanged(charSequence: CharSequence, i: Int, i1: Int, i2: Int) {}
            override fun afterTextChanged(editable: Editable) {
                if (editable.toString() != "") {
                    otp3.isFocusable = true
                    otp3.requestFocus()
                } else {
                    otp1.isFocusable = true
                    otp1.requestFocus()
                }
            }
        })
        otp3.addTextChangedListener(object : TextWatcher {
            override fun beforeTextChanged(charSequence: CharSequence, i: Int, i1: Int, i2: Int) {}
            override fun onTextChanged(charSequence: CharSequence, i: Int, i1: Int, i2: Int) {}
            override fun afterTextChanged(editable: Editable) {
                if (editable.toString() != "") {
                    otp4.isFocusable = true
                    otp4.requestFocus()
                } else {
                    otp2.isFocusable = true
                    otp2.requestFocus()
                }
            }
        })
        otp4.addTextChangedListener(object : TextWatcher {
            override fun beforeTextChanged(charSequence: CharSequence, i: Int, i1: Int, i2: Int) {}
            override fun onTextChanged(charSequence: CharSequence, i: Int, i1: Int, i2: Int) {}
            override fun afterTextChanged(editable: Editable) {
                if (editable.toString() != "") {
                    otp4.isFocusable = true
                    otp4.requestFocus()
                } else {
                    otp3.isFocusable = true
                    otp3.requestFocus()
                }
            }
        })
        otp2.setOnKeyListener { view, i, keyEvent ->
            if (i == KeyEvent.KEYCODE_DEL) {
                if (otp2.text.toString().isEmpty()) {
                    otp1.setText("")
                    otp1.requestFocus()
                }
            }
            false
        }
        otp3.setOnKeyListener { view, i, keyEvent ->
            if (i == KeyEvent.KEYCODE_DEL) {
                if (otp3.text.toString().isEmpty()) {
                    otp2.setText("")
                    otp2.requestFocus()
                }
            }
            false
        }
        otp4.setOnKeyListener { view, i, keyEvent ->
            if (i == KeyEvent.KEYCODE_DEL) {
                if (otp4.text.toString().isEmpty()) {
                    otp3.setText("")
                    otp3.requestFocus()
                }
            }
            false
        }


        mDialogView.confirmotp.setOnClickListener {
            if ( otp1.text.toString() != "" && otp2.text.toString() != "" && otp3.text.toString() != "" && otp4.text.toString() != ""){
                mAlertDialog.dismiss()
                var otp: String = mAlertDialog.otp1.text.toString()+mAlertDialog.otp2.text.toString()+mAlertDialog.otp3.text.toString()+mAlertDialog.otp4.text.toString()
                Log.d("dsvcdsv", "" + otp)

                getverifyotp(otp.toDouble())
            }else{
                UiUtils.showSnack(root, getString(R.string.entervalidotp))
            }


            mDialogView.otp_close.setOnClickListener{
                mAlertDialog.dismiss()

            }
            //  verifyAuthentication(verificationID, otp)
            // val mail = mDialogView.mobile.text.toString()

        }



    }
    private fun showDialog3() {

        val mDialogView =
            LayoutInflater.from(this as Context).inflate(R.layout.popup_otp, null as ViewGroup?)
        val mBuilder =
            AlertDialog.Builder(this as Context).setView(mDialogView).setCancelable(false)
        val mAlertDialog = mBuilder.show()
        val layoutParams = WindowManager.LayoutParams()
        val displayMetrics = DisplayMetrics()
        val windowManager = this.windowManager
        windowManager.defaultDisplay.getMetrics(displayMetrics)
        val displayWidth = displayMetrics.widthPixels
        val displayHeight = displayMetrics.heightPixels
        val window = mAlertDialog.window
        layoutParams.copyFrom(window?.attributes)
        layoutParams.width = (displayWidth.toFloat() * 0.9f).toInt()
        window!!.attributes = layoutParams
        window.setGravity(Gravity.CENTER)
        window.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))

        if (sharedHelper!!.language=="ar"){
            mDialogView.linearotp.rotation= 180F
            mDialogView.otp1.rotation= 180F
            mDialogView.otp2.rotation= 180F
            mDialogView.otp3.rotation= 180F
            mDialogView.otp4.rotation= 180F
        }
        val otp1 = mDialogView.findViewById<EditText>(R.id.otp1)
        val otp2 = mDialogView.findViewById<EditText>(R.id.otp2)
        val otp3 = mDialogView.findViewById<EditText>(R.id.otp3)
        val otp4 = mDialogView.findViewById<EditText>(R.id.otp4)
        otp1.requestFocus()
        otp1.addTextChangedListener(object : TextWatcher {
            override fun beforeTextChanged(charSequence: CharSequence, i: Int, i1: Int, i2: Int) {}
            override fun onTextChanged(charSequence: CharSequence, i: Int, i1: Int, i2: Int) {}
            override fun afterTextChanged(editable: Editable) {
                if (editable.toString() != "") {
                    otp2.isFocusable = true
                    otp2.requestFocus()
                }
            }
        })
        otp2.addTextChangedListener(object : TextWatcher {
            override fun beforeTextChanged(charSequence: CharSequence, i: Int, i1: Int, i2: Int) {}
            override fun onTextChanged(charSequence: CharSequence, i: Int, i1: Int, i2: Int) {}
            override fun afterTextChanged(editable: Editable) {
                if (editable.toString() != "") {
                    otp3.isFocusable = true
                    otp3.requestFocus()
                } else {
                    otp1.isFocusable = true
                    otp1.requestFocus()
                }
            }
        })
        otp3.addTextChangedListener(object : TextWatcher {
            override fun beforeTextChanged(charSequence: CharSequence, i: Int, i1: Int, i2: Int) {}
            override fun onTextChanged(charSequence: CharSequence, i: Int, i1: Int, i2: Int) {}
            override fun afterTextChanged(editable: Editable) {
                if (editable.toString() != "") {
                    otp4.isFocusable = true
                    otp4.requestFocus()
                } else {
                    otp2.isFocusable = true
                    otp2.requestFocus()
                }
            }
        })
        otp4.addTextChangedListener(object : TextWatcher {
            override fun beforeTextChanged(charSequence: CharSequence, i: Int, i1: Int, i2: Int) {}
            override fun onTextChanged(charSequence: CharSequence, i: Int, i1: Int, i2: Int) {}
            override fun afterTextChanged(editable: Editable) {
                if (editable.toString() != "") {
                    otp4.isFocusable = true
                    otp4.requestFocus()
                } else {
                    otp3.isFocusable = true
                    otp3.requestFocus()
                }
            }
        })
        otp2.setOnKeyListener { view, i, keyEvent ->
            if (i == KeyEvent.KEYCODE_DEL) {
                if (otp2.text.toString().isEmpty()) {
                    otp1.setText("")
                    otp1.requestFocus()
                }
            }
            false
        }
        otp3.setOnKeyListener { view, i, keyEvent ->
            if (i == KeyEvent.KEYCODE_DEL) {
                if (otp3.text.toString().isEmpty()) {
                    otp2.setText("")
                    otp2.requestFocus()
                }
            }
            false
        }
        otp4.setOnKeyListener { view, i, keyEvent ->
            if (i == KeyEvent.KEYCODE_DEL) {
                if (otp4.text.toString().isEmpty()) {
                    otp3.setText("")
                    otp3.requestFocus()
                }
            }
            false
        }

        Log.d("hhjhjh",""+sharedHelper!!.Otp.substring(0,1))
        Log.d("hhjhjh",""+sharedHelper!!.Otp.substring(1,2))
        Log.d("hhjhjh",""+sharedHelper!!.Otp.substring(2,3))
        Log.d("hhjhjh",""+sharedHelper!!.Otp.substring(3,4))

        // otp1.setText(sharedHelper!!.Otp.substring(0,1))
        //otp2.setText(sharedHelper!!.Otp.substring(1,2))
        //otp3.setText(sharedHelper!!.Otp.substring(2,3))
        //otp4.setText(sharedHelper!!.Otp.substring(3,4))
        mDialogView.confirmotp.setOnClickListener {
            if ( otp1.text.toString() != "" && otp2.text.toString() != "" && otp3.text.toString() != "" && otp4.text.toString() != "" ){
                mAlertDialog.dismiss()
                var otp: String = mAlertDialog.otp1.text.toString()+mAlertDialog.otp2.text.toString()+mAlertDialog.otp3.text.toString()+mAlertDialog.otp4.text.toString()
                Log.d("dsvcdsv", "" + otp)

                if (otp == sharedHelper!!.Otp){
                    showDialog4()

                }else{
                    UiUtils.showSnack(root, getString(R.string.entervalidotp))

                }
            }else{
                UiUtils.showSnack(root, getString(R.string.entervalidotp))
            }


            //  verifyAuthentication(verificationID, otp)
            // val mail = mDialogView.mobile.text.toString()

        }

        mDialogView.otp_close.setOnClickListener{
            mAlertDialog.dismiss()

        }



    }

}