package com.lia.yello.linedine.activity

import android.content.Intent
import android.os.Bundle
import android.os.Handler
import android.util.Log
import android.view.View
import android.widget.ImageView
import android.widget.LinearLayout
import androidx.appcompat.app.AppCompatActivity
import androidx.core.content.ContentProviderCompat.requireContext
import androidx.core.content.ContextCompat
import androidx.fragment.app.FragmentTransaction
import androidx.viewpager.widget.PagerAdapter
import androidx.viewpager.widget.ViewPager
import androidx.viewpager2.widget.ViewPager2
import androidx.viewpager2.widget.ViewPager2.SCROLL_STATE_DRAGGING
import com.facebook.FacebookSdk
import com.google.android.material.tabs.TabLayoutMediator
import com.lia.yello.linedine.R
import com.lia.yello.linedine.Session.SharedHelper
import com.lia.yello.linedine.adapter.HomeBannerFragment2
import com.lia.yello.linedine.adapter.OnBoardAdapter
import com.lia.yello.linedine.adapter.OnBoardAdapter2
import com.lia.yello.linedine.fragment.CartFragment
import com.lia.yello.linedine.fragment.HomeFragment
import com.lia.yello.linedine.fragment.MyOrderFragment
import com.lia.yello.linedine.interfaces.OnClickListener
import com.lia.yello.linedine.interfaces.SingleTapListener
import kotlinx.android.synthetic.main.activity_choose_app.*
import kotlinx.android.synthetic.main.fragment_home.*
import kotlinx.android.synthetic.main.popup_mobile.view.*
import java.util.*


class OnBoardActivity : BaseActivity() {
    var imageList1 = arrayListOf(
        R.drawable.onboardfinedine,
        R.drawable.onboardinqueue,
        R.drawable.onboardpickup
    )




    var sharedHelper: SharedHelper? = null


    var timer: Timer? = null
    public var dotscount = 0
    var currentPage = 0
    val DELAY_MS: Long = 2000 //delay in milliseconds before task is to be executed
    val PERIOD_MS: Long = 2000
    private var dots: Array<ImageView?>? = null
    private var scrollStarted:Boolean=false
    private  var checkDirection:Boolean = false
    private val thresholdOffset = 0.5f
    private val thresholdOffsetPixels = 1
    var isSkipclicked = false

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_choose_app)
        val handler = Handler()
        sharedHelper = SharedHelper(this)

        Log.d("obboard",""+sharedHelper!!.language)


        var descriptionList = arrayListOf(
            getString(R.string.finedescription),
            getString(R.string.inqueuedescription),
            getString(R.string.pickupdescription)
        )
        var descriptionhead = arrayListOf(
            getString(R.string.finedine),
            getString(R.string.inqueue),
            getString(R.string.pickup)
        )


        val boardAdapter:PagerAdapter =
            OnBoardAdapter2(
                this@OnBoardActivity,
                this,
                imageList1,
                descriptionList,
                descriptionhead,
                object :
                    OnClickListener {
                    override fun onClickItem(position: Int) {
                        onBoard.currentItem = position
                    }

                },
                object : SingleTapListener {
                    override fun singleTap() {

                        proceed()

                    }
                })
        onBoard.adapter = boardAdapter
      dotscount = boardAdapter.count

        dots = arrayOfNulls<ImageView>(dotscount)

        for (i in 0 until dotscount) {
            dots!![i] = ImageView(this)
            dots!![i]!!.setImageDrawable(
                ContextCompat.getDrawable(
                    FacebookSdk.getApplicationContext(),
                    R.drawable.noncircledots
                )
            )
            val params = LinearLayout.LayoutParams(
                LinearLayout.LayoutParams.WRAP_CONTENT,
                LinearLayout.LayoutParams.WRAP_CONTENT
            )
            params.setMargins(8, 0, 8, 0)
            tabLayout.addView(dots!![i], params)
        }

        dots!![0]?.setImageDrawable(
            ContextCompat.getDrawable(
                FacebookSdk.getApplicationContext(),
                R.drawable.circledots
            )
        )

        onBoard.addOnPageChangeListener(object :
            ViewPager.OnPageChangeListener {
            override fun onPageScrolled(
                position: Int,
                positionOffset: Float,
                positionOffsetPixels: Int
            ) {

                if (checkDirection) {
                    if (thresholdOffset > positionOffset && positionOffsetPixels > thresholdOffsetPixels) {
                        Log.d("C.TAG", "going left");
                    } else {
                        Log.d("C.TAG", "going right");
                    }
                    checkDirection = false;
                }
            }


               override fun onPageSelected(position: Int) {
                       for (i in 0 until dotscount) {
                           dots!![i]!!.setImageDrawable(
                               ContextCompat.getDrawable(
                                   FacebookSdk.getApplicationContext(),
                                   R.drawable.noncircledots
                               )
                           )
                       }
                       dots!![position]?.setImageDrawable(
                           ContextCompat.getDrawable(
                               FacebookSdk.getApplicationContext(),
                               R.drawable.circledots
                           )
                       )
                   }

            override fun onPageScrollStateChanged(state: Int) {


                if (!scrollStarted && state == ViewPager.SCROLL_STATE_DRAGGING) {
                    scrollStarted = true;
                    checkDirection = true;
                } else {
                    scrollStarted = false;
                }
            }

        })



        /*After setting the adapter use the timer */
/*        val Update = Runnable {
            if (currentPage == dotscount) {
                currentPage = 0
            }
            if (onBoard!=null){

                onBoard.setCurrentItem(currentPage++, true)

            }
        }


        timer = Timer() // This will create a new Thread

        timer!!.schedule(object : TimerTask() {
            // task to be scheduled
            override fun run() {
                handler.post(Update)
            }
        }, DELAY_MS, PERIOD_MS)*/



    }

    fun Finedine(view: View) {
        sharedHelper!!.choose="finedine"

        if (SharedHelper(this).loggedIn) {
            startActivity(Intent(this@OnBoardActivity, DashBoardActivity::class.java))

        }else{
            startActivity(Intent(this@OnBoardActivity, LoginActivity::class.java))

        }
    }

    fun Pickup(view: View) {
        sharedHelper!!.choose="pickup"

        if (SharedHelper(this).loggedIn) {

          startActivity(Intent(this@OnBoardActivity, DashBoardActivity::class.java))
        }else{

         startActivity(Intent(this@OnBoardActivity, LoginActivity::class.java))
        }

    }
    fun Inqueue(view: View) {
        sharedHelper!!.choose="inqueue"

        if (SharedHelper(this).loggedIn) {

        startActivity(Intent(this@OnBoardActivity, DashBoardActivity::class.java))
        }else{

        startActivity(Intent(this@OnBoardActivity, LoginActivity::class.java))
        }



    }


    fun proceed(){
        if (SharedHelper(this).loggedIn) {

            Log.d("asdfghjkl",""+sharedHelper!!.choose)

            startActivity(Intent(this@OnBoardActivity, DashBoardActivity::class.java))
        }else{
            Log.d("asdfghjkl",""+sharedHelper!!.choose)

            startActivity(Intent(this@OnBoardActivity, LoginActivity::class.java))
        }
    }

    override fun onBackPressed() {
        //super.onBackPressed()
        val intent = Intent(this, ChooseLanguageActivity::class.java)
        startActivity(intent)
    }



}