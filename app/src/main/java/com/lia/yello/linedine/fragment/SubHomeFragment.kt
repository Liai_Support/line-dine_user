package com.lia.yello.linedine.fragment
import android.graphics.Color
import android.os.Bundle
import android.text.Editable
import android.text.TextWatcher
import android.util.Log
import android.view.View
import android.widget.TextView
import android.widget.Toast
import androidx.activity.OnBackPressedCallback
import androidx.core.content.ContextCompat
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentTransaction
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.facebook.FacebookSdk
import com.google.android.material.bottomsheet.BottomSheetBehavior
import com.lia.yello.linedine.Models.listresturant
import com.lia.yello.linedine.R
import com.lia.yello.linedine.Session.SharedHelper
import com.lia.yello.linedine.Utils.DialogUtils
import com.lia.yello.linedine.Utils.NetworkUtils
import com.lia.yello.linedine.Utils.UiUtils
import com.lia.yello.linedine.ViewModels.MapViewModel
import com.lia.yello.linedine.activity.DashBoardActivity
import com.lia.yello.linedine.adapter.ScrollListAdapter
import com.lia.yello.linedine.adapter.SubHomeAdapter
import com.lia.yello.linedine.databinding.FragmentSubhomeBinding
import kotlinx.android.synthetic.main.activity_dash_board.*
import kotlinx.android.synthetic.main.carticon.*
import kotlinx.android.synthetic.main.fragment_home.*
import kotlinx.android.synthetic.main.fragment_profile.*
import kotlinx.android.synthetic.main.fragment_profile.root
import kotlinx.android.synthetic.main.fragment_subhome.*
import kotlinx.android.synthetic.main.header.*
import kotlinx.android.synthetic.main.homenew.*

class SubHomeFragment : Fragment(R.layout.fragment_subhome) {
    var inputText: Int? = null
    var binding: FragmentSubhomeBinding? = null
    private var mapViewModel: MapViewModel? = null
    lateinit var adapter: RecyclerView.Adapter<RecyclerView.ViewHolder>
    lateinit var layoutManager: RecyclerView.LayoutManager
    var filterid: Int = 0
    var cuisinearray: ArrayList<Int> = arrayListOf()
    lateinit var cuisinearray2: Array<Int>
    var iscuisine: String=""
    var filter: String=""
    var type: String=""
    public var sharedHelper: SharedHelper? = null
    var search1: String=""
    public var list: ArrayList<listresturant>? = null
    val listcopy: ArrayList<listresturant> = ArrayList<listresturant>()
    var isfilterseleted: Boolean = false
    var filtertext:String = ""
    private var subHomeFragment: SubHomeFragment? = null

    private var homeFragment : HomeFragment? = null
    private lateinit var fragmentTransaction: FragmentTransaction
    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        binding = DataBindingUtil.findBinding(view)
        mapViewModel = ViewModelProvider(this).get(MapViewModel::class.java)
        sharedHelper = SharedHelper(requireContext())

        val head = view.findViewById<TextView>(R.id.head)
        if (!NetworkUtils.isNetworkConnected(requireContext())) {
            UiUtils.showSnack(
                view, "no_internet_connection"
            )
        }


        if (sharedHelper!!.language == "ar"){
           back.rotation= 180F
        }
        if (sharedHelper!!.choose.equals("finedine")||sharedHelper!!.choose.equals("inqueue")|| sharedHelper!!.cartcount == 0){

            //var cartframe: FrameLayout =findViewById(R.id.cartframe)
            cartframe.visibility=View.GONE
        }
        iscuisine = arguments?.getString("iscuisine")!!
        inputText = arguments?.getInt("imgid")
        type = arguments?.getString("type")!!
        Log.d("iscuisine,", "" + iscuisine)
        Log.d("zsxdcfvgbnm,", "" + inputText)


        if (iscuisine=="true"){
            inputText = arguments?.getInt("imgid")
            //   filterid = arguments?.getInt("id")!!
            cuisinearray = arguments?.getIntegerArrayList("cuisinearray")!!
            Log.d("zsxdcfvgbnm,", "" + inputText)
            Log.d("wedfgbnoiujhbv,", "" + filterid)
            Log.d("cuisinearray,", "" + cuisinearray)
        }

        listrestfinedine.setOnClickListener{
            sharedHelper!!.choose="finedine"
            listrestfinedine.backgroundTintList = ContextCompat.getColorStateList(FacebookSdk.getApplicationContext(),R.color.colorPrimary2)
            listrestinqueue.backgroundTintList = ContextCompat.getColorStateList(FacebookSdk.getApplicationContext(),R.color.white)
            listrestpickup.backgroundTintList = ContextCompat.getColorStateList(FacebookSdk.getApplicationContext(),R.color.white)
            textfdl.setTextColor(Color.WHITE)
            textpickupl.setTextColor(Color.BLACK)
            textinql.setTextColor(Color.BLACK)
            getvalues(filter)


        }
        listrestinqueue.setOnClickListener{
            sharedHelper!!.choose="inqueue"

            listrestfinedine.backgroundTintList = ContextCompat.getColorStateList(FacebookSdk.getApplicationContext(),R.color.white)
            listrestinqueue.backgroundTintList = ContextCompat.getColorStateList(FacebookSdk.getApplicationContext(),R.color.colorPrimary2)

            listrestpickup.backgroundTintList = ContextCompat.getColorStateList(FacebookSdk.getApplicationContext(),R.color.white)

            textfdl.setTextColor(Color.BLACK)
            textpickupl.setTextColor(Color.BLACK)
            textinql.setTextColor(Color.WHITE)
            getvalues(filter)

        }

        listrestpickup.setOnClickListener{
          sharedHelper!!.choose="pickup"

            listrestinqueue.backgroundTintList = ContextCompat.getColorStateList(FacebookSdk.getApplicationContext(),R.color.white)
            listrestpickup.backgroundTintList = ContextCompat.getColorStateList(FacebookSdk.getApplicationContext(),R.color.colorPrimary2)
            listrestfinedine.backgroundTintList = ContextCompat.getColorStateList(FacebookSdk.getApplicationContext(),R.color.white)

            textfdl.setTextColor(Color.BLACK)
            textpickupl.setTextColor(Color.WHITE)
            textinql.setTextColor(Color.BLACK)
            getvalues(filter)

        }

        head.setText(""+type)
        cart_badge.text = ""+ sharedHelper!!.cartcount
        (context as DashBoardActivity?)!!.bottom_navigation_main.setVisibility(View.VISIBLE)
        (context as DashBoardActivity?)!!.iv_add.setVisibility(View.VISIBLE)
        getvalues("all")
        (activity as DashBoardActivity?)!!.mBottomSheetBehavior2?.setState(BottomSheetBehavior.STATE_HIDDEN)
        (activity as DashBoardActivity?)!!.mBottomSheetBehavior?.setState(BottomSheetBehavior.STATE_HIDDEN)
        layoutManager=LinearLayoutManager(requireContext(), LinearLayoutManager.HORIZONTAL, false)
        recycler1.layoutManager =layoutManager
        recycler1.adapter = ScrollListAdapter(this)



        if (sharedHelper!!.choose.equals("inqueue")){

            listrestfinedine.backgroundTintList = ContextCompat.getColorStateList(FacebookSdk.getApplicationContext(),R.color.white)
            listrestinqueue.backgroundTintList = ContextCompat.getColorStateList(FacebookSdk.getApplicationContext(),R.color.colorPrimary2)

            listrestpickup.backgroundTintList = ContextCompat.getColorStateList(FacebookSdk.getApplicationContext(),R.color.white)

            textfdl.setTextColor(Color.BLACK)
            textpickupl.setTextColor(Color.BLACK)
            textinql.setTextColor(Color.WHITE)

        }else if (sharedHelper!!.choose.equals("finedine")){
            listrestfinedine.backgroundTintList = ContextCompat.getColorStateList(FacebookSdk.getApplicationContext(),R.color.colorPrimary2)

            listrestinqueue.backgroundTintList = ContextCompat.getColorStateList(FacebookSdk.getApplicationContext(),R.color.white)
            listrestpickup.backgroundTintList = ContextCompat.getColorStateList(FacebookSdk.getApplicationContext(),R.color.white)
            textfdl.setTextColor(Color.WHITE)
            textpickupl.setTextColor(Color.BLACK)
            textinql.setTextColor(Color.BLACK)

        }else{

            listrestinqueue.backgroundTintList = ContextCompat.getColorStateList(FacebookSdk.getApplicationContext(),R.color.white)
            listrestpickup.backgroundTintList = ContextCompat.getColorStateList(FacebookSdk.getApplicationContext(),R.color.colorPrimary2)
            listrestfinedine.backgroundTintList = ContextCompat.getColorStateList(FacebookSdk.getApplicationContext(),R.color.white)

            textfdl.setTextColor(Color.BLACK)
            textpickupl.setTextColor(Color.WHITE)
            textinql.setTextColor(Color.BLACK)
        }

        search.addTextChangedListener(object : TextWatcher{
            override fun afterTextChanged(p0: Editable?) {
                search1 = search.text.toString()
                listcopy.clear()
                if(search.text.toString().equals("")){
                    if(isfilterseleted == true){
                        getvalues(filtertext)
                    }
                    else{
                        getvalues("all")
                    }
                }
                else{
                    if(list.isNullOrEmpty()){

                    }
                    else{
                        for (i in 0 until list!!.size){
                            Log.d("jbhcdv",""+ list!!.size)
                            Log.d("xjvbhbxv",""+(list!![i].name+"=="+search1))
                            if(list!![i].name!!.contains(search1,true)){
                                Log.d("sucess","dsfcsd")
                                Log.d("index",""+i)
                                Log.d("index1",""+list!![i].name)
                                //listcopy!!.addAll(listOf(list!![i]))
                                // listcopy!!.add(list!![i])
                                listcopy!!.add(0,list!![i])
                                //listcopy!!.addAll(i, listOf(list!![i]))
                                Log.d("sixeeeee",""+ listcopy!!.size)
                            }
                            else{

                            }
                        }

                        recycler.layoutManager =
                            GridLayoutManager(
                                context,
                                2
                            )
                        recycler.adapter = SubHomeAdapter(
                            this@SubHomeFragment,
                            requireContext(),
                            listcopy!!
                        )
                    }
                }

            }

            override fun beforeTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {
            }

            override fun onTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {
            }
        })



        activity?.onBackPressedDispatcher?.addCallback(this, object : OnBackPressedCallback(true) {
            override fun handleOnBackPressed() {

                homeFragment = HomeFragment()
                fragmentTransaction =requireActivity().supportFragmentManager.beginTransaction()
                fragmentTransaction.setCustomAnimations(R.anim.screen_in, R.anim.screen_out)
                fragmentTransaction.replace(R.id.container, homeFragment!!)
                fragmentTransaction.addToBackStack(null)
                fragmentTransaction.commit()
            }
        })

    }

    fun getvalues(filter:String){
        DialogUtils.showLoader(requireContext())
        inputText?.let {
            mapViewModel?.getlistresturant(requireContext(), it, cuisinearray,iscuisine,filter)
                ?.observe(viewLifecycleOwner,
                    Observer {
                        DialogUtils.dismissLoader()
                        Log.d("zxcvbnmcv","lklkllkklkl")

                        it?.let {
                            it.error?.let { error ->
                                Log.d("vbvbvbvnb","mnmnnmmnmnmn")

                                if (error) {
                                    Log.d("ghhhghghgghgh","fcgfggfgfg")

                                    it.message?.let { msg ->
                                        UiUtils.showSnack(root, msg)
                                        Toast.makeText(context, msg, Toast.LENGTH_SHORT).show()

                                    }

                                    } else {

                                    Log.d("nbvcxnbv","mnbvnb")
                                    it.message?.let { msg ->
                                       if(msg.equals("No data found")){
                                           Log.d("zasxdcfvg","zxcvbn")
                                           recycler.visibility=View.GONE
                                       }else{
                                           recycler.visibility=View.VISIBLE

                                           it.data?.let { data ->
                                               Log.d("wsedrftgyhujikl","hbxshsaxbhjbdjh")
                                               list = data.listres
                                             /*  recycler.layoutManager =
                                                   GridLayoutManager(
                                                       context,
                                                       2
                                                   )*/
                                               recycler.layoutManager = LinearLayoutManager(requireContext())

                                               recycler.adapter = SubHomeAdapter(
                                                   this,
                                                   requireContext(),
                                                   data.listres
                                               )

/*
                                        requireActivity().recycler_filter.layoutManager =
                                            GridLayoutManager(
                                                context,
                                                4
                                            )
                                        requireActivity().recycler_filter.adapter = FilterAdapter(this,
                                            requireContext(),
                                            data.data2
                                        )*/

                                           }
                                       }

                                    }

                                }
                            }

                        }
                    })
        }
    }

}