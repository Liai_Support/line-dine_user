@file:Suppress("DEPRECATION")

package com.lia.yello.linedine.activity

import android.content.Intent
import android.net.Uri
import android.os.Bundle
import android.os.Handler
import android.os.Looper
import android.util.Log
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.fragment.app.FragmentTransaction
import com.lia.yello.linedine.Session.SharedHelper
import com.lia.yello.linedine.fragment.MyOrderFragment
import kotlinx.android.synthetic.main.activity_splash.*
import android.widget.MediaController
import android.R




class SplashActivity : BaseActivity() {
    private val SPLASH_TIME_OUT:Long = 3000 // 1 sec
    var sharedHelper: SharedHelper? = null
    private lateinit var fragmentTransaction: FragmentTransaction
    private var myOrderFragment: MyOrderFragment? = null
    var fromnotification = false
    var mediaControls: MediaController? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(com.lia.yello.linedine.R.layout.activity_splash)
        sharedHelper = SharedHelper(this)
        sharedHelper?.location = ""
        sharedHelper?.selectedLat = "0.0"
        sharedHelper?.selectedLng = "0.0"





        val mediaController = MediaController(application)
        mediaController.setAnchorView(videoview)
        // set the media controller for video view
        videoview!!.setMediaController(mediaControls)

        // set the absolute path of the video file which is going to be played

        val uri = Uri.parse("android.resource://" + packageName + "/" + com.lia.yello.linedine.R.raw.logo)

        videoview!!.setVideoURI(uri)

        videoview!!.requestFocus()

        // starting the video
        videoview!!.start()

        Log.d("gcgcv1", "" + intent.hasExtra("data"))
        Log.d("gcgcv1", "" + intent.getStringExtra("data"))

        if (intent.hasExtra("data") == true) {
            if (sharedHelper?.loggedIn==true){
                if (intent.getStringExtra("data").equals("inqueue")) {
                    finish()
                    val intent = Intent(this, DashBoardActivity::class.java)
                    intent.putExtra("fromnotification", "tableassigned")
                    startActivity(intent)
                }
            }else{

                startActivity(Intent(this, LoginActivity::class.java))
                finish()
            }

        }else{
            Handler(Looper.getMainLooper()).postDelayed({

                startActivity(Intent(this, ChooseLanguageActivity::class.java))
                finish()

            }, 5000)
        }


        /* if (intent.extras==null){
            Handler(Looper.getMainLooper()).postDelayed({


                startActivity(Intent(this, ChooseLanguageActivity::class.java))
                finish()

            }, 2000)
        }else{
            Log.d("gcgcv1", "" + getIntent().hasExtra("fromnotification"));
            Log.d("gcgcv1", "" + getIntent().getStringExtra("fromnotification"));
            if (intent.hasExtra("fromnotification") && (intent.getStringExtra("fromnotification").equals("tableassigned"))){
                finish();
                Handler(Looper.getMainLooper()).postDelayed({

                    val intent = Intent(this, DashBoardActivity::class.java)
                    intent.putExtra("fromnotification", "tableassigned")
                    startActivity(intent)

                }, 2000)

            }
        }*/




    }


    override fun onNewIntent(intent: Intent?){
        super.onNewIntent(intent)
        if(getIntent().hasExtra("fromnotification") == true){
            if (getIntent().getStringExtra("fromnotification").equals("tableassigned", ignoreCase = true)) {
                finish()
                val intent1 = Intent(this, DashBoardActivity::class.java)
                intent1.putExtra("fromnotification", "tableassigned")
                startActivity(intent1)
            }
        }
    }

 /*  override fun onResume(){
        super.onResume()

        Handler(Looper.getMainLooper()).postDelayed({

            startActivity(Intent(this, ChooseLanguageActivity::class.java))
            finish()

        }, 2000)


    }*/
}