package com.lia.yello.linedine.adapter

import android.content.Context
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.FragmentTransaction
import androidx.recyclerview.widget.RecyclerView
import com.lia.yello.linedine.Models.BookingList
import com.lia.yello.linedine.Models.Menudetail
import com.lia.yello.linedine.R
import com.lia.yello.linedine.Utils.UiUtils
import com.lia.yello.linedine.databinding.CardOrdersBinding
import com.lia.yello.linedine.fragment.*
import java.util.*
import kotlin.collections.ArrayList


class BookingListAdapter(
    var myOrderFragment: MyOrderFragment,
    var context: Context,
    var list: ArrayList<BookingList>?

) :
    RecyclerView.Adapter<BookingListAdapter.HomeHeaderViewHolder>() {

    var id:Int=0
    var price:Double=0.0
    var img:String=""
    var menu_name:String=""
    var splins:String=""
    var quantity:Int=0
    var list2: ArrayList<Menudetail>? = null
    private var inQueueStatusFragment: InQueueStatusFragment? = null

    private lateinit var fragmentTransaction: FragmentTransaction
    private var myCompleteOrderFragment: MyCompleteOrderFragment? = null
    private var fineDineOrderListStatus: FineDineOrderListStatusFragment? = null
    private var fineDineCompleteBookingFragment: FineDineCompleteBookingFragment? = null
    inner class HomeHeaderViewHolder(view: View) : RecyclerView.ViewHolder(view) {
        var binding: CardOrdersBinding = CardOrdersBinding.bind(view)
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): HomeHeaderViewHolder {
        return HomeHeaderViewHolder(
            LayoutInflater.from(context).inflate(
                R.layout.card_orders,
                parent,
                false
            )
        )
    }

    override fun getItemCount(): Int {
        return list!!.size
    }

    override fun onBindViewHolder(holder: HomeHeaderViewHolder, position: Int) {

        holder.binding.time.text = list!![position].timeslot
        holder.binding.orderid.text = "id "+ list!![position].id.toString()
        holder.binding.date.text = list!![position].date
        holder.binding.resName.text = list!![position].resname
        holder.binding.bookingamount.text =  String.format(Locale("en", "US"),"%.2f",list!![position].amount)+ " SAR"

     //   holder.binding.bookingamount.text =list!![position].amount.toString() +" SAR"
        Log.d("asdfghjkl.;/",""+list!![position].amount.toString())

        list!![position].resimg?.let { imageUrl ->
            UiUtils.loadImage(holder.binding.img, imageUrl)
        }


        holder.binding.card.setOnClickListener(View.OnClickListener {
            if (list!![position].status!!.equals("cancelled")||list!![position].status!!.equals("closed")){
                val bundle = Bundle()
                bundle.putInt("orderid",list!![position].id!! )
                bundle.putString("status",list!![position].status!! )
                bundle.putDouble("lat", list!![position].lat!!)
                bundle.putDouble("lng",list!![position].lng!!)
                bundle.putString("resname",list!![position].resname)
                bundle.putString("resimg",list!![position].resimg)
                fineDineCompleteBookingFragment = FineDineCompleteBookingFragment()
                fineDineCompleteBookingFragment!!.arguments = bundle
                fragmentTransaction = myOrderFragment.requireActivity().supportFragmentManager.beginTransaction()
                fragmentTransaction.setCustomAnimations(R.anim.screen_in, R.anim.screen_out)
                fragmentTransaction.replace(R.id.container, fineDineCompleteBookingFragment!!)
                fragmentTransaction.addToBackStack(null)
                fragmentTransaction.commit()

            }else if(list!![position].status!!.equals("booked")||list!![position].status!!.equals("pending")||list!![position].status!!.equals("processing")){
                val bundle = Bundle()
                bundle.putInt("orderid",list!![position].id!! )
                Log.d("sdfghjkl;",""+list!![position].id!!)
                bundle.putString("status",list!![position].status!! )
                bundle.putString("resname",list!![position].resname)
                bundle.putString("resimg",list!![position].resimg)
                bundle.putInt("resid",list!![position].resid!!)
                bundle.putDouble("lat", list!![position].lat!!)
                bundle.putDouble("lng",list!![position].lng!!)
                bundle.putString("paidstatus",list!![position].paidstatus!!)
                Log.d("sxdcfvghjkl",""+list!![position].paidstatus!!)
                bundle.putString("paymenttype",list!![position].paymenttype!!)
                fineDineOrderListStatus = FineDineOrderListStatusFragment()
                fineDineOrderListStatus!!.arguments = bundle
                fragmentTransaction = myOrderFragment.requireActivity().supportFragmentManager.beginTransaction()
                fragmentTransaction.setCustomAnimations(R.anim.screen_in, R.anim.screen_out)
                fragmentTransaction.replace(R.id.container, fineDineOrderListStatus!!)
                fragmentTransaction.addToBackStack(null)
                fragmentTransaction.commit()
            }
            else if (list!![position].status!!.equals("inqueue")){
                val bundle = Bundle()
                bundle.putInt("orderid",list!![position].id!! )
                bundle.putString("status",list!![position].status!!)
                bundle.putString("date",list!![position].date)
                bundle.putInt("resid", list!![position].resid!!)
                Log.d("resssid",""+list!![position].resid!!)
                bundle.putString("resname",list!![position].resname)
                bundle.putString("resimg",list!![position].resimg)
                bundle.putDouble("lat", list!![position].lat!!)
                bundle.putDouble("lng",list!![position].lng!!)
                bundle.putString("phone",list!![position].phonee!!)
                bundle.putString("resstatus",list!![position].res_status!!)
                inQueueStatusFragment = InQueueStatusFragment()
                inQueueStatusFragment!!.arguments = bundle
                fragmentTransaction = myOrderFragment.requireActivity().supportFragmentManager.beginTransaction()
                fragmentTransaction.setCustomAnimations(R.anim.screen_in, R.anim.screen_out)
                fragmentTransaction.replace(R.id.container, inQueueStatusFragment!!)
                fragmentTransaction.addToBackStack(null)
                fragmentTransaction.commit()
            }


        })



    }
}