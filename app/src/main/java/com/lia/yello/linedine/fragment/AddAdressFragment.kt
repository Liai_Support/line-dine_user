package com.lia.yello.linedine.fragment

import android.Manifest
import android.content.pm.PackageManager
import android.location.Location
import android.os.Build
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.*
import androidx.core.app.ActivityCompat
import androidx.core.content.ContextCompat
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentTransaction
import androidx.lifecycle.ViewModelProvider
import com.facebook.FacebookSdk.getApplicationContext
import com.google.android.gms.location.*
import com.google.android.gms.maps.CameraUpdateFactory
import com.google.android.gms.maps.GoogleMap
import com.google.android.gms.maps.OnMapReadyCallback
import com.google.android.gms.maps.SupportMapFragment
import com.google.android.gms.maps.model.LatLng
import com.google.android.gms.maps.model.MarkerOptions
import com.google.android.gms.tasks.Task
import com.google.android.material.bottomsheet.BottomSheetBehavior
import com.google.android.material.snackbar.BaseTransientBottomBar
import com.google.android.material.snackbar.Snackbar
import com.google.android.material.textfield.TextInputLayout
import com.karumi.dexter.Dexter
import com.karumi.dexter.PermissionToken
import com.karumi.dexter.listener.PermissionDeniedResponse
import com.karumi.dexter.listener.PermissionGrantedResponse
import com.karumi.dexter.listener.single.PermissionListener
import com.lia.yello.linedine.R
import com.lia.yello.linedine.Session.SharedHelper
import com.lia.yello.linedine.Utils.Constants
import com.lia.yello.linedine.Utils.DialogUtils
import com.lia.yello.linedine.Utils.NetworkUtils
import com.lia.yello.linedine.Utils.UiUtils
import com.lia.yello.linedine.ViewModels.MapViewModel
import com.lia.yello.linedine.activity.DashBoardActivity
import kotlinx.android.synthetic.main.activity_dash_board.*
import kotlinx.android.synthetic.main.activity_login.*
import kotlinx.android.synthetic.main.carticon.*
import kotlinx.android.synthetic.main.header.*
import java.util.*


class AddAdressFragment : Fragment(),View.OnClickListener {
    var gmap: GoogleMap? = null
    var mapFragment: SupportMapFragment? = null
    private var client: FusedLocationProviderClient? = null

    var latitude: Double? = 0.0
    var longitude: Double? = 0.0
    var address: String? = ""
    var fcity: String? = ""
    var faid: Int? = 0

    var spinner_edit_txt: TextView? =null
    var txt_add_address: TextView? =null
    var txt_home:TextView? = null
    var txt_office:TextView? = null
    var txt_others:TextView? = null
    var addr:TextView? = null
    var addr1:EditText? = null
    var backmap:TextView? = null
    var confirm_btn:Button? = null
    var update:Boolean = false
    var home:Boolean = false
    var office:Boolean = false


    private lateinit var fragmentTransaction: FragmentTransaction
    private var mapFragment1: MapFragment? = null
    private var homeFragment: HomeFragment? = null
    var add_type:String=""
    var addressid:Int=0
    private var mapViewModel: MapViewModel? = null
    var adresstype:String=""
    var edt_others: EditText? = null
    var building_name: EditText? = null
    var door_no: EditText? = null
    var dropDown: RelativeLayout? = null
    public var sharedHelper: SharedHelper? = null

    var lay_others: TextInputLayout? = null

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        var view= inflater.inflate(R.layout.fragment_add_address, container, false)
        mapViewModel = ViewModelProvider(this).get(MapViewModel::class.java)
        sharedHelper = SharedHelper(requireContext())
        (context as DashBoardActivity?)!!.bottom_navigation_main.setVisibility(View.VISIBLE)
        (context as DashBoardActivity?)!!.iv_add.setVisibility(View.VISIBLE)

        val head = view.findViewById<TextView>(R.id.head)
        val cart_badge = view.findViewById<TextView>(R.id.cart_badge)
        val cartframe = view.findViewById<FrameLayout>(R.id.cartframe)
        val back = view.findViewById<ImageView>(R.id.back)

        if (sharedHelper!!.language == "ar"){
            back.rotation= 180F
        }
        if (sharedHelper!!.choose.equals("finedine")||sharedHelper!!.choose.equals("inqueue")|| sharedHelper!!.cartcount == 0){

            //var cartframe: FrameLayout =findViewById(R.id.cartframe)
            cartframe.visibility=View.GONE
        }
        if (!NetworkUtils.isNetworkConnected(requireContext())) {
            UiUtils.showSnack(
                view, "no_internet_connection"
            )
        }
        head.setText(getString(R.string.address))
        cart_badge.text = ""+ sharedHelper!!.cartcount
      //  View view = R.id.txt_save_loc
        back.setOnClickListener(View.OnClickListener {
            requireActivity().supportFragmentManager.popBackStack()
        })
        if (!NetworkUtils.isNetworkConnected(requireContext())){
            UiUtils.showSnack(
                view.findViewById(R.id.content), "no_internet_connection"
            )



        }

        addr = view.findViewById(R.id.txt_save_loc);
        addr1 = view.findViewById(R.id.addr1);
        building_name = view.findViewById(R.id.building_name);
        door_no = view.findViewById(R.id.door_no);


        // for add address drop down id
        backmap = view.findViewById(R.id.gobackmap);
        spinner_edit_txt = view.findViewById(R.id.dropdown_edit_txt);
        txt_home = view.findViewById(R.id.txt_home);
        txt_office = view.findViewById(R.id.txt_office);
        txt_others = view.findViewById(R.id.txt_others);
        txt_add_address =  view.findViewById(R.id.txt_add_address);
        edt_others = view.findViewById(R.id.othertag) ;
        lay_others = view.findViewById(R.id.othertag_lay);
        dropDown= view.findViewById(R.id.dropdown);
        confirm_btn=view.findViewById(R.id.btn_confirm);


        (context as DashBoardActivity?)!!.mBottomSheetBehavior2?.setState(
            BottomSheetBehavior.STATE_HIDDEN
        )

        getaddresslist()

        client = LocationServices.getFusedLocationProviderClient(context!!)
        mapFragment = childFragmentManager.findFragmentById(R.id.google_map) as SupportMapFragment?
        Dexter.withContext(getApplicationContext())
            .withPermission(Manifest.permission.ACCESS_FINE_LOCATION)
            .withListener(object : PermissionListener {
                override fun onPermissionGranted(permissionGrantedResponse: PermissionGrantedResponse?) {
                    getmylocation()
                }

                override fun onPermissionDenied(permissionDeniedResponse: PermissionDeniedResponse?) {}
                override fun onPermissionRationaleShouldBeShown(
                    p0: com.karumi.dexter.listener.PermissionRequest?,
                    permissionToken: PermissionToken?
                ) {
                    permissionToken!!.continuePermissionRequest()
                }
            }).check()

        faid = arguments?.getInt("faid")
        address = arguments?.getString("faddr")!!
        latitude = arguments?.getDouble("flat")
        longitude = arguments?.getDouble("flng")!!
        fcity = arguments?.getString("fcity")!!
        update = arguments?.getBoolean("update")!!
        add_type = arguments?.getString("fatype")!!
        addr1!!.setText("" + address)
        addr1!!.hint = address
        addr!!.setText("" + address)

        if (!add_type.equals("")) {
            spinner_edit_txt!!.text = ""+add_type
            edt_others!!.setText("" + add_type)
        }


        txt_home!!.setOnClickListener(this)
        txt_office!!.setOnClickListener(this)
        txt_others!!.setOnClickListener(this)
        spinner_edit_txt!!.setOnClickListener(this)
        txt_add_address!!.setOnClickListener(this)
        backmap!!.setOnClickListener(this)
        confirm_btn!!.setOnClickListener(this)
        askLocationPermission()

        return view

    }

    fun getmylocation() {
        if (ActivityCompat.checkSelfPermission(
                context!!,
                Manifest.permission.ACCESS_FINE_LOCATION
            ) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(
                context!!, Manifest.permission.ACCESS_COARSE_LOCATION
            ) != PackageManager.PERMISSION_GRANTED
        ) {
            // TODO: Consider calling
            //    ActivityCompat#requestPermissions
            // here to request the missing permissions, and then overriding
            //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
            //                                          int[] grantResults)
            // to handle the case where the user grants the permission. See the documentation
            // for ActivityCompat#requestPermissions for more details.
            return
        }
        val task = client!!.lastLocation
        task.addOnSuccessListener {
            mapFragment!!.getMapAsync { googleMap ->
                val latLng = LatLng(latitude!!, longitude!!)
                val markerOptions = MarkerOptions().position(latLng).title("" + address)
                googleMap.addMarker(markerOptions)
                googleMap.animateCamera(CameraUpdateFactory.newLatLngZoom(latLng, 20f))
                gmap = googleMap
                /* gmap.setOnMapClickListener(new GoogleMap.OnMapClickListener() {
                                    @Override
                                    public void onMapClick(LatLng latLng) {
                                        // creating marker
                                        MarkerOptions markerOptions = new MarkerOptions();

                                        // set marker position
                                        markerOptions.position(latLng);

                                        // set Lattitude & longitude on marker

                                        markerOptions.title(latLng.latitude + ":" + latLng.longitude);
                                        Geocoder geocoder;
                                        List<Address> addresses;
                                        geocoder = new Geocoder(getContext(), Locale.getDefault());

                                        try {
                                            addresses = geocoder.getFromLocation(latLng.latitude, latLng.longitude, 1);
                                            String address = addresses.get(0).getAddressLine(0); // If any additional address line present than only, check with max available address lines by getMaxAddressLineIndex()
                                            String city = addresses.get(0).getLocality();
                                            String state = addresses.get(0).getAdminArea();
                                            String country = addresses.get(0).getCountryName();
                                            String postalCode = addresses.get(0).getPostalCode();
                                            String knownName = addresses.get(0).getFeatureName();
                                            Log.d("kkkkkkk",""+address);
                                            addr.setText(""+address);
                                            faddr = address;
                                            flat = latLng.latitude;
                                            flng = latLng.longitude;
                                            // Here 1 represent max location result to returned, by documents it recommended 1 to 5
                                        } catch (IOException e) {
                                            e.printStackTrace();
                                        }
                                        gmap.clear();
                                        gmap.animateCamera(CameraUpdateFactory.newLatLngZoom(latLng, 17));
                                        gmap.addMarker(markerOptions);
                                    }
                                });*/
            }
        }
    }


    override fun onRequestPermissionsResult(
        requestCode: Int,
        permissions: Array<out String>,
        grantResults: IntArray
    ) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)

        if (requestCode == Constants.Permission.LOCATION_PERMISSION) {

            if (grantResults.size >= 2 && grantResults[0] == PackageManager.PERMISSION_GRANTED && grantResults[1] == PackageManager.PERMISSION_GRANTED) {
                getmylocation()
            }
        }

    }


    private fun askLocationPermission() {

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            if (requireContext().checkSelfPermission(Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && requireContext().checkSelfPermission(
                    Manifest.permission.ACCESS_COARSE_LOCATION
                ) != PackageManager.PERMISSION_GRANTED
            ) {
                requestPermissions(
                    Constants.Permission.LOCATION_PERMISSION_PERMISSON_LIST,
                    Constants.Permission.LOCATION_PERMISSION
                )
                return
            } else {
                getmylocation()
            }
        } else {
            getmylocation()
        }
    }


    /*  private fun locateMyLocation() {
          myLat?.let { lat ->
              myLng?.let { lng ->
                  map?.animateCamera(CameraUpdateFactory.newLatLngZoom(longitude?.let {
                      latitude?.let { it1 ->
                          LatLng(
                              it1,
                              it
                          )
                      }
                  }, 12.0f))
              }
          }
      }*/

    override fun onClick(v: View?) {

        if (v == spinner_edit_txt) {
            if (update == true) {
                // snackbarToast(getString(R.string.youcouldnotchangeaddresstype));
                if (home == true || office == true) {
                    if (home == true && office == true) {
                        txt_home!!.visibility = View.GONE
                        txt_office!!.visibility = View.GONE
                        dropDown!!.visibility = View.VISIBLE
                        spinner_edit_txt!!.visibility = View.GONE
                    }
                    else if(home == true){
                        txt_home!!.visibility = View.GONE
                        dropDown!!.visibility = View.VISIBLE
                        spinner_edit_txt!!.visibility = View.GONE
                    }
                    else if (office == true) {
                        txt_office!!.visibility = View.GONE
                        dropDown!!.visibility = View.VISIBLE
                        spinner_edit_txt!!.visibility = View.GONE
                    }
                } else {
                    dropDown!!.visibility = View.VISIBLE
                    spinner_edit_txt!!.visibility = View.GONE
                }
            } else {
                if (home == true || office == true) {
                    if (home == true && office == true) {
                        txt_home!!.visibility = View.GONE
                        txt_office!!.visibility = View.GONE
                        dropDown!!.visibility = View.VISIBLE
                        spinner_edit_txt!!.visibility = View.GONE
                    }
                    else if(home == true){
                        txt_home!!.visibility = View.GONE
                        dropDown!!.visibility = View.VISIBLE
                        spinner_edit_txt!!.visibility = View.GONE
                    }
                    else if (office == true) {
                        txt_office!!.visibility = View.GONE
                        dropDown!!.visibility = View.VISIBLE
                        spinner_edit_txt!!.visibility = View.GONE
                    }
                } else {
                    dropDown!!.visibility = View.VISIBLE
                    spinner_edit_txt!!.visibility = View.GONE
                }
            }
        }
        else if (v == txt_add_address) {
            dropDown!!.visibility = View.GONE
            spinner_edit_txt!!.visibility = View.VISIBLE
        }
        else if (v == txt_home) {
            txt_add_address!!.setText(R.string.homee)
            spinner_edit_txt!!.setText(R.string.homee)
            dropDown!!.visibility = View.GONE
            spinner_edit_txt!!.visibility = View.VISIBLE
            lay_others!!.visibility = View.GONE
            edt_others!!.setText(R.string.homee)
        }
        else if (v == txt_office) {
            txt_add_address!!.setText(R.string.office)
            spinner_edit_txt!!.setText(R.string.office)
            dropDown!!.visibility = View.GONE
            spinner_edit_txt!!.visibility = View.VISIBLE
            edt_others!!.setText(R.string.office)
            lay_others!!.visibility = View.GONE
        }
        else if (v == txt_others) {
            txt_add_address!!.setText(R.string.others)
            spinner_edit_txt!!.setText(R.string.others)
            edt_others!!.setText("")
            dropDown!!.visibility = View.GONE
            spinner_edit_txt!!.visibility = View.VISIBLE
            lay_others!!.visibility = View.VISIBLE
        }
        else if (v == backmap) {
            if (update==true){
                val bundle = Bundle()
                bundle.putInt("faid", faid!!)
                bundle.putString("faddr", address)
                bundle.putDouble("flat", latitude!!)
                bundle.putDouble("flng", longitude!!)
                bundle.putString("fcity", fcity)
                bundle.putBoolean("update", update)
                bundle.putString("fatype", add_type)
                mapFragment1 = MapFragment()
                mapFragment1!!.arguments=bundle
                fragmentTransaction = requireActivity().supportFragmentManager.beginTransaction()
                fragmentTransaction.setCustomAnimations(R.anim.screen_in, R.anim.screen_out)
                fragmentTransaction.replace(R.id.container, mapFragment1!!)
                fragmentTransaction.addToBackStack(null)
                fragmentTransaction.commit()
            }
            else{
                val bundle = Bundle()
                bundle.putBoolean("update", false)
                mapFragment1 = MapFragment()
                mapFragment1!!.arguments=bundle
                fragmentTransaction = requireActivity().supportFragmentManager.beginTransaction()
                fragmentTransaction.setCustomAnimations(R.anim.screen_in, R.anim.screen_out)
                fragmentTransaction.replace(R.id.container, mapFragment1!!)
                fragmentTransaction.addToBackStack(null)
                fragmentTransaction.commit()
            }


        }
        else if (v==confirm_btn){
            add_type = edt_others!!.text.toString()
            if (add_type.equals("")) {
                snackbarToast(getString(R.string.enterallfields))
            }
            else{
                if (update==true){
                    updateaddress()
                }
                else{
                    addaddress()
                }
            }
        }
    }

    private fun updateaddress(){
        DialogUtils.showLoader(requireContext())
        address?.let {
            latitude?.let { it1 ->
                longitude?.let { it2 ->
                    mapViewModel?.getupdateadress(
                        requireContext(), faid!!,
                        it, it1, it2, add_type, fcity!!
                    )
                        ?.observe(viewLifecycleOwner,
                            androidx.lifecycle.Observer {
                                DialogUtils.dismissLoader()

                                it?.let {
                                    it.error?.let { error ->
                                        if (error) {
                                            it.message?.let { msg ->
                                                // UiUtils.showSnack(root, msg)
                                                snackbarToast(getString(R.string.adressalreadyexits))
                                            }
                                        } else {
                                            it.message?.let { msg ->
                                                Log.d("response", msg)
                                                snackbarToast(getString(R.string.addresssuccessfullyupdated))
                                                homeFragment = HomeFragment()
                                                fragmentTransaction =
                                                    requireActivity().supportFragmentManager.beginTransaction()
                                                fragmentTransaction.setCustomAnimations(
                                                    R.anim.screen_in,
                                                    R.anim.screen_out
                                                )
                                                fragmentTransaction.replace(
                                                    R.id.container,
                                                    homeFragment!!
                                                )
                                                fragmentTransaction.addToBackStack(null)
                                                fragmentTransaction.commit()

                                            }

                                        }
                                    }
                                }

                            })
                }
            }
        }
    }

    private fun addaddress(){
        DialogUtils.showLoader(requireContext())
        address?.let {
            latitude?.let { it1 ->
                longitude?.let { it2 ->
                    mapViewModel?.getaddderss(requireContext(), it, it1, it2, add_type, fcity!!)
                        ?.observe(viewLifecycleOwner,
                            androidx.lifecycle.Observer {
                                DialogUtils.dismissLoader()

                                it?.let {
                                    it.error?.let { error ->
                                        if (error) {
                                            it.message?.let { msg ->
                                                //  UiUtils.showSnack(root, msg)
                                                snackbarToast(msg)

                                            }
                                        } else {
                                            it.message?.let { msg ->
                                                Log.d("response", msg)
                                            //    sharedHelper!!.selectedaddressid = it.data!!.id!!

                                                snackbarToast(getString(R.string.addresssuccessfullyadded))
                                                homeFragment = HomeFragment()
                                                fragmentTransaction =
                                                    requireActivity().supportFragmentManager.beginTransaction()
                                                fragmentTransaction.setCustomAnimations(
                                                    R.anim.screen_in,
                                                    R.anim.screen_out
                                                )
                                                fragmentTransaction.replace(
                                                    R.id.container,
                                                    homeFragment!!
                                                )
                                                fragmentTransaction.addToBackStack(null)
                                                fragmentTransaction.commit()
                                            }

                                        }
                                    }
                                }
                            })
                }
            }
        }
    }



    private fun snackbarToast(msg: String) {
        var snackbar: Snackbar
        Snackbar.make(view!!, msg, Snackbar.LENGTH_SHORT)
            .setTextColor(ContextCompat.getColor(context!!, R.color.design_default_color_error))
            .setBackgroundTint(ContextCompat.getColor(context!!, R.color.white))
            .setDuration(BaseTransientBottomBar.LENGTH_LONG)
            .setAnimationMode(BaseTransientBottomBar.ANIMATION_MODE_SLIDE)
            .show()
    }

    public fun getaddresslist(){
        DialogUtils.showLoader(requireContext())

        mapViewModel?.getlistadress(requireContext())
            ?.observe(viewLifecycleOwner,
                androidx.lifecycle.Observer {
                    DialogUtils.dismissLoader()

                    it?.let {
                        it.error?.let { error ->
                            if (error) {
                                it.message?.let { msg ->
                                    // UiUtils.showSnack(root, msg)
                                    // snackbarToast(msg)
                                }
                            } else {
                                it.data?.let { data ->
                                    for (i in 0 until data.listaddrs!!.size) {
                                        Log.d("gvggv",""+data.listaddrs!![i].type)
                                        if (data.listaddrs!![i].type.equals(
                                                getString(R.string.homee),
                                                true
                                            )
                                        ) {
                                            Log.d("gcgfvggv11",""+data.listaddrs!![i].type)

                                            home = true
                                        } else if (data.listaddrs!![i].type.equals(
                                                getString(R.string.office),
                                                true
                                            )
                                        ) {
                                            Log.d("gcgfvggv22",""+data.listaddrs!![i].type)

                                            office = true
                                        }
                                    }
                                }
                            }
                        }

                    }
                })
    }


}





