package com.lia.yello.linedine.fragment

import android.annotation.SuppressLint
import android.app.AlertDialog
import android.os.Bundle
import android.text.Editable
import android.text.TextWatcher
import android.util.Log
import android.view.View
import android.widget.*
import androidx.core.content.ContextCompat
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentTransaction
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.facebook.FacebookSdk.getApplicationContext
import com.lia.yello.linedine.R
import com.lia.yello.linedine.Session.SharedHelper
import com.lia.yello.linedine.Utils.DialogUtils
import com.lia.yello.linedine.Utils.NetworkUtils
import com.lia.yello.linedine.Utils.UiUtils
import com.lia.yello.linedine.ViewModels.MapViewModel
import com.lia.yello.linedine.adapter.OrderProcessingAdapter
import com.lia.yello.linedine.adapter.OrderProcessingMenuAdapter
import com.lia.yello.linedine.databinding.FragmentMyActiveOrdersBinding
import kotlinx.android.synthetic.main.carticon.*
import kotlinx.android.synthetic.main.fragment_fine_dine_order_list_status.*
import kotlinx.android.synthetic.main.fragment_order_processing.*
import kotlinx.android.synthetic.main.fragment_order_processing.menushowrecycler
import kotlinx.android.synthetic.main.fragment_profile.root
import kotlinx.android.synthetic.main.fragment_suborders.*
import kotlinx.android.synthetic.main.header.*
import kotlinx.android.synthetic.main.popup_cancelorder.*
import kotlinx.android.synthetic.main.popup_orderprocess.*

import java.util.*

class MyActiveOrderFragment : Fragment(R.layout.fragment_order_processing),View.OnClickListener {

    var binding: FragmentMyActiveOrdersBinding? = null

    private var mapViewModel: MapViewModel? = null
    var status: String = ""
    lateinit var adapter: RecyclerView.Adapter<RecyclerView.ViewHolder>
    lateinit var layoutManager: RecyclerView.LayoutManager
    var orderid:Int=0

    var paymenttype: String = ""
    var subtotalcount:Double=0.0
    var subtotalcount_total:Double=0.0
    var vattocal:Double=0.0
    var finaltotal:Double=0.0
    var discoumtamtt:Double=0.0
    public var sharedHelper: SharedHelper? = null
    var descrip:String=""
    var cardpaymentorderid:String=""

    private lateinit var fragmentTransaction: FragmentTransaction
    private var myOrderFragment: MyOrderFragment? = null
    private var homeFragment: HomeFragment? = null
    @SuppressLint("NewApi")
    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        binding = DataBindingUtil.findBinding(view)
        sharedHelper = SharedHelper(requireContext())

        mapViewModel = ViewModelProvider(this).get(MapViewModel::class.java)
        val head = view.findViewById<TextView>(R.id.head)
        cart_badge.text = ""+ sharedHelper!!.cartcount
        if (sharedHelper!!.language == "ar"){
            back.rotation= 180F
        }
        // head.setText("My Orders")
        head.setText(getString(R.string.myactiveorders))

        back.setOnClickListener(View.OnClickListener {
            requireActivity().supportFragmentManager.popBackStack()
        })
        if (!NetworkUtils.isNetworkConnected(requireContext())) {
            UiUtils.showSnack(
                view, "no_internet_connection"
            )
        }
        orderid = arguments?.getInt("orderid")!!
        status = arguments?.getString("status")!!
        paymenttype = arguments?.getString("paymentype")!!


        Log.d("asdfghjkl;'", "" + orderid)
        Log.d("asdfghjkl;'", "" + status)
        Log.d("paymenttype", "" + paymenttype)

        /* if(status=="pending"|| paymenttype == "card" || paymenttype == "wallet"){
             cancel_order.visibility=View.VISIBLE


         }else if(status=="pending" || paymenttype == "wallet" || paymenttype == "card"){
             cancel_order.visibility=View.VISIBLE
         }else{
             cancel_order.visibility=View.GONE

         }*/
        //  getpending()

        if (status.equals("confirmed")){
            getpending("confirmed")
            gif11.visibility=View.VISIBLE
            r1.visibility=View.VISIBLE
            r11.visibility=View.GONE
            // r11.setText(getString(R.string.confirmed))

            //gif11.visibility=View.VISIBLE

            // gif.backgroundTintList=ContextCompat.getColorStateList(getApplicationContext(),R.color.white)
            view1.backgroundTintList = ContextCompat.getColorStateList(getApplicationContext(),
                R.color.colorPrimary)
            view2.backgroundTintList = ContextCompat.getColorStateList(getApplicationContext(),
                R.color.black)

            /* r2.setTextColor(ContextCompat.getColorStateList(getApplicationContext(), R.color.black))
             r3.setTextColor(ContextCompat.getColorStateList(getApplicationContext(), R.color.black))
             r2.compoundDrawableTintList = ContextCompat.getColorStateList(getApplicationContext(),
                 R.color.black)
             r3.compoundDrawableTintList = ContextCompat.getColorStateList(getApplicationContext(),
                 R.color.black)*/
            cancel_order.visibility=View.VISIBLE

            /* r2.imageTintList = ContextCompat.getColorStateList(getApplicationContext(), R.color.black)
             r3.imageTintList = ContextCompat.getColorStateList(getApplicationContext(), R.color.black)*/
        }  else if (status.equals("pending")){
            getpending("pending")
            gif11.visibility=View.GONE
            r11.setText(getString(R.string.pending))
            r11.visibility=View.VISIBLE
            // gif1.visibility=View.GONE

            // gif.backgroundTintList=ContextCompat.getColorStateList(getApplicationContext(),R.color.white)
            view1.backgroundTintList = ContextCompat.getColorStateList(getApplicationContext(),
                R.color.grey2)
            view2.backgroundTintList = ContextCompat.getColorStateList(getApplicationContext(),
                R.color.grey2)
            cancel_order.visibility=View.VISIBLE

            r2.setTextColor(ContextCompat.getColorStateList(getApplicationContext(), R.color.black))
            r1.setTextColor(ContextCompat.getColorStateList(getApplicationContext(), R.color.black))
            r3.setTextColor(ContextCompat.getColorStateList(getApplicationContext(), R.color.black))
            r2.compoundDrawableTintList = ContextCompat.getColorStateList(getApplicationContext(),
                R.color.black)
            r3.compoundDrawableTintList = ContextCompat.getColorStateList(getApplicationContext(),
                R.color.black)

        }
        else if (status.equals("process")){
            getpending("process")
            gif11.visibility=View.VISIBLE
            r1.visibility=View.VISIBLE
            r11.visibility=View.GONE
            // gif1.visibility=View.VISIBLE

            gifr2.visibility=View.VISIBLE
            r2.visibility=View.GONE
            //   gif2.visibility=View.VISIBLE
            r22.visibility=View.VISIBLE

            view1.backgroundTintList = ContextCompat.getColorStateList(getApplicationContext(),
                R.color.colorPrimary)
            view2.backgroundTintList = ContextCompat.getColorStateList(getApplicationContext(),
                R.color.black)
            r2.setTextColor(ContextCompat.getColorStateList(getApplicationContext(),
                R.color.colorPrimary))
            r2.compoundDrawableTintList = ContextCompat.getColorStateList(getApplicationContext(),
                R.color.colorPrimary)
            r3.setTextColor(ContextCompat.getColorStateList(getApplicationContext(), R.color.black))
            r3.compoundDrawableTintList = ContextCompat.getColorStateList(getApplicationContext(),
                R.color.black)
            cancel_order.visibility=View.GONE

        }
        else if(status.equals("ready_to_pickup")){
            getpending("delivered")
            gifr3.visibility=View.VISIBLE
            r33.visibility=View.VISIBLE
            r3.visibility=View.GONE
            // gif3.visibility=View.VISIBLE


/*            gif11.visibility=View.GONE
            r11.visibility=View.VISIBLE
          //  gif1.visibility=View.VISIBLE

            gifr2.visibility=View.GONE
           // gif2.visibility=View.VISIBLE
            r22.visibility=View.VISIBLE
            */

            // sat


            gif11.visibility=View.VISIBLE
            r1.visibility=View.VISIBLE
            r11.visibility=View.GONE
            // gif1.visibility=View.VISIBLE

            gifr2.visibility=View.VISIBLE
            r2.visibility=View.GONE
            //   gif2.visibility=View.VISIBLE
            r22.visibility=View.VISIBLE



            view1.backgroundTintList = ContextCompat.getColorStateList(getApplicationContext(),
                R.color.colorPrimary)
            view2.backgroundTintList = ContextCompat.getColorStateList(getApplicationContext(),
                R.color.colorPrimary)
            r2.setTextColor(ContextCompat.getColorStateList(getApplicationContext(),
                R.color.colorPrimary))
            r2.compoundDrawableTintList = ContextCompat.getColorStateList(getApplicationContext(),
                R.color.colorPrimary)
            r3.setTextColor(ContextCompat.getColorStateList(getApplicationContext(),
                R.color.colorPrimary))
            r3.compoundDrawableTintList = ContextCompat.getColorStateList(getApplicationContext(),
                R.color.colorPrimary)
            cancel_order.visibility=View.GONE


        }
        else if (status.equals("cancelled")){
            getpending("cancelled")
            head.setText(getString(R.string.mycompleteorders))


            cancel_order.visibility=View.GONE
            relative_status.visibility=View.GONE


        }
        cancel_order.setOnClickListener(this)
        popup_cancel_orderpickup.setOnClickListener(this)
        popup_cancel_order_close.setOnClickListener(this)
        poporder_process_close.setOnClickListener(this)
        popup_ok.setOnClickListener(this)
        radioGroup.setOnClickListener(this)
        radio1.setOnClickListener(this)
        radio2.setOnClickListener(this)
        radio3.setOnClickListener(this)
        others.setOnClickListener(this)


        if (sharedHelper!!.choose.equals("finedine")||sharedHelper!!.choose.equals("inqueue")|| sharedHelper!!.cartcount == 0){

            cartframe.visibility=View.GONE
        }



    }


    private fun getpending(s: String) {
        DialogUtils.showLoader(requireContext())
        mapViewModel?.getplaceorderlist(requireContext(), s)
            ?.observe(viewLifecycleOwner,
                Observer {
                    DialogUtils.dismissLoader()

                    it?.let {
                        it.error?.let { error ->
                            if (error) {
                                it.message?.let { msg ->
                                    UiUtils.showSnack(root, msg)
                                }
                            } else {
                                it.data?.let { data ->

                                    for (i in 0 until data!!.size) {
                                        if (data[i].id!! == orderid) {
                                            if (data[i].discount_amount=="0"){
                                                discountorder_list.visibility=View.GONE
                                            }else{

                                                discoumtamtt=data[i].discount_amount!!.toDouble()
                                                discountorder_list.visibility=View.VISIBLE

                                                disamt.text =" - "+String.format(Locale("en", "US"),"%.2f", data[i].discount_amount!!.toDouble())+ " SAR"

                                            }


                                            subtotal1.text =String.format(Locale("en", "US"),"%.2f", data[i].subtotal)+ " SAR"
                                            total1.text =String.format(Locale("en", "US"),"%.2f", data[i].amount)+ " SAR"
                                            finaltotal=data[i].amount!!.toDouble()



                                            Log.d("qwedrfghjnm,",
                                                "" + data[i].subtotal.toString() + " SAR")
                                            menushowrecycler.layoutManager =
                                                LinearLayoutManager(context)
                                            menushowrecycler.adapter =
                                                OrderProcessingMenuAdapter(
                                                    this,
                                                    requireContext(),
                                                    data[i].menudetails
                                                )
                                            cardpaymentorderid= data[i].transactionid!!
                                            orderidd.text =
                                                getString(R.string.orderid) + " : " + data[i].id.toString()
                                            subtotalcount=0.0
                                            subtotalcount_total=0.0
                                            vattocal=data[i].vat!!
                                            vat1.text =String.format(Locale("en", "US"),"%.2f", data[i].vat)+ " SAR"



                                            orderprocessing_recycler.layoutManager =
                                                LinearLayoutManager(context)
                                            orderprocessing_recycler.adapter =
                                                OrderProcessingAdapter(
                                                    this,
                                                    requireContext(),
                                                    data[i]

                                                )


                                        }

                                    }


                                }
                            }
                        }

                    }
                })
    }

    override fun onClick(v: View?) {
        if (v==cancel_order){
            if (status=="pending"  || paymenttype == "wallet" || paymenttype == "card"){

                linear_cancel_order.visibility=View.VISIBLE
            }
            else if (status=="confirmed" || paymenttype == "wallet" || paymenttype == "card"){
                cancel_order.visibility=View.VISIBLE
                linear_cancel_order.visibility=View.VISIBLE

                //  popup_orderprocess.visibility=View.VISIBLE
            }
            else if(status=="readytopickup"){
                cancel_order.visibility=View.GONE
                popup_orderprocess.visibility=View.VISIBLE
            } else if(status=="process"){
                cancel_order.visibility=View.GONE
                popup_orderprocess.visibility=View.VISIBLE
            }
        }
        else if (v==popup_cancel_orderpickup){

            if(paymenttype.equals("wallet") || paymenttype.equals("card")){

                if (status.equals("pending")){
                    linear_cancel_order.visibility=View.GONE

                    getcancelorder(paymenttype,finaltotal)

                }else if (status.equals("confirmed")){

                    val builder = AlertDialog.Builder(context)
                    builder.setTitle(R.string.alert)
                    builder.setMessage(R.string.fivepercentcancellationfeewillchege)
                    builder.setCancelable(true)
                    builder.setPositiveButton(android.R.string.yes) { dialog, which ->

                        linear_cancel_order.visibility=View.GONE
                        Log.d("wertyuiop",""+finaltotal)

                        val refundamt= finaltotal*5/100.0f.toDouble()
                        finaltotal -= refundamt

                        Log.d("wertyuiop",""+finaltotal)
                        finaltotal =String.format(Locale("en", "US"),"%.2f", finaltotal).toDouble()

                        getcancelorder(paymenttype,finaltotal)

                    }

                    builder.setNegativeButton(android.R.string.no) { dialog, which ->
                        builder.setCancelable(true)
                    }
                    builder.show()





                }

            }



        }
        else if (v==popup_cancel_order_close){
            linear_cancel_order.visibility=View.GONE
        }
        else if (v==poporder_process_close){
            popup_orderprocess.visibility=View.GONE
        }
        else if (v==popup_ok){
            popup_orderprocess.visibility=View.GONE
        }else if (v==others){
            Log.d("sdfghjkl","zxcvbgnjm,k.")
            description_edittxt.visibility=View.VISIBLE

            description_edittxt.addTextChangedListener(object : TextWatcher {
                override fun beforeTextChanged(s: CharSequence, start: Int, count: Int, after: Int) {

                }

                override fun onTextChanged(s: CharSequence, start: Int, before: Int, count: Int) {
                }

                override fun afterTextChanged(s: Editable) {
                    descrip=description_edittxt.text.toString()
                    Log.d("sdfghjkl",""+descrip)
                }
            })




        }
        else if (v==radio1){
            descrip="Delivery time is too long"
        }
        else if (v==radio2){
            descrip = "Duplicate order"


        }
        else if (v==radio3){
            descrip = "Change of mind/order"

        }

    }

    private fun getcancelorder(s:String,refundamt:Double){
        DialogUtils.showLoader(requireContext())

        mapViewModel?.getcancelorder(requireContext(), orderid,descrip,s,refundamt)
            ?.observe(viewLifecycleOwner,
                Observer {
                    DialogUtils.dismissLoader()

                    it?.let {
                        it.error?.let { error ->
                            if (error) {
                                it.message?.let { msg ->
                                    UiUtils.showSnack(root, msg)

                                }
                            } else {
                                it.message?.let { msg ->
                                    UiUtils.showSnack(root, msg)
                                    if(status.equals("pending") && paymenttype.equals("card")){
                                        refundamouunt(finaltotal,cardpaymentorderid)


                                    }else if (status.equals("confirmed") && paymenttype.equals("card")){
                                        refundamouunt(finaltotal,cardpaymentorderid)
                                    }
                                    else if (status.equals("confirmed") && paymenttype.equals("wallet")){
                                        homeFragment = HomeFragment()
                                        fragmentTransaction =
                                            requireActivity().supportFragmentManager.beginTransaction()
                                        fragmentTransaction.setCustomAnimations(R.anim.screen_in,
                                            R.anim.screen_out)
                                        fragmentTransaction.replace(R.id.container, homeFragment!!)
                                        fragmentTransaction.commit()
                                        //  refundamouunt(finaltotal,cardpaymentorderid)
                                    }else{
                                        homeFragment = HomeFragment()
                                        fragmentTransaction =
                                            requireActivity().supportFragmentManager.beginTransaction()
                                        fragmentTransaction.setCustomAnimations(R.anim.screen_in,
                                            R.anim.screen_out)
                                        fragmentTransaction.replace(R.id.container, homeFragment!!)
                                        fragmentTransaction.commit()
                                    }


                                }
                            }
                        }

                    }
                })
    }



    private fun refundamouunt(d: Double, orderid: String) {
        // DialogUtils.showLoader(requireContext())

        mapViewModel?.refundpayment(requireContext(), d,orderid)
            ?.observe(viewLifecycleOwner,
                Observer {
                    DialogUtils.dismissLoader()
                    it?.let {
                        it.rcode?.let { rcode ->
                            if (rcode!=0) {
                                it.message?.let { msg ->
                                    UiUtils.showSnack(root, msg)

                                }
                            }
                            else {
                                it.message?.let { msg ->
                                    UiUtils.showSnack(root, msg)
                                    Log.d("ertyul;",""+msg)
                                    homeFragment = HomeFragment()
                                    fragmentTransaction =
                                        requireActivity().supportFragmentManager.beginTransaction()
                                    fragmentTransaction.setCustomAnimations(R.anim.screen_in,
                                        R.anim.screen_out)
                                    fragmentTransaction.replace(R.id.container, homeFragment!!)
                                    fragmentTransaction.commit()

                                }
                            }
                        }

                    }
                })

    }


}

