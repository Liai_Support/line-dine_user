package com.lia.yello.linedine.fragment

import android.annotation.SuppressLint
import android.content.Intent
import android.content.res.ColorStateList
import android.graphics.Color
import android.os.Bundle
import android.util.Log
import android.view.View
import android.widget.TextView
import androidx.activity.OnBackPressedCallback
import androidx.core.content.ContextCompat
import androidx.core.view.GravityCompat
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentTransaction
import com.lia.yello.linedine.R
import com.lia.yello.linedine.Utils.UiUtils
import com.lia.yello.linedine.ViewModels.MapViewModel
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.facebook.FacebookSdk
import com.facebook.FacebookSdk.getApplicationContext
import com.google.android.material.bottomsheet.BottomSheetBehavior
import com.lia.yello.linedine.Session.SharedHelper
import com.lia.yello.linedine.Utils.DialogUtils
import com.lia.yello.linedine.Utils.NetworkUtils
import com.lia.yello.linedine.activity.DashBoardActivity
import com.lia.yello.linedine.activity.OnBoardActivity
import com.lia.yello.linedine.adapter.*
import com.lia.yello.linedine.databinding.*
import kotlinx.android.synthetic.main.activity_dash_board.*
import kotlinx.android.synthetic.main.carticon.*
import kotlinx.android.synthetic.main.fragment_fine_dine2.*
import kotlinx.android.synthetic.main.fragment_orders.*
import kotlinx.android.synthetic.main.fragment_subhome.*
import kotlinx.android.synthetic.main.header.*

class MyOrderFragment : Fragment(R.layout.fragment_orders) {
    var binding: FragmentOrdersBinding? = null
    private var mapViewModel: MapViewModel? = null
    var status:String =""
    public var sharedHelper: SharedHelper? = null
    var bookingid:Int=0
    lateinit var adapter: RecyclerView.Adapter<RecyclerView.ViewHolder>
    lateinit var layoutManager: RecyclerView.LayoutManager
    var homeFragment:HomeFragment?=null
    private lateinit var fragmentTransaction: FragmentTransaction
    var total1:Double=0.0
    var totalfinal:Double=0.0
    @SuppressLint("ResourceType")
    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        binding = DataBindingUtil.findBinding(view)
        sharedHelper = SharedHelper(requireContext())
        mapViewModel = ViewModelProvider(this).get(MapViewModel::class.java)
        val head = view.findViewById<TextView>(R.id.head)
        head.setText(getString(R.string.myorders))
        cart_badge.text = ""+ sharedHelper!!.cartcount


        if (sharedHelper!!.language == "ar"){
            back.rotation= 180F

        }

        if (!NetworkUtils.isNetworkConnected(requireContext())) {
            UiUtils.showSnack(
                view, "no_internet_connection"
            )
        }
        back.setOnClickListener(View.OnClickListener {

            /*if (sharedHelper!!.choose.equals("inqueue")){
                val i = Intent(activity, DashBoardActivity::class.java)
                startActivity(i)
            }
            else if (sharedHelper!!.choose.equals("finedine")){
                val i = Intent(activity, DashBoardActivity::class.java)
                startActivity(i)
            }
            else if (sharedHelper!!.choose.equals("pickup")){
                val i = Intent(activity, DashBoardActivity::class.java)
                startActivity(i)
            }*/

            homeFragment = HomeFragment()
            fragmentTransaction =
                requireActivity().supportFragmentManager.beginTransaction()
            fragmentTransaction.setCustomAnimations(R.anim.screen_in, R.anim.screen_out)
            fragmentTransaction.replace(R.id.container, homeFragment!!)
            fragmentTransaction.addToBackStack(null)
            fragmentTransaction.commit()
        })

        Log.d("sharedhelper",""+sharedHelper!!.choose)

        activity?.onBackPressedDispatcher?.addCallback(this, object : OnBackPressedCallback(true) {
            override fun handleOnBackPressed() {
                isEnabled = false
            /*    if (sharedHelper!!.choose.equals("inqueue")){
                    val i = Intent(activity, DashBoardActivity::class.java)
                    startActivity(i)
                }
                else if (sharedHelper!!.choose.equals("finedine")){
                    val i = Intent(activity, DashBoardActivity::class.java)
                    startActivity(i)
                }
                else if (sharedHelper!!.choose.equals("pickup")){
                    val i = Intent(activity, DashBoardActivity::class.java)
                    startActivity(i)
                }*/

                homeFragment = HomeFragment()
                fragmentTransaction =
                    requireActivity().supportFragmentManager.beginTransaction()
                fragmentTransaction.setCustomAnimations(R.anim.screen_in, R.anim.screen_out)
                fragmentTransaction.replace(R.id.container, homeFragment!!)
                fragmentTransaction.addToBackStack(null)
                fragmentTransaction.commit()
            }
        })


        Log.d("sharedhelper",""+ sharedHelper!!.choose)
        if (sharedHelper!!.choose.equals("finedine")){


            cardfinedine.backgroundTintList = ContextCompat.getColorStateList(getApplicationContext(),R.color.colorPrimary)
            cardpickup.backgroundTintList = ContextCompat.getColorStateList(getApplicationContext(),R.color.white)
            cardinqueue.backgroundTintList = ContextCompat.getColorStateList(getApplicationContext(),R.color.white)
           /* linear_finedine.background=requireContext().getDrawable(R.drawable.border_layout)
            linear_pickup.background=requireContext().getDrawable(R.drawable.border_layoutwhite)
            linear_inqueue.background=requireContext().getDrawable(R.drawable.border_layoutwhite)*/
            linear_active_history.visibility=View.VISIBLE
            myorderfinedine.setTextColor(Color.WHITE)
            getbookinglist("active")

        }
        else if (sharedHelper!!.choose.equals("pickup")){
            cardfinedine.backgroundTintList = ContextCompat.getColorStateList(getApplicationContext(),R.color.white)
            cardpickup.backgroundTintList = ContextCompat.getColorStateList(getApplicationContext(),R.color.colorPrimary)
            cardinqueue.backgroundTintList = ContextCompat.getColorStateList(getApplicationContext(),R.color.white)
/*            linear_pickup.backgroundTintList = ContextCompat.getColorStateList(getApplicationContext(),R.color.colorPrimary)
            linear_finedine.background=requireContext().getDrawable(R.drawable.border_layoutwhite)
            linear_pickup.background=requireContext().getDrawable(R.drawable.border_layout)
            linear_inqueue.background=requireContext().getDrawable(R.drawable.border_layoutwhite)*/
            linear_active_history.visibility=View.VISIBLE
            myoderpickup.setTextColor(Color.WHITE)
            getpending("pending")

        }
        else if (sharedHelper!!.choose.equals("inqueue")){

            cardfinedine.backgroundTintList = ContextCompat.getColorStateList(getApplicationContext(),R.color.white)
            cardinqueue.backgroundTintList = ContextCompat.getColorStateList(getApplicationContext(),R.color.colorPrimary)
            cardpickup.backgroundTintList = ContextCompat.getColorStateList(getApplicationContext(),R.color.white)
/*            linear_pickup.backgroundTintList = ContextCompat.getColorStateList(getApplicationContext(),R.color.colorPrimary)
            linear_finedine.background=requireContext().getDrawable(R.drawable.border_layoutwhite)
            linear_pickup.background=requireContext().getDrawable(R.drawable.border_layout)
            linear_inqueue.background=requireContext().getDrawable(R.drawable.border_layoutwhite)*/
            myorderinqueue.setTextColor(Color.WHITE)
            linear_active_history.visibility=View.GONE
            //  getbookinglistinqueue("active")
            getbookinglist("inqueue")

        }

  /*      history.backgroundTintList = ContextCompat.getColorStateList(getApplicationContext(),R.color.grey)
        history.setBackgroundResource(R.drawable.rectanglegreen)
        history.setTextColor(Color.BLACK)
        active.setBackgroundResource(R.drawable.rectanglegreen2)
        active.backgroundTintList = ContextCompat.getColorStateList(getApplicationContext(),R.color.green)
        active.setTextColor(Color.WHITE)
        status="pending"
*/
        active.setOnClickListener(View.OnClickListener {

                if (sharedHelper!!.choose.equals("pickup")){

                    if (sharedHelper!!.language.equals("ar")){
                        Log.d("sharedhelper",""+sharedHelper!!.choose)
                        history.backgroundTintList = ContextCompat.getColorStateList(getApplicationContext(),R.color.grey)
                        active.backgroundTintList = ContextCompat.getColorStateList(getApplicationContext(),R.color.colorPrimary)
                       history.setBackgroundResource(R.drawable.rectanglegreen2)
                        history.setTextColor(Color.BLACK)
                        active.setBackgroundResource(R.drawable.rectanglegreen)
                        active.setTextColor(Color.WHITE)

                        status="pending"
                        getpending(status)

                    }else{
                        Log.d("sharedhelper",""+sharedHelper!!.choose)
                        history.backgroundTintList = ContextCompat.getColorStateList(getApplicationContext(),R.color.grey)
                        active.backgroundTintList = ContextCompat.getColorStateList(getApplicationContext(),R.color.colorPrimary)
                        history.setBackgroundResource(R.drawable.rectanglegreen)
                        history.setTextColor(Color.BLACK)
                        active.setBackgroundResource(R.drawable.rectanglegreen2)
                        active.setTextColor(Color.WHITE)
                        status="pending"
                        getpending(status)
                    }

                }
            else if (sharedHelper!!.choose.equals("finedine")){


                if (sharedHelper!!.language.equals("ar")){
                    Log.d("aedcvfghbhnjm","awedfgtyghyujk")
                    Log.d("sharedhelper",""+sharedHelper!!.choose)

                    history.backgroundTintList = ContextCompat.getColorStateList(getApplicationContext(),R.color.grey)
                    history.setBackgroundResource(R.drawable.rectanglegreen2)
                    history.setTextColor(Color.BLACK)
                    active.setBackgroundResource(R.drawable.rectanglegreen)
                    active.setTextColor(Color.WHITE)
                    active.backgroundTintList = ContextCompat.getColorStateList(getApplicationContext(),R.color.colorPrimary)
                    status="active"
                    getbookinglist(status)
                }else{
                    Log.d("aedcvfghbhnjm","awedfgtyghyujk")
                    Log.d("sharedhelper",""+sharedHelper!!.choose)

                    history.backgroundTintList = ContextCompat.getColorStateList(getApplicationContext(),R.color.grey)
                    history.setBackgroundResource(R.drawable.rectanglegreen)
                    history.setTextColor(Color.BLACK)
                    active.setBackgroundResource(R.drawable.rectanglegreen2)
                    active.setTextColor(Color.WHITE)
                    active.backgroundTintList = ContextCompat.getColorStateList(getApplicationContext(),R.color.colorPrimary)
                    status="active"
                    getbookinglist(status)
                }


                }
        })

        history.setOnClickListener(View.OnClickListener {

            if (sharedHelper!!.choose.equals("pickup")){
                if (sharedHelper!!.language.equals("ar")){
                    Log.d("sharedhelper",""+sharedHelper!!.choose)
                    active.backgroundTintList = ContextCompat.getColorStateList(getApplicationContext(),R.color.grey)
                    active.setTextColor(Color.BLACK)
                    active.setBackgroundResource(R.drawable.rectanglegreen)
                    history.setBackgroundResource(R.drawable.rectanglegreen2);
                    history.setTextColor(Color.WHITE)
                    history.backgroundTintList = ContextCompat.getColorStateList(getApplicationContext(),R.color.colorPrimary)
                    status="history"
                    getpending(status)

                }else{
                    Log.d("sharedhelper",""+sharedHelper!!.choose)
                    active.backgroundTintList = ContextCompat.getColorStateList(getApplicationContext(),R.color.grey)
                    active.setTextColor(Color.BLACK)
                    active.setBackgroundResource(R.drawable.rectanglegreen2)
                    history.setBackgroundResource(R.drawable.rectanglegreen);
                    history.setTextColor(Color.WHITE)
                    history.backgroundTintList = ContextCompat.getColorStateList(getApplicationContext(),R.color.colorPrimary)
                    status="history"
                    getpending(status)
                }

            }
            else if (sharedHelper!!.choose.equals("finedine")){

                if (sharedHelper!!.language.equals("ar")){
                    Log.d("sharedhelper",""+sharedHelper!!.choose)
                    active.backgroundTintList = ContextCompat.getColorStateList(getApplicationContext(),R.color.grey)
                    active.setTextColor(Color.BLACK)
                    active.setBackgroundResource(R.drawable.rectanglegreen)
                    history.setBackgroundResource(R.drawable.rectanglegreen2);
                    history.setTextColor(Color.WHITE)
                    history.backgroundTintList = ContextCompat.getColorStateList(getApplicationContext(),R.color.colorPrimary)
                    status="history"
                    getbookinglist(status)
                }else{
                    Log.d("sharedhelper",""+sharedHelper!!.choose)
                    active.backgroundTintList = ContextCompat.getColorStateList(getApplicationContext(),R.color.grey)
                    active.setTextColor(Color.BLACK)
                    active.setBackgroundResource(R.drawable.rectanglegreen2)
                    history.setBackgroundResource(R.drawable.rectanglegreen);
                    history.setTextColor(Color.WHITE)
                    history.backgroundTintList = ContextCompat.getColorStateList(getApplicationContext(),R.color.colorPrimary)
                    status="history"
                    getbookinglist(status)
                }


            }



        })

        linear_pickup.setOnClickListener(View.OnClickListener {

            if (sharedHelper!!.language.equals("ar")){



                if (sharedHelper!!.choose.equals("finedine")||sharedHelper!!.choose.equals("inqueue")|| sharedHelper!!.cartcount == 0){

                    //var cartframe: FrameLayout =findViewById(R.id.cartframe)
                    cartframe.visibility=View.GONE
                    sharedHelper!!.choose="pickup"
                    Log.d("sharedhelper",""+sharedHelper!!.choose)

                    linear_active_history.visibility=View.VISIBLE
                    cartframe.visibility=View.VISIBLE

                    cardfinedine.backgroundTintList = ContextCompat.getColorStateList(getApplicationContext(),R.color.white)
                    cardpickup.backgroundTintList = ContextCompat.getColorStateList(getApplicationContext(),R.color.colorPrimary)
                    cardinqueue.backgroundTintList = ContextCompat.getColorStateList(getApplicationContext(),R.color.white)
                    history.backgroundTintList = ContextCompat.getColorStateList(getApplicationContext(),R.color.grey)
                    history.setBackgroundResource(R.drawable.rectanglegreen2)
                    history.setTextColor(Color.BLACK)
                    active.setBackgroundResource(R.drawable.rectanglegreen)
                    active.setTextColor(Color.WHITE)
                    active.backgroundTintList = ContextCompat.getColorStateList(getApplicationContext(),R.color.colorPrimary)

                    myoderpickup.setTextColor(Color.WHITE)
                    myorderfinedine.setTextColor(Color.BLACK)
                    myorderinqueue.setTextColor(Color.BLACK)
                    getpending("pending")
                }else{
                    sharedHelper!!.choose="pickup"
                    Log.d("sharedhelper",""+sharedHelper!!.choose)

                    linear_active_history.visibility=View.VISIBLE
                    cartframe.visibility=View.VISIBLE

                    cardfinedine.backgroundTintList = ContextCompat.getColorStateList(getApplicationContext(),R.color.white)
                    cardpickup.backgroundTintList = ContextCompat.getColorStateList(getApplicationContext(),R.color.colorPrimary)
                    cardinqueue.backgroundTintList = ContextCompat.getColorStateList(getApplicationContext(),R.color.white)
                    history.backgroundTintList = ContextCompat.getColorStateList(getApplicationContext(),R.color.grey)
                    history.setBackgroundResource(R.drawable.rectanglegreen2)
                    history.setTextColor(Color.BLACK)
                    active.setBackgroundResource(R.drawable.rectanglegreen)
                    active.setTextColor(Color.WHITE)
                    active.backgroundTintList = ContextCompat.getColorStateList(getApplicationContext(),R.color.colorPrimary)

                    myoderpickup.setTextColor(Color.WHITE)
                    myorderfinedine.setTextColor(Color.BLACK)
                    myorderinqueue.setTextColor(Color.BLACK)
                    getpending("pending")
                }


            }else{


                if (sharedHelper!!.choose.equals("finedine")||sharedHelper!!.choose.equals("inqueue")|| sharedHelper!!.cartcount == 0){

                    //var cartframe: FrameLayout =findViewById(R.id.cartframe)
                    cartframe.visibility=View.GONE
                    sharedHelper!!.choose="pickup"
                    Log.d("sharedhelper",""+sharedHelper!!.choose)

                    linear_active_history.visibility=View.VISIBLE
                   // cartframe.visibility=View.VISIBLE

                    cardfinedine.backgroundTintList = ContextCompat.getColorStateList(getApplicationContext(),R.color.white)
                    cardpickup.backgroundTintList = ContextCompat.getColorStateList(getApplicationContext(),R.color.colorPrimary)
                    cardinqueue.backgroundTintList = ContextCompat.getColorStateList(getApplicationContext(),R.color.white)
                    history.backgroundTintList = ContextCompat.getColorStateList(getApplicationContext(),R.color.grey)
                    history.setBackgroundResource(R.drawable.rectanglegreen)
                    history.setTextColor(Color.BLACK)
                    active.setBackgroundResource(R.drawable.rectanglegreen2)
                    active.setTextColor(Color.WHITE)
                    active.backgroundTintList = ContextCompat.getColorStateList(getApplicationContext(),R.color.colorPrimary)

                    myoderpickup.setTextColor(Color.WHITE)
                    myorderfinedine.setTextColor(Color.BLACK)
                    myorderinqueue.setTextColor(Color.BLACK)
                    getpending("pending")
                }else{
                    sharedHelper!!.choose="pickup"
                    Log.d("sharedhelper",""+sharedHelper!!.choose)

                    linear_active_history.visibility=View.VISIBLE
                    cartframe.visibility=View.VISIBLE

                    cardfinedine.backgroundTintList = ContextCompat.getColorStateList(getApplicationContext(),R.color.white)
                    cardpickup.backgroundTintList = ContextCompat.getColorStateList(getApplicationContext(),R.color.colorPrimary)
                    cardinqueue.backgroundTintList = ContextCompat.getColorStateList(getApplicationContext(),R.color.white)
                    history.backgroundTintList = ContextCompat.getColorStateList(getApplicationContext(),R.color.grey)
                    history.setBackgroundResource(R.drawable.rectanglegreen)
                    history.setTextColor(Color.BLACK)
                    active.setBackgroundResource(R.drawable.rectanglegreen2)
                    active.setTextColor(Color.WHITE)
                    active.backgroundTintList = ContextCompat.getColorStateList(getApplicationContext(),R.color.colorPrimary)

                    myoderpickup.setTextColor(Color.WHITE)
                    myorderfinedine.setTextColor(Color.BLACK)
                    myorderinqueue.setTextColor(Color.BLACK)
                    getpending("pending")
                }


            }


        })

        linear_finedine.setOnClickListener(View.OnClickListener {

            if (sharedHelper!!.language.equals("ar")){
                sharedHelper!!.choose="finedine"
                Log.d("sharedhelper",""+sharedHelper!!.choose)
                cartframe.visibility=View.INVISIBLE

                linear_active_history.visibility=View.VISIBLE

                cardfinedine.backgroundTintList = ContextCompat.getColorStateList(getApplicationContext(),R.color.colorPrimary)
                cardpickup.backgroundTintList = ContextCompat.getColorStateList(getApplicationContext(),R.color.white)
                cardinqueue.backgroundTintList = ContextCompat.getColorStateList(getApplicationContext(),R.color.white)

                history.backgroundTintList = ContextCompat.getColorStateList(getApplicationContext(),R.color.grey)
                history.setBackgroundResource(R.drawable.rectanglegreen2)
                history.setTextColor(Color.BLACK)
                active.setBackgroundResource(R.drawable.rectanglegreen)
                active.setTextColor(Color.WHITE)
                active.backgroundTintList = ContextCompat.getColorStateList(getApplicationContext(),R.color.colorPrimary)
                myorderfinedine.setTextColor(Color.WHITE)
                myoderpickup.setTextColor(Color.BLACK)
                myorderinqueue.setTextColor(Color.BLACK)
                getbookinglist("active")
            }else{
                sharedHelper!!.choose="finedine"
                Log.d("sharedhelper",""+sharedHelper!!.choose)
                cartframe.visibility=View.INVISIBLE

                linear_active_history.visibility=View.VISIBLE

                cardfinedine.backgroundTintList = ContextCompat.getColorStateList(getApplicationContext(),R.color.colorPrimary)
                cardpickup.backgroundTintList = ContextCompat.getColorStateList(getApplicationContext(),R.color.white)
                cardinqueue.backgroundTintList = ContextCompat.getColorStateList(getApplicationContext(),R.color.white)

                history.backgroundTintList = ContextCompat.getColorStateList(getApplicationContext(),R.color.grey)
                history.setBackgroundResource(R.drawable.rectanglegreen)
                history.setTextColor(Color.BLACK)
                active.setBackgroundResource(R.drawable.rectanglegreen2)
                active.setTextColor(Color.WHITE)
                active.backgroundTintList = ContextCompat.getColorStateList(getApplicationContext(),R.color.colorPrimary)
                myorderfinedine.setTextColor(Color.WHITE)
                myoderpickup.setTextColor(Color.BLACK)
                myorderinqueue.setTextColor(Color.BLACK)
                getbookinglist("active")
            }



        })

        linear_inqueue.setOnClickListener(View.OnClickListener {
            linear_active_history.visibility=View.GONE
            cartframe.visibility=View.INVISIBLE
            sharedHelper!!.choose="inqueue"
            getbookinglist("inqueue")
            cardfinedine.backgroundTintList = ContextCompat.getColorStateList(getApplicationContext(),R.color.white)
            cardinqueue.backgroundTintList = ContextCompat.getColorStateList(getApplicationContext(),R.color.colorPrimary)
            cardpickup.backgroundTintList = ContextCompat.getColorStateList(getApplicationContext(),R.color.white)
            myorderfinedine.setTextColor(Color.BLACK)
            myoderpickup.setTextColor(Color.BLACK)
            myorderinqueue.setTextColor(Color.WHITE)

        })


        if (sharedHelper!!.choose.equals("finedine")||sharedHelper!!.choose.equals("inqueue")|| sharedHelper!!.cartcount == 0){

            //var cartframe: FrameLayout =findViewById(R.id.cartframe)
            cartframe.visibility=View.GONE
        }


    }



    public fun getpending(status: String){
        DialogUtils.showLoader(requireContext())

        mapViewModel?.getplaceorderlist(requireContext(),status)
            ?.observe(viewLifecycleOwner,
                Observer {
                    DialogUtils.dismissLoader()

                    it?.let {
                        it.error?.let { error ->
                            if (error) {
                                it.message?.let { msg ->
                                    UiUtils.showSnack(rooot_ordersfragment, msg)
                                    myorderrecycler.visibility=View.GONE
                                }
                            } else {
                                it.data?.let { data ->
                                    myorderrecycler.visibility=View.VISIBLE
                                    if (data.size==0){
                                        myorderrecycler.visibility=View.GONE
                                     }
                                    else{
                                         myorderrecycler.layoutManager = LinearLayoutManager(context)
                                         myorderrecycler.adapter = OrderAdapter(
                                             this,
                                             requireContext(),
                                             data
                                         )
                                     }


                                }
                            }
                        }

                    }
                })
    }

    public fun getbookinglist(status:String){
        DialogUtils.showLoader(requireContext())

        mapViewModel?.getbookinglist(requireContext(),status)
            ?.observe(viewLifecycleOwner,
                Observer {
                    DialogUtils.dismissLoader()

                    it?.let {
                        it.error?.let { error ->
                            if (error) {
                                it.message?.let { msg ->
                                    UiUtils.showSnack(rooot_ordersfragment, msg)
                                    myorderrecycler.visibility=View.GONE
                                }
                            } else {
                                it.data?.let { data ->
                                    myorderrecycler.visibility=View.VISIBLE
                                    if (data.size==0){
                                        myorderrecycler.visibility=View.GONE
                                    }
                                    else{
                                        myorderrecycler.layoutManager = LinearLayoutManager(context)
                                        myorderrecycler.adapter = BookingListAdapter(
                                            this,
                                            requireContext(),
                                            data
                                        )
                                    }


                                }
                            }
                        }

                    }
                })
    }

    public fun getbookinglistinqueue(status:String){
        DialogUtils.showLoader(requireContext())

        mapViewModel?.getbookinglist(requireContext(),status)
            ?.observe(viewLifecycleOwner,
                Observer {
                    DialogUtils.dismissLoader()

                    it?.let {
                        it.error?.let { error ->
                            if (error) {
                                it.message?.let { msg ->
                                    UiUtils.showSnack(rooot_ordersfragment, msg)
                                    myorderrecycler.visibility=View.GONE
                                }
                            } else {
                                it.data?.let { data ->
                                    myorderrecycler.visibility=View.VISIBLE
                                    if (data.size==0){
                                        myorderrecycler.visibility=View.GONE
                                    }
                                    else{
                                        myorderrecycler.layoutManager = LinearLayoutManager(context)
                                        myorderrecycler.adapter = InQueueActiveStatusAdapter(
                                            this,
                                            requireContext(),
                                            data
                                        )
                                    }


                                }
                            }
                        }

                    }
                })
    }


}