package com.lia.yello.linedine.fragment


import android.app.DatePickerDialog
import android.content.Intent
import android.net.Uri
import android.os.Bundle
import android.view.View
import android.widget.TextView
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.LinearLayoutManager
import com.lia.yello.linedine.Models.ProfileDetails
import com.lia.yello.linedine.R
import com.lia.yello.linedine.Session.SharedHelper
import com.lia.yello.linedine.Utils.BaseUtils
import com.lia.yello.linedine.Utils.DialogUtils
import com.lia.yello.linedine.Utils.UiUtils
import com.lia.yello.linedine.ViewModels.MapViewModel
import com.lia.yello.linedine.ViewModels.ProfileViewModel
import kotlinx.android.synthetic.main.activity_login.*
import kotlinx.android.synthetic.main.fragment_home.*
import kotlinx.android.synthetic.main.fragment_profile.*
import kotlinx.android.synthetic.main.fragment_profile.root
import kotlinx.android.synthetic.main.popup_filter.*
import java.io.File
import java.util.*
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import com.lia.yello.linedine.Utils.NetworkUtils
import com.lia.yello.linedine.activity.DashBoardActivity
import com.lia.yello.linedine.adapter.*
import com.lia.yello.linedine.databinding.*
import com.lia.yello.linedine.interfaces.SingleTapListener
import kotlinx.android.synthetic.main.carticon.*
import kotlinx.android.synthetic.main.fragment_helpsupport.*
import kotlinx.android.synthetic.main.fragment_subhome.*
import kotlinx.android.synthetic.main.fragment_wallet.*
import kotlinx.android.synthetic.main.header.*


class HelpSupportFragment : Fragment(R.layout.fragment_helpsupport) {
    public var sharedHelper: SharedHelper? = null

    var binding: FragmentHelpsupportBinding? = null
    private var mapViewModel: MapViewModel? = null
    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        binding = DataBindingUtil.findBinding(view)
        sharedHelper = SharedHelper(requireContext())

        mapViewModel = ViewModelProvider(this).get(MapViewModel::class.java)
        val head = view.findViewById<TextView>(R.id.head)
        head.setText(getString(R.string.helpsupport))
        cart_badge.text = ""+ sharedHelper!!.cartcount
        back.setOnClickListener(View.OnClickListener {
            //requireActivity().supportFragmentManager.popBackStack()
            val intent = Intent(requireContext(), DashBoardActivity::class.java)
            startActivity(intent)
        })
        if (!NetworkUtils.isNetworkConnected(requireContext())) {
            UiUtils.showSnack(
                view, "no_internet_connection"
            )
        }
        //  getfaqq()

        faq.setOnClickListener(View.OnClickListener {


            val intent = Intent(
                "android.intent.action.VIEW",
                Uri.parse("https://lineandine.com/faq.php")
            )
            startActivity(intent)
              /*if(faq_recycler.visibility==View.VISIBLE){
                  faq_recycler.visibility=View.GONE

              }else{
                  faq_recycler.visibility=View.VISIBLE

              }*/

        })


        contact.setOnClickListener(View.OnClickListener {
           // showComingSoon()

            val intent = Intent(
                "android.intent.action.VIEW",
                Uri.parse("https://lineandine.com/contact.php")
            )
            startActivity(intent)




        })
        chat.setOnClickListener(View.OnClickListener {
            //showComingSoon()

            val intent = Intent(
                "android.intent.action.VIEW",
                Uri.parse("https://lineandine.com/")
            )
            startActivity(intent)

        })


        if (sharedHelper!!.language == "ar"){
            back.rotation= 180F
        }
        if (sharedHelper!!.choose.equals("finedine")||sharedHelper!!.choose.equals("inqueue")|| sharedHelper!!.cartcount == 0){

            //var cartframe: FrameLayout =findViewById(R.id.cartframe)
            cartframe.visibility=View.GONE
        }

    }


    private fun getfaqq(){
        DialogUtils.showLoader(requireContext())

        mapViewModel?.getfaqq(requireContext())
            ?.observe(viewLifecycleOwner, androidx.lifecycle.Observer {
                DialogUtils.dismissLoader()

                it?.let {
                    it.error?.let { error ->
                        if (error) {
                            it.message?.let { msg ->
                                // UiUtils.showSnack(root_wallet, msg)
                            }
                        } else {
                            it.data?.let { data ->
                                faq_recycler.layoutManager = LinearLayoutManager(context)
                                faq_recycler.adapter = FaqAdapter(
                                    this,
                                    requireContext(),
                                    data.faq
                                )
                            }

                        }
                    }

                }
            })

    }


    fun showComingSoon() {

        DialogUtils.showAlertWithHeader(requireContext(), object : SingleTapListener {
            override fun singleTap() {

            }
        }, getString(R.string.comingsoon), getString(R.string.sorry))
    }
}