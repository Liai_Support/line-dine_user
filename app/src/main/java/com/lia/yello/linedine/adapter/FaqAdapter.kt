package com.lia.yello.linedine.adapter

import android.content.Context
import android.os.Build
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.annotation.RequiresApi
import androidx.recyclerview.widget.RecyclerView
import com.lia.yello.linedine.Models.FaqDataa
import com.lia.yello.linedine.Models.listfav
import com.lia.yello.linedine.R
import com.lia.yello.linedine.fragment.HelpSupportFragment

class FaqAdapter(
    var helpSupportFragment: HelpSupportFragment,
    var context: Context,
    var list: ArrayList<FaqDataa>?
):
    RecyclerView.Adapter<FaqAdapter.HomeHeaderViewHolder>(){
    inner class HomeHeaderViewHolder(view: View) : RecyclerView.ViewHolder(view) {

        var question: TextView
        var answer: TextView

        init {
            question=view.findViewById(R.id.question)
            answer=view.findViewById(R.id.answer)

        }    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): HomeHeaderViewHolder {
        return HomeHeaderViewHolder(
            LayoutInflater.from(context).inflate(
                R.layout.faq,
                parent,
                false
            )
        )
    }

    override fun getItemCount(): Int {
        return list!!.size
    }

    @RequiresApi(Build.VERSION_CODES.M)
    override fun onBindViewHolder(holder: HomeHeaderViewHolder, position: Int) {
                if (helpSupportFragment.sharedHelper!!.choose.equals("en")){

                    holder.question.text= list!![position].question
                    holder.answer.text= list!![position].answer

                    holder.question.setOnClickListener(View.OnClickListener {
                        if (holder.answer.visibility == View.GONE) {


                            holder.question.setCompoundDrawablesWithIntrinsicBounds(
                                null,
                                null,
                                context.getDrawable(R.drawable.dropdown),
                                null
                            )
                            holder.answer.visibility = View.VISIBLE
                        } else {
                            holder.question.setCompoundDrawablesWithIntrinsicBounds(
                                null,
                                null,
                                context.getDrawable(R.drawable.ic_baseline_arrow_drop_up_24),
                                null
                            )
                            holder.answer.visibility = View.GONE
                        }

                    })


                }else{
                    holder.question.text= list!![position].question
                    holder.answer.text= list!![position].answer
                    holder.question.setOnClickListener(View.OnClickListener {
                        if (holder.answer.visibility == View.GONE) {


                            holder.question.setCompoundDrawablesWithIntrinsicBounds(
                                null,
                                null,
                                context.getDrawable(R.drawable.dropdown),
                                null
                            )
                            holder.answer.visibility = View.VISIBLE
                        } else {
                            holder.question.setCompoundDrawablesWithIntrinsicBounds(
                                null,
                                null,
                                context.getDrawable(R.drawable.ic_baseline_arrow_drop_up_24),
                                null
                            )
                            holder.answer.visibility = View.GONE
                        }


                    })


                }



    }
}
