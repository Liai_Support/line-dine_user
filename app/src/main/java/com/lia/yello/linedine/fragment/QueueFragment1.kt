package com.lia.yello.linedine.fragment

import android.app.AlertDialog
import android.content.Intent
import android.content.res.ColorStateList
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.location.Geocoder
import android.net.Uri
import android.os.Bundle
import android.util.DisplayMetrics
import android.util.Log
import android.view.Gravity
import android.view.LayoutInflater
import android.view.View
import android.view.WindowManager
import android.widget.TextView
import androidx.activity.OnBackPressedCallback
import androidx.core.content.ContextCompat
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentTransaction
import androidx.lifecycle.ViewModelProvider
import com.lia.yello.linedine.R
import com.lia.yello.linedine.Session.SharedHelper
import com.lia.yello.linedine.Utils.DialogUtils
import com.lia.yello.linedine.Utils.NetworkUtils
import com.lia.yello.linedine.Utils.UiUtils
import com.lia.yello.linedine.ViewModels.MapViewModel
import com.lia.yello.linedine.activity.DashBoardActivity
import com.lia.yello.linedine.databinding.FragmentInQueue1Binding
import kotlinx.android.synthetic.main.activity_dash_board.*
import kotlinx.android.synthetic.main.carticon.*
import kotlinx.android.synthetic.main.fragment_in_queue1.*
import kotlinx.android.synthetic.main.fragment_suborders.*
import kotlinx.android.synthetic.main.header.*
import kotlinx.android.synthetic.main.rulepagepickup.view.*
import kotlinx.android.synthetic.main.rulespage.*
import java.util.*


class QueueFragment1 : Fragment(R.layout.fragment_in_queue1) {

    var binding: FragmentInQueue1Binding? = null
    private var mapViewModel: MapViewModel? = null
    public var sharedHelper: SharedHelper? = null
    var hotelname: String = ""
    var resimage: String = ""
    var fulldate:String=""
    var time:String=""
    var inqueuee:String="false"
    var notinqueue:String="false"
    var resid:Int=0
    var seatcount:Int=0
    var queuecount:Int=0
    var inqueue:String=""
    var orderid:Int=0
    private var fineDineMenuList : FineDineMenuListFragment? = null
    private var homeFragment : HomeFragment? = null
    private var fineDineFragment1 : FineDineFragment1? = null
    private var myOrderFragment : MyOrderFragment? = null
    private lateinit var fragmentTransaction: FragmentTransaction
    var phoneee:String=""
    var res_status:String=""
    var fcity:String=""
    var lat:Double=0.0
    var lng:Double=0.0
    var imgid:Int=0
    var typee:String=""
    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        binding = DataBindingUtil.findBinding(view)
        sharedHelper = SharedHelper(requireContext())
        mapViewModel = ViewModelProvider(this).get(MapViewModel::class.java)
        val head = view.findViewById<TextView>(R.id.head)
        head.setText(getString(R.string.resturant))
        inqueue = arguments?.getString("inqueue")!!
        (activity as DashBoardActivity?)!!.bottom_navigation_main?.visibility=View.VISIBLE
        (activity as DashBoardActivity?)!!.iv_add?.visibility=View.VISIBLE
        if (sharedHelper!!.language == "ar"){
            back.rotation= 180F
        }
        any.background = requireContext().getDrawable(R.drawable.border_layoutred)
        any.setTextColor(Color.WHITE)
        if (!NetworkUtils.isNetworkConnected(requireContext())) {
            UiUtils.showSnack(
                view, "no_internet_connection"
            )
        }


        if (sharedHelper!!.choose.equals("finedine")||sharedHelper!!.choose.equals("inqueue")|| sharedHelper!!.cartcount == 0){

            //var cartframe: FrameLayout =findViewById(R.id.cartframe)
            cartframe.visibility=View.GONE
        }
        if (inqueue.equals("true")){
            hotelname = arguments?.getString("resname")!!
            fulldate = arguments?.getString("date")!!
            resid = arguments?.getInt("resid")!!
            seatcount = arguments?.getInt("seatcount")!!
            resimage = arguments?.getString("resimg")!!
            time = arguments?.getString("time")!!
            phoneee = arguments?.getString("phone")!!
            res_status = arguments?.getString("resstatus")!!
            lat = arguments?.getDouble("lat")!!
            lng = arguments?.getDouble("lng")!!
            imgid = arguments?.getInt("imgid")!!
            typee = arguments?.getString("type")!!
            Log.d("kanika", "" + hotelname)
            Log.d("kanika", "" + fulldate)
            Log.d("kanika", "" + resid)
            Log.d("kanika", "" + res_status)
            Log.d("lat", "" + lat)
            Log.d("lng", "" + lng)
            //  resturantname.text=hotelname

            resimage.let {
                UiUtils.loadImage(
                    listresturantimg,
                    it,
                    ContextCompat.getDrawable(
                        requireContext(),
                        R.drawable.dummylogo
                    )!!
                )
            }
            //  Log.d("kanika",""+inqueue
            getqueuecount(0)

        }else{

            resid = arguments?.getInt("resid")!!
            fulldate = arguments?.getString("date")!!
            resimage = arguments?.getString("resimg")!!
            hotelname = arguments?.getString("resname")!!
            orderid = arguments?.getInt("bookingid")!!
            phoneee = arguments?.getString("phone")!!
            res_status = arguments?.getString("resstatus")!!
            lat = arguments?.getDouble("lat")!!
            lng = arguments?.getDouble("lng")!!
            Log.d("resssid", "" + resid)
            Log.d("resssid", "" + phoneee)
            Log.d("resssid", "" + res_status)
            // resturantname.text=hotelname
            resimage.let {
                UiUtils.loadImage(
                    listresturantimg,
                    it,
                    ContextCompat.getDrawable(
                        requireContext(),
                        R.drawable.dummylogo
                    )!!
                )
            }
            inqueuee="true"
            getqueuecount(orderid)

        }

        val gcd = Geocoder(context, Locale.getDefault())
        val addresses = gcd.getFromLocation(lat!!, lng!!, 1)
        if (addresses.size > 0) {
            println(addresses[0].locality)
            Log.d("adedretrgygyuihu", "" + addresses[0].locality)
            fcity=addresses[0].locality
            resturantloc.text=fcity
        } else {
            // do your stuff
        }
        if (res_status.equals("A")){
            onlinestatus.text="online"
        }else{
            onlinestatus.text="not active"
        }
        cart_badge.text = ""+ sharedHelper!!.cartcount
        back.setOnClickListener(View.OnClickListener {
            //requireActivity().supportFragmentManager.popBackStack()
            linr_popup_inqueue.visibility = View.GONE

            if (inqueue.equals("false")) {
                myOrderFragment = MyOrderFragment()
                fragmentTransaction =
                    requireActivity().supportFragmentManager.beginTransaction()
                fragmentTransaction.setCustomAnimations(R.anim.screen_in, R.anim.screen_out)
                fragmentTransaction.replace(R.id.container, myOrderFragment!!)
                fragmentTransaction.addToBackStack(null)
                fragmentTransaction.commit()
            } else {
                if (notinqueue.equals("false")){
                    val bundle = Bundle()
                    bundle.putInt("id", resid)
                    bundle.putInt("imgid", imgid)
                    bundle.putString("type", typee)
                    fineDineFragment1 = FineDineFragment1()
                    fineDineFragment1!!.arguments=bundle
                    fragmentTransaction = requireActivity().supportFragmentManager.beginTransaction()
                    fragmentTransaction.setCustomAnimations(R.anim.screen_in, R.anim.screen_out)
                    fragmentTransaction.replace(R.id.container, fineDineFragment1!!)
                    fragmentTransaction.addToBackStack(null)
                    fragmentTransaction.commit()
                }else{
                    homeFragment = HomeFragment()
                    fragmentTransaction =requireActivity().supportFragmentManager.beginTransaction()
                    fragmentTransaction.setCustomAnimations(R.anim.screen_in, R.anim.screen_out)
                    fragmentTransaction.replace(R.id.container, homeFragment!!)
                    fragmentTransaction.addToBackStack(null)
                    fragmentTransaction.commit()
                }


            }

        })


        activity?.onBackPressedDispatcher?.addCallback(this, object : OnBackPressedCallback(true) {
            override fun handleOnBackPressed() {
                if (inqueue.equals("false")) {
                    isEnabled = false
                    myOrderFragment = MyOrderFragment()
                    fragmentTransaction =
                        requireActivity().supportFragmentManager.beginTransaction()
                    fragmentTransaction.setCustomAnimations(R.anim.screen_in, R.anim.screen_out)
                    fragmentTransaction.replace(R.id.container, myOrderFragment!!)
                    fragmentTransaction.addToBackStack(null)
                    fragmentTransaction.commit()
                } else {
                    isEnabled = false
                    if (notinqueue.equals("false")){
                        val bundle = Bundle()
                        bundle.putInt("id", resid)
                        bundle.putInt("imgid", imgid)
                        bundle.putString("type", typee)
                        fineDineFragment1 = FineDineFragment1()
                        fineDineFragment1!!.arguments=bundle
                        fragmentTransaction = requireActivity().supportFragmentManager.beginTransaction()
                        fragmentTransaction.setCustomAnimations(R.anim.screen_in, R.anim.screen_out)
                        fragmentTransaction.replace(R.id.container, fineDineFragment1!!)
                        fragmentTransaction.addToBackStack(null)
                        fragmentTransaction.commit()
                    }else{
                        homeFragment = HomeFragment()
                        fragmentTransaction =requireActivity().supportFragmentManager.beginTransaction()
                        fragmentTransaction.setCustomAnimations(R.anim.screen_in, R.anim.screen_out)
                        fragmentTransaction.replace(R.id.container, homeFragment!!)
                        fragmentTransaction.addToBackStack(null)
                        fragmentTransaction.commit()
                    }


                }

            }
        })



        phonecall.setOnClickListener(View.OnClickListener {
            /* val callIntent = Intent(Intent.ACTION_CALL)
            callIntent.data = Uri.parse(phoneee.toString())sswwwwwwwwwwwweww
            startActivity(callIntent)*/
            val intent = Intent(Intent.ACTION_DIAL, Uri.fromParts("tel", phoneee.toString(), null))
            startActivity(intent)
        })
        addmetoqueue.setOnClickListener(View.OnClickListener {

            val mDialogView = LayoutInflater.from(requireContext()).inflate(R.layout.rulepagepickup, null)
            val mBuilder = AlertDialog.Builder(requireContext()).setView(mDialogView).setCancelable(false)
            val mAlertDialog = mBuilder.show()
            val layoutParams = WindowManager.LayoutParams()
            val displayMetrics = DisplayMetrics()
            requireActivity().windowManager.defaultDisplay.getMetrics(displayMetrics)
            val displayWidth = displayMetrics.widthPixels
            val displayHeight = displayMetrics.heightPixels
            layoutParams.copyFrom(mAlertDialog.window?.attributes)
            layoutParams.width = ((displayWidth * 0.9f).toInt())
            mAlertDialog.window?.setAttributes(layoutParams)
            mAlertDialog.window?.setGravity(Gravity.CENTER)
            mAlertDialog.window?.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
            mDialogView.termscond.text=getString(R.string.termscond)
            mDialogView.check1.text=getString(R.string.check1)

            mDialogView.check1.setOnCheckedChangeListener { buttonView, isChecked ->
                if (isChecked) {
                    mDialogView.check1.buttonTintList= ColorStateList.valueOf(resources.getColor(R.color.colorPrimary))
                }else{
                    mDialogView.check1.buttonTintList= ColorStateList.valueOf(resources.getColor(R.color.grey2))
                }
            }
            mDialogView.procced.setOnClickListener {

                if (mDialogView.check1.isChecked) {
                    mAlertDialog.dismiss()
                    queuecount = queuecount + 1
                    addinqueue(queuecount)
                    notinqueue="true"
                } else {
                    UiUtils.showSnack(root_inqueue,getString(R.string.plscheckthebox))

                }
            }
            mDialogView.back.setOnClickListener {

                mAlertDialog.dismiss()

            }


        })


        img1.setOnClickListener(View.OnClickListener {

            linr_popup_inqueue.visibility = View.GONE
            homeFragment = HomeFragment()
            fragmentTransaction =requireActivity().supportFragmentManager.beginTransaction()
            fragmentTransaction.setCustomAnimations(R.anim.screen_in, R.anim.screen_out)
            fragmentTransaction.replace(R.id.container, homeFragment!!)
            fragmentTransaction.addToBackStack(null)
            fragmentTransaction.commit()

        })

        img2.setOnClickListener(View.OnClickListener {

            linr_popup_inqueue.visibility = View.GONE
            homeFragment = HomeFragment()
            fragmentTransaction =requireActivity().supportFragmentManager.beginTransaction()
            fragmentTransaction.setCustomAnimations(R.anim.screen_in, R.anim.screen_out)
            fragmentTransaction.replace(R.id.container, homeFragment!!)
            fragmentTransaction.addToBackStack(null)
            fragmentTransaction.commit()
        })

        back.setOnClickListener(View.OnClickListener {
            if (inqueue.equals("false")) {
                myOrderFragment = MyOrderFragment()
                fragmentTransaction = requireActivity().supportFragmentManager.beginTransaction()
                fragmentTransaction.setCustomAnimations(R.anim.screen_in, R.anim.screen_out)
                fragmentTransaction.replace(R.id.container, myOrderFragment!!)
                fragmentTransaction.addToBackStack(null)
                fragmentTransaction.commit()
            } else {
                homeFragment = HomeFragment()
                fragmentTransaction = requireActivity().supportFragmentManager.beginTransaction()
                fragmentTransaction.setCustomAnimations(R.anim.screen_in, R.anim.screen_out)
                fragmentTransaction.replace(R.id.container, homeFragment!!)
                fragmentTransaction.addToBackStack(null)
                fragmentTransaction.commit()

            }


        })

        inside.setOnClickListener(View.OnClickListener {
            outside.background = requireContext().getDrawable(R.drawable.border_layoutwhite)
            inside.background = requireContext().getDrawable(R.drawable.border_layoutred)
            any.background = requireContext().getDrawable(R.drawable.border_layoutwhite)
            inside.setTextColor(Color.WHITE)
            outside.setTextColor(Color.BLACK)
            any.setTextColor(Color.BLACK)

        })

        outside.setOnClickListener(View.OnClickListener {
            inside.background = requireContext().getDrawable(R.drawable.border_layoutwhite)
            outside.background = requireContext().getDrawable(R.drawable.border_layoutred)
            any.background = requireContext().getDrawable(R.drawable.border_layoutwhite)
            outside.setTextColor(Color.WHITE)
            inside.setTextColor(Color.BLACK)
            any.setTextColor(Color.BLACK)
        })

        any.setOnClickListener(View.OnClickListener {
            outside.background = requireContext().getDrawable(R.drawable.border_layoutwhite)
            any.background = requireContext().getDrawable(R.drawable.border_layoutred)
            inside.background = requireContext().getDrawable(R.drawable.border_layoutwhite)
            any.setTextColor(Color.WHITE)
            outside.setTextColor(Color.BLACK)
            inside.setTextColor(Color.BLACK)
        })


/*
        share.setOnClickListener(View.OnClickListener {
            val myIntent = Intent(Intent.ACTION_SEND)
            myIntent.type = "text/plain"
            val body =
                getString(R.string.sharecont) + " : " + "https://play.google.com/store/apps/details?id=com.lia.yello.roha"
            */
/*String sub = "Your Subject";
                        myIntent.putExtra(Intent.EXTRA_SUBJECT,sub);*//*

            */
/*String sub = "Your Subject";
                        myIntent.putExtra(Intent.EXTRA_SUBJECT,sub);*//*
myIntent.putExtra(
            Intent.EXTRA_TEXT,
            body
        )
            startActivity(Intent.createChooser(myIntent, "Share Using"))
        })
*/



    }

    public fun getqueuecount(orderid: Int){
        DialogUtils.showLoader(requireContext())
        mapViewModel?.getqueuecount(requireContext(), orderid, resid, inqueuee, fulldate, "finedine")
            ?.observe(viewLifecycleOwner, androidx.lifecycle.Observer {

                DialogUtils.dismissLoader()

                it?.let {
                    it.error?.let { error ->
                        if (error) {


                        } else {
                            it.data?.let { data ->

                                if (inqueue.equals("false")) {
                                    queuecountt.text = data.user_queuecount.toString()
                                    addmetoqueue.visibility = View.INVISIBLE
                                    //textinline.text = getString(R.string.waitinghrs)
                                    textinline1.visibility=View.GONE
                                    currentlyinline.visibility=View.VISIBLE
                                } else {
                                    queuecount = data.totcount!!
                                    queuecountt.text = data.totcount.toString()
                                    //  textinline1.text = getString(R.string.currentlyinline)
                                    textinline1.visibility=View.VISIBLE
                                    currentlyinline.visibility=View.GONE
                                }

                            }


                        }
                    }

                }
            })

    }


    public fun addinqueue(queuecounnt: Int){
        DialogUtils.showLoader(requireContext())
        mapViewModel?.addinqueue(
            requireContext(),
            seatcount,
            resid,
            queuecounnt,
            "any",
            fulldate,
            time
        )
            ?.observe(viewLifecycleOwner, androidx.lifecycle.Observer {

                DialogUtils.dismissLoader()

                it?.let {
                    it.error?.let { error ->
                        if (error) {

                        } else {

                            it.message?.let {
                                UiUtils.showSnack(
                                    root_inqueue,
                                    getString(R.string.youaresuccessfullyaddedintothequeue)
                                )
                            }
                            it.data?.let { data ->
                                (activity as DashBoardActivity?)!!.bottom_navigation_main?.visibility =
                                    View.GONE
                                (activity as DashBoardActivity?)!!.iv_add?.visibility =
                                    View.GONE
                                addmetoqueue.visibility=View.INVISIBLE

                                linr_popup_inqueue.visibility = View.VISIBLE
                                showinqueueno.text = "#" + data.queuecount.toString()
                                inqueuee = "true"
                                orderid= data.bookingid!!
                                gotomenu.setOnClickListener(View.OnClickListener {
                                    val bundle = Bundle()

                                    bundle.putString("inqueue", "true")

                                    bundle.putInt("resid",resid )
                                    bundle.putInt("orderid", orderid)
                                    bundle.putString("isfd","true")
                                    bundle.putSerializable("menulist",null)
                                    bundle.putString("status","")
                                    bundle.putString("resname",hotelname)
                                    bundle.putString("resimg",resimage)
                                    bundle.putDouble("lat", lat!!)
                                    bundle.putDouble("lng", lng!!)
                                    bundle.putString("paidstatus","")
                                    bundle.putString("paymenttype","")
                                    fineDineMenuList = FineDineMenuListFragment()
                                    fineDineMenuList!!.arguments=bundle
                                    fragmentTransaction = requireActivity().supportFragmentManager.beginTransaction()
                                    fragmentTransaction.setCustomAnimations(R.anim.screen_in,R.anim.screen_out)
                                    fragmentTransaction.replace(R.id.container, fineDineMenuList!!)
                                    fragmentTransaction.addToBackStack(null)
                                    fragmentTransaction.commit()

                                })


                            }

                        }
                    }

                }
            })

    }


}