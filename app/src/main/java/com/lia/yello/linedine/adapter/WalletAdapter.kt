package com.lia.yello.linedine.adapter

import android.content.Context
import android.provider.Settings.Global.getString
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.core.content.ContextCompat
import androidx.recyclerview.widget.RecyclerView
import com.lia.yello.linedine.Models.walletdata
import com.lia.yello.linedine.R
import com.lia.yello.linedine.Utils.BaseUtils
import com.lia.yello.linedine.databinding.CardWalletBinding
import com.lia.yello.linedine.databinding.TimeslotBinding
import com.lia.yello.linedine.fragment.WalletFragment
import kotlinx.android.synthetic.main.fragment_wallet.*
import java.text.SimpleDateFormat
import java.util.*
import kotlin.collections.ArrayList

class WalletAdapter(
    var walletFragment: WalletFragment,
    var context: Context,
    var list: ArrayList<walletdata>?
) :
    RecyclerView.Adapter<WalletAdapter.HomeHeaderViewHolder>() {
    var row_index: Int = -1
    lateinit var txt: String


    inner class HomeHeaderViewHolder(view: View) : RecyclerView.ViewHolder(view) {
        var binding: CardWalletBinding = CardWalletBinding.bind(view)

    /*    lateinit var amt: TextView
        lateinit var trancid: TextView
        lateinit var date: TextView


        init {
            amt = view.findViewById(R.id.amount)
            trancid = view.findViewById(R.id.trancid)
            date = view.findViewById(R.id.date)

        }*/
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): HomeHeaderViewHolder {
        return HomeHeaderViewHolder(
            LayoutInflater.from(context).inflate(
                R.layout.card_wallet,
                parent,
                false
            )
        )
    }

    override fun getItemCount(): Int {
        return list!!.size
    }

    override fun onBindViewHolder(holder: HomeHeaderViewHolder, position: Int) {


        if (list!![position].operation!!.equals("credit")) {

            holder.binding.amount.text ="+" + String.format(Locale("en", "US"),"%.2f",list!![position].amount)+" SAR"

/*
            holder.amt.text = "+ " + list!![position].amount.toString() + " SAR"
*/
            holder.binding.amount.setTextColor(
                ContextCompat.getColor(
                    context,
                    R.color.green
                )
            )
            if (!list!![position].transaction_id.equals("")){
                holder.binding.trancid.text = list!![position].transaction_id.toString()

            }else if ( list!![position].transaction_id.equals("")){
                holder.binding.trancid.text="Refund"
            }


        } else {
            holder.binding.amount.text ="-" + String.format(Locale("en", "US"),"%.2f",list!![position].amount)+" SAR"

/*
            holder.amt.text = "- " + list!![position].amount.toString() + "  SAR"
*/
            holder.binding.amount.setTextColor(
                ContextCompat.getColor(
                    context,
                    R.color.colorPrimary
                )
            )

            holder.binding.trancid.text =
                "" + context.getString(R.string.transactionid) + ":" + list!![position].order_id

        }
      holder.binding.date.text = list!![position].date.toString()


    }

}