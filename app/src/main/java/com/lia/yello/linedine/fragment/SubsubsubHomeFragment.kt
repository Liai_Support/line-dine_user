package com.lia.yello.linedine.fragment

import android.app.AlertDialog
import android.content.Intent
import android.net.Uri
import android.os.Bundle
import android.text.Editable
import android.text.TextWatcher
import android.util.Log
import android.view.View
import android.widget.TextView
import android.widget.Toast
import androidx.activity.OnBackPressedCallback
import androidx.core.content.ContextCompat
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentTransaction
import com.lia.yello.linedine.R
import com.lia.yello.linedine.Utils.UiUtils
import com.lia.yello.linedine.ViewModels.MapViewModel
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import com.google.android.material.snackbar.BaseTransientBottomBar
import com.google.android.material.snackbar.Snackbar
import com.lia.yello.linedine.Models.itemdetail
import com.lia.yello.linedine.Models.orderlist
import com.lia.yello.linedine.Session.SharedHelper
import com.lia.yello.linedine.Utils.DialogUtils
import com.lia.yello.linedine.Utils.NetworkUtils
import com.lia.yello.linedine.activity.OnSwipeTouchListener
import com.lia.yello.linedine.activity.OnSwipeTouchListener1
import kotlinx.android.synthetic.main.carticon.*

import kotlinx.android.synthetic.main.fragment_subsubsubhome.*
import kotlinx.android.synthetic.main.header.*
import java.util.*

import kotlin.collections.ArrayList



class SubsubsubHomeFragment : Fragment(R.layout.fragment_subsubsubhome) {

    lateinit var list: ArrayList<itemdetail>
    private var mapViewModel: MapViewModel? = null
    public var menuid1:Int? = 0
    public var resid:Int? = 0
    public var fav:Boolean? =false
    private var subsubHomeFragment: SubsubHomeFragment? = null
    private var cartFragment: CartFragment? = null
    private lateinit var fragmentTransaction: FragmentTransaction
    public var menuid:Int? = 0
    public var menuid2:Int? = 0
    public var resid1:Int? = 0
    public var cost:Double? = 0.0
    public var qnty:Int? = 0
    public var cartlist: ArrayList<orderlist>? = null
    public var sharedHelper: SharedHelper? = null

    var count:Int=0
    var iscart:Boolean=false
    var totalcost:Double=0.0
    var cartid:Int=0

    var subtotal: Double = 0.0
    var totalamt:Double=0.0
    var vat:Double=0.0
    var lat:Double=0.0
    var lng:Double=0.0
    var menurating:Double=0.0
    var imgid:Int=0
    var typee:String=""
    var descrip:String=""


    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        sharedHelper = SharedHelper(requireContext())
        subtotal = 0.0
        totalamt = 0.0
        vat = 0.0
        mapViewModel = ViewModelProvider(this).get(MapViewModel::class.java)
        val head = view.findViewById<TextView>(R.id.head)
        head.setText(getString(R.string.menu))
        back.setOnClickListener(View.OnClickListener {
            //  requireActivity().supportFragmentManager.popBackStack()

            val bundle = Bundle()
            bundle.putString("iscuisine", "false")
            bundle.putInt("imgid", imgid)
            bundle.putString("type", typee)
            resid?.let { it1 -> bundle.putInt("id", it1) }


            subsubHomeFragment = SubsubHomeFragment()
            subsubHomeFragment!!.arguments = bundle
            fragmentTransaction = requireActivity().supportFragmentManager.beginTransaction()
            fragmentTransaction.setCustomAnimations(R.anim.screen_in, R.anim.screen_out)
            fragmentTransaction.replace(R.id.container, subsubHomeFragment!!)
            fragmentTransaction.addToBackStack(null)
            fragmentTransaction.commit()
        })

        if (!NetworkUtils.isNetworkConnected(requireContext())) {
            UiUtils.showSnack(
                view, "no_internet_connection"
            )
        }
        if (sharedHelper!!.language.equals("ar")){

            preparetime.setBackgroundResource(R.drawable.border_greysubsubsub1)
            total.setBackgroundResource(R.drawable.border_greysubsubsub2)
            Log.d("arabic","arabicbg")

        }
        if (sharedHelper!!.choose.equals("finedine")||sharedHelper!!.choose.equals("inqueue")|| sharedHelper!!.cartcount == 0){

            //var cartframe: FrameLayout =findViewById(R.id.cartframe)
            cartframe.visibility=View.GONE
        }


        menuid1 = arguments?.getInt("id")
        resid = arguments?.getInt("resid")
        menurating = arguments?.getDouble("menurating")!!
        imgid = arguments?.getInt("imgid")!!
        typee = arguments?.getString("type")!!
        //   fav = arguments?.getBoolean("fav")
        Log.d("menuid,",""+menuid1)
        Log.d("ewrdfgvbn,",""+resid)
        Log.d("ewrdfgvbn,",""+menurating)


        if(menurating.equals("")){
            ratingitemdetail.text= "0"+"/" + "5"

        }else{
            ratingitemdetail.text= menurating.toString()+"/" + "5"

        }
        //Log.d("ewrdfgvbn,",""+fav)
        cart_badge.text = ""+ sharedHelper!!.cartcount

        txtcount.text=count.toString()

        getvalues()
        getcartlist()

        additem.setOnClickListener{

            if(cartlist.isNullOrEmpty()){
                if(iscart==true){
                    UiUtils.showSnack(additem,getString(R.string.menalreadyadded))
                }
                else{
                    if (count==0){
                        UiUtils.showSnack(root_subsubsubhome,getString(R.string.productquantityis0))

                    }
                    else{
                        addcart(descrip)

                    }
                }
            }
            else
            {
                var hd:Boolean = false
                for (i in 0 until cartlist!!.size){
                    Log.d("dbvhfd",""+resid+"==="+ cartlist!![i].res_id)
                    if (resid != cartlist!![i].res_id){
                        hd = true
                        break
                    }
                    else{
                        hd = false
                    }
                }
                if(hd == true){
                    val builder = AlertDialog.Builder(context)
                    builder.setTitle(R.string.alert)
                    builder.setMessage(R.string.youareaddingfromdiffresturant)

                    builder.setPositiveButton(android.R.string.yes) { dialog, which ->

                        if (count==0){
                            UiUtils.showSnack(root_subsubsubhome,getString(R.string.productquantityis0))

                        }
                        else{
                            getdeleteall()

                        }
                    }

                    builder.setNegativeButton(android.R.string.no) { dialog, which ->
                        builder.setCancelable(true)
                    }
                    builder.show()
                }
                else{
                    if(iscart == true){
                        getupdatecart(cartid, count)
                    }
                    else{

                        if (count==0){

                            UiUtils.showSnack(root_subsubsubhome,getString(R.string.productquantityis0))


                        }else{
                            addcart(descrip)

                        }
                    }
                }
            }
        }


        backtomenu.setOnClickListener{

            val bundle = Bundle()
            resid?.let { it1 -> bundle.putInt("id", it1) }
            bundle.putInt("imgid", imgid)
            bundle.putString("type", typee)
            subsubHomeFragment = SubsubHomeFragment()
            subsubHomeFragment!!.arguments=bundle
            fragmentTransaction = requireActivity().supportFragmentManager.beginTransaction()
            fragmentTransaction.setCustomAnimations(R.anim.screen_in,R.anim.screen_out)
            fragmentTransaction.replace(R.id.container, subsubHomeFragment!!)
            fragmentTransaction.commit()

        }

        plus.setOnClickListener(View.OnClickListener {
            count++
            txtcount.setText("" + count)
            Log.d("count", "" + count)



        })


        minus.setOnClickListener(View.OnClickListener {
            if (count == 0||count == 1) {

                txtcount.text = "" + count
                //   UiUtils.showSnack(root_subsubsubhome,getString(R.string.itemcannotbelessthan2))

            } else {
                count--
                txtcount.text = "" + count

            }

        })


        activity?.onBackPressedDispatcher?.addCallback(this, object : OnBackPressedCallback(true) {
            override fun handleOnBackPressed() {

                val bundle = Bundle()
                bundle.putString("iscuisine", "false")
                bundle.putInt("imgid", imgid)
                bundle.putString("type", typee)
                resid?.let { it1 -> bundle.putInt("id", it1) }


                subsubHomeFragment = SubsubHomeFragment()
                subsubHomeFragment!!.arguments = bundle
                fragmentTransaction = requireActivity().supportFragmentManager.beginTransaction()
                fragmentTransaction.setCustomAnimations(R.anim.screen_in, R.anim.screen_out)
                fragmentTransaction.replace(R.id.container, subsubHomeFragment!!)
                fragmentTransaction.addToBackStack(null)
                fragmentTransaction.commit()
            }
        })


        if (sharedHelper!!.language == "ar"){
            back.rotation= 180F
        }


        swipeimplemented.setOnTouchListener(object : OnSwipeTouchListener1(context){
            override fun onSwipeLeft() {
                //    Toast.makeText(context, "left", Toast.LENGTH_SHORT).show();

                Log.d("asdfrgtyhujikolp;","wertyuiop[")
                val bundle = Bundle()
                resid?.let { it1 -> bundle.putInt("id", it1) }
                bundle.putInt("imgid", imgid)
                bundle.putString("type", typee)
                subsubHomeFragment = SubsubHomeFragment()
                subsubHomeFragment!!.arguments=bundle
                fragmentTransaction = requireActivity().supportFragmentManager.beginTransaction()
                fragmentTransaction.setCustomAnimations(R.anim.screen_in,R.anim.screen_out)
                fragmentTransaction.replace(R.id.container, subsubHomeFragment!!)
                fragmentTransaction.commit()

            }
            override fun onSwipeRight() {
                Toast.makeText(context, "left", Toast.LENGTH_SHORT).show();

            }
        })



        specialins.addTextChangedListener(object : TextWatcher {
            override fun beforeTextChanged(s: CharSequence, start: Int, count: Int, after: Int) {

            }

            override fun onTextChanged(s: CharSequence, start: Int, before: Int, count: Int) {
            }

            override fun afterTextChanged(s: Editable) {
                descrip=specialins.text.toString()
                Log.d("sdfghjkl",""+descrip)
            }
        })


    }





    public fun getupdatecart(id: Int,qty: Int){
        DialogUtils.showLoader(requireContext())
        mapViewModel?.getupdatecart(requireContext(),id,qty,descrip)
            ?.observe(viewLifecycleOwner, Observer {
                DialogUtils.dismissLoader()

                it?.let {
                    it.error?.let { error ->
                        if (error) {
                            it.message?.let { msg ->
                                //UiUtils.showSnack(root, msg)
                                // snackbarToast(msg)
                                //Toast.makeText(context, msg, Toast.LENGTH_SHORT).show()

                            }
                        } else {
                            it.message?.let { msg ->
                                // Toast.makeText(context, msg, Toast.LENGTH_SHORT).show()
                                // cartFragment.getcartlist()
                                subtotal= 0.0
                                totalamt=0.0
                                vat =0.0
                                getcartlist()
                            }
                        }
                    }

                }
            })


    }


    // items
    private fun getvalues() {
        DialogUtils.showLoader(requireContext())
        resid?.let {
            menuid1?.let { it1 ->
                mapViewModel?.getItemDetail(requireContext(), it1, it)
                    ?.observe(viewLifecycleOwner,
                        Observer {

                            DialogUtils.dismissLoader()

                            it?.let {
                                it.error?.let { error ->
                                    if (error) {
                                        it.message?.let { msg ->
                                            UiUtils.showSnack(root_subsubsubhome, msg)
                                            snackbarToast(msg)

                                        }
                                    } else {
                                        it.data?.let { data ->
                                            handleResponse(data)
                                        }
                                    }
                                }

                            }
                        })
            }
        }
    }


    // deletecart
    public fun getdeleteall(){
        DialogUtils.showLoader(requireContext())

        mapViewModel?.getdeleteall(requireContext())
            ?.observe(viewLifecycleOwner,
                Observer {
                    DialogUtils.dismissLoader()

                    it?.let {
                        it.error?.let { error ->
                            if (error) {
                                it.message?.let { msg ->
                                    UiUtils.showSnack(root_subsubsubhome, msg)
                                    // snackbarToast(msg)
                                }
                            } else {
                                it.message?.let { msg ->
                                    subtotal= 0.0
                                    totalamt=0.0
                                    vat =0.0
                                    addcart(descrip)
                                    UiUtils.showSnack(root_subsubsubhome, msg)
                                    // snackbarToast(msg)
                                }
                            }
                        }

                    }
                })



    }

    //addcart
    private fun addcart(s:String) {


        //  totalcost= cost!!*count

        Log.d("totalcost",""+totalcost)
        Log.d("totalcost",""+cost)
        Log.d("totalcost",""+count)
        DialogUtils.showLoader(requireContext())

        menuid?.let {
            cost?.let { it1 ->
                resid1?.let { it2 ->
                    mapViewModel?.getAddCart(requireContext(), it,count, it1, it2,s)
                        ?.observe(viewLifecycleOwner,
                            Observer {
                                DialogUtils.dismissLoader()

                                it?.let {
                                    it.error?.let { error ->
                                        if (error) {
                                            it.message?.let { msg ->
                                                UiUtils.showSnack(root_subsubsubhome, msg)
                                            }
                                        } else {
                                            it.message?.let { msg ->
                                                UiUtils.showSnack(root_subsubsubhome, msg)
                                                subtotal= 0.0
                                                totalamt=0.0
                                                vat =0.0
                                                getcartlist()
                                                /* viewurcart.setVisibility(View.VISIBLE)
                                                 viewurcart.setOnClickListener{
                                                     cartFragment = CartFragment()
                                                     fragmentTransaction = requireActivity().supportFragmentManager.beginTransaction()
                                                     fragmentTransaction.setCustomAnimations(R.anim.screen_in,R.anim.screen_out)
                                                     fragmentTransaction.replace(R.id.container, cartFragment!!)
                                                     fragmentTransaction.addToBackStack(null)
                                                     fragmentTransaction.commit()

                                                     Log.d("zxcvfgbnm,",""+msg)



                                              }*/


                                            }


                                        }
                                    }


                                }
                            })
                }
            }
        }
    }

    public fun getcartlist(){
        DialogUtils.showLoader(requireContext())

        mapViewModel?.getorderlist(requireContext())
            ?.observe(viewLifecycleOwner,
                Observer {
                    DialogUtils.dismissLoader()

                    it?.let {
                        it.error?.let { error ->
                            if (error) {
                                it.message?.let { msg ->
                                    //  UiUtils.showSnack(root_subsubsubhome, msg)
                                    // snackbarToast(msg)
                                    cart_badge.text = "0"
                                    sharedHelper!!.cartcount = 0
                                    count=0
                                    txtcount.text=count.toString()
                                }
                            } else {
                                it.data?.let { data ->

                                    cartlist = data.orderdata
                                    cart_badge.text = ""+data.orderdata?.size

                                    for (i in 0 until data.orderdata!!.size) {


                                        if (menuid1== data.orderdata!![i].menu_id){
                                            iscart=true
                                            cartid = data.orderdata!![i].id!!
                                            count= data.orderdata!![i].quantity!!
                                            txtcount.text=count.toString()

                                        }
                                    }

                                    if (data.orderdata!!.size!=0) {
                                        sharedHelper!!.cartcount = data.orderdata?.size!!
                                        cart_badge.text = ""+data.orderdata?.size
                                        for (i in 0 until data.orderdata!!.size) {
                                            if (data.orderdata!![i].menuavilable==1){
                                                subtotal = subtotal + (data.orderdata!![i].cost!! * data.orderdata!![i].quantity!!)

                                            }                                        }
                                        vat = (subtotal / 100.0f) * sharedHelper!!.vat
                                        totalamt = subtotal + vat


                                        viewurcart.visibility=View.VISIBLE
                                        // txt_price_subsubsubhome!!.text = "$subtotal SAR"
                                        txt_price_subsubsubhome!!.text= String.format(Locale("en", "US"),"%.2f",subtotal)+ " SAR"

                                        viewurcart.setOnClickListener {
                                            cartFragment = CartFragment()
                                            fragmentTransaction =
                                                requireActivity().supportFragmentManager.beginTransaction()
                                            fragmentTransaction.setCustomAnimations(R.anim.screen_in,
                                                R.anim.screen_out)
                                            fragmentTransaction.replace(R.id.container,
                                                cartFragment!!)
                                            fragmentTransaction.addToBackStack(null)
                                            fragmentTransaction.commit()
                                        }
                                        // var list: ArrayList<orderlist>? = data.orderdata
                                    }

                                }


                            }
                        }
                    }


                })



    }

    private fun handleResponse(data: itemdetail) {

        data.name?.let { name.text = it }
        data.description?.let { itemdescription.text = it }
        /* data.price?.let { total.text = it.toString() + " SAR" }*/

        total.text=String.format(Locale("en", "US"),"%.2f",data.price)+ " SAR"

        data.prepTime?.let { preparetime.text = it }
        data.description?.let { itemdescription.text = it }
        resid1=data.resid
        menuid=data.menuid
        cost=data.price
        lat= data.lat!!
        lng= data.lng!!
        data.images?.let {
            UiUtils.loadImage(
                itemimg,
                it,
                ContextCompat.getDrawable(requireContext(), R.drawable.maskgroup46)!!
            )
        }
    }

    private fun snackbarToast(msg: String) {
        var snackbar: Snackbar
        Snackbar.make(view!!, msg, Snackbar.LENGTH_SHORT)
            .setTextColor(ContextCompat.getColor(context!!, R.color.design_default_color_error))
            .setBackgroundTint(ContextCompat.getColor(context!!, R.color.white))
            .setDuration(BaseTransientBottomBar.LENGTH_LONG)
            .setAnimationMode(BaseTransientBottomBar.ANIMATION_MODE_SLIDE)
            .show()
    }

}



