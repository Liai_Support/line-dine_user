package com.lia.yello.linedine.adapter

import android.content.Context
import android.os.Build
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import androidx.annotation.RequiresApi
import androidx.recyclerview.widget.RecyclerView
import com.lia.yello.linedine.R
import com.lia.yello.linedine.Utils.DialogUtils
import com.lia.yello.linedine.Utils.UiUtils
import com.lia.yello.linedine.activity.Banner
import com.lia.yello.linedine.databinding.DayslotBinding
import com.lia.yello.linedine.databinding.HomebannerBinding
import com.lia.yello.linedine.fragment.HomeBannerFragment
import com.lia.yello.linedine.fragment.SubsubHomeFragment
import kotlinx.android.synthetic.main.fragment_myfavorites.*

class HomeBannerAdapter(
    var homeBannerFragment: HomeBannerFragment,
    var context: Context,
    var list: ArrayList<Banner>?
) :
    RecyclerView.Adapter<HomeBannerAdapter.HomeHeaderViewHolder>() {
    inner class HomeHeaderViewHolder(view: View) : RecyclerView.ViewHolder(view) {
        var binding: HomebannerBinding = HomebannerBinding.bind(view)

       /* lateinit var image:ImageView
        init {
            image=view.findViewById(R.id.imageViewbanner)
        }*/
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): HomeHeaderViewHolder {
        return HomeHeaderViewHolder(
            LayoutInflater.from(context).inflate(
                R.layout.homebanner,
                parent,
                false
            )
        )
    }

    override fun getItemCount(): Int {
        return if (list == null) 0 else list!!.size * 2 + 1
    }

    @RequiresApi(Build.VERSION_CODES.M)
    override fun onBindViewHolder(holder: HomeHeaderViewHolder, position: Int) {

        list!![position].image?.let { imageUrl ->
            UiUtils.loadImage(holder.binding.imageViewbanner, imageUrl)
        }

        holder.binding.imageViewbanner.setOnClickListener{

        }

    }



}