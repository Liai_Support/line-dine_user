package com.lia.yello.linedine.Models

import com.google.gson.annotations.SerializedName
import com.lia.yello.linedine.activity.LoginData
import java.io.Serializable

class OtpResponseModel : Serializable {

    @SerializedName("error")
    var error: Boolean? = null

    @SerializedName("message")
    var message: String? = null

    @SerializedName("data")
    var data: OtpData? = null


}


class OtpData : Serializable {

    @SerializedName("id")
    var id: String? = null

    @SerializedName("name")
    var name: String? = null

    @SerializedName("phone")
    var mobileNumber: String? = null

    @SerializedName("accessToken")
    var token: String? = null

    @SerializedName("OTP")
    var OTP: String? = null

    @SerializedName("isotpverified")
    var isotpverified: String? = null
}