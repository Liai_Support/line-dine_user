package com.lia.yello.linedine.adapter

import android.content.Context
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.lia.yello.linedine.Models.Menudetail
import com.lia.yello.linedine.R
import com.lia.yello.linedine.Utils.UiUtils
import com.lia.yello.linedine.databinding.CardSubordersBinding
import com.lia.yello.linedine.fragment.MyActiveOrderFragment1
import java.util.*
import kotlin.collections.ArrayList

class MyActiveOrdersAdapter(
    var myCompleteOrderFragment: MyActiveOrderFragment1,
    var context: Context,
    var list: ArrayList<Menudetail>?
) :
    RecyclerView.Adapter<MyActiveOrdersAdapter.HomeHeaderViewHolder>() {

    // var list2: ArrayList<menudetail>? = null

    inner class HomeHeaderViewHolder(view: View) : RecyclerView.ViewHolder(view) {
        var binding: CardSubordersBinding = CardSubordersBinding.bind(view)
    }


    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): HomeHeaderViewHolder {
        return HomeHeaderViewHolder(
            LayoutInflater.from(parent.context).inflate(
                R.layout.card_suborders,
                parent,
                false
            )
        )
    }

    override fun getItemCount(): Int {
        return list!!.size
        // return itemCount
    }


    override fun onBindViewHolder(holder: HomeHeaderViewHolder, position: Int) {
        holder.binding.specialins.isEnabled=false

        //   myCompleteOrderFragment.subtotalcount= list!![position].amount!! * list!![position].quantity!!
        myCompleteOrderFragment.subtotalcount_total+=list!![position].amount!!
        Log.d("wedrftghyjkl",""+myCompleteOrderFragment.subtotalcount)
        Log.d("wedrftghyjkl",""+myCompleteOrderFragment.subtotalcount_total)


        myCompleteOrderFragment.ordersubtotal.text =String.format(Locale("en", "US"),"%.2f", myCompleteOrderFragment.subtotalcount_total)+ " SAR"
        val total=myCompleteOrderFragment.subtotalcount_total+myCompleteOrderFragment.vattocal
        //   myCompleteOrderFragment.totalmount.text =String.format(Locale("en", "US"),"%.2f", total)+ " SAR"

        holder.binding.title.text = list!![position].menu_name.toString()
        var amt:Double=0.0
        amt= list!![position].amount!! / list!![position].quantity!!
        holder.binding.cost.text=  String.format(Locale("en", "US"),"%.2f",amt)+ " SAR"
        //  holder.binding.cost.text =list!![position].amount.toString() + " SAR"
        holder.binding.txtquantity.text = list!![position].quantity.toString()

        Log.d("asdfghnjmk,", myCompleteOrderFragment.vat_txt.text as String)
        list!![position].image?.let { imageUrl ->
            UiUtils.loadImage(holder.binding.menuimage, imageUrl)
        }

        holder.binding.specialins.setText(list!![position].splins.toString())

        Log.d("specailins",""+list!![position].splins.toString())





        if (myCompleteOrderFragment.discoumtamtt==0.0){
            Log.d("yes","yes")
            myCompleteOrderFragment.totalmount.text =String.format(Locale("en", "US"),"%.2f", total)+ " SAR"

        }else{
            Log.d("no","no")
            myCompleteOrderFragment.totalmount.text =String.format(Locale("en", "US"),"%.2f", total-myCompleteOrderFragment.discoumtamtt)+ " SAR"


        }

    }

}












