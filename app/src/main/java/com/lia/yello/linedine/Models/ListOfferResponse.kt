package com.lia.yello.linedine.Models

import com.google.gson.annotations.SerializedName
import java.io.Serializable

class ListOfferResponse : Serializable {


    @SerializedName("error")
    var error: Boolean? = null

    @SerializedName("message")
    var message: String? = null

    @SerializedName("data")
    var data:  ArrayList<ListOffers>? = null


}



class ListOffers : Serializable {

    @SerializedName("id")
    var id: Int? = null


    @SerializedName("name")
    var name: String? = null

    @SerializedName("promocode")
    var promocode: String? = null

    @SerializedName("valid_from")
    var valid_from: String? = null

    @SerializedName("valid_to")
    var valid_to: String? = null

    @SerializedName("percentage")
    var percentage: String? = null

    @SerializedName("minimum_order_amount")
    var minimum_order_amount: String? = null


    @SerializedName("upto_amt")
    var upto_amt: String? = null

    @SerializedName("resid")
    var resid: Int? = null

    @SerializedName("SAR")
    var sar: Int? = null

    @SerializedName("offerfor")
    var offerfor: String? = null



}