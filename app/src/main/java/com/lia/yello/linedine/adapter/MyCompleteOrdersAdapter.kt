package com.lia.yello.linedine.adapter

import android.content.Context
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.lia.yello.linedine.Models.Menudetail
import com.lia.yello.linedine.R
import com.lia.yello.linedine.Utils.UiUtils
import com.lia.yello.linedine.databinding.CardSubordersBinding
import com.lia.yello.linedine.fragment.MyCompleteOrderFragment
import kotlinx.android.synthetic.main.fragment_order_processing.*
import java.util.*
import kotlin.collections.ArrayList

class MyCompleteOrdersAdapter(
    var myCompleteOrderFragment: MyCompleteOrderFragment,
    var context: Context,
    var list: ArrayList<Menudetail>?
) :
    RecyclerView.Adapter<MyCompleteOrdersAdapter.HomeHeaderViewHolder>() {

    // var list2: ArrayList<menudetail>? = null

    inner class HomeHeaderViewHolder(view: View) : RecyclerView.ViewHolder(view) {
        var binding: CardSubordersBinding = CardSubordersBinding.bind(view)
    }


    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): HomeHeaderViewHolder {
        return HomeHeaderViewHolder(
            LayoutInflater.from(parent.context).inflate(
                R.layout.card_suborders,
                parent,
                false
            )
        )
    }

    override fun getItemCount(): Int {
        return list!!.size
        // return itemCount
    }


    override fun onBindViewHolder(holder: HomeHeaderViewHolder, position: Int) {


        //   myCompleteOrderFragment.subtotalcount= list!![position].amount!! * list!![position].quantity!!
        myCompleteOrderFragment.subtotalcount_total+=list!![position].amount!!
        Log.d("wedrftghyjkl",""+myCompleteOrderFragment.subtotalcount)
        Log.d("wedrftghyjkl",""+myCompleteOrderFragment.subtotalcount_total)


        myCompleteOrderFragment.menuid= list!![position].id!!
        holder.binding.title.text = list!![position].menu_name.toString()
        holder.binding.specialins.setText(list!![position].splins.toString())
        holder.binding.cost.text=  String.format(Locale("en", "US"),"%.2f",list!![position].amount)+ " SAR"

        //holder.binding.cost.text =list!![position].amount.toString() + " SAR"
        holder.binding.txtquantity.text = list!![position].quantity.toString()
        // holder.binding.txtquantity.text= list!![position].quantity.toString()+ " SAR"

        Log.d("asdfghnjmk,", myCompleteOrderFragment.vat_txt.text as String)
        list!![position].image?.let { imageUrl ->
            UiUtils.loadImage(holder.binding.menuimage, imageUrl)
        }


        myCompleteOrderFragment.ordersubtotal.text =String.format(Locale("en", "US"),"%.2f", myCompleteOrderFragment.subtotalcount_total)+ " SAR"
        myCompleteOrderFragment.total=myCompleteOrderFragment.subtotalcount_total+myCompleteOrderFragment.vattocal
        myCompleteOrderFragment.totalmount.text =String.format(Locale("en", "US"),"%.2f", myCompleteOrderFragment.total)+ " SAR"


        if (myCompleteOrderFragment.discoumtamtt==0.0){
            Log.d("yes","yes")
            myCompleteOrderFragment.totalmount.text =String.format(Locale("en", "US"),"%.2f", myCompleteOrderFragment.total)+ " SAR"

        }else{
            Log.d("no","no")
            myCompleteOrderFragment.totalmount.text =String.format(Locale("en", "US"),"%.2f", myCompleteOrderFragment.total-myCompleteOrderFragment.discoumtamtt)+ " SAR"


        }


    }

}












