package com.lia.yello.linedine.Models

import com.google.gson.annotations.SerializedName
import java.io.Serializable

class BookingListResponse : Serializable {


    @SerializedName("error")
    var error: Boolean? = null

    @SerializedName("message")
    var message: String? = null

    @SerializedName("data")
    var data: ArrayList<BookingList>? = null
}

class BookingList : Serializable {

    @SerializedName("id")
    var id: Int? = null

    @SerializedName("resname")
    var resname: String? = null


    @SerializedName("resid")
    var resid: Int? = null

    @SerializedName("lat")
    var lat: Double? = null

    @SerializedName("lng")
    var lng: Double? = null


    @SerializedName("phone")
    var phonee: String? = null

    @SerializedName("res_status")
    var res_status: String? = null

    @SerializedName("tableid")
    var tableid: Int? = null

    @SerializedName("date")
    var date: String? = null

    @SerializedName("timeslot")
    var timeslot: String? = null

    @SerializedName("amount")
    var amount: Double? = null

    @SerializedName("seatcount")
    var seatcount: Int? = null

    @SerializedName("resimg")
    var resimg: String? = null

    @SerializedName("status")
    var status: String? = null


    @SerializedName("paymenttype")
    var paymenttype: String? = null

    @SerializedName("vat")
    var vat: Double? = null


    @SerializedName("subtotal")
    var subtotal: Double? = null


    @SerializedName("initial_amount")
    var initial_amount: Double? = null


    @SerializedName("paidstatus")
    var paidstatus: String? = null

    @SerializedName("transactionid")
    var transactionid: String? = null

    @SerializedName("Payment_orderid")
    var Payment_orderid: String? = null


    @SerializedName("discount_name")
    var discount_name: String? = null

    @SerializedName("discount_amount")
    var discount_amount: String? = null


    @SerializedName("menudetails")
    var menudetails: ArrayList<FineDineMenudetail>? = null
}
class FineDineMenudetail : Serializable {

    @SerializedName("id")
    var id: Int? = null


    @SerializedName("menu_name")
    var menu_name: String? = null

    @SerializedName("quantity")
    var quantity: Int? = null

    @SerializedName("amount")
    var amount: Double? = null

    @SerializedName("images")
    var image: String? = null

}
