package com.lia.yello.linedine.activity

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.ViewModelProvider
import com.lia.yello.linedine.R
import com.app.hakeemUser.models.SocialRegisterData
import com.lia.yello.linedine.Session.SharedHelper
import com.lia.yello.linedine.Utils.BaseUtils
import com.lia.yello.linedine.Utils.Constants
import com.lia.yello.linedine.Utils.UiUtils
import com.lia.yello.linedine.ViewModels.RegisterViewModel
import java.io.File
import com.lia.yello.linedine.databinding.ActivitySocialLoginDetailsBinding
import kotlinx.android.synthetic.main.activity_social_login_details.*


class SocialLoginDetailsActivity : BaseActivity() {
    var binding: ActivitySocialLoginDetailsBinding? = null

    var socialToken = ""
    var loginType = ""
    var profileImage = ""
    var uploadFile: File? = null
    var sharedHelper: SharedHelper? = null
    private var viewmodel: RegisterViewModel? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = DataBindingUtil.setContentView(this, R.layout.activity_social_login_details)
        viewmodel = ViewModelProvider(this).get(RegisterViewModel::class.java)
        sharedHelper = SharedHelper(this)
        getIntentValues()
    }

    private fun getIntentValues() {

        intent.extras?.let { extra ->

            extra.getString(Constants.IntentKeys.EMAIL)?.let { value ->
                if (value.isNotEmpty()) {
                    emailId.setText(value)
                    emailId.isEnabled = false
                } else {
                    emailId.isEnabled = true
                }

            }

            extra.getString(Constants.IntentKeys.NAME)?.let { value ->
                name.setText(value)
            }

            extra.getString(Constants.IntentKeys.SOCIALTOKEN)?.let { value ->
                socialToken = value
            }

            extra.getString(Constants.IntentKeys.LOGINTYPE)?.let { value ->
                loginType = value
            }

            extra.getString(Constants.IntentKeys.PROFILE_PICTURE)?.let { value ->
                if (value.isNotEmpty()) {
                    profileImage = value
                    UiUtils.loadImage(profilePicture, value)
                }
            }


        }

    }

    fun onSubmitClicked(view: View) {

        if (isValidInputs()) {
         //   proceedSignIn("")

          /*  DialogUtils.showLoader(this)
            if (uploadFile != null) {
                amazonViewModel?.uploadImage(this, uploadFile!!)?.observe(this, Observer {
                    it.error?.let { value ->
                        if (!value) {
                            proceedSignIn(it.message)
                        } else {
                            DialogUtils.dismissLoader()
                        }
                    }
                })
            } else {
                proceedSignIn("")
            }*/
        }

    }

/*
    private fun proceedSignIn(imageUrlString: String?) {

        imageUrlString?.let { image ->

            var imageurl = ""

            imageurl = if (image.isEmpty()) {
                profileImage
            } else {
                imageUrlString
            }

            viewmodel?.socialRegister(
                emailId.text.toString().trim(),
                imageurl,
                name.text.toString().trim(),
                socialToken,
                countryCodePicker.selectedCountryCode,
                phoneNumber.text.toString().trim(), loginType
            )?.observe(this, Observer {
                it?.let {
                    DialogUtils.dismissLoader()
                    it.error?.let { error ->
                        if (error) {
                            it.message?.let { msg ->
                                UiUtils.showSnack(root, msg)
                            }
                        } else {
                            it.data?.let { data ->
                                handleResponse(data)
                            }
                        }
                    }
                }
            })

        }

    }
*/

    private fun handleResponse(data: SocialRegisterData) {

        data.email?.let { sharedHelper?.email = it }
        data.id?.let { sharedHelper?.id = it }
        data.name?.let { sharedHelper?.name = it }
        data.token?.let { sharedHelper?.token = it }

        sharedHelper?.loggedIn = true
        val intent =
            Intent(this, DashBoardActivity::class.java)
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP)
        intent.flags =
            Intent.FLAG_ACTIVITY_NEW_TASK or Intent.FLAG_ACTIVITY_CLEAR_TASK
        startActivity(intent)
        finish()

    }


    private fun isValidInputs(): Boolean {
        when {

            uploadFile == null && profileImage.isEmpty() -> {
                UiUtils.showSnack(root, getString(R.string.pleaseuploadprofilepicture))
            }
            name.text.toString().trim() == "" -> {
                UiUtils.showSnack(root, getString(R.string.pleaseenteryourname))
                return false
            }
            emailId.text.toString().trim() == "" -> {
                UiUtils.showSnack(root, getString(R.string.pleaseenteryouremailid))
                return false
            }
            !BaseUtils.isValidEmail(emailId.text.toString()) -> {
                UiUtils.showSnack(root, getString(R.string.pleaseentervalidemailid))
                return false
            }
            phoneNumber.text.toString().trim() == "" -> {
                UiUtils.showSnack(root, getString(R.string.pleaseenteryourmobilenumber))
                return false
            }

            else -> return true
        }

        return true
    }

    fun onBackPressed(view: View) {
        onBackPressed()
    }


}