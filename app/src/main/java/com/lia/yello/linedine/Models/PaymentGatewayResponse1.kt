package com.lia.yello.linedine.Models
import com.google.gson.annotations.SerializedName
import java.io.Serializable

class PaymentGatewayResponse1 {
    @SerializedName("resultCode")
    var rcode: Int? = null

    @SerializedName("message")
    var message: String? = null


    @SerializedName("result")
    var result: PaymentResult1? = null
}
class PaymentResult1 : Serializable {

    @SerializedName("transaction")
    var transaction: TransactionData1? = null

    @SerializedName("order")
    var order: OrderData1? = null
}

class TransactionData1 : Serializable {

    @SerializedName("type")
    var type: String? = null

    @SerializedName("id")
    var id: String? = null

}

class OrderData1 : Serializable {

    @SerializedName("id")
    var id: String? = null

}