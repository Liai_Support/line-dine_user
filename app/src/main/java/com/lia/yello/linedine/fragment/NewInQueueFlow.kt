package com.lia.yello.linedine.fragment

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import com.lia.yello.linedine.R
import com.lia.yello.linedine.Session.SharedHelper
import com.lia.yello.linedine.databinding.FragmentFineDine3Binding
import com.lia.yello.linedine.databinding.FragmentNewInQueueFlowBinding

class NewInQueueFlow : Fragment(R.layout.fragment_new_in_queue_flow) {
    var binding: FragmentNewInQueueFlowBinding? = null
    public var sharedHelper: SharedHelper? = null
    var imgid:Int=0
    var typee:String=""
    public var id:Int? = 0

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
       binding = DataBindingUtil.findBinding(view)
        sharedHelper = SharedHelper(requireContext())

        id = arguments?.getInt("id")
        imgid = arguments?.getInt("imgid")!!
        typee = arguments?.getString("type")!!
    }


}