package com.lia.yello.linedine.Models

import com.google.gson.annotations.SerializedName
import java.io.Serializable

class PlaceOrderResponse : Serializable {

    @SerializedName("error")
    var error: Boolean? = null

    @SerializedName("message")
    var message: String? = null

    @SerializedName("data")
    var data: InsertData? = null
}
class InsertData : Serializable {

    @SerializedName("insertid")
    var insert_data: Int? = null
}