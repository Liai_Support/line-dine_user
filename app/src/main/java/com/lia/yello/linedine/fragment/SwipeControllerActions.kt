package com.lia.yello.linedine.fragment

abstract class SwipeControllerActions {
    fun onLeftClicked(position: kotlin.Int) {}
    open fun onRightClicked(position: kotlin.Int) {}
}
