package com.lia.yello.linedine.fragment

import android.Manifest
import android.annotation.SuppressLint
import android.app.Activity
import android.app.AlertDialog
import android.content.Context
import android.content.Intent
import android.content.pm.PackageManager
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.os.Build
import android.os.Bundle
import android.os.Handler
import android.util.DisplayMetrics
import android.util.Log
import android.view.Gravity
import android.view.LayoutInflater
import android.view.View
import android.view.WindowManager
import android.widget.FrameLayout
import android.widget.ImageView
import android.widget.LinearLayout
import android.widget.Toast
import androidx.activity.OnBackPressedCallback
import androidx.core.content.ContextCompat
import androidx.core.view.GravityCompat
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import androidx.fragment.app.FragmentTransaction
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.viewpager.widget.PagerAdapter
import androidx.viewpager.widget.ViewPager

import com.facebook.FacebookSdk
import com.google.android.gms.location.*
import com.google.android.material.bottomsheet.BottomSheetBehavior
import com.lia.yello.linedine.Models.Prediction
import com.lia.yello.linedine.Models.ProfileDetails
import com.lia.yello.linedine.Models.listaddress
import com.lia.yello.linedine.R
import com.lia.yello.linedine.Session.SharedHelper
import com.lia.yello.linedine.Utils.*
import com.lia.yello.linedine.ViewModels.MapViewModel
import com.lia.yello.linedine.ViewModels.ProfileViewModel
import com.lia.yello.linedine.activity.DashBoardActivity
import com.lia.yello.linedine.activity.HomeData
import com.lia.yello.linedine.activity.NoonPaymentActivity
import com.lia.yello.linedine.activity.OnBoardActivity
import com.lia.yello.linedine.adapter.*
import com.lia.yello.linedine.databinding.FragmentHomeBinding
import com.lia.yello.linedine.network.ApiInput
import com.lia.yello.linedine.network.UrlHelper
import kotlinx.android.synthetic.main.activity_dash_board.*
import kotlinx.android.synthetic.main.carticon.*
import kotlinx.android.synthetic.main.fragment_fine_dine3.*
import kotlinx.android.synthetic.main.fragment_fine_dine_order_list_status.*
import kotlinx.android.synthetic.main.fragment_home.*
import kotlinx.android.synthetic.main.fragment_home.location
import kotlinx.android.synthetic.main.fragment_home.recycler2
import kotlinx.android.synthetic.main.fragment_home.root1
import kotlinx.android.synthetic.main.fragment_home.viewpager
import kotlinx.android.synthetic.main.fragment_myfavorites.*
import kotlinx.android.synthetic.main.fragment_orders.*
import kotlinx.android.synthetic.main.fragment_subhome.*
import kotlinx.android.synthetic.main.homenew.*
import kotlinx.android.synthetic.main.layout_home_drawer1.*
import kotlinx.android.synthetic.main.notification.*
import kotlinx.android.synthetic.main.placeorder.view.*
import kotlinx.android.synthetic.main.popup_filter.*
import kotlinx.android.synthetic.main.popup_location.*
import java.util.*
import kotlin.collections.ArrayList
import kotlin.system.exitProcess


@Suppress("DEPRECATION")
class HomeFragment : Fragment(R.layout.homenew),FragmentManager.OnBackStackChangedListener {
    private var myLat = 0.0
    public var mapViewModel: MapViewModel? = null
    var binding: FragmentHomeBinding? = null;
    private var myLng = 0.0
    private var fusedLocationClient: FusedLocationProviderClient? = null
    public var sharedHelper: SharedHelper? = null
    private var locationRequest: LocationRequest? = null
    private var locationCallback: LocationCallback? = null
    private lateinit var fragmentTransaction: FragmentTransaction
    private var subsubHomeFragment: SubsubHomeFragment? = null
    private var homeBannerFragment: HomeBannerFragment? = null
    private var mapFragment: MapFragment? = null
    var cuisineid: Int = 0
    var imgid2: Int? = 0
    public var type: String? = ""
    public var promocode: String? = ""
    var  bookingid:Int?=0
    private var banner: HomeData? = null
    private var subHomeFragment: SubHomeFragment? = null
    private var homeFragment: HomeFragment? = null
    private var swipeDeleteFragment: SwipeDeleteFragment? = null
    var idarray: MutableList<Int> = mutableListOf<Int>()
    var idarray2: MutableList<Int> = mutableListOf<Int>()
    public var addressarray: ArrayList<listaddress>? = null
    lateinit var idarray3: ArrayList<String>
    private var automaticHandler = Handler()
    lateinit var runnable: Runnable
    public var iscuisine: Boolean = false
    private var profileViewModel: ProfileViewModel? = null
    var langcode :String = ""
    lateinit var fm: FragmentManager
    var doubleBackToExitPressedOnce = false

    var timer: Timer? = null
    var timerTask: TimerTask? = null
    var currentPage = 0
    val DELAY_MS: Long = 5000 //delay in milliseconds before task is to be executed
    val PERIOD_MS: Long = 5000 // time in milliseconds between successive task executions.
    var count = 0
    public var dotscount = 0
    private var dots: Array<ImageView?>? = null
    var version = ""
    var clearallfilter:Boolean=false

    @SuppressLint("NewApi")
    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        binding = DataBindingUtil.findBinding(view)
        sharedHelper = SharedHelper(requireContext())
        fusedLocationClient = LocationServices.getFusedLocationProviderClient(requireActivity())
        mapViewModel = ViewModelProvider(this).get(MapViewModel::class.java)
        profileViewModel = ViewModelProvider(this).get(ProfileViewModel::class.java)

        if (!NetworkUtils.isNetworkConnected(requireContext())) {
            UiUtils.showSnack(
                view, "no_internet_connection"
            )
        }

        if (sharedHelper!!.choose.equals("finedine")||sharedHelper!!.choose.equals("inqueue")|| sharedHelper!!.cartcount == 0){

            //var cartframe: FrameLayout =findViewById(R.id.cartframe)
            cartframe.visibility=View.GONE
        }


        GetAppVersion(requireContext())

        activity?.onBackPressedDispatcher?.addCallback(this, object : OnBackPressedCallback(true) {
            override fun handleOnBackPressed() {
                // this for single back press
                // isEnabled = false
                Log.d("xcx", "backsucess")
                if ((activity as DashBoardActivity?)!!.drawer.isDrawerOpen(GravityCompat.START)) {
                    (activity as DashBoardActivity?)!!.drawer.closeDrawer(GravityCompat.START)
                } else {
                    if ((context as DashBoardActivity?)!!.mBottomSheetBehavior?.state == BottomSheetBehavior.STATE_COLLAPSED || (context as DashBoardActivity?)!!.mBottomSheetBehavior2?.state == BottomSheetBehavior.STATE_COLLAPSED) {
                        (context as DashBoardActivity?)!!.mBottomSheetBehavior?.setState(
                            BottomSheetBehavior.STATE_HIDDEN
                        )
                        (activity as DashBoardActivity?)!!.mBottomSheetBehavior2?.setState(
                            BottomSheetBehavior.STATE_HIDDEN
                        )
                    } else {
                        (activity as DashBoardActivity?)!!.mBottomSheetBehavior?.setState(
                            BottomSheetBehavior.STATE_HIDDEN
                        )
                        //moveTaskToBack(true);
                        //exitProcess(-1)
                        if (doubleBackToExitPressedOnce) {
                            requireActivity().moveTaskToBack(true);
                            exitProcess(-1)
                            /* val intent = Intent(Intent.ACTION_MAIN)
                            intent.addCategory(Intent.CATEGORY_HOME)
                            startActivity(intent)*/
                            return
                        }

                        doubleBackToExitPressedOnce = true
                        Toast.makeText(requireContext(), getString(R.string.pleaseclickbackagaintoexit), Toast.LENGTH_SHORT).show()

                        Handler().postDelayed({ doubleBackToExitPressedOnce = false }, 2000)
                    }
                }
            }
        })

        getProfileData()
        getcartlist()
        getaddressvalue()
        locationListner()
        askLocationPermission()
        getvalues()
        location.setOnClickListener{
            (context as DashBoardActivity?)!!.mBottomSheetBehavior?.setState(BottomSheetBehavior.STATE_HIDDEN)
            (activity as DashBoardActivity?)!!.mBottomSheetBehavior2?.setState(BottomSheetBehavior.STATE_COLLAPSED)
        }





        requireActivity().current_location.setOnClickListener(View.OnClickListener {
            if (sharedHelper!!.cartcount == 0) {
                sharedHelper!!.iscurrentlocationseleted = true
                sharedHelper!!.selectedaddressid = 0
                getupdateaddressid()
                getUserLocation()
                (activity as DashBoardActivity?)!!.mBottomSheetBehavior2?.setState(
                    BottomSheetBehavior.STATE_HIDDEN
                )
            } else {
                if (sharedHelper!!.selectedaddressid == 0) {
                    sharedHelper!!.iscurrentlocationseleted = true
                    getUserLocation()
                    (activity as DashBoardActivity?)!!.mBottomSheetBehavior2?.setState(
                        BottomSheetBehavior.STATE_HIDDEN
                    )
                } else {
                    val builder = AlertDialog.Builder(context)
                    builder.setTitle(R.string.alert)
                    builder.setMessage(R.string.ifyoucanchangeaddressyourcartproductwillremoveautomatically)
                    builder.setCancelable(true)
                    builder.setPositiveButton(android.R.string.yes) { dialog, which ->
                        sharedHelper!!.selectedaddressid = 0
                        sharedHelper!!.iscurrentlocationseleted = true
                        getdeleteall()
                    }

                    builder.setNegativeButton(android.R.string.no) { dialog, which ->
                        builder.setCancelable(true)
                    }
                    builder.show()
                }
            }


        })
        // for location pop up
        requireActivity().close_pop2.setOnClickListener{
            (activity as DashBoardActivity?)!!.mBottomSheetBehavior2?.setState(BottomSheetBehavior.STATE_HIDDEN)
        }
        // for filter popup
        requireActivity().close_pop.setOnClickListener{
            (activity as DashBoardActivity?)!!.mBottomSheetBehavior?.setState(BottomSheetBehavior.STATE_HIDDEN)
        }
        requireActivity().clear_filter.setOnClickListener{
            getvalues()
         //   (activity as DashBoardActivity?)!!.mBottomSheetBehavior?.setState(BottomSheetBehavior.STATE_HIDDEN)
        }

        (context as DashBoardActivity?)!!.bottom_navigation_main.setVisibility(View.VISIBLE)
        (context as DashBoardActivity?)!!.iv_add.setVisibility(View.VISIBLE)
           // get address click
        requireActivity().add_location.setOnClickListener{
            val bundle = Bundle()
            bundle.putBoolean("update", false)
          //  bundle.putString("type", "home")
            mapFragment = MapFragment()
            mapFragment!!.arguments=bundle
            fragmentTransaction = requireActivity().supportFragmentManager.beginTransaction()
            fragmentTransaction.setCustomAnimations(R.anim.screen_in, R.anim.screen_out)
            fragmentTransaction.replace(R.id.container, mapFragment!!)
            fragmentTransaction.addToBackStack(null)
            fragmentTransaction.commit()
        }
        requireActivity().apply.setOnClickListener(View.OnClickListener {
            if (idarray.size != 0) {
                (activity as DashBoardActivity?)!!.mBottomSheetBehavior?.setState(
                    BottomSheetBehavior.STATE_HIDDEN
                )

                val bundle = Bundle()
                bundle.putIntegerArrayList("cuisinearray", ArrayList(idarray))
                bundle.putString("iscuisine", iscuisine.toString())
                imgid2?.let { it1 -> bundle.putInt("imgid", it1) }
                type?.let { it2 -> bundle.putString("type", it2) }

                subHomeFragment = SubHomeFragment()
                subHomeFragment!!.arguments = bundle
                fragmentTransaction = requireActivity().supportFragmentManager.beginTransaction()
                fragmentTransaction.setCustomAnimations(R.anim.screen_in, R.anim.screen_out)
                fragmentTransaction.replace(R.id.container, subHomeFragment!!)
                fragmentTransaction.addToBackStack(null)
                fragmentTransaction.commit()
            } else {
                UiUtils.showSnack(requireActivity().apply, getString(R.string.choosefitst))
            }

        })


        if (sharedHelper!!.choose.equals("inqueue")){

            sharedHelper!!.choose="inqueue"
            homefd.backgroundTintList = ContextCompat.getColorStateList(FacebookSdk.getApplicationContext(),R.color.white)
            homeinqueue.backgroundTintList = ContextCompat.getColorStateList(FacebookSdk.getApplicationContext(),R.color.colorPrimary2)

            homepickup.backgroundTintList = ContextCompat.getColorStateList(FacebookSdk.getApplicationContext(),R.color.white)

            textfd.setTextColor(Color.BLACK)
            textpickup.setTextColor(Color.BLACK)
            textinq.setTextColor(Color.WHITE)

        }else if (sharedHelper!!.choose.equals("finedine")){

            homefd.backgroundTintList = ContextCompat.getColorStateList(FacebookSdk.getApplicationContext(),R.color.colorPrimary2)
            homeinqueue.backgroundTintList = ContextCompat.getColorStateList(FacebookSdk.getApplicationContext(),R.color.white)
            homepickup.backgroundTintList = ContextCompat.getColorStateList(FacebookSdk.getApplicationContext(),R.color.white)
            textfd.setTextColor(Color.WHITE)
            textpickup.setTextColor(Color.BLACK)
            textinq.setTextColor(Color.BLACK)

        }else{
            sharedHelper!!.choose="pickup"
            homeinqueue.backgroundTintList = ContextCompat.getColorStateList(FacebookSdk.getApplicationContext(),R.color.white)
            homepickup.backgroundTintList = ContextCompat.getColorStateList(FacebookSdk.getApplicationContext(),R.color.colorPrimary2)
            homefd.backgroundTintList = ContextCompat.getColorStateList(FacebookSdk.getApplicationContext(),R.color.white)

            textfd.setTextColor(Color.BLACK)
            textpickup.setTextColor(Color.WHITE)
            textinq.setTextColor(Color.BLACK)
        }



        homefd.setOnClickListener{
            sharedHelper!!.choose="finedine"
            homefd.backgroundTintList = ContextCompat.getColorStateList(FacebookSdk.getApplicationContext(),R.color.colorPrimary2)
            homeinqueue.backgroundTintList = ContextCompat.getColorStateList(FacebookSdk.getApplicationContext(),R.color.white)
            homepickup.backgroundTintList = ContextCompat.getColorStateList(FacebookSdk.getApplicationContext(),R.color.white)
            textfd.setTextColor(Color.WHITE)
            textpickup.setTextColor(Color.BLACK)
            textinq.setTextColor(Color.BLACK)
        }
        homeinqueue.setOnClickListener{
            sharedHelper!!.choose="inqueue"
            homefd.backgroundTintList = ContextCompat.getColorStateList(FacebookSdk.getApplicationContext(),R.color.white)
            homeinqueue.backgroundTintList = ContextCompat.getColorStateList(FacebookSdk.getApplicationContext(),R.color.colorPrimary2)

            homepickup.backgroundTintList = ContextCompat.getColorStateList(FacebookSdk.getApplicationContext(),R.color.white)

            textfd.setTextColor(Color.BLACK)
            textpickup.setTextColor(Color.BLACK)
            textinq.setTextColor(Color.WHITE)
        }

        homepickup.setOnClickListener{
            sharedHelper!!.choose="pickup"
            homeinqueue.backgroundTintList = ContextCompat.getColorStateList(FacebookSdk.getApplicationContext(),R.color.white)
            homepickup.backgroundTintList = ContextCompat.getColorStateList(FacebookSdk.getApplicationContext(),R.color.colorPrimary2)
            homefd.backgroundTintList = ContextCompat.getColorStateList(FacebookSdk.getApplicationContext(),R.color.white)

            textfd.setTextColor(Color.BLACK)
            textpickup.setTextColor(Color.WHITE)
            textinq.setTextColor(Color.BLACK)
        }
      //  automaticSilder(true)
    // getcheckpending()
    }

    private fun getProfileData() {
        DialogUtils.showLoader(requireContext())
        profileViewModel?.getProfileDetails(requireContext())
            ?.observe(viewLifecycleOwner, Observer {
                DialogUtils.dismissLoader()
                it?.let {
                    it.error?.let { error ->
                        if (error) {
                            it.message?.let { msg ->
                                // UiUtils.showSnack(root, msg)
                            }
                        } else {
                            it.data?.let { data ->
                                handleResponse(data)

                                /* data.profileDetails?.let { list ->
                                     if (list.size != 0) {
                                         handleResponse(list[0])
                                     }

                                 }*/

                            }
                        }
                    }

                }
            })
    }
    private fun handleResponse(data: ProfileDetails) {

        data.name?.let {
            requireActivity().name_head.text = it
        }
        sharedHelper?.selectedaddressid = data.addressid!!
    }
    public fun getdeleteall(){
        DialogUtils.showLoader(requireContext())

        mapViewModel?.getdeleteall(requireContext())
            ?.observe(viewLifecycleOwner,
                Observer {
                    DialogUtils.dismissLoader()

                    it?.let {
                        it.error?.let { error ->
                            if (error) {
                                it.message?.let { msg ->
                                    UiUtils.showSnack(root1, msg)
                                    // snackbarToast(msg)

                                }
                            } else {
                                it.message?.let { msg ->
                                    //UiUtils.showSnack(root1, msg)
                                    // snackbarToast(msg)
                                    getupdateaddressid()
                                    cart_badge.text = "0"
                                    sharedHelper!!.cartcount = 0
                                    getUserLocation()
                                    (activity as DashBoardActivity?)!!.mBottomSheetBehavior2?.setState(
                                        BottomSheetBehavior.STATE_HIDDEN
                                    )
                                }
                            }
                        }

                    }
                })

    }
    public fun getupdateaddressid(){
        DialogUtils.showLoader(requireContext())
        mapViewModel?.getupdateaddressid(requireContext())
            ?.observe(viewLifecycleOwner, Observer {
                DialogUtils.dismissLoader()
                it?.let {
                    it.error?.let { error ->
                        if (error) {
                            it.message?.let { msg ->
                                // UiUtils.showSnack(root, msg)
                            }
                        } else {

                        }
                    }

                }
            })

    }
    public fun getcartlist(){
        DialogUtils.showLoader(requireContext())
        mapViewModel?.getorderlist(requireContext())
            ?.observe(viewLifecycleOwner,
                Observer {
                    DialogUtils.dismissLoader()
                    it?.let {
                        it.error?.let { error ->
                            Log.d("hgdsh", "" + error)
                            if (error) {
                                it.message?.let { msg ->
                                    // UiUtils.showSnack(root1_suborder, msg)
                                    //snackbarToast(getString(R.string.cartisempty))
                                    cart_badge.text = "0"
                                    sharedHelper!!.cartcount = 0
                                    cartframe.visibility=View.GONE

                                }
                            } else {
                                it.data?.let { data ->
                                    sharedHelper!!.cartcount = data.orderdata?.size!!
                                    cart_badge.text = "" + data.orderdata?.size

                                    Log.d("hhdgs", "" + data.orderdata?.size)

                                }

                            }
                        }

                    }
                })
    }
    private fun getvalues(){
        DialogUtils.showLoader(requireContext())
        mapViewModel?.gethomeDetails(requireContext())
            ?.observe(viewLifecycleOwner,
                Observer {
                    DialogUtils.dismissLoader()

                    it?.let {
                        it.error?.let { error ->
                            if (error) {
                                it.message?.let { msg ->
                                    UiUtils.showSnack(root1, msg)
                                }
                            } else {
                                it.data?.let { data ->

                                  //sharedHelper!!.vat = data.vat!!.toFloat()
                                    recycler2.layoutManager = LinearLayoutManager(activity)
                                    recycler2.adapter = HomeAdapter(
                                        this,
                                        requireContext(),
                                        data.resType
                                    )
                                    requireActivity().recycler_filter.layoutManager =
                                        GridLayoutManager(
                                            context, 4
                                        )
                                    /*requireActivity().recycler_filter.layoutManager =
                                        LinearLayoutManager(context,LinearLayoutManager.HORIZONTAL,false)*/


                                    requireActivity().recycler_filter.adapter = FilterAdapter(
                                        this,
                                        requireContext(),
                                        data.cuisines
                                    )
                                   // banner = data
                                  //  handleResponse(data)

                                    val adapter3: PagerAdapter = HomeBannerFragment2(
                                        this,
                                        requireContext(),
                                        data.banner
                                    )
                                    viewpager!!.adapter = adapter3
                                    /*dotscount = adapter3.count

                                    dots = arrayOfNulls<ImageView>(dotscount)

                                    for (i in 0 until dotscount) {
                                        dots!![i] = ImageView(context)
                                        dots!![i]!!.setImageDrawable(
                                            ContextCompat.getDrawable(
                                                FacebookSdk.getApplicationContext(),
                                                R.drawable.non_active_dot
                                            )
                                        )
                                        val params = LinearLayout.LayoutParams(
                                            LinearLayout.LayoutParams.WRAP_CONTENT,
                                            LinearLayout.LayoutParams.WRAP_CONTENT
                                        )
                                        params.setMargins(8, 0, 8, 0)
                                        SliderDotss.addView(dots!![i], params)
                                    }

                                    dots!![0]?.setImageDrawable(
                                        ContextCompat.getDrawable(
                                            FacebookSdk.getApplicationContext(),
                                            R.drawable.active_dot
                                        )
                                    )*/

                                   /* viewpager.addOnPageChangeListener(object :
                                        ViewPager.OnPageChangeListener {
                                        override fun onPageScrolled(
                                            position: Int,
                                            positionOffset: Float,
                                            positionOffsetPixels: Int
                                        ) {
                                        }

                                        override fun onPageSelected(position: Int) {
                                            for (i in 0 until dotscount) {
                                                dots!![i]!!.setImageDrawable(
                                                    ContextCompat.getDrawable(
                                                        FacebookSdk.getApplicationContext(),
                                                        R.drawable.non_active_dot
                                                    )
                                                )
                                            }
                                            dots!![position]?.setImageDrawable(
                                                ContextCompat.getDrawable(
                                                    FacebookSdk.getApplicationContext(),
                                                    R.drawable.active_dot
                                                )
                                            )
                                        }

                                        override fun onPageScrollStateChanged(state: Int) {}
                                    })*/

                                    /*After setting the adapter use the timer */
                                    val handler = Handler()
                                    val Update = Runnable {
                                        if (currentPage == dotscount) {
                                            currentPage = 0
                                        }
                                        if (viewpager!=null){

                                            viewpager.setCurrentItem(currentPage++, true)

                                        }
                                    }


                                    timer = Timer() // This will create a new Thread

                                    timer!!.schedule(object : TimerTask() {
                                        // task to be scheduled
                                        override fun run() {
                                            handler.post(Update)
                                        }
                                    }, DELAY_MS, PERIOD_MS)






                                }
                            }
                        }

                    }
                })
    }


    fun checkfavroite(resid:Int){
        DialogUtils.showLoader(requireContext())

        mapViewModel?.getcheckFavRest(requireContext(),resid)
            ?.observe(viewLifecycleOwner,
                Observer {
                    DialogUtils.dismissLoader()

                    it?.let {
                        it.error?.let { error ->
                            if (error) {
                                it.message?.let { msg ->
                                    UiUtils.showSnack(root1, msg)
                                }
                            } else  {
                                it.data?.let { data ->
                                    Log.d("nnncnc",""+sharedHelper!!.Promocodee)
                                    val bundle = Bundle()
                                    bundle.putInt("id", data.resid!!)
                                    bundle.putInt("imgid",0)
                                    bundle.putString("type", "")
                                    bundle.putString("promocode", sharedHelper!!.Promocodee)

                                    subsubHomeFragment = SubsubHomeFragment()
                                    subsubHomeFragment!!.arguments=bundle
                                    fragmentTransaction = requireActivity().supportFragmentManager.beginTransaction()
                                    fragmentTransaction.setCustomAnimations(R.anim.screen_in,R.anim.screen_out)
                                    fragmentTransaction.replace(R.id.container, subsubHomeFragment!!)
                                    fragmentTransaction.addToBackStack(null)
                                    fragmentTransaction.commit()
                                }
                            }
                        }

                    }
                })
    }

    public fun getaddressvalue(){
        DialogUtils.showLoader(requireContext())

        mapViewModel?.getlistadress(requireContext())
            ?.observe(viewLifecycleOwner,
                Observer {
                    DialogUtils.dismissLoader()

                    it?.let {
                        it.error?.let { error ->
                            if (error) {
                                it.message?.let { msg ->
                                    // UiUtils.showSnack(root11, msg)
                                    requireActivity().address_recycler.visibility = View.GONE
                                    addressarray = null
                                }
                            } else {
                                it.data?.let { data ->
                                    addressarray = data.listaddrs
                                    requireActivity().address_recycler.visibility = View.VISIBLE
                                    requireActivity().address_recycler.layoutManager =
                                        LinearLayoutManager(
                                            context
                                        )
                                    requireActivity().address_recycler.adapter =
                                        HomeListAddressAdapter(
                                            this,
                                            requireContext(),
                                            data.listaddrs
                                        )
                                }
                            }
                        }

                    }
                })
    }
    private fun handleResponse(data: HomeData) {
        data.banner?.let {
           // setHomeAdapter(it)
        }
    }
/*
    private fun setHomeAdapter(listBanner: ArrayList<Banner>) {

        var homeAdapter = HomeHeaderAdapter(requireContext(), listBanner)
        viewPager.layoutManager = LinearLayoutManager(context, RecyclerView.HORIZONTAL, false)
        viewPager.adapter = homeAdapter

        viewPager.addOnScrollListener(object : RecyclerView.OnScrollListener() {
            override fun onScrollStateChanged(recyclerView: RecyclerView, scrollState: Int) {
            }

            override fun onScrolled(recyclerView: RecyclerView, i: Int, i2: Int) {
                Log.d("Value", "SCroll Called")

                val childCount = viewPager.childCount
                val width = viewPager.getChildAt(0).width
                val padding = (viewPager.width - width) / 2

                for (j in 0 until childCount) {

                    if (viewPager.currentPosition == j) {

//                        recyclerView.getChildAt(j).alpha = 0.7f
                        recyclerView.getChildAt(j).animate()
                            .alpha(1f)
                            .setDuration(1000)
                            .setListener(null)

                    } else {
//                        recyclerView.getChildAt(j).alpha = 1f
                        recyclerView.getChildAt(j).animate()
                            .alpha(0.7f)
                            .setDuration(1000)
                            .setListener(null)
                    }

                    val v = recyclerView.getChildAt(j)
                    var rate = 0f
                    if (v.left <= padding) {
                        rate = if (v.left >= padding - v.width) {
                            (padding - v.left) * 1f / v.width
                        } else {
                            1f
                        }
                        v.scaleY = 1 - rate * 0.1f
                        v.scaleX = 1 - rate * 0.1f

                    } else {
                        if (v.left <= recyclerView.width - padding) {
                            rate = (recyclerView.width - padding - v.left) * 1f / v.width
                        }
                        v.scaleY = 0.9f + rate * 0.1f
                        v.scaleX = 0.9f + rate * 0.1f
                    }
                }
            }
        })
        viewPager.addOnPageChangedListener(
            RecyclerViewPager.OnPageChangedListener
            { oldPosition, newPosition ->
                Log.d(
                    "test",
                    "oldPosition:$oldPosition newPosition:$newPosition"
                )
            })

        viewPager.addOnLayoutChangeListener(View.OnLayoutChangeListener { _, _, _, _, _, _, _, _, _ ->
            Log.d("Value", "Called")
            if (viewPager.childCount < 3) {
                if (viewPager.getChildAt(1) != null) {
                    if (viewPager.currentPosition == 0) {
                        val v1 = viewPager.getChildAt(1)
                        v1.scaleY = 0.8f
                        v1.scaleX = 0.8f
                    } else {
                        val v1 = viewPager.getChildAt(0)
                        v1.scaleY = 0.8f
                        v1.scaleX = 0.8f
                    }
                }
            } else {
                if (viewPager.getChildAt(0) != null) {
                    val v0 = viewPager.getChildAt(0)
                    v0.scaleY = 0.8f
                    v0.scaleX = 0.8f
                }
                if (viewPager.getChildAt(2) != null) {
                    val v2 = viewPager.getChildAt(2)
                    v2.scaleY = 0.8f
                    v2.scaleX = 0.8f
                }
            }

        })
      //  automaticSilder(true)
    }
*/
    private fun askLocationPermission() {

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            if (requireContext().checkSelfPermission(Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && requireContext().checkSelfPermission(
                    Manifest.permission.ACCESS_COARSE_LOCATION
                ) != PackageManager.PERMISSION_GRANTED
            ) {
                requestPermissions(
                    Constants.Permission.LOCATION_PERMISSION_PERMISSON_LIST,
                    Constants.Permission.LOCATION_PERMISSION
                )
                return
            } else {
                getUserLocation()
            }
        } else {
            getUserLocation()
        }
    }
    private fun getUserLocation() {
        GpsUtils(requireActivity()).turnGPSOn(object : GpsUtils.OnGpsListener {
            override fun gpsStatus(isGPSEnable: Boolean) {
                if (isGPSEnable) {
                    getLastKnownLocation()
                }
            }
        })
    }
    @SuppressLint("MissingPermission")
    private fun getLastKnownLocation() {

        fusedLocationClient?.lastLocation?.addOnSuccessListener { location ->
            // Got last known location. In some rare situations this can be null.
            if (location != null) {

                if (sharedHelper?.location.toString().isNotEmpty()) {

                } else {
                    myLat = location.latitude
                    myLng = location.longitude

                    searchPlaces()
                }

            } else {

                fusedLocationClient?.requestLocationUpdates(
                    locationRequest,
                    locationCallback,
                    null
                )

            }
        }

    }
    private fun searchPlaces() {
        val url = UrlHelper.getAddress(myLat, myLng)
        val inputForAPI = ApiInput()
        inputForAPI.context = requireContext()
        inputForAPI.url = url
        DialogUtils.showLoader(requireContext())
        mapViewModel?.getAddress(requireContext(), inputForAPI)
            ?.observe(this, androidx.lifecycle.Observer {
                DialogUtils.dismissLoader()

                it.let { responses ->
                    responses.error?.let { errorValue ->
                        if (!errorValue) {
                            responses.predictions?.let { value ->
                                getAddressFromResponse(value)
                            }
                        }
                    }

                }

            })
    }
    private fun getAddressFromResponse(value: List<Prediction>) {
        Log.d("xvc", "xdc");
        if (value.isNotEmpty()) {

            value[0].formatted_address?.let {
                //location.text = it
                Log.d("ghscvgxdc", "" + sharedHelper!!.iscurrentlocationseleted)

                if(sharedHelper!!.cartcount == 0){
                    if(sharedHelper!!.selectedaddressid == 0){
                        Log.d("kanikashanmugak",""+it)
                        location.setText(it)
                        requireActivity().current_location.setText(it)
                        sharedHelper?.currentlocation  = it
                        sharedHelper?.currentLat = myLat.toString()
                        sharedHelper?.currentLng = myLng.toString()
                        Log.d("sdfghjkl;", "" + sharedHelper?.currentLat)
                        Log.d("sdfghjkltrf;", "" + sharedHelper?.currentLng)
                    }
                    else{
                        if (addressarray!=null){
                            for (i in 0 until addressarray!!.size){
                                if(addressarray!![i].id == sharedHelper!!.selectedaddressid){

                                    Log.d("kanikeeeashanmugak",""+it)

                                    requireActivity().current_location.setText(it)
                                    location.setText(addressarray!![i].type)
                                    sharedHelper?.currentLat = addressarray!![i].lat.toString()
                                    sharedHelper?.currentLng = addressarray!![i].lng.toString()
                                }
                            }

                        }
                        else{
                          getaddressvalue()
                           // getUserLocation()
                        }
                    }
                }
                else{
                    if(sharedHelper!!.selectedaddressid == 0){
                        Log.d("kanikashanmugak",""+it)

                        location.setText(it)
                        requireActivity().current_location.setText(it)
                        sharedHelper?.currentlocation  = it
                        sharedHelper?.currentLat = myLat.toString()
                        sharedHelper?.currentLng = myLng.toString()
                        Log.d("sdfghjkl;", "" + sharedHelper?.currentLat)
                        Log.d("sdfghjkltrf;", "" + sharedHelper?.currentLng)
                    }
                    else{
                        if (addressarray!=null){
                            for (i in 0 until addressarray!!.size){
                                if(addressarray!![i].id == sharedHelper!!.selectedaddressid){
                                    Log.d("swswwsws",""+addressarray!![i].type)

                                    requireActivity().current_location.setText(it)
                                    location.setText(addressarray!![i].type)
                                    sharedHelper?.currentLat = addressarray!![i].lat.toString()
                                    sharedHelper?.currentLng = addressarray!![i].lng.toString()
                                }
                            }

                        }
                        else{
                            getUserLocation()

                        }
                    }
                }
            }

//            if (value[0].addressComponents?.size != 0) {
//                value[0].addressComponents?.get(0)?.long_name?.let { address ->
//                    location.text = address
//                }
//            }

        }
    }
    private fun locationListner() {
        locationRequest = LocationRequest.create()
        locationRequest?.priority = LocationRequest.PRIORITY_HIGH_ACCURACY
        locationRequest?.interval = (20 * 1000).toLong()
        locationCallback = object : LocationCallback() {
            override fun onLocationResult(locationResult: LocationResult?) {
                if (locationResult == null) {
                    return
                }
                for (location in locationResult.locations) {
                    if (location != null) {
                        if (myLat == 0.0 && myLng == 0.0)
                            getLastKnownLocation()

                    }
                }
            }
        }

    }
/*
    private fun automaticSilder(resume: Boolean) {

        banner?.banner.let {

            runnable = Runnable {

                viewPager?.let {
                    if (it.size - 1 > viewPager.currentPosition) {
                        viewPager.smoothScrollToPosition(viewPager.currentPosition + 1)
                    } else {
                        viewPager.smoothScrollToPosition(0)
                    }
                    automaticHandler.removeCallbacks(runnable)
                    automaticHandler.postDelayed(runnable, 4000)
                }

            }

            if (resume) {
                automaticHandler.removeCallbacks(runnable)
                automaticHandler.postDelayed(runnable, 4000)

            } else {

                automaticHandler.removeCallbacks(runnable)

            }

        }


    }
*/


    override fun onRequestPermissionsResult(
    requestCode: Int,
    permissions: Array<out String>,
    grantResults: IntArray
) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)

        if (requestCode == Constants.Permission.LOCATION_PERMISSION) {

            if (grantResults.size >= 2 && grantResults[0] == PackageManager.PERMISSION_GRANTED && grantResults[1] == PackageManager.PERMISSION_GRANTED) {
                getUserLocation()
            }
        }

    }

    override fun onBackStackChanged() {
        TODO("Not yet implemented")
        Log.d("backytgt", "hbg")
    }




    fun GetAppVersion(context: Context): String {

        try {
            val pInfo = context.packageManager.getPackageInfo(context.packageName, 0)
            version = pInfo.versionName

            Log.d("kanikashanmugam",""+pInfo.versionName)
        } catch (e: PackageManager.NameNotFoundException) {
            e.printStackTrace()
        }

        return version
    }



    private fun getcheckpending() {
        DialogUtils.showLoader(requireContext())
        mapViewModel?.getcheckpendingstatus(requireContext())
            ?.observe(requireActivity(), Observer {
                DialogUtils.dismissLoader()
                it?.let {
                    it.error?.let { error ->
                        if (error) {
                            it.message?.let { msg ->
                                // UiUtils.showSnack(root, msg)
                            }
                        } else {

                            it.data?.let { data ->

                                     Log.d("qwertyuiop",""+data.order_id)
                                     bookingid=data.order_id!!.toInt()

                                     val builder1 = AlertDialog.Builder(requireContext())
                                     builder1.setTitle(R.string.alert)
                                     builder1.setMessage(R.string.wouldyouliketoconfirmyourbooking)
                                     // Set a positive button and its click listener on alert dialog
                                      builder1.setPositiveButton("YES") { dialog, which ->


                                         val mDialogView = LayoutInflater.from(requireContext()).inflate(R.layout.placeorder, null)
                                         val mBuilder = AlertDialog.Builder(requireContext()).setView(mDialogView).setCancelable(false)
                                         val mAlertDialog = mBuilder.show()
                                         val layoutParams = WindowManager.LayoutParams()
                                         val displayMetrics = DisplayMetrics()
                                         requireActivity().windowManager.defaultDisplay.getMetrics(displayMetrics)
                                         val displayWidth = displayMetrics.widthPixels
                                         val displayHeight = displayMetrics.heightPixels
                                         layoutParams.copyFrom(mAlertDialog.getWindow()?.getAttributes())
                                         layoutParams.width = ((displayWidth * 0.9f).toInt())
                                         mAlertDialog.getWindow()?.setAttributes(layoutParams)
                                         mAlertDialog.getWindow()?.setGravity(Gravity.CENTER)
                                         mAlertDialog.getWindow()?.setBackgroundDrawable(
                                             ColorDrawable(
                                                 Color.TRANSPARENT)
                                         )
                                         mDialogView.checkoutcard.setOnClickListener {
                                             startActivityForResult(
                                                 Intent(requireContext(), NoonPaymentActivity::class.java)
                                                     .putExtra(
                                                         "amount",
                                                         50.0
                                                     ), 100
                                             )
                                             mAlertDialog.dismiss()
                                         }

                                         mDialogView.checkoutwallet.setOnClickListener {
                                             getwalletamount(50.0)

                                             mAlertDialog.dismiss()

                                         }


                                     }
                                        builder1.setNegativeButton("No") { dialog, which ->
                                         val builder = AlertDialog.Builder(requireContext())
                                         builder.setTitle(R.string.alert)
                                         builder.setMessage(R.string.areyousureyouwanttocancelyourbooking)
                                         // Set a positive button and its click listener on alert dialog
                                         builder.setPositiveButton("YES") { dialog, which ->
                                            // cancelbooking(bookingid!!)
                                         }
                                         builder.setNegativeButton("No") { dialog, which ->

                                         }

                                         val dialog: AlertDialog = builder.create()
                                         dialog.show()
                                         dialog.setCancelable(false)


                                     }

                                val dialog: AlertDialog = builder1.create()
                                dialog.show()
                                dialog.setCancelable(false)


                                 }




                                Log.d("wertyuio","kanikakakak")


                            }

                    }

                }
            })
    }


    private fun getwalletamount(totalamt: Double) {
        DialogUtils.showLoader(requireContext())

        mapViewModel?.getwalletamount(requireContext())
            ?.observe(viewLifecycleOwner, androidx.lifecycle.Observer {
                DialogUtils.dismissLoader()

                it?.let {
                    it.error?.let { error ->
                        if (error) {
                            it.message?.let { msg ->
                                UiUtils.showSnack(root1, msg)
                            }
                        } else {
                            it.data?.let { data ->

                                Log.d("wertyu",""+totalamt+">"+data.amount.toString())
                                if( data.amount!! >= totalamt){

                                    Log.d("ertyuiop","kanikmakak")
                                    getcomfirmbooking("wallet","","")
                                }else{
                                    Log.d("ertyuiop","werftgy")

                                    UiUtils.showSnack(rootfinedine3, getString(R.string.yourwalletamountislowpleaseuplateyourwalletamount))



                                }


                            }

                        }
                    }

                }
            })

    }

    public fun getcomfirmbooking(s:String,orderid:String,transid:String) {
        DialogUtils.showLoader(requireContext())
        mapViewModel?.getcomfirmbooking(requireContext(),
            bookingid!!, 50.0, "booked", s,orderid,transid)
            ?.observe(viewLifecycleOwner, androidx.lifecycle.Observer {
                DialogUtils.dismissLoader()

                it?.let {
                    it.error?.let { error ->
                        if (error) {
                            it.message?.let { msg ->

                                UiUtils.showSnack(root1, msg)
                            }
                        } else {
                            it.message?.let { msg ->
                                UiUtils.showSnack(root1, msg)

/*

                                val builder = AlertDialog.Builder(requireContext())
                                builder.setTitle(R.string.alert)
                                builder.setMessage(R.string.wouldyouliketoorderfromourmenu)
                                // Set a positive button and its click listener on alert dialog
                                builder.setPositiveButton(getString(R.string.yes)) { dialog, which ->

                                }


                                builder.setNegativeButton(getString(R.string.no)) { dialog, which ->
                                    builder.setCancelable(true)
                                    //
                                    // finalreserve.visibility = View.VISIBLE
                                    homeFragment = HomeFragment()
                                    fragmentTransaction =
                                        requireActivity().supportFragmentManager.beginTransaction()
                                    fragmentTransaction.setCustomAnimations(
                                        R.anim.screen_in,
                                        R.anim.screen_out
                                    )
                                    fragmentTransaction.replace(R.id.container, homeFragment!!)
                                    fragmentTransaction.commit()


                                }
                                val dialog: AlertDialog = builder.create()
                                dialog.show()
                                dialog.setCancelable(false)
*/


                            }
                        }

                    }
                }
            })


    }




   /* private fun cancelbooking(id:Int){
        DialogUtils.showLoader(requireContext())

        mapViewModel?.cancelbooking(requireContext(),id)
            ?.observe(viewLifecycleOwner,
                Observer {
                    DialogUtils.dismissLoader()
                    it?.let {
                        it.error?.let { error ->
                            if (error) {
                                it.message?.let { msg ->
                                    UiUtils.showSnack(root1, msg)

                                }
                            }
                            else {
                                it.message?.let { msg ->
                                    UiUtils.showSnack(root1, msg)
                                    homeFragment = HomeFragment()

                                    fragmentTransaction = requireActivity().supportFragmentManager.beginTransaction()
                                    fragmentTransaction.setCustomAnimations(R.anim.screen_in,R.anim.screen_out)
                                    fragmentTransaction.replace(R.id.container, homeFragment!!)
                                    fragmentTransaction.addToBackStack(null)
                                    fragmentTransaction.commit()

                                }
                            }
                        }

                    }
                })

    }*/
    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (requestCode == 100) {
            if (resultCode == Activity.RESULT_OK) {
                getcomfirmbooking("card",data!!.getStringExtra("paymentorderid").toString(),data!!.getStringExtra("paymenttransactionid").toString());



            }
        }
    }


}