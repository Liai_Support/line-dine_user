package com.lia.yello.linedine.adapter

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.bumptech.glide.request.RequestOptions
import com.lia.yello.linedine.R
import com.lia.yello.linedine.Utils.UiUtils
import com.lia.yello.linedine.activity.Banner
import com.lia.yello.linedine.activity.HomeData

class HomeHeaderAdapter(
    var context: Context,
    var listBanner: ArrayList<Banner>
) :
    RecyclerView.Adapter<HomeHeaderAdapter.HomeHeaderViewHolder>() {

    inner class HomeHeaderViewHolder(view: View) : RecyclerView.ViewHolder(view) {

        lateinit var img:ImageView

        init {
            img=view.findViewById(R.id.imgtemp)

        }

    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): HomeHeaderViewHolder {
        return HomeHeaderViewHolder(
            LayoutInflater.from(context).inflate(
                R.layout.viewpager,
                parent,
                false
            )
        )
    }

    override fun getItemCount(): Int {
        return listBanner.size
    }

    override fun onBindViewHolder(holder: HomeHeaderViewHolder, position: Int) {
        listBanner!![position].image?.let { imageUrl ->
            UiUtils.loadImage(holder.img, imageUrl)
            Glide.with(context)
                .load(imageUrl)
                .apply(
                    RequestOptions()
                         .placeholder(R.drawable.logo)
                        .error(R.drawable.logo)
                )
                .into(holder.img)
        }


    }
}