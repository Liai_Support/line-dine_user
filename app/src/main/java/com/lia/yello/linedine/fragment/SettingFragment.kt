package com.lia.yello.linedine.fragment


import android.content.Intent
import android.os.Build
import android.os.Bundle
import android.os.LocaleList
import android.util.Log
import android.view.View
import android.widget.CompoundButton
import android.widget.TextView
import androidx.activity.OnBackPressedCallback
import androidx.core.view.GravityCompat
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentTransaction
import androidx.lifecycle.ViewModelProvider
import com.google.android.material.bottomsheet.BottomSheetBehavior
import com.lia.yello.linedine.R
import com.lia.yello.linedine.Session.SharedHelper
import com.lia.yello.linedine.Utils.DialogUtils
import com.lia.yello.linedine.Utils.NetworkUtils
import com.lia.yello.linedine.Utils.UiUtils
import com.lia.yello.linedine.ViewModels.MapViewModel
import com.lia.yello.linedine.activity.ChooseLanguageActivity
import com.lia.yello.linedine.activity.DashBoardActivity
import com.lia.yello.linedine.activity.OnBoardActivity
import com.lia.yello.linedine.databinding.*
import kotlinx.android.synthetic.main.activity_dash_board.*
import kotlinx.android.synthetic.main.carticon.*
import kotlinx.android.synthetic.main.fragment_settings.*
import kotlinx.android.synthetic.main.header.*
import java.util.*

class SettingFragment : Fragment(R.layout.fragment_settings) {

    var binding: FragmentSettingsBinding? = null
    private var mapViewModel: MapViewModel? = null
    public var sharedHelper: SharedHelper? = null
    var langcode :String = ""
    private lateinit var fragmentTransaction: FragmentTransaction
    private var settingFragment: SettingFragment? = null

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        binding = DataBindingUtil.findBinding(view)
        mapViewModel = ViewModelProvider(this).get(MapViewModel::class.java)
        val head = view.findViewById<TextView>(R.id.head)
        head.setText(getString(R.string.settings))
        sharedHelper = SharedHelper(requireContext())
        cart_badge.text = ""+ sharedHelper!!.cartcount
        back.setOnClickListener(View.OnClickListener {
            //requireActivity().supportFragmentManager.popBackStack()
            val intent = Intent(requireContext(), DashBoardActivity::class.java)
            startActivity(intent)
        })
        if (!NetworkUtils.isNetworkConnected(requireContext())) {
            UiUtils.showSnack(
                view, "no_internet_connection"
            )
        }

        if (sharedHelper!!.language == "ar"){
            back.rotation= 180F
        }

        activity?.onBackPressedDispatcher?.addCallback(this, object : OnBackPressedCallback(true) {
            override fun handleOnBackPressed() {
                // this for single back press
                // isEnabled = false
                val intent = Intent(requireContext(), DashBoardActivity::class.java)
                startActivity(intent)
            }
        })


        Log.d("language",""+sharedHelper?.language)
        if ( sharedHelper?.language .equals("en")) {
            customSwitch1.setChecked(false)
            Log.d("enfldv1", "xvxvxc")
        } else {
            customSwitch1.setChecked(true)
            Log.d("enfld2", "xvxvxc")
        }


        if (sharedHelper!!.isnotify.equals("A")){
            customSwitch2.isChecked=true
        }else{
            customSwitch2.isChecked=false
        }
        customSwitch1.setOnCheckedChangeListener(CompoundButton.OnCheckedChangeListener { buttonView, isChecked ->
            if (isChecked) {
                Log.d("enfldv3", "xvxvxc")
                langcode = "ar"
                sharedHelper?.language = "ar"
             //  setPhoneLanguage()
                reloadApp()
            } else {
                Log.d("enfldv4", "xvxvxc")
                langcode = "en"
                sharedHelper?.language = "en"
              //  setPhoneLanguage()
                reloadApp()
            }
        })



        if (sharedHelper!!.choose.equals("finedine")||sharedHelper!!.choose.equals("inqueue")|| sharedHelper!!.cartcount == 0){

            //var cartframe: FrameLayout =findViewById(R.id.cartframe)
            cartframe.visibility=View.GONE
        }

        Log.d("ertyu",""+customSwitch2.isChecked)

        customSwitch2.setOnCheckedChangeListener(CompoundButton.OnCheckedChangeListener { buttonView, isChecked ->
            if (isChecked) {
                getreview("A")
            } else {
                getreview("I")


            }
        })


    }


    private fun setPhoneLanguage() {
        val res = resources
        val conf = res.configuration
        val locale =
            Locale(sharedHelper?.language?.toLowerCase()!!)
        Locale.setDefault(locale)
        conf.setLocale(locale)
        requireActivity().createConfigurationContext(conf)
        val dm = res.displayMetrics
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
            conf.setLocales(LocaleList(locale))
        } else {
            conf.locale = locale
        }
        res.updateConfiguration(conf, dm)
        reloadApp()
    }
    private fun reloadApp() {

        val intent = Intent(requireContext(), DashBoardActivity::class.java)
        intent.putExtra("langfrom", "settings")
        startActivity(intent)

     /*   settingFragment = SettingFragment()
        fragmentTransaction =requireActivity().supportFragmentManager.beginTransaction()
        fragmentTransaction.setCustomAnimations(R.anim.screen_in, R.anim.screen_out)
        fragmentTransaction.replace(R.id.container, settingFragment!!)
        fragmentTransaction.addToBackStack(null)
        fragmentTransaction.commit()*/
    }



    private fun getreview(status: String) {
        DialogUtils.showLoader(requireContext())
        mapViewModel?.notifystatus(requireContext(), status)
            ?.observe(viewLifecycleOwner,
                androidx.lifecycle.Observer {
                    DialogUtils.dismissLoader()
                    it?.let {
                        it.error?.let { error ->
                            if (error) {
                                it.message?.let { msg ->
                                UiUtils.showSnack(settings,msg)
                                }
                            } else {

                                it.data?.let { data ->
                                    sharedHelper!!.isnotify= data.notify_status!!

                                }
                            }
                        }

                    }
                })
    }

}