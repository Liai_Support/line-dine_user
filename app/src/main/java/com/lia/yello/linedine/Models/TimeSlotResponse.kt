package com.lia.yello.linedine.Models

import com.google.gson.annotations.SerializedName
import java.io.Serializable

class TimeSlotResponse: Serializable {


    @SerializedName("error")
    var error: Boolean? = null

    @SerializedName("message")
    var message: String? = null

    @SerializedName("data")
    var data: ArrayList<ListTime>? = null
}

class ListTime : Serializable {

    @SerializedName("time")
    var time: String? = null

}