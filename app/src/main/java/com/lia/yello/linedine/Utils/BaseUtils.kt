package com.lia.yello.linedine.Utils

import android.util.Log
import android.util.Patterns
import android.view.View
import com.google.android.material.snackbar.Snackbar
import java.text.ParseException
import java.text.SimpleDateFormat
import java.util.*

object BaseUtils {
    fun isValidEmail(email: String): Boolean {
        return Patterns.EMAIL_ADDRESS.matcher(email).matches()
    }

    fun numberFormat(value: Int): String {
        return "%02d".format(value)
    }

    fun numberFormat(value: Double): String {
        return String.format("%.2f", value)
    }

/*
    fun getTimeFromTimeStamp(value: String): String {

        var date = Date(value.toLong())
        val simpleDateFormat = SimpleDateFormat("hh:mm aa", Locale.ENGLISH)

        return simpleDateFormat.format(date)

    }

    fun getUtcTime(date: String, dateFormat: String): String {

        var spf = SimpleDateFormat(dateFormat, Locale.ENGLISH)
        var newDate: Date? = null
        try {
            newDate = spf.parse(date)
        } catch (e: ParseException1) {
            e.printStackTrace()
        }

        newDate?.let {
            var simpleDateFormat = SimpleDateFormat(dateFormat, Locale.ENGLISH)
            simpleDateFormat.timeZone = TimeZone.getTimeZone("UTC")
            return simpleDateFormat.format(newDate)
        }

        return ""
    }



    fun getFormatedDate(date: String, inputFormat: String, outputFormat: String): String {

        var spf = SimpleDateFormat(inputFormat, Locale.ENGLISH)
        var newDate: Date? = null
        try {
            newDate = spf.parse(date)
        } catch (e: ParseException1) {
            e.printStackTrace()
        }


        spf = SimpleDateFormat(outputFormat, Locale.ENGLISH)
        return if (newDate != null)
            spf.format(newDate)
        else
            ""

    }*/

    fun getFormatedDateUtc(
        date: String,
        inputFormat: String?,
        outputFormat: String?
    ): String? {
        var spf = SimpleDateFormat(inputFormat, Locale.ENGLISH)
        spf.timeZone = TimeZone.getTimeZone("UTC")
        var newDate: Date? = null
        try {
            newDate = spf.parse(date)


        } catch (e: ParseException) {
            e.printStackTrace()

        }


            spf = SimpleDateFormat(outputFormat, Locale.ENGLISH)

          Log.d("kakakak",""+spf.format(newDate))
           return spf.format(newDate)

    }

}




