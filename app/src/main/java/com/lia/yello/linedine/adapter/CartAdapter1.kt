package com.lia.yello.linedine.adapter

import android.content.Context
import android.os.Handler
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import androidx.lifecycle.Observer
import androidx.recyclerview.widget.RecyclerView
import com.lia.yello.linedine.Models.orderlist
import com.lia.yello.linedine.R
import com.lia.yello.linedine.Utils.DialogUtils
import com.lia.yello.linedine.Utils.UiUtils
import com.lia.yello.linedine.activity.OnSwipeTouchListener
import com.lia.yello.linedine.databinding.CardSubordersBinding
import com.lia.yello.linedine.fragment.SwipeDeleteFragment

class CartAdapter1(
    var cartFragment: SwipeDeleteFragment,
    var context: Context,
    var list: ArrayList<orderlist>?
) :
    RecyclerView.Adapter<CartAdapter1.HomeHeaderViewHolder>() {
    var count: Int = 1
    var id: Int = 1

    inner class HomeHeaderViewHolder(view: View) : RecyclerView.ViewHolder(view) {
        var binding: CardSubordersBinding = CardSubordersBinding.bind(view)

    }


    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): HomeHeaderViewHolder {
        return HomeHeaderViewHolder(
            LayoutInflater.from(context).inflate(
                R.layout.card_suborders,
                parent,
                false
            )
        )
    }

    override fun getItemCount(): Int {
        return list!!.size
    }


    override fun onBindViewHolder(holder: HomeHeaderViewHolder, position: Int) {

        Log.d("dgrfdgdg", "" + list!!.size)


        /*     if(list!![position].menuavilable==0){
            holder.binding.rootcartadapter.alpha=0.5f
            holder.binding.delclick.alpha=0.5f
            holder.binding.imgminus.isEnabled=false
            holder.binding.imgplus.isEnabled=false
            holder.binding.delclick.isEnabled=false
            holder.binding.carddel.isEnabled=false
            holder.binding.title.text = list!![position].menuname.toString()
            holder.binding.cost.text = list!![position].cost.toString() + " SAR"
            holder.binding.txtquantity.text = list!![position].quantity.toString()
            list!![position].menu_images?.let { imageUrl ->
                UiUtils.loadImage(holder.binding.menuimage, imageUrl)
            }

            holder.binding.delete.setOnClickListener(View.OnClickListener {
                DialogUtils.showLoader(context)

                cartFragment.mapViewModel?.getdelete(context, list!![position].id!!)
                    ?.observe(cartFragment, androidx.lifecycle.Observer {
                        DialogUtils.dismissLoader()

                        it?.let {
                            it.error?.let { error ->
                                if (error) {
                                    it.message?.let { msg ->

                                        Log.d("zxcvfgbnm,", "" + msg)
                                        UiUtils.showSnack(holder.binding.carddel,msg)

                                    }
                                } else {

                                    it.message?.let { msg ->
                                        cartFragment.subtotal = 0.0
                                        cartFragment.vat = 0.0
                                        cartFragment.totalamt = 0.0
                                        Log.d("kanika,", "" + msg)
                                        cartFragment.stocknotavailable=false
                                        cartFragment.getcartlist()

                                    }


                                }
                            }
                        }

                    })

            })


            holder.binding.carddel.setOnClickListener(View.OnClickListener {
                holder.binding.carddel2.visibility = View.VISIBLE
                holder.binding.carddel.visibility = View.INVISIBLE
                val param = holder.binding.lin.layoutParams as ViewGroup.MarginLayoutParams
                param.setMargins(0,0,75,0)
                holder.binding.lin.layoutParams = param
            })

            holder.binding.delclick.setOnClickListener(View.OnClickListener {
                holder.binding.carddel2.visibility = View.GONE
                holder.binding.carddel.visibility = View.VISIBLE

                val param = holder.binding.lin.layoutParams as ViewGroup.MarginLayoutParams
                param.setMargins(0,0,0,0)
                holder.binding.lin.layoutParams = param
            })
            holder.binding.rootcartadapter.setOnTouchListener(object : OnSwipeTouchListener(cartFragment.requireActivity()) {
                override fun onSwipeLeft() {
                    super.onSwipeLeft()
                    *//*  Toast.makeText(context, "Swipe Left gesture detected",
                             Toast.LENGTH_SHORT)
                             .show()*//*
                    holder.binding.carddel2.visibility = View.VISIBLE
                    val param = holder.binding.lin.layoutParams as ViewGroup.MarginLayoutParams
                    param.setMargins(0,0,75,0)
                    holder.binding.lin.layoutParams = param
                }

                override fun onSwipeRight() {
                    super.onSwipeRight()
                    *//*Toast.makeText(
                            context,
                            "Swipe Right gesture detected",
                            Toast.LENGTH_SHORT
                    ).show()*//*
                    holder.binding.carddel2.visibility = View.GONE
                    val param = holder.binding.lin.layoutParams as ViewGroup.MarginLayoutParams
                    param.setMargins(0,0,0,0)
                    holder.binding.lin.layoutParams = param
                }
            })
            holder.binding.rootcartadapter.setOnClickListener(View.OnClickListener {
                UiUtils.showSnack( holder.binding.rootcartadapter,context.getString(R.string.menuisnotavailable))

            })

        }
        else{
            cartFragment.placeorderboolean = true
            holder.binding.title.text = list!![position].menuname.toString()
            holder.binding.cost.text = list!![position].cost.toString() + " SAR"
            holder.binding.txtquantity.text = list!![position].quantity.toString()

            list!![position].menu_images?.let { imageUrl ->
                UiUtils.loadImage(holder.binding.menuimage, imageUrl)
            }

            holder.binding.imgminus.setOnClickListener(View.OnClickListener {
                Log.d("asdfghjkm,l.", "zxcgfvhbjnkml,.")

                var count2: Int = list!![position].quantity!!
                holder.binding.txtquantity.setText("" + count2)
                if (count2 <= 1) {
                    *//*  count2 = 1
                    holder.binding.txtquantity.setText("" + count2)*//*
                    //just show the snack bar only
                    //  UiUtils.showSnack(holder.binding.imgminus,"")
                    UiUtils.showSnack(holder.binding.imgminus,context.getString(R.string.itemcannotbelessthan1))
                } else {
                    *//*  count2--
                    subOrderFragment.subtotal = subOrderFragment.subtotal-list!![position].cost!!
                    subOrderFragment.vat = (subOrderFragment.subtotal/100.0f)*56.23
                    subOrderFragment.totalamt =  subOrderFragment.subtotal+subOrderFragment.vat

                    subOrderFragment.ordersubtotal.setText(""+subOrderFragment.subtotal)
                    subOrderFragment.vat_txt.setText(""+subOrderFragment.vat)
                    subOrderFragment.totalmount.setText(""+subOrderFragment.totalamt)
                    holder.binding.txtquantity.setText("" + count2)*//*
                    //just call update cart and in response do as same delete module

                    // updatecart

                    holder.binding.imgminus.isEnabled = false

                    Handler().postDelayed({
                        holder.binding.imgminus.isEnabled = true
                    }, 1000)
                    getupdatecart(
                        list!![position].id!!,
                        list!![position].quantity!!-1,
                        holder.binding.imgplus
                    )


                }
            })
            holder.binding.imgplus.setOnClickListener(View.OnClickListener {
                Log.d("asdfghjkm,l.", "zxcgfvhbjnkml,.")

                *//* var count2 : Int = list!![position].quantity!!
                count2++
                subOrderFragment.subtotal =  subOrderFragment.subtotal+list!![position].cost!!
                subOrderFragment.vat = (subOrderFragment.subtotal/100.0f)*56.23
                subOrderFragment.totalamt =  subOrderFragment.subtotal+subOrderFragment.vat

                subOrderFragment.ordersubtotal.setText(""+subOrderFragment.subtotal)
                subOrderFragment.vat_txt.setText(""+subOrderFragment.vat)
                subOrderFragment.totalmount.setText(""+subOrderFragment.totalamt)
                holder.binding.txtquantity.setText("" + count2)*//*
                //just call update cart and in response do as same delete module


                holder.binding.imgplus.isEnabled = false

                Handler().postDelayed({
                    holder.binding.imgplus.isEnabled = true
                }, 1000)

                getupdatecart(list!![position].id!!, list!![position].quantity!!+1,holder.binding.imgplus)

            })

            holder.binding.delete.setOnClickListener(View.OnClickListener {
                DialogUtils.showLoader(context)

                cartFragment.mapViewModel?.getdelete(context, list!![position].id!!)
                    ?.observe(cartFragment, androidx.lifecycle.Observer {
                        DialogUtils.dismissLoader()

                        it?.let {
                            it.error?.let { error ->
                                if (error) {
                                    it.message?.let { msg ->

                                        Log.d("zxcvfgbnm,", "" + msg)
                                        UiUtils.showSnack(holder.binding.carddel,msg)

                                    }
                                } else {

                                    it.message?.let { msg ->
                                        cartFragment.subtotal = 0.0
                                        cartFragment.vat = 0.0
                                        cartFragment.totalamt = 0.0
                                        Log.d("kanika,", "" + msg)
                                        cartFragment.getcartlist()

                                    }


                                }
                            }
                        }

                    })

            })


            holder.binding.carddel.setOnClickListener(View.OnClickListener {
                holder.binding.carddel2.visibility = View.VISIBLE
                holder.binding.carddel.visibility = View.INVISIBLE
                val param = holder.binding.lin.layoutParams as ViewGroup.MarginLayoutParams
                param.setMargins(0,0,75,0)
                holder.binding.lin.layoutParams = param
            })

            holder.binding.delclick.setOnClickListener(View.OnClickListener {
                holder.binding.carddel2.visibility = View.GONE
                holder.binding.carddel.visibility = View.VISIBLE

                val param = holder.binding.lin.layoutParams as ViewGroup.MarginLayoutParams
                param.setMargins(0,0,0,0)
                holder.binding.lin.layoutParams = param
            })


            holder.binding.rootcartadapter.setOnTouchListener(object : OnSwipeTouchListener(cartFragment.requireActivity()) {
                override fun onSwipeLeft() {
                    super.onSwipeLeft()
                    *//*  Toast.makeText(context, "Swipe Left gesture detected",
                             Toast.LENGTH_SHORT)
                             .show()*//*
                    holder.binding.carddel2.visibility = View.VISIBLE
                    val param = holder.binding.lin.layoutParams as ViewGroup.MarginLayoutParams
                    param.setMargins(0,0,75,0)
                    holder.binding.lin.layoutParams = param
                }

                override fun onSwipeRight() {
                    super.onSwipeRight()
                    *//*Toast.makeText(
                            context,
                            "Swipe Right gesture detected",
                            Toast.LENGTH_SHORT
                    ).show()*//*
                    holder.binding.carddel2.visibility = View.GONE
                    val param = holder.binding.lin.layoutParams as ViewGroup.MarginLayoutParams
                    param.setMargins(0,0,0,0)
                    holder.binding.lin.layoutParams = param
                }
            })



        }




    }


    public fun getupdatecart(id: Int, qty: Int, imgplus: ImageView){
        DialogUtils.showLoader(context)

        cartFragment.mapViewModel?.getupdatecart(context,id,qty,"")
            ?.observe(cartFragment, Observer {
                DialogUtils.dismissLoader()

                it?.let {
                    it.error?.let { error ->
                        if (error) {
                            it.message?.let { msg ->
                                //UiUtils.showSnack(root, msg)
                                // snackbarToast(msg)
                                //Toast.makeText(context, msg, Toast.LENGTH_SHORT).show()

                            }
                        } else {
                            it.message?.let { msg ->
                                // Toast.makeText(context, msg, Toast.LENGTH_SHORT).show()

                                cartFragment.subtotal = 0.0
                                cartFragment.vat = 0.0
                                cartFragment.totalamt = 0.0
                                Log.d("kanika,", "" + msg)
                                cartFragment.getcartlist()




                            }
                        }
                    }

                }
            })



    }*/


    }

}




