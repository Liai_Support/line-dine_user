package com.lia.yello.linedine.interfaces

interface OnClickListener {
    fun onClickItem(position: Int)
}
