package com.lia.yello.linedine.interfaces

interface DialogCallBack {
    fun onPositiveClick()
    fun onNegativeClick()
}