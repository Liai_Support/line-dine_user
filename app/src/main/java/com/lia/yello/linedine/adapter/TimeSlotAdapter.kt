package com.lia.yello.linedine.adapter

import android.content.Context
import android.content.res.Resources
import android.graphics.Color
import android.graphics.PorterDuff
import android.os.Build
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.LinearLayout
import android.widget.TextView
import androidx.annotation.RequiresApi
import androidx.core.content.ContextCompat
import androidx.core.content.ContextCompat.getColor
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentTransaction
import androidx.recyclerview.widget.RecyclerView
import com.google.android.material.bottomsheet.BottomSheetBehavior
import com.lia.yello.linedine.Models.ListDay
import com.lia.yello.linedine.Models.ListTime
import com.lia.yello.linedine.Models.listfav
import com.lia.yello.linedine.R
import com.lia.yello.linedine.Utils.UiUtils
import com.lia.yello.linedine.databinding.*

import com.lia.yello.linedine.fragment.FavoritesFragment
import com.lia.yello.linedine.fragment.FineDineFragment1
import kotlinx.android.synthetic.main.fragment_fine_dine2.*
import java.text.ParseException
import java.text.SimpleDateFormat
import java.util.*
import kotlin.collections.ArrayList

class TimeSlotAdapter(
    var fineDineFragment1: FineDineFragment1,
    var context: Context,
    var list: ArrayList<ListTime>?
) :
    RecyclerView.Adapter<TimeSlotAdapter.HomeHeaderViewHolder>() {

    var row_index:Int=-1
    inner class HomeHeaderViewHolder(view: View) : RecyclerView.ViewHolder(view) {

        var binding: TimeslotBinding = TimeslotBinding.bind(view)
/*
        lateinit var linr_time_slot:LinearLayout
        lateinit var timeslot:TextView
        lateinit var imgtable:ImageView
        lateinit var imgtable2:ImageView

        init {
            linr_time_slot=view.findViewById(R.id.linr_time_slot)
            timeslot=view.findViewById(R.id.time_slot)
            imgtable=view.findViewById(R.id.imgtable)
            imgtable2=view.findViewById(R.id.imgtable2)
        }*/

    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): HomeHeaderViewHolder {
        return HomeHeaderViewHolder(
            LayoutInflater.from(context).inflate(
                R.layout.timeslot,
                parent,
                false
            )
        )
    }

    override fun getItemCount(): Int {
        return list!!.size
    }

    @RequiresApi(Build.VERSION_CODES.M)
    override fun onBindViewHolder(holder: HomeHeaderViewHolder, position: Int) {

        holder.binding.timeSlot.text= list!![position].time
        fineDineFragment1.timee= list!![position].time!!

        if(fineDineFragment1.istoday == true){
            val str = list!![position].time
            val time = ""+str!![0]+str!![1]+":"+str!![3]+str!![4]
            Log.d("xgvcx",""+time)
            val currentDate = SimpleDateFormat("yyyy/MM/dd", Locale.getDefault()).format(Calendar.getInstance().time)
            var datetime = "$currentDate"+" "+time
            if(isValidTime(datetime, "yyyy/MM/dd HH:mm")){
                holder.binding.linrTimeSlot.setOnClickListener {
                    fineDineFragment1.recycler_time.alpha=1.0f
                    fineDineFragment1.recycler_time.isEnabled=true
                    fineDineFragment1.timeval= list!![position].time.toString()
                    Log.d("werfdcgthynjm",""+list!![position].time.toString())
                    row_index=position;
                    notifyDataSetChanged();
                }
            }
            else{
                holder.binding.linrTimeSlot.setOnClickListener {
                    UiUtils.showSnack( holder.binding.linrTimeSlot,context.getString(R.string.timeexpired))
                }
            }
        }
        else{
            holder.binding.linrTimeSlot.setOnClickListener {
                fineDineFragment1.recycler_time.alpha=1.0f
                fineDineFragment1.recycler_time.isEnabled=true
                fineDineFragment1.timeval= list!![position].time.toString()
                Log.d("werfdcgthynjm",""+list!![position].time.toString())
                row_index=position;
                notifyDataSetChanged();
            }
        }


        if(row_index==position){

            holder.binding.linrTimeSlot.setBackgroundColor(Color.parseColor("#000000"));
            holder.binding.timeSlot.setTextColor(Color.parseColor("#ffffff"));
            // holder.imgtable.setColorFilter(R.color.white)
            holder.binding.imgtable2.visibility=View.VISIBLE
            holder.binding.imgtable.visibility=View.GONE

        }
        else {
            holder.binding.linrTimeSlot.setBackgroundColor(Color.parseColor("#ffffff"));
            holder.binding.timeSlot.setTextColor(Color.parseColor("#000000"));
            holder.binding.imgtable.setColorFilter(R.color.grey1)
            holder.binding.imgtable2.visibility=View.GONE
            holder.binding.imgtable.visibility=View.VISIBLE

        }

      }

    fun isValidTime(date: String, format: String): Boolean {

        Log.d("ndv1",""+date)
        Log.d("ndv2",""+format)


        var spf = SimpleDateFormat(format, Locale.ENGLISH)
        var newDate: Date? = null
        try {
            newDate = spf.parse(date)
        } catch (e: ParseException) {
            e.printStackTrace()
        }

        var secondsInMilli = 1000
        var minutesInMilli = secondsInMilli * 60

        var currentDate: Date = Calendar.getInstance().time
        var currentString = spf.format(currentDate)

        var curretDate: Date? = null
        try {
            curretDate = spf.parse(currentString)
        } catch (e: ParseException) {
            e.printStackTrace()
        }

        Log.d("ndv4",""+newDate)
        Log.d("ndv5",""+currentDate)


        newDate?.let { nDate ->
            curretDate?.let { cDate ->

                var diff = nDate.time - cDate.time
                var elapsedMinutes: Int = (diff / minutesInMilli).toInt()

                Log.d("diff", elapsedMinutes.toString())


                return elapsedMinutes >= 0


            }
        }

        return false
    }



}
