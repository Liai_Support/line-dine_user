package com.lia.yello.linedine

import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import android.telephony.SmsMessage
import android.util.Log
import java.util.regex.Matcher
import java.util.regex.Pattern


class SmsReceiver : BroadcastReceiver() {
    override fun onReceive(context: Context?, intent: Intent) {
        val data = intent.extras
        val pdus = data!!["pdus"] as Array<Any>?
        for (i in pdus!!.indices) {
            val smsMessage: SmsMessage = SmsMessage.createFromPdu(pdus[i] as ByteArray)

            //String sender = smsMessage.getDisplayOriginatingAddress();
            //You must check here if the sender is your provider and not another one with same text.
            val messageBody: String = smsMessage.getMessageBody()

            //Pass on the text to our listener.
            mListener?.messageReceived(parseCode(messageBody))
            Log.d("dvd",""+parseCode(messageBody));
        }
    }

    private fun parseCode(message: String): String {
        val p: Pattern = Pattern.compile("\\b\\d{4}\\b")
        val m: Matcher = p.matcher(message)
        var code = ""
        while (m.find()) {
            code = m.group(0)
        }
        return code
    }

    companion object {
        private var mListener: SmsListener? = null
        fun bindListener(listener: SmsListener?) {
            mListener = listener
        }
    }
}