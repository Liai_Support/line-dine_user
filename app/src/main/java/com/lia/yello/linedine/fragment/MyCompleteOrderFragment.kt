package com.lia.yello.linedine.fragment

import android.app.Activity
import android.app.AlertDialog
import android.content.Context
import android.content.Intent
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.os.Bundle
import android.text.Editable
import android.text.TextWatcher
import android.util.DisplayMetrics
import android.util.Log
import android.view.Gravity
import android.view.LayoutInflater
import androidx.fragment.app.Fragment
import android.view.View
import android.view.WindowManager
import android.view.inputmethod.InputMethodManager
import android.widget.Button
import android.widget.LinearLayout
import android.widget.RelativeLayout
import android.widget.TextView
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.FragmentTransaction
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.lia.yello.linedine.R
import com.lia.yello.linedine.Session.SharedHelper
import com.lia.yello.linedine.Utils.DialogUtils
import com.lia.yello.linedine.Utils.NetworkUtils
import com.lia.yello.linedine.Utils.UiUtils
import com.lia.yello.linedine.ViewModels.MapViewModel
import com.lia.yello.linedine.activity.NoonPaymentActivity
import com.lia.yello.linedine.adapter.MyCompleteOrdersAdapter
import com.lia.yello.linedine.databinding.FragmentMyActiveOrdersBinding
import kotlinx.android.synthetic.main.carticon.*
import kotlinx.android.synthetic.main.fragment_fine_dine3.*
import kotlinx.android.synthetic.main.fragment_my_active_orders.*

import kotlinx.android.synthetic.main.fragment_my_active_orders.discountlevel
import kotlinx.android.synthetic.main.fragment_order_processing.*
import kotlinx.android.synthetic.main.fragment_profile.*
import kotlinx.android.synthetic.main.fragment_suborders.*
import kotlinx.android.synthetic.main.header.*
import kotlinx.android.synthetic.main.placeorder.view.*
import java.util.*


class MyCompleteOrderFragment : Fragment(R.layout.fragment_my_active_orders) {

    var binding: FragmentMyActiveOrdersBinding? = null
    var mapViewModel: MapViewModel? = null
    lateinit var adapter: RecyclerView.Adapter<RecyclerView.ViewHolder>
    lateinit var layoutManager: RecyclerView.LayoutManager
    var orderid:Int=0
    lateinit var ordersubtotal:TextView
    lateinit var vat_txt:TextView
    lateinit var totalmount:TextView
    lateinit var discountreorder:TextView
    lateinit var discountlevel:RelativeLayout
    var totalamountt:Double = 0.0
    var status: String=""
    var menuid:Int=0
    lateinit var bot:LinearLayout
    lateinit var reorder:Button
    lateinit var review:Button
    public var sharedHelper: SharedHelper? = null
    var subtotalcount:Double=0.0
    var subtotalcount_total:Double=0.0
    var vattocal:Double=0.0
    var promocode:String=""
    var restuarntid:Int=0
    var minimumanount:Double=0.0
    var total:Double=0.0
    var discoumtamtt:Double=0.0

    var promocodelist: MutableList<String> = mutableListOf<String>()
    var uptoamount :Double=0.0

    var discountid:Int=0
    var diss:Double=0.0
    var tmt:Double=0.0

    private lateinit var fragmentTransaction: FragmentTransaction
    private var reviewFragment: ReviewFragment? = null

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        binding = DataBindingUtil.findBinding(view)
        sharedHelper = SharedHelper(requireContext())

        mapViewModel = ViewModelProvider(this).get(MapViewModel::class.java)
        val head = view.findViewById<TextView>(R.id.head)
        ordersubtotal = view.findViewById<TextView>(R.id.ordersubtotal)
        vat_txt = view.findViewById<TextView>(R.id.vat_txt)
        totalmount = view.findViewById<TextView>(R.id.totalmount)
        bot = view.findViewById<LinearLayout>(R.id.bot)
        reorder = view.findViewById<Button>(R.id.reorder)
        review = view.findViewById<Button>(R.id.review)
        discountreorder = view.findViewById<Button>(R.id.discountreorder)
        discountlevel = view.findViewById<RelativeLayout>(R.id.discountlevel)
        // linaer_activeorder2 = view.findViewById<LinearLayout>(R.id.linaer_activeorder2)
        if (!NetworkUtils.isNetworkConnected(requireContext())) {
            UiUtils.showSnack(
                rootmyactiveorders, "no_internet_connection"
            )
        }

        if (sharedHelper!!.language == "ar"){
            back.rotation= 180F
        }
        head.setText(getString(R.string.mycompleteorders))
        cart_badge.text = ""+ sharedHelper!!.cartcount

        back.setOnClickListener(View.OnClickListener {
            requireActivity().supportFragmentManager.popBackStack()
        })
        orderid = arguments?.getInt("orderid")!!
        status = arguments?.getString("status")!!
        Log.d("klklk",""+status)

        if (status.equals("cancelled")){
            couponcodereorder.visibility=View.GONE
            applycounponcodeee.visibility=View.GONE
        }else if (status.equals("delivered")){
            couponcodereorder.visibility=View.GONE
            applycounponcodeee.visibility=View.GONE
        }
        Log.d("wedrfghnjm,",""+orderid)

        reorder.setOnClickListener(View.OnClickListener {


            val mDialogView = LayoutInflater.from(requireContext()).inflate(R.layout.placeorder, null)
            val mBuilder = AlertDialog.Builder(requireContext()).setView(mDialogView).setCancelable(true)
            val mAlertDialog = mBuilder.show()
            val layoutParams = WindowManager.LayoutParams()
            val displayMetrics = DisplayMetrics()
            requireActivity().windowManager.defaultDisplay.getMetrics(displayMetrics)
            val displayWidth = displayMetrics.widthPixels
            val displayHeight = displayMetrics.heightPixels
            layoutParams.copyFrom(mAlertDialog.getWindow()?.getAttributes())
            layoutParams.width = ((displayWidth * 0.9f).toInt())
            mAlertDialog.getWindow()?.setAttributes(layoutParams)
            mAlertDialog.getWindow()?.setGravity(Gravity.CENTER)
            mAlertDialog.getWindow()?.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
            mDialogView.checkoutcard.setOnClickListener {
                startActivityForResult(
                    Intent(requireContext(), NoonPaymentActivity::class.java)
                        .putExtra(
                            "amount",
                            totalamountt
                        ), 100
                )
                mAlertDialog.dismiss()

            }
            mDialogView.checkoutwallet.setOnClickListener {
                getwalletamount(totalamountt)
                mAlertDialog.dismiss()

            }
            //getreorder()
        })

        if (sharedHelper!!.choose.equals("finedine")||sharedHelper!!.choose.equals("inqueue")|| sharedHelper!!.cartcount == 0){

            //var cartframe: FrameLayout =findViewById(R.id.cartframe)
            cartframe.visibility=View.GONE
        }
        review.setOnClickListener(View.OnClickListener {

            val bundle = Bundle()
            bundle.putInt("menuid",menuid )
            bundle.putInt("orderid",orderid )
            reviewFragment = ReviewFragment()
            reviewFragment!!.arguments = bundle
            fragmentTransaction = requireActivity().supportFragmentManager.beginTransaction()
            fragmentTransaction.setCustomAnimations(R.anim.screen_in, R.anim.screen_out)
            fragmentTransaction.replace(R.id.container, reviewFragment!!)
            fragmentTransaction.addToBackStack(null)
            fragmentTransaction.commit()

        })



        couponcodereorder.addTextChangedListener(object : TextWatcher {
            override fun beforeTextChanged(s: CharSequence, start: Int, count: Int, after: Int) {

            }

            override fun onTextChanged(s: CharSequence, start: Int, before: Int, count: Int) {
            }

            override fun afterTextChanged(s: Editable) {


                promocode=couponcodereorder.text.toString()
                Log.d("kanikakjakajk",""+promocode)
            }
        })

        applycouponreorder.setOnClickListener {
            for (i in 0 until promocodelist.size){
                Log.d("sdfghjkl",promocodelist[i] )
                Log.d("xcvbncxvb",promocode )
                val promo=promocodelist[i]







            }



            if (promocode.equals("")){
                UiUtils.showSnack(rootmyactiveorders,getString(R.string.pleaseenterthecouponcode))

            }else{
                if (total>minimumanount){
                    getvalidateoffer(promocode,restuarntid)



                }else{
                    val imm: InputMethodManager =
                        activity!!.getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
                    imm.hideSoftInputFromWindow(view!!.windowToken, 0)
                    UiUtils.showSnack(rootmyactiveorders,getString(R.string.minimummorderamountislow))
                }
            }




        }




        getpending()


    }

    private fun getpending(){
        DialogUtils.showLoader(requireContext())

        mapViewModel?.getplaceorderlist(requireContext(),status)
            ?.observe(viewLifecycleOwner,
                Observer {
                    DialogUtils.dismissLoader()

                    it?.let {
                        it.error?.let { error ->
                            if (error) {
                                it.message?.let { msg ->
                                    UiUtils.showSnack(rootmyactiveorders, msg)
                                }
                            } else {
                                it.data?.let { data ->

                                    for (i in 0 until data!!.size) {

                                        if (data[i].id!! ==  orderid) {

                                            if (data[i].discount_amount=="0"){
                                                discountlevel.visibility=View.GONE
                                            }else{
                                                discoumtamtt=data[i].discount_amount!!.toDouble()

                                                discountlevel.visibility=View.VISIBLE

                                                discountreorder.text =" - " +String.format(Locale("en", "US"),"%.2f", data[i].discount_amount!!.toDouble())+ " SAR"

                                            }
                                            Log.d("qwedrfghjnm,",""+menuid)

                                            Log.d("qwedrfghjnm,",""+data[i].subtotal.toString()+ " SAR")
                                            if (data[i].reviewed.equals("true")){
                                                Log.d("ertyu,","kmkmik")

                                                review.visibility=View.INVISIBLE
                                            }else{
                                                Log.d("kkkk,","kkkkk")

                                                review.visibility=View.VISIBLE

                                            }

                                            vattocal=data[i].vat!!
                                            vat_txt.text =String.format(Locale("en", "US"),"%.2f", data[i].vat)+ " SAR"
                                            //   ordersubtotal.text =String.format(Locale("en", "US"),"%.2f", data[i].subtotal)+ " SAR"
                                            // vat_txt.text =String.format(Locale("en", "US"),"%.2f", data[i].vat)+ " SAR"
                                            //   totalmount.text =String.format(Locale("en", "US"),"%.2f", data[i].amount)+ " SAR"
                                            /*  ordersubtotal.text =data[i].subtotal.toString()+ " SAR"
                                              vat_txt.text= data[i].vat.toString()+ " SAR"
                                              totalmount.text= data[i].amount.toString()+ " SAR"*/
                                            //  totalamountt= data[i].amount!!

                                            totalamountt=   String.format(Locale("en", "US"),"%.2f", data[i].amount).toDouble()
                                            /*  if(data[i].splins.equals("")){
                                                  splinss.text=getString(R.string.specialinstruction)
                                              }else{
                                                  splinss.text= data[i].splins

                                              }*/
                                            getlistoffers(data[i].res_id!!)
                                            restuarntid=data[i].res_id!!

                                            menurecycler.layoutManager =
                                                LinearLayoutManager(context)
                                            menurecycler.adapter = MyCompleteOrdersAdapter(
                                                this,
                                                requireContext(),
                                                data[i].menudetails
                                            )
                                        }

                                    }


                                }
                            }
                        }

                    }
                })
    }
    private fun getreorder(s: String, s1: String, s2: String) {

        Log.d("ryttyttytytuy",""+s)
        Log.d("tfggfhghg",""+s1)
        Log.d("hgjuhiio",""+s2)
        DialogUtils.showLoader(requireContext())

        mapViewModel?.getreorder(requireContext(),orderid,s,s1,s2)
            ?.observe(viewLifecycleOwner,
                Observer {
                    DialogUtils.dismissLoader()

                    it?.let {
                        it.error?.let { error ->
                            if (error) {
                                it.message?.let { msg ->
                                    UiUtils.showSnack(rootmyactiveorders,msg)
                                }
                            } else {
                                it.message?.let { msg ->
                                    UiUtils.showSnack(rootmyactiveorders,msg)
                                    var myOrderFragment: MyOrderFragment? = null
                                    lateinit var fragmentTransaction: FragmentTransaction
                                    myOrderFragment = MyOrderFragment()
                                    fragmentTransaction = requireActivity().supportFragmentManager.beginTransaction()
                                    fragmentTransaction.setCustomAnimations(R.anim.screen_in, R.anim.screen_out)
                                    fragmentTransaction.replace(R.id.container, myOrderFragment!!)
                                    fragmentTransaction.commit()

                                }

                            }
                        }

                    }
                })
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (requestCode == 100) {
            if (resultCode == Activity.RESULT_OK) {

                getreorder("card",data!!.getStringExtra("paymentorderid").toString(),data!!.getStringExtra("paymenttransactionid").toString())

            }
        }
    }



    private fun getwalletamount(totalamt: Double) {
        DialogUtils.showLoader(requireContext())

        mapViewModel?.getwalletamount(requireContext())
            ?.observe(viewLifecycleOwner, androidx.lifecycle.Observer {
                DialogUtils.dismissLoader()

                it?.let {
                    it.error?.let { error ->
                        if (error) {
                            it.message?.let { msg ->
                                UiUtils.showSnack(rootmyactiveorders, msg)
                            }
                        } else {
                            it.data?.let { data ->

                                //  getbookingpayment()
                                Log.d("wertyu",""+totalamt+">"+data.amount.toString())
                                if( data.amount!! >= totalamt){

                                    Log.d("ertyuiop","kanikmakak")
                                    getreorder("wallet","","")
                                }else{
                                    Log.d("ertyuiop","werftgy")

                                    UiUtils.showSnack(rootmyactiveorders, getString(R.string.yourwalletamountislowpleaseuplateyourwalletamount))


                                }

                            }
                        }

                    }
                }

            })
    }





    private fun getlistoffers(restuarntid:Int) {
        DialogUtils.showLoader(requireContext())

        mapViewModel?.getListoffers(requireContext(), restuarntid)
            ?.observe(viewLifecycleOwner, androidx.lifecycle.Observer {
                DialogUtils.dismissLoader()

                it?.let {
                    it.error?.let { error ->
                        if (error) {
                            it.message?.let { msg ->
                                //  UiUtils.showSnack(rootmyactiveorders, msg)
                            }
                        } else {
                            it.data?.let { data ->
                                for (i in 0 until data.size){
                                    promocodelist.add(data[i].promocode!!)
                                    minimumanount=data[i].minimum_order_amount!!.toDouble()
                                }
                                Log.d("promocodelist.size",""+promocodelist.size)

                            }

                        }
                    }

                }
            })

    }

    private fun getvalidateoffer(promocode: String,restuarntid:Int) {
        DialogUtils.showLoader(requireContext())

        mapViewModel?.getvalidateoffer(requireContext(), restuarntid,promocode)
            ?.observe(viewLifecycleOwner, androidx.lifecycle.Observer {
                DialogUtils.dismissLoader()

                it?.let {
                    it.error?.let { error ->
                        if (error) {
                            it.message?.let { msg ->

                                UiUtils.showSnack(rootmyactiveorders, msg)

                            }
                        }else {
                            it.data?.let { data ->
                                discountlevel.visibility=View.VISIBLE
                                UiUtils.showSnack(rootmyactiveorders,getString(R.string.promocodeapplied))
                                couponcodereorder.setEnabled(false)
                                applycouponreorder.setEnabled(false)
                                uptoamount=data[0].upto_amt!!.toDouble()

                                if (data[0].percentage.equals("0")){
                                    discountid= data[0].id!!
                                    // discountpickup.text= data[0].sar.toString()+" SAR"
                                    discountpickup.text= String.format(Locale("en", "US"),"%.2f",data[0].sar!!.toDouble())+" SAR"
                                    total -= data[0].sar!!.toDouble()
                                    totalmount.text= String.format(Locale("en", "US"),"%.2f",total) +" SAR"
                                }else{
                                    discountid= data[0].id!!
                                    val dis=data[0].percentage!!.toInt()

                                    diss=total * dis/100.0f
                                    tmt=total-diss


                                    if(total<=uptoamount){
                                        Log.d("wertyhujkl",""+dis)
                                        Log.d("wertyhujkl",""+diss)

                                        // discountpickup.text= diss.toString()+" SAR"
                                        discountreorder.text= String.format(Locale("en", "US"),"%.2f",diss.toDouble())+" SAR"

                                        total -= diss
                                        totalmount.text= String.format(Locale("en", "US"),"%.2f",total) +" SAR"


                                    }else{
                                        discountreorder.text= String.format(Locale("en", "US"),"%.2f",uptoamount)+" SAR"

                                        total -= uptoamount
                                        totalmount.text= String.format(Locale("en", "US"),"%.2f",total) +" SAR"


                                    }



                                }

                            }

                        }
                    }

                }
            })

    }
}