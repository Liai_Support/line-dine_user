package com.lia.yello.linedine.Repository

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import com.app.hakeemUser.models.SocialLoginResponse
import com.app.hakeemUser.models.SocialRegisterResponse

import com.google.gson.Gson
import com.lia.yello.linedine.Models.ForgotpasswordResponse
import com.lia.yello.linedine.Models.OtpResponseModel
import com.lia.yello.linedine.Models.UpdatePasswordResponse
import com.lia.yello.linedine.activity.LoginResponse
import com.lia.yello.linedine.activity.RegisterResponse
import org.json.JSONObject
import com.lia.yello.linedine.interfaces.ApiResponseCallback
import com.lia.yello.linedine.network.Api
import com.lia.yello.linedine.network.ApiInput


class RegisterRepository private constructor() {

    companion object {
        private var registerRepository: RegisterRepository? = null

        fun getInstance(): RegisterRepository {
            if (registerRepository == null) {
                registerRepository = RegisterRepository()
            }
            return registerRepository as RegisterRepository
        }
    }


    fun createAccount(input: ApiInput): LiveData<RegisterResponse>? {

        val apiResponse: MutableLiveData<RegisterResponse> = MutableLiveData()

        Api.postMethod(input, object : ApiResponseCallback {
            override fun setResponseSuccess(jsonObject: JSONObject) {
                val gson = Gson()
                var response: RegisterResponse =
                    gson.fromJson(jsonObject.toString(), RegisterResponse::class.java)
                apiResponse.value = response
            }

            override fun setErrorResponse(error: String) {
                var response = RegisterResponse()
                response.message = error
                response.error = true
                apiResponse.value = response
            }
        })

        return apiResponse
    }

    fun signIn(input: ApiInput): LiveData<LoginResponse>? {

        val apiResponse: MutableLiveData<LoginResponse> = MutableLiveData()

        Api.postMethod(input, object : ApiResponseCallback {
            override fun setResponseSuccess(jsonObject: JSONObject) {
                val gson = Gson()
                var response: LoginResponse =
                    gson.fromJson(jsonObject.toString(), LoginResponse::class.java)
                apiResponse.value = response
            }

            override fun setErrorResponse(error: String) {
                var response = LoginResponse()
                response.message = error
                response.error = true
               apiResponse.value = response
            }
        })

        return apiResponse
    }

    fun socialLogin(apiParams: ApiInput): LiveData<SocialLoginResponse>? {

        val apiResponse: MutableLiveData<SocialLoginResponse> = MutableLiveData()

        Api.postMethod(apiParams, object : ApiResponseCallback {
            override fun setResponseSuccess(jsonObject: JSONObject) {
                val gson = Gson()
                var response: SocialLoginResponse =
                    gson.fromJson(jsonObject.toString(), SocialLoginResponse::class.java)
                apiResponse.value = response
            }

            override fun setErrorResponse(error: String) {
                var response = SocialLoginResponse()
                response.message = error
                response.error = true
                apiResponse.value = response
            }
        })

        return apiResponse

    }

    fun socilaRegisterDetails(apiParams: ApiInput): LiveData<SocialRegisterResponse>? {
        val apiResponse: MutableLiveData<SocialRegisterResponse> = MutableLiveData()

        Api.postMethod(apiParams, object : ApiResponseCallback {
            override fun setResponseSuccess(jsonObject: JSONObject) {
                val gson = Gson()
                var response: SocialRegisterResponse =
                    gson.fromJson(jsonObject.toString(), SocialRegisterResponse::class.java)
                apiResponse.value = response
            }

            override fun setErrorResponse(error: String) {
                var response = SocialRegisterResponse()
                response.message = error
                response.error = true
                apiResponse.value = response
            }
        })

        return apiResponse
    }



    fun forgotpassword(input: ApiInput): LiveData<ForgotpasswordResponse>? {

        val apiResponse: MutableLiveData<ForgotpasswordResponse> = MutableLiveData()

        Api.postMethod(input, object : ApiResponseCallback {
            override fun setResponseSuccess(jsonObject: JSONObject) {
                val gson = Gson()
                var response: ForgotpasswordResponse =
                    gson.fromJson(jsonObject.toString(), ForgotpasswordResponse::class.java)
                apiResponse.value = response
            }

            override fun setErrorResponse(error: String) {
                var response = ForgotpasswordResponse()
                response.message = error
                response.error = true
                apiResponse.value = response
            }
        })

        return apiResponse
    }


    fun updatepassword(input: ApiInput): LiveData<UpdatePasswordResponse>? {

        val apiResponse: MutableLiveData<UpdatePasswordResponse> = MutableLiveData()

        Api.postMethod(input, object : ApiResponseCallback {
            override fun setResponseSuccess(jsonObject: JSONObject) {
                val gson = Gson()
                var response: UpdatePasswordResponse =
                    gson.fromJson(jsonObject.toString(), UpdatePasswordResponse::class.java)
                apiResponse.value = response
            }

            override fun setErrorResponse(error: String) {
                var response = UpdatePasswordResponse()
                response.message = error
                response.error = true
                apiResponse.value = response
            }
        })

        return apiResponse
    }
    fun getverifyotp(apiParams: ApiInput): MutableLiveData<OtpResponseModel> {
        val apiResponse: MutableLiveData<OtpResponseModel> = MutableLiveData()

        Api.postMethod(apiParams, object : ApiResponseCallback {
            override fun setResponseSuccess(jsonObject: JSONObject) {
                val gson = Gson()
                var response: OtpResponseModel =
                    gson.fromJson(jsonObject.toString(), OtpResponseModel::class.java)
                apiResponse.value = response
            }

            override fun setErrorResponse(error: String) {
                var response = OtpResponseModel()
                  response.error = true
                response.message = error
                apiResponse.value = response
            }
        })

        return apiResponse

    }


}