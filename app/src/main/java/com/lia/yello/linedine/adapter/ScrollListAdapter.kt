package com.lia.yello.linedine.adapter

import android.graphics.Color
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.LinearLayout
import android.widget.TextView
import androidx.cardview.widget.CardView
import androidx.recyclerview.widget.RecyclerView
import com.lia.yello.linedine.R
import com.lia.yello.linedine.databinding.ScrollistBinding
import com.lia.yello.linedine.fragment.SubHomeFragment

class ScrollListAdapter(
    var subHomeFragment: SubHomeFragment
) :
    RecyclerView.Adapter<ScrollListAdapter.MyViweHolder>() {
    var intt: Int = 0
    val fname= arrayOf(subHomeFragment.getString(R.string.all),subHomeFragment.getString(R.string.trending),subHomeFragment.getString(R.string.mostpopular),subHomeFragment.getString(R.string.favorites))
    val pics= intArrayOf(R.drawable.trending,R.drawable.heart,R.drawable.trending,R.drawable.parchment)
    var row_index:Int=-1



    class MyViweHolder(view: View) : RecyclerView.ViewHolder(view) {

        var binding: ScrollistBinding = ScrollistBinding.bind(view)

        /* lateinit var text:TextView
         lateinit var img:ImageView
         lateinit var card: CardView
         lateinit var linear: LinearLayout
         init {
             text=view.findViewById(R.id.txtscroll)
             img=view.findViewById(R.id.imgscroll)
             card = view.findViewById(R.id.cardlunch)
             linear = view.findViewById(R.id.linearlunchh)
         }*/
    }

    override fun onCreateViewHolder(
        parent: ViewGroup,
        viewType: Int
    ): ScrollListAdapter.MyViweHolder {

        return MyViweHolder(
            LayoutInflater.from(parent.context).inflate(  R.layout.scrollist, parent, false)
        )
    }

    override fun getItemCount(): Int {
        return fname.size
    }

    override fun onBindViewHolder(holder: ScrollListAdapter.MyViweHolder, position: Int) {

        holder.binding.txtscroll.text=fname[position].toString()
        holder.binding.imgscroll.setImageResource(pics[position])

        holder.binding.cardlunch.setOnClickListener(View.OnClickListener {
            row_index = position;
            notifyDataSetChanged();

        })
        if (row_index==position){
            holder.binding.linearlunchh.setBackgroundResource(R.drawable.border_layoutred)
            holder.binding.txtscroll.setTextColor(Color.WHITE)
            if (holder.binding.txtscroll.text.toString().equals("MostPopular")){
                subHomeFragment.getvalues("Most Popular")
                subHomeFragment.isfilterseleted = true
                subHomeFragment.filtertext = holder.binding.txtscroll.text.toString()
            }else{
                if (holder.binding.txtscroll.text.toString()=="الكل"){
                    subHomeFragment.getvalues("All")
                    subHomeFragment.isfilterseleted = true
                    subHomeFragment.filtertext = holder.binding.txtscroll.text.toString()
                }else if (holder.binding.txtscroll.text.toString()=="الأكثر شهرة"){
                    subHomeFragment.getvalues("Most Popular")
                    subHomeFragment.isfilterseleted = true
                    subHomeFragment.filtertext = holder.binding.txtscroll.text.toString()

                }else if (holder.binding.txtscroll.text.toString()=="المفضلة"){

                    subHomeFragment.getvalues("Favorites")
                    subHomeFragment.isfilterseleted = true
                    subHomeFragment.filtertext = holder.binding.txtscroll.text.toString()
                }else if (holder.binding.txtscroll.text.toString()=="الشائع"){

                    subHomeFragment.getvalues("Trending")
                    subHomeFragment.isfilterseleted = true
                    subHomeFragment.filtertext = holder.binding.txtscroll.text.toString()
                }



/*

                subHomeFragment.getvalues(holder.binding.txtscroll.text.toString())
                subHomeFragment.isfilterseleted = true
                subHomeFragment.filtertext = holder.binding.txtscroll.text.toString()*/
            }


        }
        else
        {
            holder.binding.linearlunchh.setBackgroundColor(Color.parseColor("#ffffff"));
            holder.binding.txtscroll.setTextColor(Color.parseColor("#000000"));
        }


    }

}












