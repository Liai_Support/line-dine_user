package com.lia.yello.linedine.Models

import com.google.gson.annotations.SerializedName
import java.io.Serializable

class FavoriteResponse : Serializable {


    @SerializedName("error")
    var error: Boolean? = null

    @SerializedName("message")
    var message: String? = null

    @SerializedName("data")
    var data: FavData? = null

}

    class FavData : Serializable {

    @SerializedName("list_favourites")
    var listfav: ArrayList<listfav>? = null
}

 class listfav : Serializable {

    @SerializedName("item_name")
    var item_name: String? = null

    @SerializedName("images")
    var images: String? = null

     @SerializedName("res_id")
     var res_id: Int? = null

    @SerializedName("name")
    var name: String? = null

    @SerializedName("cost")
    var cost: Double? = null


     @SerializedName("menuavilable")
     var menuavilable: Int? = null


     @SerializedName("rating")
     var rating: Double? = null
}
