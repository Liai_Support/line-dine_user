package com.lia.yello.linedine.fragment
import android.app.Activity
import android.app.AlertDialog
import android.content.Intent
import android.content.res.ColorStateList
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.net.Uri
import android.os.Bundle
import android.util.DisplayMetrics
import android.util.Log
import android.view.Gravity
import android.view.LayoutInflater
import android.view.View
import android.view.WindowManager
import android.widget.TextView
import androidx.activity.OnBackPressedCallback
import androidx.core.content.ContextCompat
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentTransaction
import androidx.lifecycle.ViewModelProvider
import com.google.android.material.snackbar.BaseTransientBottomBar
import com.google.android.material.snackbar.Snackbar
import com.lia.yello.linedine.R
import com.lia.yello.linedine.Session.SharedHelper
import com.lia.yello.linedine.Utils.DialogUtils
import com.lia.yello.linedine.Utils.NetworkUtils
import com.lia.yello.linedine.Utils.UiUtils
import com.lia.yello.linedine.ViewModels.MapViewModel
import com.lia.yello.linedine.activity.DashBoardActivity
import com.lia.yello.linedine.activity.NoonPaymentActivity
import com.lia.yello.linedine.databinding.FragmentFineDine3Binding
import kotlinx.android.synthetic.main.activity_dash_board.*
import kotlinx.android.synthetic.main.carticon.*
import kotlinx.android.synthetic.main.fragment_fine_dine3.*
import kotlinx.android.synthetic.main.fragment_fine_dine_order_list_status.*
import kotlinx.android.synthetic.main.fragment_suborders.*
import kotlinx.android.synthetic.main.fragment_wallet.*
import kotlinx.android.synthetic.main.header.*
import kotlinx.android.synthetic.main.header.back
import kotlinx.android.synthetic.main.placeorder.view.*
import kotlinx.android.synthetic.main.rulepagepickup.view.*
import kotlinx.android.synthetic.main.rulepagepickup.view.back
import kotlinx.android.synthetic.main.rulepagepickup.view.check1
import kotlinx.android.synthetic.main.rulepagepickup.view.procced
import kotlinx.android.synthetic.main.rulespage.*
import kotlinx.android.synthetic.main.rulespage.view.*
import java.text.SimpleDateFormat
import java.util.*

class FineDineFragment2 : Fragment(R.layout.fragment_fine_dine3) {
    var binding: FragmentFineDine3Binding? = null
    private var mapViewModel: MapViewModel? = null
    public var sharedHelper: SharedHelper? = null
    var hotelname: String = ""
    var desc: String = ""
    var workinghrs: String = ""
    var resrating: String = ""
    var resimage: String = ""
    var timetopass: String = ""
    var countadult: Int = 0
    var countkids: Int = 0
    var totalcount:Int=0

    var fulldate:String=""
    var resid:Int=0
    var timee:String=""
    var iskids:String=""
    var lat: Double = 0.0
    var lng: Double = 0.0
    var phoneee:String=""
    var res_status:String=""
    var  orderid:Int=0
    var  bookingid:Int=0
    var  totalamt:Double=0.0
    var imgid:Int=0
    var typee:String=""
    private var queueFragment1 : QueueFragment1? = null
    private var homeFragment : HomeFragment? = null
    private var fineDineMenuList : FineDineMenuListFragment? = null
    private var fineDineFragment1 : FineDineFragment1? = null
    var arraymenuids: MutableList<Int> = mutableListOf<Int>()
    private lateinit var fragmentTransaction: FragmentTransaction
    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        binding = DataBindingUtil.findBinding(view)
        sharedHelper = SharedHelper(requireContext())
        mapViewModel = ViewModelProvider(this).get(MapViewModel::class.java)
        val head = view.findViewById<TextView>(R.id.head)

        cart_badge.text = "" + sharedHelper!!.cartcount
        head.setText(getString(R.string.resturant))
        if (!NetworkUtils.isNetworkConnected(requireContext())) {
            UiUtils.showSnack(
                view, "no_internet_connection"
            )
        }

        if (sharedHelper!!.language == "ar"){
            back.rotation= 180F
        }
        if (sharedHelper!!.choose.equals("finedine")||sharedHelper!!.choose.equals("inqueue")|| sharedHelper!!.cartcount == 0){

            //var cartframe: FrameLayout =findViewById(R.id.cartframe)
            cartframe.visibility=View.GONE
        }
        (activity as DashBoardActivity?)!!.bottom_navigation_main?.visibility=View.VISIBLE

        (activity as DashBoardActivity?)!!.iv_add?.visibility=View.VISIBLE

        back.setOnClickListener(View.OnClickListener {
          //  requireActivity().supportFragmentManager.popBackStack()

            val bundle = Bundle()
            bundle.putInt("id",resid)
            bundle.putInt("imgid", imgid)
            bundle.putString("type", typee)

            fineDineFragment1 = FineDineFragment1()
            fineDineFragment1!!.arguments = bundle

            fragmentTransaction =requireActivity().supportFragmentManager.beginTransaction()
            fragmentTransaction.setCustomAnimations(R.anim.screen_in, R.anim.screen_out)
            fragmentTransaction.replace(R.id.container, fineDineFragment1!!)
            fragmentTransaction.addToBackStack(null)
            fragmentTransaction.commit()
        })

        hotelname = arguments?.getString("resname")!!
        desc = arguments?.getString("descrip")!!
        resrating = arguments?.getString("rating")!!
        fulldate = arguments?.getString("date")!!
        timee = arguments?.getString("time")!!
        resid = arguments?.getInt("resid")!!
        workinghrs = arguments?.getString("workinghrs")!!
        resimage = arguments?.getString("resimage")!!
        timetopass = arguments?.getString("timetopass")!!
        lat = arguments?.getDouble("lat")!!
        lng = arguments?.getDouble("lng")!!
        iskids = arguments?.getString("iskids")!!
        phoneee = arguments?.getString("phone")!!
        res_status = arguments?.getString("resstatus")!!
        resname.text = hotelname
        description.text = desc

        if (resrating.equals("")){
            resratingg.text = "0/5"

        }else{
            resratingg.text = resrating + "/"+"5"

        }
        workingtime.text = workinghrs
        timedatetoshow.text = timetopass
        imgid = arguments?.getInt("imgid")!!
        typee = arguments?.getString("type")!!

        resimage.let {
            UiUtils.loadImage(
                resimg,
                it,
                ContextCompat.getDrawable(
                    requireContext(),
                    R.drawable.maskgroup46
                )!!
            )
        }

        timedatetoshow.setOnClickListener(View.OnClickListener {
            val bundle = Bundle()
            bundle.putInt("id", resid)
            bundle.putInt("imgid", imgid)
            bundle.putString("type", typee)
            fineDineFragment1 = FineDineFragment1()
            fineDineFragment1!!.arguments = bundle
            fragmentTransaction = requireActivity().supportFragmentManager.beginTransaction()
            fragmentTransaction.setCustomAnimations(R.anim.screen_in, R.anim.screen_out)
            fragmentTransaction.replace(R.id.container, fineDineFragment1!!)
            fragmentTransaction.addToBackStack(null)
            fragmentTransaction.commit()
        })
        if (iskids.equals("false")){
            kidslinear.alpha=0.5f
            kidslinear.isEnabled=false
            kidsimgplus.isEnabled=false
            kidsimgminus.isEnabled=false
        }
        else{
            kidslinear.alpha=1.0f
            kidslinear.isEnabled=true
            kidsimgplus.isEnabled=true
            kidsimgminus.isEnabled=true
        }
        finedineloc2.setOnClickListener(View.OnClickListener {
            val uri = String.format(
                Locale.ENGLISH,
                "google.navigation:q=$lat,$lng"
            )
            val gmmIntentUri = Uri.parse(uri)
            val mapIntent = Intent(Intent.ACTION_VIEW, gmmIntentUri)
            mapIntent.setPackage("com.google.android.apps.maps")
            requireActivity().startActivity(mapIntent)
        })
        // for adults
        imgadultplus.setOnClickListener(View.OnClickListener {
            countadult++
            adulttxtquantity.setText("" + countadult)
            Log.d("count", "" + countadult)
            totalcount = countadult + countkids
        })
        imgadultminus.setOnClickListener(View.OnClickListener {
            /* if (countadult == 0) {
                adulttxtquantity.setText("" + countadult)
            } else {
                countadult--
                adulttxtquantity.setText("" + countadult)
                totalcount =countadult+countkids
            }*/

            if (countadult > 0) {
                countadult = countadult - 1
                Log.d("zxcdfvgbhnjmk,.", "" + countadult)
                adulttxtquantity.setText("" + countadult)
                totalcount = countadult + countkids
            }
        })
        // for kids
        kidsimgplus.setOnClickListener(View.OnClickListener {
            countkids++
            kidstxtquantity.setText("" + countkids)
            Log.d("count", "" + countkids)
            totalcount = countadult + countkids
        })
        kidsimgminus.setOnClickListener(View.OnClickListener {
            /*  if (countkids == 0 ) {
                kidstxtquantity.setText("" + countkids)
                totalcount =countadult+countkids
            } else {
                countkids--
                kidstxtquantity.setText("" + countkids)
                totalcount =countadult+countkids
            }*/


            if (countkids > 0) {
                countkids = countkids - 1
                Log.d("zxcdfvgbhnjmk,.", "" + countkids)
                kidstxtquantity.setText("" + countkids)
                totalcount = countadult + countkids
            }
        })
        finalreserve.setOnClickListener(View.OnClickListener {
            if (countadult == 0) {

                UiUtils.showSnack(
                    rootfinedine3,
                    getString(R.string.bookingcannotreservewithoutanuadult)
                )
            } else if (totalcount == 0) {
                UiUtils.showSnack(rootfinedine3, getString(R.string.pleaseselecthenoofseats))
            } else {



              getreservetable(totalcount)
            }
        })
        checkformenu.setOnClickListener(View.OnClickListener {
            val bundle = Bundle()
            bundle.putInt("resid", resid)
            bundle.putInt("orderid", orderid)
            bundle.putString("isfd", "true")
            bundle.putSerializable("menulist", null)


            bundle.putString("status","")
            bundle.putString("resname","")
            bundle.putString("resimg",resimage)
            bundle.putDouble("lat", 0.0)
            bundle.putDouble("lng", 0.0)
            bundle.putString("paidstatus","")
            bundle.putString("paymenttype","")
            bundle.putString("inqueue", "false")

            fineDineMenuList = FineDineMenuListFragment()
            fineDineMenuList!!.arguments = bundle
            fragmentTransaction = requireActivity().supportFragmentManager.beginTransaction()
            fragmentTransaction.setCustomAnimations(R.anim.screen_in, R.anim.screen_out)
            fragmentTransaction.replace(R.id.container, fineDineMenuList!!)
            fragmentTransaction.addToBackStack(null)
            fragmentTransaction.commit()
        })

        finalreserve.visibility=View.VISIBLE
        activity?.onBackPressedDispatcher?.addCallback(this, object : OnBackPressedCallback(true) {
            override fun handleOnBackPressed() {
                val bundle = Bundle()
                  bundle.putInt("id",resid)
                bundle.putInt("imgid", imgid)
                bundle.putString("type", typee)

                fineDineFragment1 = FineDineFragment1()
                fineDineFragment1!!.arguments = bundle

                fragmentTransaction =requireActivity().supportFragmentManager.beginTransaction()
                fragmentTransaction.setCustomAnimations(R.anim.screen_in, R.anim.screen_out)
                fragmentTransaction.replace(R.id.container, fineDineFragment1!!)
                fragmentTransaction.addToBackStack(null)
                fragmentTransaction.commit()
            }
        })


    }
    public fun getreservetable(totalcount: Int){
        ///  totalcount =countadult+countkids
        DialogUtils.showLoader(requireContext())
        mapViewModel?.getreservetable(requireContext(), resid, timee, fulldate, totalcount, "any")
            ?.observe(viewLifecycleOwner, androidx.lifecycle.Observer {
                DialogUtils.dismissLoader()
                it?.let {
                    it.error?.let { error ->
                        if (error) {
                            finalreserve.visibility = View.GONE
                            checkformenu.visibility = View.GONE
                            val builder = AlertDialog.Builder(context)
                            builder.setTitle(R.string.alert)
                            builder.setMessage(R.string.tableisnotavailablewanttocintinueinqueue)
                            builder.setPositiveButton(android.R.string.yes) { dialog, which ->
                                finalreserve.visibility = View.GONE
                                checkformenu.visibility = View.GONE
                                continuetoqueue.visibility = View.VISIBLE

                            }
                            builder.setNegativeButton(android.R.string.no) { dialog, which ->
                                builder.setCancelable(true)
                                finalreserve.visibility = View.VISIBLE


                            }
                            val dialog: AlertDialog = builder.create()
                            dialog.show()
                            dialog.setCancelable(false)
                            //   builder.show()
                            //  builder.setCancelable(false)
                            //   continuetoqueue.visibility=View.VISIBLE
                            continuetoqueue.setOnClickListener(View.OnClickListener {
                                val bundle = Bundle()
                                bundle.putString("resname", hotelname)
                                bundle.putString("resimg", resimage)
                                bundle.putInt("resid", resid)
                                bundle.putString("time", timee)
                                bundle.putInt("seatcount", totalcount)
                                bundle.putString("date", fulldate)
                                bundle.putString("phone", phoneee)
                                bundle.putString("resstatus", res_status)
                                bundle.putDouble("lat", lat)
                                bundle.putDouble("lng", lng)
                                bundle.putString("inqueue", "true")
                                bundle.putInt("imgid", imgid)
                                bundle.putString("type", typee)
                                queueFragment1 = QueueFragment1()
                                queueFragment1!!.arguments = bundle
                                fragmentTransaction =
                                    requireActivity().supportFragmentManager.beginTransaction()
                                fragmentTransaction.setCustomAnimations(
                                    R.anim.screen_in,
                                    R.anim.screen_out
                                )
                                fragmentTransaction.replace(R.id.container, queueFragment1!!)
                                fragmentTransaction.commit()
                            })
                        } else {
                            it.data?.let { data ->
                                imgadultplus.isEnabled=false
                                imgadultminus.isEnabled=false
                                kidsimgplus.isEnabled=false
                                kidsimgminus.isEnabled=false

                                bookingid = data.insert_data!!

                                val mDialogView = LayoutInflater.from(requireContext()).inflate(R.layout.rulespage, null)
                                val mBuilder = AlertDialog.Builder(requireContext()).setView(mDialogView).setCancelable(false)
                                val mAlertDialog = mBuilder.show()
                                val layoutParams = WindowManager.LayoutParams()
                                val displayMetrics = DisplayMetrics()
                                requireActivity().windowManager.defaultDisplay.getMetrics(displayMetrics)
                                val displayWidth = displayMetrics.widthPixels
                                val displayHeight = displayMetrics.heightPixels
                                layoutParams.copyFrom(mAlertDialog.window?.attributes)
                                layoutParams.width = ((displayWidth * 0.9f).toInt())
                                mAlertDialog.window?.setAttributes(layoutParams)
                                mAlertDialog.window?.setGravity(Gravity.CENTER)
                                mAlertDialog.window?.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))

                                mDialogView.check1.setOnCheckedChangeListener { buttonView, isChecked ->
                                    if (isChecked) {
                                        mDialogView.check1.buttonTintList= ColorStateList.valueOf(resources.getColor(R.color.colorPrimary))
                                    }else{
                                        mDialogView.check1.buttonTintList= ColorStateList.valueOf(resources.getColor(R.color.grey2))
                                    }
                                }
                                mDialogView.check2.setOnCheckedChangeListener { buttonView, isChecked ->
                                    if (isChecked) {
                                        mDialogView.check2.buttonTintList= ColorStateList.valueOf(resources.getColor(R.color.colorPrimary))
                                    }else{
                                        mDialogView.check2.buttonTintList= ColorStateList.valueOf(resources.getColor(R.color.grey2))
                                    }
                                }

                                mDialogView.proccedfd.setOnClickListener {

                                    if (mDialogView.check1.isChecked && mDialogView.check2.isChecked ){
                                        mAlertDialog.dismiss()



                                        val mDialogView = LayoutInflater.from(requireContext()).inflate(R.layout.placeorder, null)
                                        val mBuilder = AlertDialog.Builder(requireContext()).setView(mDialogView).setCancelable(true)
                                        val mAlertDialog = mBuilder.show()
                                        val layoutParams = WindowManager.LayoutParams()
                                        val displayMetrics = DisplayMetrics()
                                        requireActivity().windowManager.defaultDisplay.getMetrics(displayMetrics)
                                        val displayWidth = displayMetrics.widthPixels
                                        val displayHeight = displayMetrics.heightPixels
                                        layoutParams.copyFrom(mAlertDialog.getWindow()?.getAttributes())
                                        layoutParams.width = ((displayWidth * 0.9f).toInt())
                                        mAlertDialog.getWindow()?.setAttributes(layoutParams)
                                        mAlertDialog.getWindow()?.setGravity(Gravity.CENTER)
                                        mAlertDialog.getWindow()?.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
                                        mDialogView.checkoutcard.setOnClickListener {
                                            startActivityForResult(
                                                Intent(requireContext(), NoonPaymentActivity::class.java)
                                                    .putExtra(
                                                        "amount",
                                                        50.0
                                                    ), 100
                                            )
                                            mAlertDialog.dismiss()
                                        }

                                        mDialogView.checkoutwallet.setOnClickListener {
                                            getwalletamount(50.0)

                                            mAlertDialog.dismiss()

                                        }


                                    }else{
                                        UiUtils.showSnack(rootfinedine3,getString(R.string.plscheckthebox))
                                        //  mAlertDialog.dismiss()


                                    }


                                }
                                mDialogView.backfd.setOnClickListener {

                                    mAlertDialog.dismiss()

                                }

                                /*        if (s.equals("card")){
                                            startActivityForResult(
                                                Intent(requireContext(), NoonPaymentActivity::class.java)
                                                    .putExtra(
                                                        "amount",
                                                        50.0
                                                    ), 100
                                            )
                                        }else{
                                            getwalletamount(50.0)
                                        }*/


                              //  getcomfirmbooking()
                           /*     UiUtils.showSnack(
                                    rootfinedine3,
                                    getString(R.string.yourtableisbookedsuccessfully)
                                )*/
                                orderid = data.insert_data!!
                                continuetoqueue.visibility = View.GONE
                                finalreserve.visibility = View.VISIBLE
                                //  checkformenu.visibility=View.VISIBLE
                                // Initialize a new instance of
              /*                  val builder = AlertDialog.Builder(requireContext())
                                builder.setTitle(R.string.alert)
                                builder.setMessage(R.string.wouldyouliketoorderfromourmenu)
                                // Set a positive button and its click listener on alert dialog
                                builder.setPositiveButton("YES") { dialog, which ->


                                    checkformenu.visibility = View.VISIBLE

                                }


                                builder.setNegativeButton("No") { dialog, which ->
                                    builder.setCancelable(true)
                                    //
                                    // finalreserve.visibility = View.VISIBLE
                                    homeFragment = HomeFragment()
                                    fragmentTransaction =
                                        requireActivity().supportFragmentManager.beginTransaction()
                                    fragmentTransaction.setCustomAnimations(
                                        R.anim.screen_in,
                                        R.anim.screen_out
                                    )
                                    fragmentTransaction.replace(R.id.container, homeFragment!!)
                                    fragmentTransaction.commit()


                                }
                                val dialog: AlertDialog = builder.create()
                                dialog.show()
                                dialog.setCancelable(false)*/

                                /*    val builder = AlertDialog.Builder(context)
                                    builder.setTitle(R.string.alert)
                                    builder.setMessage(R.string.tableisnotavailablewanttocintinueinqueue)
                                    builder.setPositiveButton(android.R.string.yes) { dialog, which ->
                                        checkformenu.visibility=View.VISIBLE
                                    }
                                    builder.setNegativeButton(android.R.string.no) { dialog, which ->
                                        builder.setCancelable(true)
                                    }
                                    builder.show()
                                  //  builder.setCancelable(false)*/
                            }
                        }
                    }
                }
            })
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (requestCode == 100) {
            if (resultCode == Activity.RESULT_OK) {
                getcomfirmbooking("card",data!!.getStringExtra("paymentorderid").toString(),data!!.getStringExtra("paymenttransactionid").toString());

                // getreservetable(totalcount)

                //  sendPaymentDetails(data!!.getStringExtra("paymentorderid").toString(),data!!.getStringExtra("paymenttransactionid").toString())

            }
        }
    }

    public fun getcomfirmbooking(s:String,orderid:String,transid:String) {
        DialogUtils.showLoader(requireContext())
        mapViewModel?.getcomfirmbooking(requireContext(), bookingid, 50.0, "booked", s,orderid,transid)
            ?.observe(viewLifecycleOwner, androidx.lifecycle.Observer {
                DialogUtils.dismissLoader()

                it?.let {
                    it.error?.let { error ->
                        if (error) {
                            it.message?.let { msg ->

                                UiUtils.showSnack(rootfinedine3, msg)
                            }
                        } else {
                            it.message?.let { msg ->
                                UiUtils.showSnack(rootfinedine3, msg)

                                finalreserve.visibility=View.GONE

                                val builder = AlertDialog.Builder(requireContext())
                                builder.setTitle(R.string.alert)
                                builder.setMessage(R.string.wouldyouliketoorderfromourmenu)
                                // Set a positive button and its click listener on alert dialog
                                builder.setPositiveButton(getString(R.string.yes)) { dialog, which ->
                                    checkformenu.visibility = View.VISIBLE

                                }


                                builder.setNegativeButton(getString(R.string.no)) { dialog, which ->
                                    builder.setCancelable(true)
                                    //
                                    // finalreserve.visibility = View.VISIBLE
                                    homeFragment = HomeFragment()
                                    fragmentTransaction =
                                        requireActivity().supportFragmentManager.beginTransaction()
                                    fragmentTransaction.setCustomAnimations(
                                        R.anim.screen_in,
                                        R.anim.screen_out
                                    )
                                    fragmentTransaction.replace(R.id.container, homeFragment!!)
                                    fragmentTransaction.commit()


                                }
                                val dialog: AlertDialog = builder.create()
                                dialog.show()
                                dialog.setCancelable(false)


                            }
                        }

                    }
                }
            })


    }

    private fun getwalletamount(totalamt: Double) {
        DialogUtils.showLoader(requireContext())

        mapViewModel?.getwalletamount(requireContext())
            ?.observe(viewLifecycleOwner, androidx.lifecycle.Observer {
                DialogUtils.dismissLoader()

                it?.let {
                    it.error?.let { error ->
                        if (error) {
                            it.message?.let { msg ->
                                UiUtils.showSnack(rootfinedine3, msg)
                            }
                        } else {
                            it.data?.let { data ->

                                //  getbookingpayment()
                                Log.d("wertyu",""+totalamt+">"+data.amount.toString())
                                if( data.amount!! >= totalamt){

                                    Log.d("ertyuiop","kanikmakak")
                                    getcomfirmbooking("wallet","","")
                                }else{
                                    Log.d("ertyuiop","werftgy")

                                    UiUtils.showSnack(rootfinedine3, getString(R.string.yourwalletamountislowpleaseuplateyourwalletamount))

/*
                                    Snackbar.make(rootfinedine3,getString(R.string.yourwalletamountislowpleaseuplateyourwalletamount),
                                        BaseTransientBottomBar.LENGTH_SHORT)*/
                                    // getbookinglist("active")
                                }
                                /*Log.d("amount", data.amount.toString())
                                var amount:Double
                                amount= data.amount!!
                                walletamount.text = amount.toString()+" SAR"
                                Log.d("sdxcfvgbnm",""+requireActivity().walletamount.text)*/

                            }

                        }
                    }

                }
            })

    }

}