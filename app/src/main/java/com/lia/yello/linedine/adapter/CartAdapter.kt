package com.lia.yello.linedine.adapter

import android.content.Context
import android.os.Build
import android.os.Handler
import android.text.Editable
import android.text.TextWatcher
import android.util.Log
import android.view.KeyEvent
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.inputmethod.EditorInfo
import android.view.inputmethod.InputMethodManager
import android.widget.TextView
import androidx.annotation.RequiresApi
import androidx.lifecycle.Observer
import androidx.recyclerview.widget.RecyclerView
import com.lia.yello.linedine.Models.orderlist
import com.lia.yello.linedine.R
import com.lia.yello.linedine.Utils.DialogUtils
import com.lia.yello.linedine.Utils.UiUtils
import com.lia.yello.linedine.databinding.CardSubordersBinding
import com.lia.yello.linedine.fragment.CartFragment
import kotlinx.android.synthetic.main.card_filter.view.*
import kotlinx.android.synthetic.main.fragment_suborders.*
import java.util.*
import kotlin.collections.ArrayList


class CartAdapter(
    var cartFragment: CartFragment,
    var context: Context,
    var list: ArrayList<orderlist>?
):
    RecyclerView.Adapter<CartAdapter.HomeHeaderViewHolder>() {
    var uid: Int = 0
    var uqty: Int = 0
    var usins: String = ""
    var id: Int = 1
    var delay: Long = 2000 // 1 seconds after user stops typing
    var last_text_edit: Long = 0
    var handler = Handler()
    private val input_finish_checker = Runnable {
        if (System.currentTimeMillis() > last_text_edit + delay - 500) {
            //DoStuff()
            getupdatecart(uid,uqty,usins)
        }
    }

    inner class HomeHeaderViewHolder(view: View) : RecyclerView.ViewHolder(view) {

        var binding: CardSubordersBinding = CardSubordersBinding.bind(view)

    }


    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): HomeHeaderViewHolder {
        return HomeHeaderViewHolder(
            LayoutInflater.from(context).inflate(
                R.layout.card_suborders,
                parent,
                false
            )
        )
    }

    override fun getItemCount(): Int {
        return list!!.size
    }


    @RequiresApi(Build.VERSION_CODES.O)
    override fun onBindViewHolder(holder: HomeHeaderViewHolder, position: Int) {
        holder.binding.specialins.isEnabled=false
        holder.binding.specialins.setText(list!![position].special_instruction.toString())

        if(list!![position].menuavilable==0){
            holder.binding.rootcartadapter.alpha=0.5f
          //  holder.binding.delclick.alpha=0.5f
            holder.binding.imgminus.isEnabled=false
            holder.binding.imgplus.isEnabled=false
        //    holder.binding.delclick.isEnabled=false
        //    holder.binding.carddel.isEnabled=false
            holder.binding.title.text = list!![position].menuname.toString()

                // holder.binding.cost.text = list!![position].cost.toString() + " SAR"
            holder.binding.cost.text =  String.format(Locale("en", "US"),"%.2f",list!![position].cost)+ " SAR"
            holder.binding.txtquantity.text = list!![position].quantity.toString()
            list!![position].menu_images?.let { imageUrl ->
                UiUtils.loadImage(holder.binding.menuimage, imageUrl)
            }






            holder.binding.rootcartadapter.setOnClickListener(View.OnClickListener {
                UiUtils.showSnack(
                    holder.binding.rootcartadapter,
                    context.getString(R.string.menuisnotavailable)
                )
            })

        }
        else{
            cartFragment.placeorderboolean = true
            holder.binding.title.text = list!![position].menuname.toString()
            //holder.binding.cost.text = list!![position].cost.toString() + " SAR"
            holder.binding.cost.text =  String.format(Locale("en", "US"),"%.2f",list!![position].cost)+ " SAR"

            holder.binding.txtquantity.text = list!![position].quantity.toString()

            list!![position].menu_images?.let { imageUrl ->
                UiUtils.loadImage(holder.binding.menuimage, imageUrl)
            }

            holder.binding.imgminus.setOnClickListener(View.OnClickListener {
                Log.d("asdfghjkm,l.", "zxcgfvhbjnkml,.")


                if (cartFragment.isapplied=="true"){

                    cartFragment.couponcodepickup.isEnabled=true
                    cartFragment.applycouponpickup.isEnabled=true
                    cartFragment.couponcodepickup.setText("")
                    cartFragment.discountlevel.visibility=View.GONE
                    cartFragment.productotalrelative.visibility=View.GONE

                    cartFragment.getcartlist()


                }else{

                }

                var count2: Int = list!![position].quantity!!
                holder.binding.txtquantity.setText("" + count2)
                if (count2 <= 1) {

                    UiUtils.showSnack(holder.binding.imgminus,context.getString(R.string.itemcannotbelessthan1))
                } else {

                    holder.binding.imgminus.isEnabled = false

                    Handler().postDelayed({
                        holder.binding.imgminus.isEnabled = true
                    }, 1000)
                    getupdatecart(
                        list!![position].id!!,
                        list!![position].quantity!! - 1,list!![position].special_instruction!!
                    )


                }
            })
            holder.binding.imgplus.setOnClickListener(View.OnClickListener {




                if (cartFragment.isapplied=="true"){
                    cartFragment.couponcodepickup.isEnabled=true
                    cartFragment.couponcodepickup.setText("")

                    cartFragment.applycouponpickup.isEnabled=true
                    cartFragment.discountlevel.visibility=View.GONE
                    cartFragment.productotalrelative.visibility=View.GONE
                    cartFragment.getcartlist()
                }
                Log.d("asdfghjkm,l.", "zxcgfvhbjnkml,.")


                holder.binding.imgplus.isEnabled = false

                Handler().postDelayed({
                    holder.binding.imgplus.isEnabled = true
                }, 1000)

                getupdatecart(
                    list!![position].id!!,
                    list!![position].quantity!! + 1,
                    list!![position].special_instruction!!                )

            })
            holder.binding.splecialinsimg.setOnClickListener(View.OnClickListener {
                holder.binding.specialins.isEnabled = true
                holder.binding.specialins.requestFocus()

                holder.binding.specialins.setOnEditorActionListener(
                    TextView.OnEditorActionListener { v, actionId, event ->
                        if (actionId == EditorInfo.IME_ACTION_SEARCH || actionId == EditorInfo.IME_ACTION_DONE || event != null && event.action === KeyEvent.ACTION_DOWN && event.keyCode === KeyEvent.KEYCODE_ENTER) {
                            if (event == null || !event.isShiftPressed) {
                                // the user is done typing.
                                Log.d("djbhdc", "cddc")
                                val imm: InputMethodManager = v.getContext()
                                    .getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
                                imm.hideSoftInputFromWindow(v.getWindowToken(), 0)
                                uid = list!![position].id!!
                                uqty = list!![position].quantity!!
                                usins = holder.binding.specialins.text.toString()
                                getupdatecart(uid,uqty,usins)
                                return@OnEditorActionListener true // consume.
                            }
                        }
                        false // pass on to other listeners.
                    }
                )



                holder.binding.specialins.addTextChangedListener(object : TextWatcher {
                    override fun beforeTextChanged(s: CharSequence, start: Int, count: Int, after: Int) {

                    }

                    override fun onTextChanged(s: CharSequence, start: Int, before: Int, count: Int) {
                    }

                    override fun afterTextChanged(s: Editable) {
                        //avoid triggering event when text is empty
                        if (s.length > 0) {
                            uid = list!![position].id!!
                            uqty = list!![position].quantity!!
                            usins = holder.binding.specialins.text.toString()
                            last_text_edit = System.currentTimeMillis();
                            handler.postDelayed(input_finish_checker, delay);
                        } else {

                        }
                    }
                })

            })

        }

    }


    fun getupdatecart(id: Int, qty: Int,splecialinstrcution: String){
        DialogUtils.showLoader(context)
        cartFragment.mapViewModel?.getupdatecart(context, id, qty, splecialinstrcution)
            ?.observe(cartFragment, Observer {
                DialogUtils.dismissLoader()

                it?.let {
                    it.error?.let { error ->
                        if (error) {
                            it.message?.let { msg ->
                                //UiUtils.showSnack(root, msg)
                                // snackbarToast(msg)
                                //Toast.makeText(context, msg, Toast.LENGTH_SHORT).show()

                            }
                        } else {
                            it.message?.let { msg ->
                                // Toast.makeText(context, msg, Toast.LENGTH_SHORT).show()

                                cartFragment.subtotal = 0.0
                                cartFragment.vat = 0.0
                                cartFragment.totalamt = 0.0
                                Log.d("kanika,", "" + msg)
                                cartFragment.getcartlist()


                            }
                        }
                    }

                }
            })



    }


}




