package com.lia.yello.linedine.Models

import com.google.gson.annotations.SerializedName
import java.io.Serializable

class PointsHistory : Serializable {


    @SerializedName("error")
    var error: Boolean? = null

    @SerializedName("message")
    var message: String? = null

    @SerializedName("data")
    var data: PointHistoryData? = null


}

class PointHistoryData : Serializable {

    @SerializedName("wallet_data")
    var walletdata: ArrayList<Walletata>? = null


}
class Walletata : Serializable {

    @SerializedName("id")
    var id: Int? = null


    @SerializedName("point")
    var point: String? = null

    @SerializedName("added_for")
    var added_for: String? = null

    @SerializedName("date")
    var date: String? = null

    @SerializedName("offered_by")
    var offered_by: String? = null

    @SerializedName("order_id")
    var order_id: String? = null

    @SerializedName("order_for")
    var order_for: String? = null

    @SerializedName("restaurant")
    var restaurant: String? = null
}