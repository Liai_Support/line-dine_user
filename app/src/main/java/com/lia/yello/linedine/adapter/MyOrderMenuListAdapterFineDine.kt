package com.lia.yello.linedine.adapter

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.lia.yello.linedine.Models.FineDineMenudetail
import com.lia.yello.linedine.R
import com.lia.yello.linedine.Utils.UiUtils
import com.lia.yello.linedine.databinding.CardMenuBinding
import com.lia.yello.linedine.fragment.MyOrderMenuListFragment
import java.util.*
import kotlin.collections.ArrayList

class MyOrderMenuListAdapterFineDine(
    var myOrderMenuListFragment: MyOrderMenuListFragment,
    var context: Context,
    var list: ArrayList<FineDineMenudetail>?
) :
    RecyclerView.Adapter<MyOrderMenuListAdapterFineDine.HomeHeaderViewHolder>() {

    // var list2: ArrayList<menudetail>? = null

    inner class HomeHeaderViewHolder(view: View) : RecyclerView.ViewHolder(view) {
        var binding: CardMenuBinding = CardMenuBinding.bind(view)
    }


    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): HomeHeaderViewHolder {
        return HomeHeaderViewHolder(
            LayoutInflater.from(parent.context).inflate(
                R.layout.card_menu,
                parent,
                false
            )
        )
    }

    override fun getItemCount(): Int {
        return list!!.size
        // return itemCount
    }


    override fun onBindViewHolder(holder: HomeHeaderViewHolder, position: Int) {

        holder.binding.title.text = list!![position].menu_name.toString()
        holder.binding.cost.text=  String.format(Locale("en", "US"),"%.2f",list!![position].amount)+ " SAR"

      //  holder.binding.cost.text =list!![position].amount.toString() + " SAR"
        holder.binding.txtquantity.text = list!![position].quantity.toString()
        // holder.binding.txtquantity.text= list!![position].quantity.toString()+ " SAR"

        list!![position].image?.let { imageUrl ->
            UiUtils.loadImage(holder.binding.menuimg, imageUrl)
        }

        /*holder.binding.specialins.setText(list!![position].splins.toString())

        Log.d("specailins",""+list!![position].splins.toString())*/

    }

}












