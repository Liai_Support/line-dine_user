package com.lia.yello.linedine.Models

import com.google.gson.annotations.SerializedName
import java.io.Serializable

class WalletHistory  : Serializable {


    @SerializedName("error")
    var error: Boolean? = null

    @SerializedName("message")
    var message: String? = null

    @SerializedName("data")
    var data: WalletHis? = null


}


class WalletHis : Serializable {

    @SerializedName("wallet_data")
    var walletdata: ArrayList<walletdata>? = null

}
class walletdata : Serializable {

    @SerializedName("id")
    var id: Int? = null

    @SerializedName("amount")
    var amount: Double? = null


    @SerializedName("transaction_id")
    var transaction_id: String? = null

    @SerializedName("date")
    var date: String? = null


    @SerializedName("operation")
    var operation: String? = null


    @SerializedName("order_id")
    var order_id: Int? = null

}