package com.lia.yello.linedine.Models

import com.google.gson.annotations.SerializedName

import java.io.Serializable

class ListResturantResponse : Serializable {


    @SerializedName("error")
    var error: Boolean? = null

    @SerializedName("message")
    var message: String? = null

    @SerializedName("data")
    var data: ListData? = null


}


class ListData : Serializable {



    @SerializedName("list_restaurant")
    var listres: ArrayList<listresturant>? = null



}
class listresturant : Serializable {

    @SerializedName("id")
    var id: Int? = null

    @SerializedName("name")
    var name: String? = null

    @SerializedName("banner")
    var resimage: String? = null


    @SerializedName("queuestatus")
    var queuestatus: String? = null


}


