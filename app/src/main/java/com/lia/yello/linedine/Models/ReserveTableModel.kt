package com.lia.yello.linedine.Models

import com.google.gson.annotations.SerializedName
import java.io.Serializable

class ReserveTableModel : Serializable {

    @SerializedName("error")
    var error: Boolean? = null

    @SerializedName("Table booked")
    var message: String? = null

    @SerializedName("data")
    var data: InsertReserveData? = null

     }

class InsertReserveData : Serializable {

    @SerializedName("bookingid")
    var insert_data: Int? = null

     }