package com.lia.yello.linedine.Models

import com.google.gson.annotations.SerializedName
import java.io.Serializable

class DaySlotResponse : Serializable {


    @SerializedName("error")
    var error: Boolean? = null

    @SerializedName("message")
    var message: String? = null

    @SerializedName("data")
    var data: ArrayList<ListDay>? = null
}

class ListDay : Serializable {

    @SerializedName("day")
    var day: String? = null

    @SerializedName("date")
    var date: String? = null

    @SerializedName("status")
    var status: String? = null

    @SerializedName("fulldate")
    var fulldate: String? = null

    @SerializedName("fulday")
    var fulday: String? = null


    @SerializedName("month")
    var month: String? = null

}