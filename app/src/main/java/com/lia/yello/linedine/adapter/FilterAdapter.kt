package com.lia.yello.linedine.adapter

import android.content.Context
import android.graphics.Color
import android.graphics.PorterDuff
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.content.ContextCompat
import androidx.recyclerview.widget.RecyclerView
import com.facebook.FacebookSdk.getApplicationContext
import com.lia.yello.linedine.R
import com.lia.yello.linedine.activity.Cuisines
import com.lia.yello.linedine.databinding.CardFilterBinding
import com.lia.yello.linedine.fragment.HomeFragment
import kotlinx.android.synthetic.main.popup_filter.*

class FilterAdapter(
    var homeFragment: HomeFragment,
    var context: Context,
    var list: ArrayList<Cuisines>?
) :
    RecyclerView.Adapter<FilterAdapter.HomeHeaderViewHolder>() {
    var row_index: Int = -1
   lateinit var txt: String


    inner class HomeHeaderViewHolder(view: View) : RecyclerView.ViewHolder(view) {
        var binding: CardFilterBinding = CardFilterBinding.bind(view)
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): HomeHeaderViewHolder {
        return HomeHeaderViewHolder(
            LayoutInflater.from(context).inflate(
                R.layout.card_filter,
                parent,
                false
            )
        )
    }

    override fun getItemCount(): Int {
        return list!!.size
    }

    override fun onBindViewHolder(holder: HomeHeaderViewHolder, position: Int) {

        Log.d("wertyui",""+homeFragment.clearallfilter)
      if (homeFragment.clearallfilter){
          Log.d("jhduhdiuehdioue","bchjdbcyhd")
          holder.binding.card.backgroundTintList = ContextCompat.getColorStateList(getApplicationContext(), R.color.white)
          holder.binding.text.setTextColor(Color.BLACK)
          homeFragment.idarray.clear()

      }else{

      }

        var gf:String = ""
        holder.binding.text.text = list!![position].cuisine

        holder.binding.card.setOnClickListener(View.OnClickListener {
            Log.d("hgcvxg",""+list!![position].cuisine)
             Log.d("c,mmnvcm",""+homeFragment.idarray.size)
                if(homeFragment.idarray.size == 0){
                    homeFragment.iscuisine = true
                    holder.binding.card.backgroundTintList = ContextCompat.getColorStateList(getApplicationContext(), R.color.colorPrimary)
                    holder.binding.text.setTextColor(Color.WHITE)
                    list!![position].id?.let { it1 -> homeFragment.idarray.add(it1) }
                    Log.d("xcdvfgbnm,.", "" + homeFragment.idarray)
                }
                else{
                    homeFragment.iscuisine = true
                    for (i in 0 until homeFragment.idarray.size){
                        Log.d("dbvhfd",""+list!![position].id+"==="+homeFragment.idarray[i])
                        if (list!![position].id==homeFragment.idarray[i]){
                            gf = i.toString()
                            break
                        }
                        else{
                            gf = ""
                        }
                    }
                    if(!gf.equals("")){
                        holder.binding.card.backgroundTintMode = PorterDuff.Mode.MULTIPLY
                        holder.binding.card.backgroundTintList = ContextCompat.getColorStateList(getApplicationContext(), R.color.white)
                        holder.binding.card.background = ContextCompat.getDrawable(getApplicationContext(),R.drawable.filter_card_back)
                        holder.binding.text.setTextColor(Color.BLACK)
                        homeFragment.idarray.removeAt(gf.toInt())
                    }
                    else{
                        holder.binding.card.backgroundTintList = ContextCompat.getColorStateList(getApplicationContext(), R.color.colorPrimary)
                        holder.binding.text.setTextColor(Color.WHITE)
                        list!![position].id?.let { it1 -> homeFragment.idarray.add(it1) }
                        Log.d("xcdvfgbnm,.", "" + homeFragment.idarray)
                    }

                }
           /* if(list!![position].cuisine.equals("All")){
                Log.d("dsgsscvsdg","sdcdds")
                holder.binding.card.backgroundTintList = ContextCompat.getColorStateList(getApplicationContext(), R.color.colorPrimary)
                holder.binding.text.setTextColor(Color.WHITE)
                homeFragment.idarray.clear()
                homeFragment.iscuisine = false
                for(i in 0 until list!!.size){
                    Log.d("cgvdc",""+(list!![i].cuisine))
                    if(list!![i].cuisine.equals("All")){

                    }
                    else{
                        holder.binding.card.backgroundTintMode = PorterDuff.Mode.MULTIPLY
                        holder.binding.card.backgroundTintList = ContextCompat.getColorStateList(getApplicationContext(), R.color.white)
                        holder.binding.card.background = ContextCompat.getDrawable(getApplicationContext(),R.drawable.filter_card_back)
                        holder.binding.text.setTextColor(Color.BLACK)
                    }
                }
            }
            else{

            }*/



        })




    }
}