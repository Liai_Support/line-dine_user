package com.lia.yello.linedine.fcm;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.Build;
import android.util.Log;
import android.widget.RemoteViews;
import androidx.core.app.NotificationCompat;
import com.google.firebase.messaging.FirebaseMessagingService;
import com.google.firebase.messaging.RemoteMessage;
import com.lia.yello.linedine.R;
import com.lia.yello.linedine.Session.SharedHelper;
import com.lia.yello.linedine.activity.DashBoardActivity;
import com.lia.yello.linedine.activity.MainActivity;
import com.lia.yello.linedine.activity.SplashActivity;

//import com.lia.vtl.SplashScreen;

public class FirebaseMessageReceiver extends FirebaseMessagingService {

    SharedHelper sharedHelper;
    @Override
    public void onMessageReceived(RemoteMessage remoteMessage) {
        //handle when receive notification via data event

        super.onMessageReceived(remoteMessage);
        sharedHelper = new SharedHelper(getApplicationContext());

        Log.d("msg", "onMessageReceived: " + remoteMessage.getData().get("data"));
        Log.d("msg", "text: " + remoteMessage.getData().get("body"));
        Log.d("msg", "aswedrftghyujikolp;[]: " + remoteMessage.getData());
        Log.d("msg", "aqwertyuiop: " + remoteMessage);
        Log.d("notificationbody", "body: " + remoteMessage.getNotification().getBody());
        Log.d("notificationtitle", "title: " + remoteMessage.getNotification().getTitle());
        Log.d("asdxcfvgbhnjmkl",""+sharedHelper.getLoggedIn());
        Log.d("awedcvfgthnjmkil",""+sharedHelper.getToken());

        if (sharedHelper.getLoggedIn()==true && !sharedHelper.getToken().equals("")){

            if (remoteMessage.getData().get("data").equals("inqueue")){

                // in app open on
                Log.d("notificationtitle", "kanikashanmugam");
                Intent intent = new Intent(this, DashBoardActivity.class);
                intent.putExtra("fromnotification","tableassigned");
                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                PendingIntent pendingIntent = PendingIntent.getActivity(this, 0, intent, PendingIntent.FLAG_UPDATE_CURRENT);
                String channelId = "Default";
                NotificationCompat.Builder builder = new  NotificationCompat.Builder(this, channelId)
                        .setSmallIcon(R.mipmap.ic_launcher)
                        .setContentTitle(remoteMessage.getNotification().getTitle())
                        .setContentText(remoteMessage.getData().get("body")).setAutoCancel(true).setContentIntent(pendingIntent);;
                NotificationManager manager = (NotificationManager) getSystemService(NOTIFICATION_SERVICE);

                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
                    NotificationChannel channel = new NotificationChannel(channelId, "Default channel", NotificationManager.IMPORTANCE_DEFAULT);
                    manager.createNotificationChannel(channel);
                }
                manager.notify(0, builder.build());

            }
            else if (remoteMessage.getData().get("data").equals("pickup")){
                Log.d("szxdcfvgbhnjmk", "wertyuio");

                Intent intent = new Intent(this, DashBoardActivity.class);
                intent.putExtra("fromnotification","pickup");
                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                PendingIntent pendingIntent = PendingIntent.getActivity(this, 0, intent, PendingIntent.FLAG_ONE_SHOT);
                String channelId = "Default";
                NotificationCompat.Builder builder = new  NotificationCompat.Builder(this, channelId)
                        .setSmallIcon(R.mipmap.ic_launcher)
                        .setContentTitle(remoteMessage.getNotification().getTitle())
                        .setContentText(remoteMessage.getData().get("body")).setAutoCancel(true).setContentIntent(pendingIntent);;
                NotificationManager manager = (NotificationManager) getSystemService(NOTIFICATION_SERVICE);
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
                    NotificationChannel channel = new NotificationChannel(channelId, "Default channel", NotificationManager.IMPORTANCE_DEFAULT);
                    manager.createNotificationChannel(channel);
                }
                manager.notify(0, builder.build());
            }
        }
        else{

        }





    }

    private RemoteViews getCustomDesign(String title, String message ){
        RemoteViews remoteViews=new RemoteViews(getApplicationContext().getPackageName(), R.layout.notification);
        remoteViews.setTextViewText(R.id.title,title);
        remoteViews.setTextViewText(R.id.message,message);
        remoteViews.setImageViewResource(R.id.icon,R.mipmap.ic_launcher);
        return remoteViews;
    }

    public void showNotification(String title, String message){

        Log.d("zxcvbnm,",""+message);
        if (message.equals("order placed")){
            Intent intent=new Intent(this, MainActivity.class);
            String channel_id="web_app_channel";
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
            PendingIntent pendingIntent= PendingIntent.getActivity(this,0,intent, PendingIntent.FLAG_ONE_SHOT);
            Uri uri= RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
            NotificationCompat.Builder builder=new NotificationCompat.Builder(getApplicationContext(),channel_id)
                    .setSmallIcon(R.mipmap.ic_launcher)
                    .setSound(uri)
                    .setAutoCancel(true)
                    .setVibrate(new long[]{1000,1000,1000,1000,1000})
                    .setOnlyAlertOnce(true)
                    .setContentIntent(pendingIntent);

            if(Build.VERSION.SDK_INT>= Build.VERSION_CODES.JELLY_BEAN){
                builder=builder.setContent(getCustomDesign(title,message));
            }
            else{
                builder=builder.setContentTitle(title)
                        .setContentText(message)
                        .setSmallIcon(R.mipmap.ic_launcher);
            }

            NotificationManager notificationManager= (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
            if(Build.VERSION.SDK_INT>= Build.VERSION_CODES.O){
                NotificationChannel notificationChannel=new NotificationChannel(channel_id,"web_app", NotificationManager.IMPORTANCE_HIGH);
                notificationChannel.setSound(uri,null);
                notificationManager.createNotificationChannel(notificationChannel);
            }
            notificationManager.notify(0,builder.build());

        }
    }

}
