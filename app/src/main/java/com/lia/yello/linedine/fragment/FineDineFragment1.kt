package com.lia.yello.linedine.fragment

import android.content.Intent
import android.graphics.Color
import android.net.Uri
import android.os.Bundle
import android.util.Log
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.activity.OnBackPressedCallback
import androidx.core.content.ContextCompat
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.FragmentTransaction
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.LinearLayoutManager
import com.lia.yello.linedine.Models.ListData
import com.lia.yello.linedine.R
import com.lia.yello.linedine.Session.SharedHelper
import com.lia.yello.linedine.Utils.DialogUtils
import com.lia.yello.linedine.Utils.NetworkUtils
import com.lia.yello.linedine.Utils.UiUtils
import com.lia.yello.linedine.ViewModels.MapViewModel
import com.lia.yello.linedine.activity.DashBoardActivity
import com.lia.yello.linedine.adapter.*
import com.lia.yello.linedine.databinding.FragmentFineDine2Binding
import com.lia.yello.linedine.databinding.FragmentMyfavoritesBinding
import kotlinx.android.synthetic.main.activity_dash_board.*
import kotlinx.android.synthetic.main.carticon.*
import kotlinx.android.synthetic.main.fragment_fine_dine2.*
import kotlinx.android.synthetic.main.fragment_fine_dine2.bannerimg
import kotlinx.android.synthetic.main.fragment_fine_dine2.menu_description
import kotlinx.android.synthetic.main.fragment_fine_dine2.menuname
import kotlinx.android.synthetic.main.fragment_fine_dine2.rating
import kotlinx.android.synthetic.main.fragment_fine_dine2.time
import kotlinx.android.synthetic.main.fragment_fine_dine2.work
import kotlinx.android.synthetic.main.fragment_fine_dine3.*
import kotlinx.android.synthetic.main.fragment_home.*
import kotlinx.android.synthetic.main.fragment_in_queue1.*
import kotlinx.android.synthetic.main.fragment_myfavorites.*
import kotlinx.android.synthetic.main.fragment_subhome.*
import kotlinx.android.synthetic.main.fragment_subsubhome.*
import kotlinx.android.synthetic.main.fragment_subsubhome.recycler
import kotlinx.android.synthetic.main.header.*
import java.text.SimpleDateFormat
import java.time.LocalDate
import java.time.format.DateTimeFormatter
import java.util.*


      class FineDineFragment1 : Fragment(R.layout.fragment_fine_dine2) {
          var binding: FragmentFineDine2Binding? = null
          private var mapViewModel: MapViewModel? = null
          public var sharedHelper: SharedHelper? = null
          private var fineDineFragment2: FineDineFragment2? = null
          private lateinit var fragmentTransaction: FragmentTransaction
          private var fineDineMenuList: FineDineMenuListFragment? = null

          var string: String = ""
          var lat: Double = 0.0
          var lng: Double = 0.0
          var isclosed = false
          var istoday = false
          public var id: Int? = 0
          var hotelname: String = ""
          var desc: String = ""
          var workinghrs: String = ""
          var resrating: String = ""
          var resimage: String = ""
          var day: String = ""
          var date: String = ""
          var fulldate: String = ""
          var timeval: String = ""
          var timetopass = ""
          var iskid: String = ""
          var phoneee: String = ""
          var res_status: String = ""
          var montthh: String = ""
          var timee: String = ""
          var imgid: Int = 0
          var typee: String = ""
          var strDate: String = ""
          var inqueuestatus: String = ""
          var countadult: Int = 0
          var countkids: Int = 0
          var totalcount:Int=0
          var queuecount:Int=0
          var Dateinqueue:String=""
          private var homeFragment: HomeFragment? = null

          private var subHomeFragment: SubHomeFragment? = null

          override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
              super.onViewCreated(view, savedInstanceState)
              binding = DataBindingUtil.findBinding(view)
              sharedHelper = SharedHelper(requireContext())
              mapViewModel = ViewModelProvider(this).get(MapViewModel::class.java)
              val head = view.findViewById<TextView>(R.id.head)

              val localdate= LocalDate.now()
              Dateinqueue=DateTimeFormatter.ofPattern("dd/MM/yyyy").format(localdate)
              println(DateTimeFormatter.ofPattern("dd/MM/yyyy").format(localdate))
              if (sharedHelper!!.language == "ar") {
                  back.rotation = 180F
              }

              if (sharedHelper!!.choose.equals("finedine") || sharedHelper!!.choose.equals("inqueue") || sharedHelper!!.cartcount == 0) {

                  //var cartframe: FrameLayout =findViewById(R.id.cartframe)
                  cartframe.visibility = View.GONE
              }
              val calendar = Calendar.getInstance()
              val mdformat = SimpleDateFormat("hh a")
              strDate = "Current Time : " + mdformat.format(calendar.time)
              Log.d("asdefrgthjkl", "" + strDate)
              id = arguments?.getInt("id")
              imgid = arguments?.getInt("imgid")!!
              typee = arguments?.getString("type")!!
              inqueuestatus = arguments?.getString("queuestatus")!!
              (activity as DashBoardActivity?)!!.bottom_navigation_main?.visibility = View.VISIBLE
              (activity as DashBoardActivity?)!!.iv_add?.visibility = View.VISIBLE
              fulldate = ""
              timeval = ""
              //   Log.d("qwerftgyhujikolp",""+montthh)
              // month.text=montthh
              linear_bld_slot.alpha = 0.5f
              linear_bld_slot.isEnabled = false
              breakfast.isEnabled = false
              lunch.isEnabled = false
              dinner.isEnabled = false

              cart_badge.text = "" + sharedHelper!!.cartcount
              head.setText(getString(R.string.resturant))
              getvalues("All")



              if (sharedHelper!!.choose == "inqueue" && inqueuestatus == "Open") {

                  tableimg.visibility = View.VISIBLE
                  inqueuetxt1.visibility = View.VISIBLE
                  txtinqueue2.visibility = View.VISIBLE
                  seeourmenu.visibility = View.VISIBLE
                  recycler_time.visibility = View.GONE
                  timetext.visibility = View.GONE
                  linear_bld_slot.visibility = View.GONE
                  chooseanavail.visibility = View.GONE
                  textclosesorry.visibility = View.GONE
                  dayllslot.visibility = View.GONE
                  continuereserve.visibility = View.GONE
                  popupnewinqueue.visibility = View.GONE
                  belowrr.visibility = View.GONE

              } else if (sharedHelper!!.choose == "inqueue" && inqueuestatus == "Full") {

                  tableimg.visibility = View.GONE
                  inqueuetxt1.visibility = View.GONE
                  txtinqueue2.visibility = View.GONE
                  seeourmenu.visibility = View.GONE
                  recycler_time.visibility = View.GONE
                  timetext.visibility = View.GONE
                  linear_bld_slot.visibility = View.GONE
                  chooseanavail.visibility = View.GONE
                  textclosesorry.visibility = View.GONE
                  dayllslot.visibility = View.GONE
                  continuereserve.visibility = View.GONE
                  popupnewinqueue.visibility = View.GONE
                  belowrr.visibility = View.VISIBLE
                  getqueuecount(0)

              } else {
                  tableimg.visibility = View.GONE
                  inqueuetxt1.visibility = View.GONE
                  txtinqueue2.visibility = View.GONE
                  seeourmenu.visibility = View.GONE
                  recycler_time.visibility = View.VISIBLE
                  timetext.visibility = View.VISIBLE
                  linear_bld_slot.visibility = View.VISIBLE
                  chooseanavail.visibility = View.VISIBLE
                  textclosesorry.visibility = View.GONE
                  dayllslot.visibility = View.VISIBLE
                  continuereserve.visibility = View.VISIBLE
                  popupnewinqueue.visibility = View.GONE
                  belowrr.visibility = View.GONE

              }

              seeourmenu.setOnClickListener {
                  val bundle = Bundle()
                  id?.let { it1 -> bundle.putInt("resid", it1) }
                  bundle.putInt("orderid", 0)
                  bundle.putString("isfd", "true")
                  bundle.putSerializable("menulist", null)
                  bundle.putString("status", "")
                  bundle.putString("resname", "")
                  bundle.putString("resimg", resimage)
                  bundle.putDouble("lat", 0.0)
                  bundle.putDouble("lng", 0.0)
                  bundle.putString("paidstatus", "")
                  bundle.putString("paymenttype", "")
                  bundle.putString("isnewinqueue", "true")
                  fineDineMenuList = FineDineMenuListFragment()
                  fineDineMenuList!!.arguments = bundle
                  fragmentTransaction = requireActivity().supportFragmentManager.beginTransaction()
                  fragmentTransaction.setCustomAnimations(R.anim.screen_in, R.anim.screen_out)
                  fragmentTransaction.replace(R.id.container, fineDineMenuList!!)
                  fragmentTransaction.addToBackStack(null)
                  fragmentTransaction.commit()
              }

              back.setOnClickListener(View.OnClickListener {

                  //   requireActivity().supportFragmentManager.popBackStack()

                  val bundle = Bundle()
                  bundle.putString("iscuisine", "false")
                  bundle.putInt("imgid", imgid)
                  bundle.putString("type", typee)


                  subHomeFragment = SubHomeFragment()
                  subHomeFragment!!.arguments = bundle
                  fragmentTransaction = requireActivity().supportFragmentManager.beginTransaction()
                  fragmentTransaction.setCustomAnimations(R.anim.screen_in, R.anim.screen_out)
                  fragmentTransaction.replace(R.id.container, subHomeFragment!!)
                  fragmentTransaction.addToBackStack(null)
                  fragmentTransaction.commit()

              })
              if (!NetworkUtils.isNetworkConnected(requireContext())) {
                  UiUtils.showSnack(
                      view, "no_internet_connection"
                  )
              }


              activity?.onBackPressedDispatcher?.addCallback(
                  this,
                  object : OnBackPressedCallback(true) {
                      override fun handleOnBackPressed() {

                          val bundle = Bundle()
                          bundle.putString("iscuisine", "false")
                          bundle.putInt("imgid", imgid)
                          bundle.putString("type", typee)


                          subHomeFragment = SubHomeFragment()
                          subHomeFragment!!.arguments = bundle
                          fragmentTransaction =
                              requireActivity().supportFragmentManager.beginTransaction()
                          fragmentTransaction.setCustomAnimations(
                              R.anim.screen_in,
                              R.anim.screen_out
                          )
                          fragmentTransaction.replace(R.id.container, subHomeFragment!!)
                          fragmentTransaction.addToBackStack(null)
                          fragmentTransaction.commit()
                      }
                  })



              continuereserve.setOnClickListener(View.OnClickListener {

                  Log.d("fulldate", "" + fulldate)

                  if (fulldate.equals("") && timeval.equals("")) {

                      UiUtils.showSnack(
                          root_finedine1,
                          getString(R.string.pleaseselectbookingtimeanddate)
                      )

                  } else if (fulldate.equals("") || timeval.equals("")) {
                      if (fulldate.equals("")) {

                          UiUtils.showSnack(
                              root_finedine1,
                              getString(R.string.pleaseselectbookinkdate)
                          )
                      } else if (timeval.equals("")) {

                          UiUtils.showSnack(
                              root_finedine1,
                              getString(R.string.pleaseselectthebookingtime)
                          )
                      }
                  } else {
                      val bundle = Bundle()
                      timetopass = date + " " + day + " ," + " " + timeval + " pm"
                      Log.d("wedrfcgb", "" + timetopass)
                      bundle.putString("resname", hotelname)
                      bundle.putString("descrip", desc)
                      bundle.putString("rating", resrating)
                      bundle.putString("workinghrs", workinghrs)
                      bundle.putString("timetopass", timetopass)
                      bundle.putString("resimage", resimage)
                      bundle.putDouble("lat", lat)
                      bundle.putDouble("lng", lng)
                      bundle.putString("time", timeval)
                      bundle.putString("date", fulldate)
                      bundle.putInt("resid", id!!)
                      bundle.putString("iskids", iskid)
                      bundle.putString("phone", phoneee)
                      bundle.putString("resstatus", res_status)
                      bundle.putInt("imgid", imgid)
                      bundle.putString("type", typee)

                      fineDineFragment2 = FineDineFragment2()
                      fineDineFragment2!!.arguments = bundle
                      fragmentTransaction =
                          requireActivity().supportFragmentManager.beginTransaction()
                      fragmentTransaction.setCustomAnimations(R.anim.screen_in, R.anim.screen_out)
                      fragmentTransaction.replace(R.id.container, fineDineFragment2!!)
                      fragmentTransaction.addToBackStack(null)
                      fragmentTransaction.commit()

                  }


              })

              breakfast.setOnClickListener {
                  linear_bld_slot.isEnabled = true
                  linear_bld_slot.alpha = 1.0f
                  recycler_time.alpha = 0.5f
                  recycler_time.isEnabled = false
                  string = "Breakfast"
                  timeval = ""
                  Log.d("wertyui", "" + string)
                  recycler_time.visibility = View.VISIBLE
                  timetext.visibility = View.VISIBLE
                  chooseanavail.visibility = View.VISIBLE
                  viewbreakfast.visibility = View.VISIBLE
                  viewlunch.visibility = View.INVISIBLE
                  viewdinner.visibility = View.INVISIBLE
                  getvaluestime(string)

              }
              lunch.setOnClickListener {
                  string = "Lunch"
                  breakfast.isEnabled = true
                  lunch.isEnabled = true
                  dinner.isEnabled = true
                  timeval = ""
                  recycler_time.visibility = View.VISIBLE
                  timetext.visibility = View.VISIBLE
                  chooseanavail.visibility = View.VISIBLE
                  viewlunch.visibility = View.VISIBLE
                  viewdinner.visibility = View.INVISIBLE
                  viewbreakfast.visibility = View.INVISIBLE
                  getvaluestime(string)
                  Log.d("wertyui", "" + string)

              }

              dinner.setOnClickListener {
                  breakfast.isEnabled = true
                  lunch.isEnabled = true
                  dinner.isEnabled = true
                  string = "Dinner"
                  timeval = ""
                  recycler_time.visibility = View.VISIBLE
                  timetext.visibility = View.VISIBLE
                  chooseanavail.visibility = View.VISIBLE
                  viewdinner.visibility = View.VISIBLE
                  viewbreakfast.visibility = View.INVISIBLE
                  viewlunch.visibility = View.INVISIBLE

                  Log.d("wertyui", "" + string)
                  getvaluestime(string)


              }




              getvaluesday()

              finedineloc.setOnClickListener(View.OnClickListener {
                  val uri = String.format(
                      Locale.ENGLISH,
                      "google.navigation:q=$lat,$lng"
                  )
                  val gmmIntentUri = Uri.parse(uri)
                  val mapIntent = Intent(Intent.ACTION_VIEW, gmmIntentUri)
                  mapIntent.setPackage("com.google.android.apps.maps")
                  requireActivity().startActivity(mapIntent)
              })

              // for adults
              imgadultpluss.setOnClickListener(View.OnClickListener {
                  countadult++
                  adulttxtquantityy.setText("" + countadult)
                  Log.d("count", "" + countadult)
                  totalcount = countadult + countkids
              })
              imgadultminuss.setOnClickListener(View.OnClickListener {
                  /* if (countadult == 0) {
                      adulttxtquantity.setText("" + countadult)
                  } else {
                      countadult--
                      adulttxtquantity.setText("" + countadult)
                      totalcount =countadult+countkids
                  }*/

                  if (countadult > 0) {
                      countadult = countadult - 1
                      Log.d("zxcdfvgbhnjmk,.", "" + countadult)
                      adulttxtquantityy.setText("" + countadult)
                      totalcount = countadult + countkids
                  }
              })
              // for kids
              kidsimgpluss.setOnClickListener(View.OnClickListener {
                  countkids++
                  kidstxtquantity.setText("" + countkids)
                  Log.d("count", "" + countkids)
                  totalcount = countadult + countkids
              })
              kidsimgminuss.setOnClickListener(View.OnClickListener {
                  /*  if (countkids == 0 ) {
                      kidstxtquantity.setText("" + countkids)
                      totalcount =countadult+countkids
                  } else {
                      countkids--
                      kidstxtquantity.setText("" + countkids)
                      totalcount =countadult+countkids
                  }*/


                  if (countkids > 0) {
                      countkids = countkids - 1
                      Log.d("zxcdfvgbhnjmk,.", "" + countkids)
                      kidstxtquantity.setText("" + countkids)
                      totalcount = countadult + countkids
                  }
              })
              continuetoqueuee.setOnClickListener(View.OnClickListener {
                  if (countadult == 0) {

                      UiUtils.showSnack(
                          rootfinedine3,
                          getString(R.string.bookingcannotreservewithoutanuadult)
                      )
                  } else if (totalcount == 0) {
                      UiUtils.showSnack(rootfinedine3, getString(R.string.pleaseselecthenoofseats))
                  } else {
                      getaddresturantqueue(totalcount)
                  }
              })
          }


          public fun getvalues(txt: String) {
              DialogUtils.showLoader(requireContext())
              mapViewModel?.getMenuDetail(requireContext(), id!!, txt)
                  ?.observe(viewLifecycleOwner,
                      Observer {
                          DialogUtils.dismissLoader()

                          it?.let {
                              it.error?.let { error ->
                                  if (error) {
                                      it.message?.let { msg ->
                                          UiUtils.showSnack(root_finedine1, msg)
                                          //  recycler.visibility = View.GONE

                                          //  Toast.makeText(context, msg, Toast.LENGTH_SHORT).show()

                                      }
                                  } else {
                                      //  recycler.visibility = View.VISIBLE
                                      it.message?.let { msg ->
                                          // UiUtils.showSnack(root_finedine1, msg)

                                          // Toast.makeText(context, msg, Toast.LENGTH_SHORT).show()
                                      }

                                      it.data?.let { data ->

                                          if (data.restdetail!!.kids.equals("notallowed")) {
                                              kidsnotallowed.visibility = View.VISIBLE
                                              iskid = "false"
                                          } else {

                                              kidsnotallowed.visibility = View.GONE
                                              iskid = "true"

                                          }
                                          Log.d("asxdcfvgb", "" + data.restdetail)
                                          Log.d("asxdcfvgb", "" + data.data2)
                                          Log.d("asxdcfvgb", "" + data.menudetail)
                                          data.restdetail?.res_name?.let { menuname.text = it }
                                          hotelname = data.restdetail!!.res_name.toString()

                                          resrating = data.restdetail!!.rating.toString()
                                          desc = data.restdetail!!.description.toString()
                                          if (data.restdetail!!.rating.equals("")) {
                                              rating.text = "0" + "/" + "5"

                                          } else {
                                              rating.text = data.restdetail!!.rating + "/" + "5"

                                          }

                                          data.restdetail?.res_name?.let { head.text = it }
                                          data.restdetail?.description?.let {
                                              menu_description.text = it
                                          }
                                          if (data.data2!![0].closed.equals("0")) {
                                              recycler_time.visibility = View.VISIBLE
                                              // linear_bld_slot.visibility = View.VISIBLE
                                              // continuereserve.visibility = View.VISIBLE

                                              if (data.data2!!.size != 0) {
                                                  var time1: String? = data.data2!![0].openingTime
                                                  var time2: String? = data.data2!![0].closingTime
                                                  var time3: String? =
                                                      " " + time1 + " - " + time2 + " " + " Hrs"
                                                  Log.d("sdfghjkl", "" + time3)
                                                  workinghrs = time3!!
                                                  work.visibility = View.VISIBLE
                                                  time.text = time3
                                              } else {
                                                  time.text = getString(R.string.invalidtime)
                                              }
                                          } else {

                                              if (sharedHelper!!.choose.equals("finedine")){

                                                  work.visibility = View.GONE
                                                  time.text = getString(R.string.closed)
                                                  isclosed = true
                                                  recycler_time.visibility = View.GONE
                                                  linear_bld_slot.visibility = View.GONE
                                                  continuereserve.visibility = View.VISIBLE
                                                  textclosesorry.visibility = View.VISIBLE
                                                  imgcloseicon.visibility = View.VISIBLE
                                                  chooseanavail.visibility = View.GONE
                                                  continuereserve.background =
                                                      requireContext().getDrawable(R.drawable.border_layoutgreyy)
                                                  continuereserve.isClickable = false
                                              }



                                          }
                                          res_status = data.restdetail!!.res_status!!
                                          phoneee = data.restdetail!!.phonee!!
                                          lat = data.restdetail?.lat!!
                                          lng = data.restdetail?.lng!!
                                          data.restdetail?.bannerimg?.let {
                                              UiUtils.loadImage(
                                                  bannerimg,
                                                  it,
                                                  ContextCompat.getDrawable(
                                                      requireContext(),
                                                      R.drawable.maskgroup46
                                                  )!!
                                              )
                                          }

                                          resimage = data.restdetail!!.bannerimg.toString()


                                      }
                                  }
                              }

                          }
                      })
          }

          public fun getvaluesday() {
              DialogUtils.showLoader(requireContext())
              mapViewModel?.getdayslot(requireContext(), id!!)
                  ?.observe(viewLifecycleOwner,
                      Observer {
                          DialogUtils.dismissLoader()

                          it?.let {
                              it.error?.let { error ->
                                  if (error) {
                                      it.message?.let { msg ->

                                          UiUtils.showSnack(root_finedine1, msg)

                                      }
                                  } else {
                                      it.data?.let { data ->
                                          montthh = data[0].month.toString()
                                          Log.d("monthijijij", "" + montthh)
                                          month.text = montthh
                                          recycler_dayslot.layoutManager = LinearLayoutManager(
                                              context,
                                              LinearLayoutManager.HORIZONTAL,
                                              false
                                          )
                                          recycler_dayslot.adapter = DayAdapter(
                                              this,
                                              requireContext(),
                                              data
                                          )
                                      }

                                  }

                              }

                          }
                      })

          }

          public fun getvaluestime(txt: String) {
              DialogUtils.showLoader(requireContext())
              mapViewModel?.gettimeslot(requireContext(), id!!, txt, day)
                  ?.observe(viewLifecycleOwner,
                      Observer {
                          DialogUtils.dismissLoader()

                          it?.let {
                              it.error?.let { error ->
                                  if (error) {
                                      it.message?.let { msg ->
                                          UiUtils.showSnack(
                                              root_finedine1,
                                              getString(R.string.timeslotiscurrentlynotavailable)
                                          )
                                          recycler_time.visibility = View.INVISIBLE
                                          timetext.visibility = View.VISIBLE

                                      }
                                  } else {
                                      it.data?.let { data ->

                                          Log.d("timeeeeee", "" + data)
                                          recycler_time.visibility = View.VISIBLE
                                          timetext.visibility = View.VISIBLE
                                          recycler_time.layoutManager = LinearLayoutManager(
                                              context,
                                              LinearLayoutManager.HORIZONTAL,
                                              false
                                          )
                                          recycler_time.adapter = TimeSlotAdapter(
                                              this,
                                              requireContext(),
                                              data
                                          )

                                      }
                                  }

                              }

                          }
                      })
          }


          public fun getqueuecount(orderid: Int) {
              DialogUtils.showLoader(requireContext())
              mapViewModel?.getqueuecount(requireContext(), orderid, id!!, "false", Dateinqueue,"inqueue")
                  ?.observe(viewLifecycleOwner, androidx.lifecycle.Observer {

                      DialogUtils.dismissLoader()

                      it?.let {
                          it.error?.let { error ->
                              if (error) {


                              } else {
                                  it.data?.let { data ->
                                      queuecount= data.totcount!!+1

                                    //  popupnewinqueue.visibility=View.VISIBLE
                                     // showinqueuenoo.text="#" +data.totcount !!
                                  }

                              }


                          }


                      }

                  })

          }


          public fun getaddresturantqueue(totalcount: Int) {
              DialogUtils.showLoader(requireContext())
              mapViewModel?.getaddresturantqueue(requireContext(),queuecount,id!!,totalcount)
                  ?.observe(viewLifecycleOwner, androidx.lifecycle.Observer {

                      DialogUtils.dismissLoader()

                      it?.let {
                          it.error?.let { error ->
                              if (error) {


                              } else {

                                  popupnewinqueue.visibility=View.VISIBLE
                                  showinqueuenoo.text="#" + queuecount+1
                              }


                          }


                      }

                  })

          }
      }