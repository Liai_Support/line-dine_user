package com.lia.yello.linedine.fragment


import  android.os.Bundle
import android.view.View
import android.widget.TextView
import androidx.activity.OnBackPressedCallback
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentTransaction
import com.lia.yello.linedine.R
import com.lia.yello.linedine.Utils.UiUtils
import com.lia.yello.linedine.ViewModels.MapViewModel
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.GridLayoutManager
import com.lia.yello.linedine.Session.SharedHelper
import com.lia.yello.linedine.Utils.DialogUtils
import com.lia.yello.linedine.Utils.NetworkUtils
import com.lia.yello.linedine.adapter.*
import com.lia.yello.linedine.databinding.*
import kotlinx.android.synthetic.main.carticon.*
import kotlinx.android.synthetic.main.fragment_myfavorites.*
import kotlinx.android.synthetic.main.header.*


class FavoritesFragment : Fragment(R.layout.fragment_myfavorites) {

    var binding: FragmentMyfavoritesBinding? = null
    private var mapViewModel: MapViewModel? = null
    public var sharedHelper: SharedHelper? = null
    private var homeFragment : HomeFragment? = null
    private var subsubHomeFragment: SubsubHomeFragment? = null
    private lateinit var fragmentTransaction: FragmentTransaction
    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        binding = DataBindingUtil.findBinding(view)
        sharedHelper = SharedHelper(requireContext())
        mapViewModel = ViewModelProvider(this).get(MapViewModel::class.java)
        val head = view.findViewById<TextView>(R.id.head)
        head.setText(getString(R.string.myfav))
        cart_badge.text = ""+ sharedHelper!!.cartcount

        if (!NetworkUtils.isNetworkConnected(requireContext())) {
            UiUtils.showSnack(
                view, "no_internet_connection"
            )
        }


        if (sharedHelper!!.language == "ar"){
            back.rotation= 180F
        }

        getvalues()
        back.setOnClickListener(View.OnClickListener {
          //  requireActivity().supportFragmentManager.popBackStack()
            homeFragment = HomeFragment()
            fragmentTransaction =requireActivity().supportFragmentManager.beginTransaction()
            fragmentTransaction.setCustomAnimations(R.anim.screen_in, R.anim.screen_out)
            fragmentTransaction.replace(R.id.container, homeFragment!!)
            fragmentTransaction.addToBackStack(null)
            fragmentTransaction.commit()
        })

        if (sharedHelper!!.choose.equals("finedine")||sharedHelper!!.choose.equals("inqueue")|| sharedHelper!!.cartcount == 0){

            //var cartframe: FrameLayout =findViewById(R.id.cartframe)
            cartframe.visibility=View.GONE
        }
        activity?.onBackPressedDispatcher?.addCallback(this, object : OnBackPressedCallback(true) {
            override fun handleOnBackPressed() {

                homeFragment = HomeFragment()
                fragmentTransaction =requireActivity().supportFragmentManager.beginTransaction()
                fragmentTransaction.setCustomAnimations(R.anim.screen_in, R.anim.screen_out)
                fragmentTransaction.replace(R.id.container, homeFragment!!)
                fragmentTransaction.addToBackStack(null)
                fragmentTransaction.commit()
            }
        })
    }

    private fun getvalues(){
        DialogUtils.showLoader(requireContext())

        mapViewModel?.getfavorites(requireContext())
            ?.observe(viewLifecycleOwner,
                Observer {
                    DialogUtils.dismissLoader()

                    it?.let {
                        it.error?.let { error ->
                            if (error) {
                                it.message?.let { msg ->
                                    UiUtils.showSnack(root_fav, msg)
                                }
                            } else  {
                                it.data?.let { data ->



                                    recyclerfav.layoutManager =
                                        GridLayoutManager(
                                            context,
                                            2
                                        )
                                    recyclerfav.adapter = FavoritesAdapter(
                                        this,
                                        requireContext(),
                                        data.listfav
                                    )

                                }
                            }
                        }

                    }
                })
    }



     fun checkfavroite(resid:Int){
        DialogUtils.showLoader(requireContext())

        mapViewModel?.getcheckFavRest(requireContext(),resid)
            ?.observe(viewLifecycleOwner,
                Observer {
                    DialogUtils.dismissLoader()

                    it?.let {
                        it.error?.let { error ->
                            if (error) {
                                it.message?.let { msg ->
                                    UiUtils.showSnack(root_fav, msg)
                                }
                            } else  {
                                it.data?.let { data ->

                                    val bundle = Bundle()
                                    bundle.putInt("id", data.resid!!)
                                    bundle.putInt("imgid",0)
                                    bundle.putString("type", "")
                                    bundle.putString("promocode", "")
                                    subsubHomeFragment = SubsubHomeFragment()
                                    subsubHomeFragment!!.arguments=bundle
                                    fragmentTransaction = requireActivity().supportFragmentManager.beginTransaction()
                                    fragmentTransaction.setCustomAnimations(R.anim.screen_in,R.anim.screen_out)
                                    fragmentTransaction.replace(R.id.container, subsubHomeFragment!!)
                                    fragmentTransaction.addToBackStack(null)
                                    fragmentTransaction.commit()
                                }
                            }
                        }

                    }
                })
    }


}