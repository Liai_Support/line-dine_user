package com.app.hakeemUser.models

import com.google.gson.annotations.SerializedName
import java.io.Serializable

class SocialLoginResponse : Serializable {

    @SerializedName("error")
    var error: Boolean? = null

    @SerializedName("message")
    var message: String? = null

    @SerializedName("data")
    var data: SocialLoginData? = null


}


class SocialLoginData : Serializable {

    @SerializedName("id")
    var id: Int? = null

    @SerializedName("firstname")
    var name: String? = null

    @SerializedName("lastname")
    var lname: String? = null

    @SerializedName("email")
    var email: String? = null

    @SerializedName("phone")
    var mobileNumber: String? = null

    @SerializedName("socialtoken")
    var token: String? = null

    @SerializedName("socialmedia")
    var loginType: String? = null

    @SerializedName("countrycode")
    var countryCode: String? = null

    @SerializedName("userexists")
    var userExists: Boolean? = null

  /*  @SerializedName("profilePic")
    var profilePic: String? = null*/


}