package com.lia.yello.linedine.adapter

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.LinearLayout
import android.widget.TextView
import androidx.fragment.app.FragmentTransaction
import androidx.recyclerview.widget.RecyclerView
import com.lia.yello.linedine.Models.Notification
import com.lia.yello.linedine.R
import com.lia.yello.linedine.databinding.Notification1Binding
import com.lia.yello.linedine.databinding.OrderprocessinngmenuadapterBinding
import com.lia.yello.linedine.fragment.*
import kotlinx.android.synthetic.main.pop_up_movetopay.*

class ListNotiAdapter(
        var notificationFragment: NotificationFragment,
        var context: Context,
        var list: ArrayList<Notification>) :
        RecyclerView.Adapter<ListNotiAdapter.HomeHeaderViewHolder>() {

        var id:Int=0
        private lateinit var fragmentTransaction: FragmentTransaction
        private var cartFragment: CartFragment? = null
        inner class HomeHeaderViewHolder(view: View) : RecyclerView.ViewHolder(view) {
            var binding: Notification1Binding = Notification1Binding.bind(view)

       /*     lateinit var title: TextView
        lateinit var date: TextView
        lateinit var message: TextView
        lateinit var notification_linear: LinearLayout

        init {

            title=view.findViewById(R.id.notification_title)
            date=view.findViewById(R.id.ordertime)
            message=view.findViewById(R.id.message)
            notification_linear=view.findViewById(R.id.notification_linear)

        }*/
        }

        override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): HomeHeaderViewHolder {

        return HomeHeaderViewHolder(LayoutInflater.from(context).inflate(R.layout.notification1, parent, false))

            }

        override fun getItemCount(): Int {
        return list!!.size

            }

    override fun onBindViewHolder(holder: HomeHeaderViewHolder, position: Int) {

        holder.binding.notificationTitle.text = list!![position].title
        holder.binding.ordertime.text = list!![position].date
        holder.binding.message.text = list!![position].message



    }
}