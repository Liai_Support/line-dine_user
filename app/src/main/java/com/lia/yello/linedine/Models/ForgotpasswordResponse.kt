package com.lia.yello.linedine.Models

import com.google.gson.annotations.SerializedName
import java.io.Serializable

class ForgotpasswordResponse : Serializable {

    @SerializedName("error")
    var error: Boolean? = null

    @SerializedName("message")
    var message: String? = null

    @SerializedName("data")
    var data: Userid? = null
}
  class Userid : Serializable {

    @SerializedName("userid")
    var userid: Int? = null


      @SerializedName("OTP")
      var OTP: String? = null
}