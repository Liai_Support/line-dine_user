package com.lia.yello.linedine.fragment

import android.app.Activity
import android.app.AlertDialog
import android.content.Intent
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.net.Uri
import android.os.Bundle
import android.util.DisplayMetrics
import android.util.Log
import android.view.Gravity
import android.view.LayoutInflater
import android.view.View
import android.view.WindowManager
import android.widget.TextView
import androidx.activity.OnBackPressedCallback
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentTransaction
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.LinearLayoutManager
import com.lia.yello.linedine.R
import com.lia.yello.linedine.Session.SharedHelper
import com.lia.yello.linedine.Utils.DialogUtils
import com.lia.yello.linedine.Utils.NetworkUtils
import com.lia.yello.linedine.Utils.UiUtils
import com.lia.yello.linedine.ViewModels.MapViewModel
import com.lia.yello.linedine.activity.NoonPaymentActivity
import com.lia.yello.linedine.adapter.*
import com.lia.yello.linedine.databinding.FragmentWalletBinding
import kotlinx.android.synthetic.main.carticon.*
import kotlinx.android.synthetic.main.fragment_wallet.*
import kotlinx.android.synthetic.main.header.*
import kotlinx.android.synthetic.main.popup_addmoney.view.*
import java.util.*

class WalletFragment : Fragment(R.layout.fragment_wallet) {

    var binding: FragmentWalletBinding? = null
    var mapViewModel: MapViewModel? = null
    var amount: Double = 0.0
    var sharedHelper: SharedHelper? = null
    private var homeFragment : HomeFragment? = null
    private lateinit var fragmentTransaction: FragmentTransaction
    var pointsb:String="false"

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        binding = DataBindingUtil.findBinding(view)
        val head = view.findViewById<TextView>(R.id.head)
        sharedHelper = SharedHelper(requireContext())

        mapViewModel = ViewModelProvider(this).get(MapViewModel::class.java)
        getwallethistry()
        getwalletamount()
        cart_badge.text = ""+ sharedHelper!!.cartcount

        head.setText(getString(R.string.wallet))
        addmoney.setOnClickListener(View.OnClickListener {
            showDialog1()
        })
        back.setOnClickListener(View.OnClickListener {
            // requireActivity().supportFragmentManager.popBackStack()
            homeFragment = HomeFragment()
            fragmentTransaction =requireActivity().supportFragmentManager.beginTransaction()
            fragmentTransaction.setCustomAnimations(R.anim.screen_in, R.anim.screen_out)
            fragmentTransaction.replace(R.id.container, homeFragment!!)
            fragmentTransaction.addToBackStack(null)
            fragmentTransaction.commit()
        })




        points.setOnClickListener {
            pointsb="true"
            wallettitle.text=getString(R.string.totalpoint)

            addmoney.visibility=View.GONE
            poinintshere.visibility=View.VISIBLE

            points.setBackgroundColor(Color.parseColor("#A41620"))
            wallet.setBackgroundColor(Color.parseColor("#AEAEAE"))

            getpointAmount()
            getPointsHistory()

        }


        wallet.setOnClickListener {
            addmoney.visibility=View.VISIBLE
            poinintshere.visibility=View.GONE
            wallettitle.text=getString(R.string.totalbalance)
            points.setBackgroundColor(Color.parseColor("#AEAEAE"))
            wallet.setBackgroundColor(Color.parseColor("#A41620"))

            getwalletamount()
            getwallethistry()

        }

        poinintshere.setOnClickListener{
            val intent = Intent(
                "android.intent.action.VIEW",
                Uri.parse("https://lineandine.com/")
            )
            startActivity(intent)

        }


        /*     recyclerwallet.setOnTouchListener(object : OnSwipeTouchListener1(context){
                 override fun onSwipeLeft() {
                     //    Toast.makeText(context, "left", Toast.LENGTH_SHORT).show();

                     homeFragment = HomeFragment()
                     fragmentTransaction =requireActivity().supportFragmentManager.beginTransaction()
                     fragmentTransaction.setCustomAnimations(R.anim.screen_in, R.anim.screen_out)
                     fragmentTransaction.replace(R.id.container, homeFragment!!)
                     fragmentTransaction.addToBackStack(null)
                     fragmentTransaction.commit()

                 }
                 override fun onSwipeRight() {
                     Toast.makeText(context, "left", Toast.LENGTH_SHORT).show();

                 }
             })*/

        if (sharedHelper!!.choose.equals("finedine")||sharedHelper!!.choose.equals("inqueue")|| sharedHelper!!.cartcount == 0){

            //var cartframe: FrameLayout =findViewById(R.id.cartframe)
            cartframe.visibility=View.GONE
        }
        if (!NetworkUtils.isNetworkConnected(requireContext())) {
            UiUtils.showSnack(
                view, "no_internet_connection"
            )
        }

        if (sharedHelper!!.language == "ar"){
            back.rotation= 180F
        }


        activity?.onBackPressedDispatcher?.addCallback(this, object : OnBackPressedCallback(true) {
            override fun handleOnBackPressed() {

                homeFragment = HomeFragment()
                fragmentTransaction =requireActivity().supportFragmentManager.beginTransaction()
                fragmentTransaction.setCustomAnimations(R.anim.screen_in, R.anim.screen_out)
                fragmentTransaction.replace(R.id.container, homeFragment!!)
                fragmentTransaction.addToBackStack(null)
                fragmentTransaction.commit()
            }
        })

    }

    private fun showDialog1() {
        val mDialogView = LayoutInflater.from(requireContext()).inflate(R.layout.popup_addmoney, null)
        val mBuilder = AlertDialog.Builder(requireContext()).setView(mDialogView)
        val  mAlertDialog = mBuilder.show()
        //mAlertDialog.getWindow()?.setLayout(100,40)
        val layoutParams = WindowManager.LayoutParams()
        val displayMetrics = DisplayMetrics()
        requireActivity().windowManager.defaultDisplay.getMetrics(displayMetrics)
        val displayWidth = displayMetrics.widthPixels
        val displayHeight = displayMetrics.heightPixels
        layoutParams.copyFrom(mAlertDialog.getWindow()?.getAttributes())

        layoutParams.width = ((displayWidth * 0.8f).toInt())
        mAlertDialog.getWindow()?.setAttributes(layoutParams)
        mAlertDialog.getWindow()?.setGravity(Gravity.CENTER)
        mAlertDialog.getWindow()?.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
        mDialogView.confirmw.setOnClickListener {

            Log.d("ertyuiop",""+mDialogView.amount.text.toString())
            if (mDialogView.amount.text.toString() == ""){
                mAlertDialog.dismiss()

                UiUtils.showSnack(root_wallet, getString(R.string.enteramount))

                //   mDialogView.amount.setError(getString(R.string.enteramount))

            }else{
                amount = mDialogView.amount.text.toString().toDouble()
                Log.d("amount",""+amount)

                startActivityForResult(
                    Intent(requireContext(), NoonPaymentActivity::class.java)
                        .putExtra(
                            "amount",
                            amount
                        ), 100
                )
                mAlertDialog.dismiss()
            }


            /*  amount = mDialogView.amount.text.toString().toDouble()
              Log.d("amount",""+amount)
              if (amount > 0) {

               //   addwalletamount()

                  // processPayment(value)
                  startActivityForResult(
                      Intent(requireContext(), NoonPaymentActivity::class.java)
                          .putExtra(
                              "amount",
                              amount
                          ), 100
                  )
                  mAlertDialog.dismiss()
              } else {
                  mDialogView.amount.setError(getString(R.string.enteramount))
              }*/
        }
        mDialogView.cancel.setOnClickListener(View.OnClickListener {
            mAlertDialog.dismiss()
        })
    }



    private fun addwalletamount(stringExtra: String?) {
        DialogUtils.showLoader(requireContext())

        if (stringExtra != null) {
            mapViewModel?.getaddwallet(requireContext(),stringExtra,amount)
                ?.observe(viewLifecycleOwner, androidx.lifecycle.Observer {
                    DialogUtils.dismissLoader()

                    it?.let {
                        it.error?.let { error ->
                            if (error) {
                                it.message?.let { msg ->
                                    UiUtils.showSnack(root_wallet, msg)
                                }
                            } else {
                                UiUtils.showSnack(root_wallet, getString(R.string.amountadded))
                                getwalletamount()
                                getwallethistry()
                            }
                        }

                    }
                })
        }

    }
    private fun getwallethistry(){
        DialogUtils.showLoader(requireContext())

        mapViewModel?.getwallethistory(requireContext())
            ?.observe(viewLifecycleOwner, androidx.lifecycle.Observer {
                DialogUtils.dismissLoader()

                it?.let {
                    it.error?.let { error ->
                        if (error) {
                            it.message?.let { msg ->
                                UiUtils.showSnack(root_wallet, msg)
                            }
                        } else {
                            it.data?.let { data ->
                                recyclerwallet.visibility=View.VISIBLE

                                recyclerwallet.layoutManager = LinearLayoutManager(context)
                                recyclerwallet.adapter = WalletAdapter(
                                    this,
                                    requireContext(),
                                    data.walletdata
                                )
                            }

                        }
                    }

                }
            })

    }
    private fun getwalletamount(){
        DialogUtils.showLoader(requireContext())

        mapViewModel?.getwalletamount(requireContext())
            ?.observe(viewLifecycleOwner, androidx.lifecycle.Observer {
                DialogUtils.dismissLoader()

                it?.let {
                    it.error?.let { error ->
                        if (error) {
                            it.message?.let { msg ->
                                UiUtils.showSnack(root_wallet, msg)
                            }
                        } else {
                            it.data?.let { data ->

                                Log.d("amount", data.amount.toString())
                                var amount:Double = data.amount!!
                                walletamount.text =String.format(Locale("en", "US"),"%.2f",amount)+" SAR"

                                /*walletamount.text = "$amount SAR"*/
                                Log.d("sdxcfvgbnm",""+requireActivity().walletamount.text)

                            }

                        }
                    }

                }
            })

    }




    private fun getPointsHistory(){
        DialogUtils.showLoader(requireContext())

        mapViewModel?.getpointhistory(requireContext())
            ?.observe(viewLifecycleOwner, androidx.lifecycle.Observer {
                DialogUtils.dismissLoader()

                it?.let {
                    it.error?.let { error ->
                        if (error) {
                            it.message?.let { msg ->
                                UiUtils.showSnack(root_wallet, msg)
                                recyclerwallet.visibility=View.GONE
                            }
                        } else {
                            it.data?.let { data ->
                                recyclerwallet.layoutManager = LinearLayoutManager(context)
                                recyclerwallet.adapter = GetPointsHistoryAdapter(
                                    this,
                                    requireContext(),
                                    data.walletdata
                                )
                            }

                        }
                    }

                }
            })

    }
    private fun getpointAmount(){
        DialogUtils.showLoader(requireContext())

        mapViewModel?.getpointResponsee(requireContext())
            ?.observe(viewLifecycleOwner, androidx.lifecycle.Observer {
                DialogUtils.dismissLoader()

                it?.let {
                    it.error?.let { error ->
                        if (error) {
                            it.message?.let { msg ->
                                UiUtils.showSnack(root_wallet, msg)
                            }
                        } else {
                            it.data?.let { data ->

                                Log.d("amount", data.points.toString())
                                var amount = data.points!!
                                walletamount.text =String.format(Locale("en", "US"),"%.2f",amount.toDouble())

                                /*walletamount.text = "$amount SAR"*/
                                Log.d("sdxcfvgbnm",""+requireActivity().walletamount.text)

                            }

                        }
                    }

                }
            })

    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (requestCode == 100) {
            if (resultCode == Activity.RESULT_OK) {
                addwalletamount(data!!.getStringExtra("paymenttransactionid"));
                //  sendPaymentDetails(data!!.getStringExtra("paymentorderid").toString(),data!!.getStringExtra("paymenttransactionid").toString())

            }
        }
    }

}