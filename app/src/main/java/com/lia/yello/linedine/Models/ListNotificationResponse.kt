package com.lia.yello.linedine.Models

import com.google.gson.annotations.SerializedName
import java.io.Serializable

class ListNotificationResponse : Serializable {


    @SerializedName("error")
    var error: Boolean? = null

    @SerializedName("message")
    var message: String? = null

    @SerializedName("data")
    var data: ListNotification? = null


}


class ListNotification : Serializable {

    @SerializedName("notification_data")
    var notification_data: ArrayList<Notification>? = null

}
class Notification : Serializable {

    @SerializedName("id")
    var id: Int? = null

    @SerializedName("message")
    var message: String? = null

    @SerializedName("title")
    var title: String? = null

    @SerializedName("date")
    var date: String? = null


}