package com.lia.yello.linedine.adapter

import android.app.AlertDialog
import android.content.Context
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.os.Build
import android.text.Editable
import android.text.TextWatcher
import android.util.DisplayMetrics
import android.util.Log
import android.view.*
import androidx.annotation.RequiresApi
import androidx.core.content.ContentProviderCompat.requireContext
import androidx.fragment.app.FragmentTransaction
import androidx.recyclerview.widget.RecyclerView
import com.lia.yello.linedine.Models.DiscountEventDateJson
import com.lia.yello.linedine.Models.EventsArray
import com.lia.yello.linedine.Models.MenuDetailJson
import com.lia.yello.linedine.R
import com.lia.yello.linedine.Utils.BaseUtils
import com.lia.yello.linedine.Utils.DialogUtils
import com.lia.yello.linedine.databinding.EventedittextBinding
import com.lia.yello.linedine.fragment.ProfileFragment
import com.lia.yello.linedine.fragment.SubsubHomeFragment
import kotlinx.android.synthetic.main.calenderview.*
import kotlinx.android.synthetic.main.fragment_fine_dine_order_list_status.*
import kotlinx.android.synthetic.main.fragment_profile.*
import kotlinx.android.synthetic.main.popup_cancelorder.*
import java.text.SimpleDateFormat
import java.time.LocalDate
import java.time.format.DateTimeFormatter
import java.util.*
import kotlin.collections.ArrayList

class EventDiscountDateAdapter(
    var profileFragment: ProfileFragment,
    var context: Context,
    var list: ArrayList<EventsArray>
) :
    RecyclerView.Adapter<EventDiscountDateAdapter.HomeHeaderViewHolder>() {

    private lateinit var fragmentTransaction: FragmentTransaction
    private var subsubHomeFragment: SubsubHomeFragment? = null
    var descrip:String=""
    lateinit var ghvhg:StringBuilder

    inner class HomeHeaderViewHolder(view: View) : RecyclerView.ViewHolder(view) {
        var binding: EventedittextBinding = EventedittextBinding.bind(view)
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): HomeHeaderViewHolder {
        return HomeHeaderViewHolder(
            LayoutInflater.from(context).inflate(
                R.layout.eventedittext,
                parent,
                false
            )
        )
    }

    override fun getItemCount(): Int {
        return list!!.size
    }

    @RequiresApi(Build.VERSION_CODES.M)
    override fun onBindViewHolder(holder: HomeHeaderViewHolder, position: Int) {

        holder.binding.eventdate.isEnabled = false
        /*      profileFragment.discountjson!!.add(DiscountEventDateJson(list[position].eventid!!,
                  list[position].date!!
              ))*/



        if (list[position].date.equals("")){
            holder.binding.eventdate.setText(list[position].event)

        }else{
            holder.binding.eventdate.setText(list[position].event + " - "+list[position].date)

        }
        Log.d("xfghujiop",""+list[position].date!!.isNotBlank())
        Log.d("xfghujiop",""+list[position].date!!.isEmpty())
        Log.d("xfghujiop",""+list[position].date!!.isBlank())
        Log.d("dcc",""+list[position].event)
        Log.d("ccc",""+list[position].eventid)

        if (profileFragment.edit.equals("true")){
            if (list[position].date!!.isEmpty()){
                holder.binding.eventdate.isEnabled = true
                // holder.binding.eventdate.setText("")

            }else{
                val today = Calendar.getInstance()
                holder.binding.eventdate.isEnabled = false

                /* descrip=""+today.get( Calendar.DAY_OF_MONTH ) +"/"+today.get( Calendar.MONDAY )+"/"+today.get( Calendar.YEAR )
                 for (i in 0 until list.size){
                     Log.d("njnfjhfj",""+descrip)
                     Log.d("njnfjhfj",""+list[i].date)
                     if (list[i].date==descrip){

                         holder.binding.eventdate.isEnabled = true

                     }else{
                         holder.binding.eventdate.isEnabled = false

                     }
                 }
*/

            }
        }


        holder.binding.eventdate.addTextChangedListener(object : TextWatcher {
            override fun beforeTextChanged(s: CharSequence, start: Int, count: Int, after: Int) {

            }

            override fun onTextChanged(s: CharSequence, start: Int, before: Int, count: Int) {

            }

            override fun afterTextChanged(s: Editable) {

                profileFragment.eventdate=holder.binding.eventdate.text.toString()
                Log.d("kanikakjakajk",""+profileFragment.eventdate)
                for (i in 0 until profileFragment.discountjson!!.size){
                    if (profileFragment.discountjson!![i].eventid==list[position].eventid){

                        profileFragment.discountjson!!.remove(DiscountEventDateJson(profileFragment.discountjson!![i].eventid,profileFragment.discountjson!![i].date))
                        profileFragment.discountjson!!.add(DiscountEventDateJson(list[position].eventid!!,profileFragment.eventdate))
                        val jsonArray = profileFragment.mapper.writeValueAsString(profileFragment.discountjson)

                        Log.d("hbbjhhjhhjhj",""+profileFragment.discountjson!!)
                        Log.d("jsonArray",""+jsonArray)



                    }
                }


            }
        })

        holder.binding.eventdate.setOnClickListener{
            val mDialogView = LayoutInflater.from(context).inflate(R.layout.calenderview, null)
            val mBuilder = AlertDialog.Builder(context).setView(mDialogView).setCancelable(true)
            val mAlertDialog = mBuilder.show()
            val layoutParams = WindowManager.LayoutParams()
            val displayMetrics = DisplayMetrics()
            profileFragment.requireActivity().windowManager.defaultDisplay.getMetrics(displayMetrics)
            val displayWidth = displayMetrics.widthPixels
            val displayHeight = displayMetrics.heightPixels
            layoutParams.copyFrom(mAlertDialog.getWindow()?.getAttributes())
            layoutParams.width = ((displayWidth * 0.9f).toInt())
            mAlertDialog.getWindow()?.setAttributes(layoutParams)
            mAlertDialog.getWindow()?.setGravity(Gravity.CENTER)
            mAlertDialog.getWindow()?.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))

            val today = Calendar.getInstance()
            mAlertDialog.calender.init(today.get(Calendar.YEAR), today.get(Calendar.MONTH),
                today.get(Calendar.DAY_OF_MONTH)

            ) { view, year, month, day ->
                ghvhg = StringBuilder().append(BaseUtils.numberFormat(day)).append("/")

                    .append(BaseUtils.numberFormat(month + 1)).append("/")
                    .append(year)
                holder.binding.eventdate.setText("")

                Log.d("azsxdcvgbnjm,",""+today.get( Calendar.DAY_OF_MONTH ))
                Log.d("azsxdcvgbnjm,",""+today.get( Calendar.MONDAY ))
                Log.d("azsxdcvgbnjm,",""+today.get( Calendar.YEAR ))

                descrip= ghvhg.toString()
                holder.binding.eventdate.setText(descrip)

            }
            //val localdate=LocalDate.now()
            // profileFragment.dateFormat.parse(localdate.toString())
            //Log.d("hjjhhjjh",""+ profileFragment.dateFormat.parse(localdate.toString()))
            mAlertDialog.submitButton.setOnClickListener(View.OnClickListener {
                Log.d("azsxdcvgbnjm,",""+today.get( Calendar.DAY_OF_MONTH ))
                Log.d("azsxdcvgbnjm,",""+today.get( Calendar.MONDAY ))
                Log.d("azsxdcvgbnjm,",""+today.get( Calendar.YEAR ))
                mAlertDialog.dismiss()
                descrip=holder.binding.eventdate.text.toString()

                if (holder.binding.eventdate.text.toString()==list[position].event){
                    descrip=""+today.get( Calendar.DAY_OF_MONTH ) +"/"+today.get( Calendar.MONDAY )+"/"+today.get( Calendar.YEAR )
                    Log.d("fgfgff",""+descrip)
                    holder.binding.eventdate.setText(descrip)
                    for (i in 0 until profileFragment.discountjson!!.size){
                        if (profileFragment.discountjson!![i].eventid==list[position].eventid){

                            profileFragment.discountjson!!.remove(DiscountEventDateJson(profileFragment.discountjson!![i].eventid,profileFragment.discountjson!![i].date))
                            profileFragment.discountjson!!.add(DiscountEventDateJson(list[position].eventid!!,descrip))
                            val jsonArray = profileFragment.mapper.writeValueAsString(profileFragment.discountjson)

                            Log.d("hbbjhhjhhjhj",""+profileFragment.discountjson!!)
                            Log.d("jsonArray",""+jsonArray)



                        }
                    }

                }else{

                    descrip=holder.binding.eventdate.text.toString()
                    Log.d("asdfghjkl;",""+descrip)
                    Log.d("asdfghjkl;",""+descrip)
                    for (i in 0 until profileFragment.discountjson!!.size){
                        if (profileFragment.discountjson!![i].eventid==list[position].eventid){

                            profileFragment.discountjson!!.remove(DiscountEventDateJson(profileFragment.discountjson!![i].eventid,profileFragment.discountjson!![i].date))
                            profileFragment.discountjson!!.add(DiscountEventDateJson(list[position].eventid!!,descrip))
                            val jsonArray = profileFragment.mapper.writeValueAsString(profileFragment.discountjson)

                            Log.d("hbbjhhjhhjhj",""+profileFragment.discountjson!!)
                            Log.d("jsonArray",""+jsonArray)



                        }
                    }
                }




            })
            //descrip=holder.binding.eventdate.text.toString()


        }





    }


    fun selectDob4(){


    }







}