
package com.lia.yello.linedine.activity

import android.content.Intent
import android.content.res.ColorStateList
import android.graphics.Color
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.os.Handler
import android.os.Looper
import android.util.Log
import android.view.View

import com.lia.yello.linedine.R
import com.lia.yello.linedine.Session.SharedHelper
import com.lia.yello.linedine.Utils.UiUtils
import kotlinx.android.synthetic.main.popup.*
import kotlinx.android.synthetic.main.popup.back
import kotlinx.android.synthetic.main.popup.procced
import kotlinx.android.synthetic.main.rulespage.*

import android.widget.RadioGroup




class PopupActivity : BaseActivity() {
    private  var sharedHelper: SharedHelper? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.rulespage)
        sharedHelper = SharedHelper(this)

        Log.d("popup",""+sharedHelper!!.language)


        if (sharedHelper!!.choose == "inqueue"){
            termscond.text=getString(R.string.termscond)
            check1.text=getString(R.string.check1)
            check2.visibility=View.GONE
            check3.visibility=View.GONE
        }else if (sharedHelper!!.choose == "pickup"){
            termscond.text=getString(R.string.termscond1)
            check1.text=getString(R.string.pickuprul1)
            check2.visibility=View.GONE
            check3.visibility=View.GONE

        }else if (sharedHelper!!.choose == "finedine"){
            termscond.text=getString(R.string.fdtruletitle)
            check1.text=getString(R.string.rule1fd)
            check2.text=getString(R.string.rule2fd)
            check3.text=getString(R.string.rule3fd)
            check1.visibility=View.VISIBLE
            check2.visibility=View.VISIBLE
            check3.visibility=View.VISIBLE

        }


        check1.setOnCheckedChangeListener { buttonView, isChecked ->
            if (isChecked) {
                check1.buttonTintList= ColorStateList.valueOf(resources.getColor(R.color.colorPrimary))
            }else{
                check1.buttonTintList= ColorStateList.valueOf(resources.getColor(R.color.grey2))
            }
        }
        check2.setOnCheckedChangeListener { buttonView, isChecked ->
            if (isChecked) {
                check2.buttonTintList= ColorStateList.valueOf(resources.getColor(R.color.colorPrimary))
            }else{
                check2.buttonTintList= ColorStateList.valueOf(resources.getColor(R.color.grey2))
            }
        }
        check3.setOnCheckedChangeListener { buttonView, isChecked ->
            if (isChecked) {
                check3.buttonTintList= ColorStateList.valueOf(resources.getColor(R.color.colorPrimary))
            }else{
                check3.buttonTintList= ColorStateList.valueOf(resources.getColor(R.color.grey2))
            }
        }
        Log.d("ahdcc",""+check1.isChecked)
        Log.d("nvnvn",""+check2.isChecked)
        Log.d("vcvcvc",""+check2.isChecked)
        procced.setOnClickListener(View.OnClickListener {
            if (SharedHelper(this).loggedIn) {

               if (sharedHelper!!.choose == "inqueue"){
                    if (check1.isChecked==true){
                        Log.d("yhzsvgvgs","inqueue")

                        val intent = Intent(this, DashBoardActivity::class.java)
                        // intent.putExtra("langfrom", "inqueue")
                        startActivity(intent)
                    }else{
                        UiUtils.showSnack(rulesidd,getString(R.string.plscheckthebox))
                    }


                }else if (sharedHelper!!.choose == "pickup"){
                     Log.d("yhzsvgvgs","enter")
                    if (check1.isChecked==true){
                        Log.d("yhzsvgvgs","enterpickup")

                        val intent = Intent(this, DashBoardActivity::class.java)
                        // intent.putExtra("langfrom", "inqueue")
                        startActivity(intent)
                    }else{
                        UiUtils.showSnack(rulesidd,getString(R.string.plscheckthebox))
                    }

                }else if (sharedHelper!!.choose == "finedine"){
                   Log.d("yhzsvgvgs","enetr")

                    if (check1.isChecked && check2.isChecked && check3.isChecked==true){
                        Log.d("yhzsvgvgs","finedine")

                        val intent = Intent(this, DashBoardActivity::class.java)
                        // intent.putExtra("langfrom", "inqueue")
                        startActivity(intent)
                    }else{
                        UiUtils.showSnack(rulesidd,getString(R.string.plscheckthebox))
                    }

                }
       }else {
                if (sharedHelper!!.choose == "inqueue") {
                    if (check1.isChecked == true) {
                        Log.d("yhzsvgvgs", "inqueue")


                        startActivity(Intent(this, LoginActivity::class.java))
                        finish()
                    } else {
                        UiUtils.showSnack(rulesidd, getString(R.string.plscheckthebox))
                    }


                } else if (sharedHelper!!.choose == "pickup") {
                    Log.d("yhzsvgvgs", "enter")
                    if (check1.isChecked == true) {
                        Log.d("yhzsvgvgs", "enterpickup")


                        startActivity(Intent(this, LoginActivity::class.java))
                        finish()
                    } else {
                        UiUtils.showSnack(rulesidd, getString(R.string.plscheckthebox))
                    }

                } else if (sharedHelper!!.choose == "finedine") {
                    Log.d("yhzsvgvgs", "enetr")

                    if (check1.isChecked && check2.isChecked && check3.isChecked == true) {
                        Log.d("yhzsvgvgs", "finedine")


                        startActivity(Intent(this, LoginActivity::class.java))
                        finish()
                    } else {
                        UiUtils.showSnack(rulesidd, getString(R.string.plscheckthebox))
                    }

                }
            }
        })
        back.setOnClickListener(View.OnClickListener {
            startActivity(Intent(this, OnBoardActivity::class.java))
        })
    }


    override fun onBackPressed() {
        //super.onBackPressed()
        val intent = Intent(this, OnBoardActivity::class.java)
        startActivity(intent)
    }

}