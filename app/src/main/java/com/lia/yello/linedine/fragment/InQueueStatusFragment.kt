package com.lia.yello.linedine.fragment

import android.annotation.SuppressLint
import android.content.Intent
import android.location.Geocoder
import android.net.Uri
import android.os.Bundle
import android.util.Log
import android.view.View
import android.widget.TextView
import androidx.core.content.ContextCompat
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentTransaction
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.LinearLayoutManager
import com.google.android.gms.location.*
import com.lia.yello.linedine.R
import com.lia.yello.linedine.Session.SharedHelper
import com.lia.yello.linedine.Utils.DialogUtils
import com.lia.yello.linedine.Utils.NetworkUtils
import com.lia.yello.linedine.Utils.UiUtils
import com.lia.yello.linedine.ViewModels.MapViewModel
import com.lia.yello.linedine.adapter.*
import com.lia.yello.linedine.databinding.FragmentInQueueStatusBinding
import kotlinx.android.synthetic.main.activity_map.*
import kotlinx.android.synthetic.main.carticon.*
import kotlinx.android.synthetic.main.fragment_fine_dine_order_list_status.*
import kotlinx.android.synthetic.main.fragment_fine_dine_order_list_status.finedineorderid
import kotlinx.android.synthetic.main.fragment_fine_dine_order_list_status.menushowrecycler
import kotlinx.android.synthetic.main.fragment_fine_dine_order_list_status.root_finedineorderstatus
import kotlinx.android.synthetic.main.fragment_in_queue1.*
import kotlinx.android.synthetic.main.fragment_in_queue_status.*
import kotlinx.android.synthetic.main.fragment_order_processing.*
import kotlinx.android.synthetic.main.fragment_orders.*
import kotlinx.android.synthetic.main.fragment_profile.*
import kotlinx.android.synthetic.main.fragment_profile.root
import kotlinx.android.synthetic.main.fragment_subhome.*
import kotlinx.android.synthetic.main.header.*
import java.util.*


class InQueueStatusFragment : Fragment(R.layout.fragment_in_queue_status) {

    var orderid:Int=0
    var resid:Int=0
    var date:String=""
    var status:String=""
    var binding: FragmentInQueueStatusBinding? = null
    private var mapViewModel: MapViewModel? = null
    public var sharedHelper: SharedHelper? = null
    private var homeFragment : HomeFragment? = null
    private var queueFragment1 : QueueFragment1? = null
    var inqueue:String="false"
    private lateinit var fragmentTransaction: FragmentTransaction
    var hotelname: String = ""
    var resimage: String = ""
    var fulldate:String=""
    var time:String=""
    private var fusedLocationClient: FusedLocationProviderClient? = null
    private var locationRequest: LocationRequest? = null
    private var locationCallback: LocationCallback? = null
    val locationRequestId=100
    private var myLat: Double? = null
    private var myLng: Double? = null
    var resstatus:String=""
    var fcity:String=""
    var phone:String=""
    @SuppressLint("NewApi")

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        binding = DataBindingUtil.findBinding(view)
        mapViewModel = ViewModelProvider(this).get(MapViewModel::class.java)
        val head = view.findViewById<TextView>(R.id.head)
        sharedHelper = SharedHelper(requireContext())
        if (sharedHelper!!.language == "ar"){
            back.rotation= 180F
        }

        orderid = arguments?.getInt("orderid")!!
        status = arguments?.getString("status")!!
        resid = arguments?.getInt("resid")!!
        fulldate = arguments?.getString("date")!!
        resimage = arguments?.getString("resimg")!!
        hotelname = arguments?.getString("resname")!!
        myLat = arguments?.getDouble("lat")!!
        myLng = arguments?.getDouble("lng")!!
        resstatus = arguments?.getString("resstatus")!!
        phone = arguments?.getString("phone")!!
        Log.d("orderid", "" + orderid)
        Log.d("resssid", "" + resid)
        Log.d("lartttt", "" + myLat)
        Log.d("lnfgtgh", "" + myLng)
        head.setText(getString(R.string.myordersdetails))
        cart_badge.text = ""+ sharedHelper!!.cartcount

        if (!NetworkUtils.isNetworkConnected(requireContext())) {
            UiUtils.showSnack(
                view, "no_internet_connection"
            )
        }

        if (sharedHelper!!.choose.equals("finedine")||sharedHelper!!.choose.equals("inqueue")|| sharedHelper!!.cartcount == 0){

            //var cartframe: FrameLayout =findViewById(R.id.cartframe)
            cartframe.visibility=View.GONE
        }
        back.setOnClickListener(View.OnClickListener {
            requireActivity().supportFragmentManager.popBackStack()
        })
        fusedLocationClient = LocationServices.getFusedLocationProviderClient(requireActivity())
       // locationListner()
       // askLocationPermission()



        resimage.let {
            UiUtils.loadImage(
                imginqueueprocessing,
                it,
                ContextCompat.getDrawable(
                    requireContext(),
                    R.drawable.dummylogo
                )!!
            )
        }

        ressname.text=hotelname
       // locateMyLocation(80.30733956738314,13.184499018746262)

      //  statusfdorderlist.text=status

        getbookinglist("inqueue")

        getqueuecount(orderid)
        linear_rest_inqueue.setOnClickListener(View.OnClickListener {
            val bundle = Bundle()
            bundle.putString("inqueue", inqueue)
            bundle.putInt("bookingid", orderid)
            bundle.putInt("resid", resid)
            bundle.putString("resname", hotelname)
            bundle.putString("resimg", resimage)
            bundle.putString("date", fulldate)
            bundle.putDouble("lat", myLat!!)
            bundle.putDouble("lng", myLng!!)
            bundle.putString("resstatus", resstatus!!)
            bundle.putString("phone", phone!!)
            queueFragment1 = QueueFragment1()
            queueFragment1!!.arguments = bundle
            fragmentTransaction = requireActivity().supportFragmentManager.beginTransaction()
            fragmentTransaction.setCustomAnimations(R.anim.screen_in, R.anim.screen_out)
            fragmentTransaction.replace(R.id.container, queueFragment1!!)
            fragmentTransaction.addToBackStack(null)
            fragmentTransaction.commit()

        })

        removemefromqueue.setOnClickListener(View.OnClickListener {
            cancelbooking(orderid)
        })


        val gcd = Geocoder(context, Locale.getDefault())
        val addresses = gcd.getFromLocation(myLat!!, myLng!!, 1)
        if (addresses.size > 0) {
            println(addresses[0].locality)
           Log.d("adedretrgygyuihu", "" + addresses[0].locality)
            fcity=addresses[0].locality
            loccationnn.text= ": $fcity"
        } else {
            // do your stuff
        }
    }



    public fun getbookinglist(status: String){
        DialogUtils.showLoader(requireContext())
        mapViewModel?.getbookinglist(requireContext(), status)
            ?.observe(viewLifecycleOwner,
                Observer {
                    DialogUtils.dismissLoader()

                    it?.let {
                        it.error?.let { error ->
                            if (error) {
                                it.message?.let { msg ->
                                    UiUtils.showSnack(root, msg)
                                }
                            } else {
                                it.data?.let { data ->

                                    for (i in 0 until data!!.size) {
                                        if (data[i].id!! == orderid) {

                                            if (data[i].menudetails!!.size != 0) {
                                                linear_inqueue_status.visibility = View.VISIBLE
                                                finedineorderid.text =
                                                    getString(R.string.orderid) + " : " + data[i].id.toString()

                                                menushowrecycler.layoutManager =
                                                    LinearLayoutManager(context)
                                                menushowrecycler.adapter =
                                                    InQueueOrderedMenuAdapter(
                                                        this,
                                                        requireContext(),
                                                        data[i].menudetails
                                                    )
                                            } else {

                                                linear_inqueue_status.visibility = View.GONE

                                            }

                                        }

                                    }


                                }
                            }
                        }

                    }
                })
    }


    public fun getqueuecount(orderid: Int){
        DialogUtils.showLoader(requireContext())
        mapViewModel?.getqueuecount(requireContext(), orderid, resid, "true", fulldate,"finedine")
            ?.observe(viewLifecycleOwner, androidx.lifecycle.Observer {

                DialogUtils.dismissLoader()

                it?.let {
                    it.error?.let { error ->
                        if (error) {


                        } else {
                            it.data?.let { data ->
                                usercount.text = data.user_queuecount.toString()
                                /*  if (inqueue.equals("false")){
                                    queuecountt.text= data.user_queuecount.toString()
                                    addmetoqueue.visibility=View.INVISIBLE
                                }else{
                                    queuecount= data.totcount!!
                                    queuecountt.text= data.totcount.toString()
                                }*/

                            }


                        }
                    }

                }
            })

    }



    private fun cancelbooking(id: Int){
        DialogUtils.showLoader(requireContext())

        mapViewModel?.cancelbooking(requireContext(), id,"",0.0,"")
            ?.observe(viewLifecycleOwner,
                Observer {
                    DialogUtils.dismissLoader()
                    it?.let {
                        it.error?.let { error ->
                            if (error) {
                                it.message?.let { msg ->
                                    UiUtils.showSnack(root_finedineorderstatus, msg)

                                }
                            } else {
                                it.message?.let { msg ->
                                    UiUtils.showSnack(root_finedineorderstatus, msg)
                                    homeFragment = HomeFragment()

                                    fragmentTransaction =
                                        requireActivity().supportFragmentManager.beginTransaction()
                                    fragmentTransaction.setCustomAnimations(
                                        R.anim.screen_in,
                                        R.anim.screen_out
                                    )
                                    fragmentTransaction.replace(R.id.container, homeFragment!!)
                                    fragmentTransaction.addToBackStack(null)
                                    fragmentTransaction.commit()

                                }
                            }
                        }

                    }
                })

    }


}