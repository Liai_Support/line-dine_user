package com.lia.yello.linedine.fragment

import android.app.AlertDialog
import android.app.DatePickerDialog
import android.content.Context
import android.content.res.Configuration
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.os.Bundle
import android.util.DisplayMetrics
import android.view.*
import android.view.inputmethod.InputMethodManager
import android.widget.*
import androidx.activity.OnBackPressedCallback
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentTransaction
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import com.lia.yello.linedine.Models.DiscountEventDateJson
import com.lia.yello.linedine.Models.ProfileDetails
import com.lia.yello.linedine.R
import com.lia.yello.linedine.Session.SharedHelper
import com.lia.yello.linedine.Utils.BaseUtils
import com.lia.yello.linedine.Utils.DialogUtils
import com.lia.yello.linedine.Utils.NetworkUtils
import com.lia.yello.linedine.Utils.UiUtils
import com.lia.yello.linedine.ViewModels.ProfileViewModel
import com.lia.yello.linedine.databinding.FragmentProfileBinding
import kotlinx.android.synthetic.main.calenderview.*
import kotlinx.android.synthetic.main.carticon.*
import kotlinx.android.synthetic.main.fragment_home.*
import kotlinx.android.synthetic.main.fragment_profile.*
import kotlinx.android.synthetic.main.header.*
import kotlinx.android.synthetic.main.layout_home_drawer1.*
import java.io.File
import java.util.*

import android.view.LayoutInflater
import androidx.recyclerview.widget.LinearLayoutManager
import com.fasterxml.jackson.module.kotlin.jacksonObjectMapper
import com.lia.yello.linedine.Models.EventsArray
import com.lia.yello.linedine.adapter.EventDiscountDateAdapter
import kotlinx.android.synthetic.main.fragment_wallet.*
import java.text.SimpleDateFormat


class ProfileFragment : Fragment(R.layout.fragment_profile) {

    var binding: FragmentProfileBinding? = null
    private var profileViewModel: ProfileViewModel? = null
    var sharedHelper: SharedHelper? = null
    var uploadFile: File? = null
    var profileImageUrl = ""
    var cCode = ""
    var mobile = ""
    var email = ""
    var eventdate :String=""
    private var homeFragment : HomeFragment? = null
    private lateinit var fragmentTransaction: FragmentTransaction
    lateinit var ghvhg:StringBuilder
    var discountjson:ArrayList<DiscountEventDateJson>? = ArrayList<DiscountEventDateJson>()
    var edit :String="false"
    var calendarView:CalendarView?= null
    val mapper = jacksonObjectMapper()
    val dateFormat = SimpleDateFormat("dd/MM/yyyy")

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        binding = DataBindingUtil.findBinding(view)
        sharedHelper = SharedHelper(requireContext())
        profileViewModel = ViewModelProvider(this).get(ProfileViewModel::class.java)
        initListeners()
        getProfileData()
        cart_badge.text = ""+ sharedHelper!!.cartcount

        back.setOnClickListener(View.OnClickListener {
            //  requireActivity().supportFragmentManager.popBackStack()

            homeFragment = HomeFragment()
            fragmentTransaction =requireActivity().supportFragmentManager.beginTransaction()
            fragmentTransaction.setCustomAnimations(R.anim.screen_in, R.anim.screen_out)
            fragmentTransaction.replace(R.id.container, homeFragment!!)
            fragmentTransaction.addToBackStack(null)
            fragmentTransaction.commit()
        })
        enableEdit(false)
        if (sharedHelper!!.language == "ar"){
            back.rotation= 180F
            //  edt_name.gravity=Gravity.END
            // edt_mail.gravity=Gravity.END
        }
        if (!NetworkUtils.isNetworkConnected(requireContext())) {
            UiUtils.showSnack(
                view, "no_internet_connection"
            )
        }

        if (sharedHelper!!.choose.equals("finedine")||sharedHelper!!.choose.equals("inqueue")|| sharedHelper!!.cartcount == 0){

            //var cartframe: FrameLayout =findViewById(R.id.cartframe)
            cartframe.visibility=View.GONE
        }
        activity?.onBackPressedDispatcher?.addCallback(this, object : OnBackPressedCallback(true) {
            override fun handleOnBackPressed() {

                homeFragment = HomeFragment()
                fragmentTransaction =requireActivity().supportFragmentManager.beginTransaction()
                fragmentTransaction.setCustomAnimations(R.anim.screen_in, R.anim.screen_out)
                fragmentTransaction.replace(R.id.container, homeFragment!!)
                fragmentTransaction.addToBackStack(null)
                fragmentTransaction.commit()
            }
        })


    }

    private fun initListeners() {

        edt_dob.setOnClickListener {

            val imm: InputMethodManager =
                activity!!.getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
            imm.hideSoftInputFromWindow(view!!.windowToken, 0)

            selectDob3()


        }

        editProfile.setOnClickListener {
            if (editProfile.text.toString().equals(getString(R.string.editprofile))) {
                // editImage.visibility = View.VISIBLE
                enableEdit(true)
                edit="true"
                getProfileData()
            } else {
                if (isValidInputs()) {
                    DialogUtils.showLoader(requireContext())
                    if (uploadFile != null) {
                        /*amazonViewModel?.uploadImage(requireContext(), uploadFile!!)
                            ?.observe(viewLifecycleOwner, Observer {

                                it.error?.let { value ->
                                    if (!value) {
                                        updateProfile(it.message)
                                    } else {
                                        DialogUtils.dismissLoader()
                                    }
                                }
                            })*/
                    } else {


                        val jsonArray = mapper.writeValueAsString(discountjson)

                        getDiscountEventDates(jsonArray)
                        updateProfile(profileImageUrl)
                    }


                }
            }

        }

        male.setOnClickListener{
            male.isChecked = true
            female.isChecked = false
        }

        female.setOnClickListener{
            male.isChecked = false
            female.isChecked = true
        }

    }

    private fun selectDob() {

        var locale:Locale= Locale("ENGLISH")
        Locale.setDefault(Locale.ENGLISH);
        val configuration: Configuration = context!!.resources.configuration
        configuration.setLocale(locale)
        configuration.setLayoutDirection(locale)

        context!!.createConfigurationContext(configuration)
        val c = Calendar.getInstance(Locale.ENGLISH)
        val year = c.get(Calendar.YEAR)
        val month = c.get(Calendar.MONTH)
        val day = c.get(Calendar.DAY_OF_MONTH)






        val dpd = DatePickerDialog(
            requireActivity(),
            DatePickerDialog.OnDateSetListener { view, year, monthOfYear, dayOfMonth ->


                var ghvhg = StringBuilder().append(year).append("-")
                    .append(BaseUtils.numberFormat(monthOfYear + 1)).append("-")
                    .append(BaseUtils.numberFormat(dayOfMonth))

                // edt_dob1.text = ghvhg.toString()
                edt_dob.setText(ghvhg)


            },
            year,
            month,
            day
        )
        dpd.getDatePicker().setSpinnersShown(true);
        dpd.getDatePicker().setCalendarViewShown(false);
        dpd.show()
    }

    /*  private fun selectDob2(){

          // Setting the listener.


          val mDialogView = LayoutInflater.from(requireContext()).inflate(R.layout.calenderview, null)
          val mBuilder = AlertDialog.Builder(requireContext()).setView(mDialogView).setCancelable(true)
          val mAlertDialog = mBuilder.show()
          val layoutParams = WindowManager.LayoutParams()
          val displayMetrics = DisplayMetrics()
          requireActivity().windowManager.defaultDisplay.getMetrics(displayMetrics)
          val displayWidth = displayMetrics.widthPixels
          val displayHeight = displayMetrics.heightPixels
          layoutParams.copyFrom(mAlertDialog.getWindow()?.getAttributes())
          layoutParams.width = ((displayWidth * 0.9f).toInt())
          mAlertDialog.getWindow()?.setAttributes(layoutParams)
          mAlertDialog.getWindow()?.setGravity(Gravity.CENTER)
          mAlertDialog.getWindow()?.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
          // Setting the listener.
       //  calendarView = ll.getChildAt(0) as CalendarView

          mAlertDialog.calender?.setOnDateChangeListener { view, year, monthOfYear, dayOfMonth ->

              var ghvhg = StringBuilder().append(year).append("-")
                  .append(BaseUtils.numberFormat(monthOfYear + 1)).append("-")
                  .append(BaseUtils.numberFormat(dayOfMonth))

              // edt_dob1.text = ghvhg.toString()
              edt_dob.setText(ghvhg)
                  mAlertDialog.dismiss()
          }



      }*/

    private fun selectDob3(){
        val mDialogView = LayoutInflater.from(requireContext()).inflate(R.layout.calenderview, null)
        val mBuilder = AlertDialog.Builder(requireContext()).setView(mDialogView).setCancelable(true)
        val mAlertDialog = mBuilder.show()
        val layoutParams = WindowManager.LayoutParams()
        val displayMetrics = DisplayMetrics()
        requireActivity().windowManager.defaultDisplay.getMetrics(displayMetrics)
        val displayWidth = displayMetrics.widthPixels
        val displayHeight = displayMetrics.heightPixels
        layoutParams.copyFrom(mAlertDialog.getWindow()?.getAttributes())
        layoutParams.width = ((displayWidth * 0.9f).toInt())
        mAlertDialog.getWindow()?.setAttributes(layoutParams)
        mAlertDialog.getWindow()?.setGravity(Gravity.CENTER)
        mAlertDialog.getWindow()?.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))

        val today = Calendar.getInstance()
        mAlertDialog.calender.init(today.get(Calendar.YEAR), today.get(Calendar.MONTH),
            today.get(Calendar.DAY_OF_MONTH)

        ) { view, year, month, day ->
            ghvhg = StringBuilder().append(year).append("-")
                .append(BaseUtils.numberFormat(month + 1)).append("-")
                .append(BaseUtils.numberFormat(day))

            edt_dob.setText(ghvhg)

        }
        mAlertDialog.submitButton.setOnClickListener(View.OnClickListener {

            mAlertDialog.dismiss()

        })


    }

    private fun showcalendiwee4(){



    }
    fun updateProfile(profilePic: String?) {
        DialogUtils.showLoader(requireContext())
        profileViewModel?.updateProfile(
            requireContext(),
            edt_name.text.toString().trim(),
            email,
            cCode,
            mobile,
            getGender(),
            edt_dob.text.toString()
            //  profilePic
        )?.observe(viewLifecycleOwner, Observer {
            DialogUtils.dismissLoader()
            it.error?.let { error ->
                if (error) {

                    it.message?.let { message -> UiUtils.showSnack(root, message) }
                    edit="false"
                    successResponse()
                } else {
                    sharedHelper?.userImage = profilePic!!
                    edit="false"
                    successResponse()
                }
            }
        })

    }

    private fun successResponse() {

        // editImage.visibility = View.GONE
        enableEdit(false)
        getProfileData()
    }

    private fun getGender(): String {
        return when {
            male.isChecked -> {
                "male"
            }
            female.isChecked -> {
                "female"
            }
            else -> {
                ""
            }
        }
    }

    fun isValidInputs(): Boolean {

        if (edt_name.text.toString() == "") {
            UiUtils.showSnack(root, getString(R.string.enterurname))
            return false
        } else if (!male.isChecked && !female.isChecked) {
            UiUtils.showSnack(root, getString(R.string.selectyourgendername))
            return false
        }/* else if (edt_dob.text.toString() == "") {
            UiUtils.showSnack(root, getString(R.string.selectyourdateofbirth))
            return false
        }*/  else {
            return true
        }

    }


    private fun getProfileData() {
        DialogUtils.showLoader(requireContext())
        profileViewModel?.getProfileDetails(requireContext())
            ?.observe(viewLifecycleOwner, Observer {
                DialogUtils.dismissLoader()
                it?.let {
                    it.error?.let { error ->
                        if (error) {
                            it.message?.let { msg ->
                                UiUtils.showSnack(root, msg)
                            }
                        } else {

                            if (edit.equals("false")){
                                it.data?.let { data ->


                                    handleResponse(data)

                                    data.events?.let { list ->
                                        if (list.size != 0) {
                                            discountjson!!.clear()
                                            for (i in 0 until list.size){
                                                discountjson!!.add(DiscountEventDateJson(list[i].eventid!!,list[i].date!!))
                                            }
                                            eventlinear.visibility=View.VISIBLE

                                            eventlinear.layoutManager = LinearLayoutManager(context)
                                            eventlinear.adapter = EventDiscountDateAdapter(
                                                this,
                                                requireContext(),
                                                list
                                            )

                                        }

                                    }

                                }
                            }else{
                                it.data?.let { data ->

                                    data.events?.let { list ->
                                        if (list.size != 0) {

                                            eventlinear.visibility=View.VISIBLE

                                            eventlinear.layoutManager = LinearLayoutManager(context)
                                            eventlinear.adapter = EventDiscountDateAdapter(
                                                this,
                                                requireContext(),
                                                list
                                            )

                                        }

                                    }

                                }
                            }

                        }
                    }

                }
            })
    }

    private fun handleResponse(data: ProfileDetails) {

        data.name?.let {
            edt_name.setText(it)
            sharedHelper?.name = it
            requireActivity().name_head.text = it
        }
        data.countryCode?.let { ccpVal ->
            cCode = ccpVal
            data.mobileNumber?.let {
                // edt_mobile1.text = "+$ccpVal  $it"
                mobile = it
                edt_mobile1.setText(it)
            }
        }
        /* data.profilePic?.let {
             profileImageUrl = it
             sharedHelper?.userImage = it
             UiUtils.loadImage(profilePicture1, it)
             UiUtils.loadImage(requireActivity().circleImageView5, sharedHelper?.userImage)
         }*/

        data.email?.let {
            edt_mail.setText(it)
            email = it
        }

        data.DOB?.let {
            if (!it.contains("not", true))
            //  dob1.text = it
                edt_dob.setText(it)
        }
        data.gender?.let {
            if (it.equals("male", true)) {
                male.isChecked = true
            } else if (it.equals("female", true)) {
                female.isChecked = true
            }
        }

        enableEdit(false)
    }

    fun enableEdit(isEnable: Boolean) {

        edt_name.isEnabled = isEnable
        edt_mail.isEnabled = false
        edt_mobile1.isEnabled = false
        male.isEnabled = isEnable
        female.isEnabled = isEnable
        edt_dob.isEnabled = isEnable
        if (isEnable)
            editProfile.text = getString(R.string.update)
        else
            editProfile.text = getString(R.string.editprofile)
        /* editImage.visibility =
             if (isEnable)
                 View.VISIBLE
             else
                 View.GONE

                  */
    }



    private fun getvalues(){
        DialogUtils.showLoader(requireContext())
        profileViewModel?.getDiscountEvent(requireContext())
            ?.observe(viewLifecycleOwner,
                Observer {
                    DialogUtils.dismissLoader()

                    it?.let {
                        it.error?.let { error ->
                            if (error) {
                                it.message?.let { msg ->
                                    UiUtils.showSnack(root1, msg)
                                }
                            } else {
                                it.data?.let { data ->


                                }
                            }

                        }
                    }
                })

    }




    private fun getDiscountEventDates(jsonArray: String){
        DialogUtils.showLoader(requireContext())
        profileViewModel?.getDiscountEventDates(requireContext(),jsonArray)
            ?.observe(viewLifecycleOwner,
                Observer {
                    DialogUtils.dismissLoader()

                    it?.let {
                        it.error?.let { error ->
                            if (error) {
                                it.message?.let { msg ->
                                    UiUtils.showSnack(root1, msg)
                                    edit="false"
                                    getProfileData()
                                }
                            } else {

                            }

                        }
                    }
                })

    }
}