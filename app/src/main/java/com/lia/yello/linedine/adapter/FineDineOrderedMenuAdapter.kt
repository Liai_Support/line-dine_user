package com.lia.yello.linedine.adapter

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.lia.yello.linedine.Models.FineDineMenudetail
import com.lia.yello.linedine.R
import com.lia.yello.linedine.databinding.OrderprocessinngmenuadapterBinding
import com.lia.yello.linedine.fragment.FineDineOrderListStatusFragment
import com.lia.yello.linedine.fragment.MyOrderMenuListFragment
import java.util.*
import kotlin.collections.ArrayList

class FineDineOrderedMenuAdapter(
    var fineDineOrderListStatusFragment: FineDineOrderListStatusFragment,
    var context: Context,
    var list: ArrayList<FineDineMenudetail>?
) :
    RecyclerView.Adapter<FineDineOrderedMenuAdapter.HomeHeaderViewHolder>() {



    inner class HomeHeaderViewHolder(view: View) : RecyclerView.ViewHolder(view) {
        var binding: OrderprocessinngmenuadapterBinding = OrderprocessinngmenuadapterBinding.bind(view)

    }


    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): HomeHeaderViewHolder {
        return HomeHeaderViewHolder(
            LayoutInflater.from(parent.context).inflate(
                R.layout.orderprocessinngmenuadapter,
                parent,
                false
            )
        )
    }

    override fun getItemCount(): Int {

        return list!!.size
        // return itemCount
    }


    override fun onBindViewHolder(holder: HomeHeaderViewHolder, position: Int) {

        val cost= list!![position].amount!! * list!![position].quantity!!
        holder.binding.menuname.text = ""+list!![position].quantity!! + " *  " +list!![position].menu_name
        holder.binding.menutotalcost.text=String.format(Locale("en", "US"),"%.2f",cost)+ " SAR"



    }

}












