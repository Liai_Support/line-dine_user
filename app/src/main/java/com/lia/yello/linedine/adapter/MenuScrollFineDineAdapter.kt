package com.lia.yello.linedine.adapter
import android.graphics.Color
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.LinearLayout
import android.widget.TextView
import androidx.cardview.widget.CardView
import androidx.core.content.ContextCompat
import androidx.recyclerview.widget.RecyclerView
import com.facebook.FacebookSdk
import com.facebook.FacebookSdk.getApplicationContext
import com.lia.yello.linedine.R
import com.lia.yello.linedine.databinding.ScrollistBinding
import com.lia.yello.linedine.fragment.FineDineMenuListFragment
import kotlinx.android.synthetic.main.fragment_fine_dine_menu_list.*
class MenuScrollFineDineAdapter(
    private var fineDineMenuListFragment: FineDineMenuListFragment
) :
    RecyclerView.Adapter<MenuScrollFineDineAdapter.MyViweHolder>() {
    var intt: Int = 0
    val fname = arrayOf(fineDineMenuListFragment.getString(R.string.breakfast),fineDineMenuListFragment.getString(R.string.lunch),fineDineMenuListFragment.getString(R.string.dinner))
    val pics =
        intArrayOf(R.drawable.ic_breakfast, R.drawable.ic_egg, R.drawable.ic_christmas_dinner)
    var row_index:Int=-1
    class MyViweHolder(view: View) : RecyclerView.ViewHolder(view) {
        var binding: ScrollistBinding = ScrollistBinding.bind(view)

        /*   lateinit var text: TextView
           lateinit var img: ImageView
           lateinit var card: CardView
           lateinit var linear:LinearLayout
           init {
               text = view.findViewById(R.id.txtscroll)
               img = view.findViewById(R.id.imgscroll)
               card = view.findViewById(R.id.cardlunch)
               linear = view.findViewById(R.id.linearlunchh)
           }*/
    }
    override fun onCreateViewHolder(
        parent: ViewGroup,
        viewType: Int
    ): MenuScrollFineDineAdapter.MyViweHolder {
        return MyViweHolder(
            LayoutInflater.from(parent.context).inflate(R.layout.scrollist, parent, false)
        )
    }
    override fun getItemCount(): Int {
        return fname.size
    }
    override fun onBindViewHolder(holder: MenuScrollFineDineAdapter.MyViweHolder, position: Int) {
        holder.binding.txtscroll.text = fname[position].toString()
        holder.binding.imgscroll.setImageResource(pics[position])
        holder.binding.cardlunch.setOnClickListener(View.OnClickListener {
            row_index = position;
            fineDineMenuListFragment.allseleted = false
            notifyDataSetChanged();
        })
        if (row_index==position){
            fineDineMenuListFragment.all.backgroundTintList = ContextCompat.getColorStateList(FacebookSdk.getApplicationContext(), R.color.white)
            fineDineMenuListFragment.all.setTextColor(Color.BLACK)
            fineDineMenuListFragment.all.background = ContextCompat.getDrawable(FacebookSdk.getApplicationContext(),R.drawable.border_layoutred)
            holder.binding.linearlunchh.setBackgroundResource(R.drawable.border_layoutred)
            holder.binding.txtscroll.setTextColor(Color.WHITE)
            /* fineDineMenuList.txt= holder.text.text as String
             Log.d("sadfghjkl",""+ fineDineMenuList.txt)*/
            fineDineMenuListFragment.all.setTextColor(Color.BLACK)
            fineDineMenuListFragment.all.background = ContextCompat.getDrawable(
                FacebookSdk.getApplicationContext(),
                R.drawable.border_layoutred
            )
            fineDineMenuListFragment.all.backgroundTintList = ContextCompat.getColorStateList(
                getApplicationContext(),
                R.color.white
            )

            if (holder.binding.txtscroll.text.toString()=="فطور"){
                fineDineMenuListFragment.getvalues("Break Fast")

            }else if (holder.binding.txtscroll.text.toString()=="غداء"){
                fineDineMenuListFragment.getvalues("Lunch")

            }else if (holder.binding.txtscroll.text.toString()=="عشاء"){
                fineDineMenuListFragment.getvalues("Dinner")

            }else{
                fineDineMenuListFragment.getvalues(holder.binding.txtscroll.text.toString())

            }


            // fineDineMenuListFragment.getvalues(holder.binding.txtscroll.text.toString() )
        }else
        {
            if(fineDineMenuListFragment.allseleted == true){
                holder.binding.linearlunchh.setBackgroundColor(Color.parseColor("#ffffff"))
                holder.binding.txtscroll.setTextColor(Color.parseColor("#000000"));
                fineDineMenuListFragment.all.backgroundTintList = ContextCompat.getColorStateList(FacebookSdk.getApplicationContext(), R.color.colorPrimary)
                fineDineMenuListFragment.all.setTextColor(Color.WHITE)
                fineDineMenuListFragment.all.background = ContextCompat.getDrawable(FacebookSdk.getApplicationContext(),R.drawable.border_layoutred)
            }
            else {
                holder.binding.linearlunchh.setBackgroundColor(Color.parseColor("#ffffff"))
                holder.binding.txtscroll.setTextColor(Color.parseColor("#000000"));
                fineDineMenuListFragment.all.setTextColor(Color.BLACK)
                fineDineMenuListFragment.all.background = ContextCompat.getDrawable(
                    FacebookSdk.getApplicationContext(),
                    R.drawable.border_layoutred
                )
                fineDineMenuListFragment.all.backgroundTintList = ContextCompat.getColorStateList(
                    FacebookSdk.getApplicationContext(),
                    R.color.white
                )
            }
        }
    }
}